#include "JointControlSkillProvider.h"

#include <RobotAPI/libraries/aron/core/type/variant/container/Object.h>
#include <RobotAPI/libraries/aron/core/type/variant/primitive/String.h>
#include <RobotAPI/libraries/aron/converter/json/NLohmannJSONConverter.h>

//#include <RobotAPI/libraries/armem/client/MemoryNameSystem.h>
#include <RobotAPI/libraries/armem/client.h>

namespace armarx::skills::provider
{
    armarx::PropertyDefinitionsPtr JointControlSkillProvider::createPropertyDefinitions()
    {
        armarx::PropertyDefinitionsPtr defs = new ComponentPropertyDefinitions(getConfigIdentifier());

        jointControlSkillContext.defineProperties(defs, "jointControlSkills.");

        return defs;
    }

    std::string JointControlSkillProvider::getDefaultName() const
    {
        return "JointControlSkillProvider";
    }

    void JointControlSkillProvider::onInitComponent()
    {
        jointControlSkillContext.onInit(*this);
    }

    void JointControlSkillProvider::onConnectComponent()
    {
        jointControlSkillContext.onConnected(*this);

        auto& mns = memoryNameSystem();

        // Add move skill
        addSkill(std::make_unique<MoveJointsToPosition>(mns, jointControlSkillContext));

    }

    void JointControlSkillProvider::onDisconnectComponent()
    {

    }

    void JointControlSkillProvider::onExitComponent()
    {}
}
