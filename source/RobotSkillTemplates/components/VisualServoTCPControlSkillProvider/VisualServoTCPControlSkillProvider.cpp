#include "VisualServoTCPControlSkillProvider.h"

#include <RobotAPI/libraries/aron/core/type/variant/container/Object.h>
#include <RobotAPI/libraries/aron/core/type/variant/primitive/String.h>
#include <RobotAPI/libraries/aron/converter/json/NLohmannJSONConverter.h>

//#include <RobotAPI/libraries/armem/client/MemoryNameSystem.h>
#include <RobotAPI/libraries/armem/client.h>

namespace armarx::skills::provider
{
    armarx::PropertyDefinitionsPtr VisualServoTCPControlSkillProvider::createPropertyDefinitions()
    {
        armarx::PropertyDefinitionsPtr defs = new ComponentPropertyDefinitions(getConfigIdentifier());

        visualServoTCPControlSkillContext.defineProperties(defs, "visualServoTCPControlSkills.");

        return defs;
    }

    std::string VisualServoTCPControlSkillProvider::getDefaultName() const
    {
        return "VisualServoTCPControlSkillProvider";
    }

    void VisualServoTCPControlSkillProvider::onInitComponent()
    {
        visualServoTCPControlSkillContext.onInit(*this);
    }

    void VisualServoTCPControlSkillProvider::onConnectComponent()
    {
        visualServoTCPControlSkillContext.onConnected(*this);

        auto& mns = memoryNameSystem();

        // Add move skill
        addSkill(std::make_unique<VisualServoToTargetPose>(mns, arviz, visualServoTCPControlSkillContext));
    }

    void VisualServoTCPControlSkillProvider::onDisconnectComponent()
    {

    }

    void VisualServoTCPControlSkillProvider::onExitComponent()
    {}
}
