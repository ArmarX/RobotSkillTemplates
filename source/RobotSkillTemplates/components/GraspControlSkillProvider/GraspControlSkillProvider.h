
/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once


// ArmarX
#include <ArmarXCore/core/Component.h>

// RobotAPI
#include <RobotAPI/libraries/skills/provider/SkillProviderComponentPlugin.h>
#include <RobotAPI/libraries/armem/client/plugins/PluginUser.h>
#include <RobotAPI/libraries/RobotAPIComponentPlugins/ArVizComponentPlugin.h>

// Best put your skills in seperate libs
#include <RobotSkillTemplates/libraries/skill_grasp_object/grasp/GraspObject.h>
#include <RobotSkillTemplates/libraries/skill_grasp_object/putdown/PutdownObject.h>

namespace armarx::skills::provider
{
    class GraspControlSkillProvider :
            virtual public armarx::Component,
            virtual public SkillProviderComponentPluginUser,
            virtual public armem::ClientPluginUser,
            virtual public armarx::ArVizComponentPluginUser
    {
    public:
        GraspControlSkillProvider() = default;

        /// @see armarx::ManagedIceObject::getDefaultName()
        std::string getDefaultName() const override;

    protected:

        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

        void onInitComponent() override;
        void onConnectComponent() override;
        void onDisconnectComponent() override;
        void onExitComponent() override;

    private:
        // skills
        TwoArmGraspControlSkillContext graspControlSkillContext;
    };
}
