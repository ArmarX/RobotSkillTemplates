#include "GraspControlSkillProvider.h"


namespace armarx::skills::provider
{
    armarx::PropertyDefinitionsPtr GraspControlSkillProvider::createPropertyDefinitions()
    {
        armarx::PropertyDefinitionsPtr defs = new ComponentPropertyDefinitions(getConfigIdentifier());

        graspControlSkillContext.defineProperties(defs, "graspControlSkills.");

        return defs;
    }

    std::string GraspControlSkillProvider::getDefaultName() const
    {
        return "GraspControlSkillProvider";
    }

    void GraspControlSkillProvider::onInitComponent()
    {
        graspControlSkillContext.onInit(*this);
    }

    void GraspControlSkillProvider::onConnectComponent()
    {
        graspControlSkillContext.onConnected(*this);

        auto& mns = memoryNameSystem();

        // Add move skill
        addSkill(std::make_unique<CloseHandAndAttachSkill>(mns, arviz, graspControlSkillContext));
        addSkill(std::make_unique<MovePlatformForGraspSkill>(mns, arviz, graspControlSkillContext));
        addSkill(std::make_unique<MovePlatformAfterGraspSkill>(mns, arviz, graspControlSkillContext));
        addSkill(std::make_unique<ExecuteGraspSkill>(mns, arviz, graspControlSkillContext));
        addSkill(std::make_unique<GraspObjectSkill>(mns, arviz, graspControlSkillContext));

        addSkill(std::make_unique<OpenHandAndDetachSkill>(mns, arviz, graspControlSkillContext));
        addSkill(std::make_unique<MovePlatformForPutdownSkill>(mns, arviz, graspControlSkillContext));
        addSkill(std::make_unique<MovePlatformAfterPutdownSkill>(mns, arviz, graspControlSkillContext));
        addSkill(std::make_unique<ExecutePutdownSkill>(mns, arviz, graspControlSkillContext));
        addSkill(std::make_unique<PutdownObjectSkill>(mns, arviz, graspControlSkillContext));
    }

    void GraspControlSkillProvider::onDisconnectComponent()
    {

    }

    void GraspControlSkillProvider::onExitComponent()
    {}
}
