#include "TCPControlSkillProvider.h"

#include <RobotAPI/libraries/aron/core/type/variant/container/Object.h>
#include <RobotAPI/libraries/aron/core/type/variant/primitive/String.h>
#include <RobotAPI/libraries/aron/converter/json/NLohmannJSONConverter.h>

//#include <RobotAPI/libraries/armem/client/MemoryNameSystem.h>
#include <RobotAPI/libraries/armem/client.h>

namespace armarx::skills::provider
{
    armarx::PropertyDefinitionsPtr TCPControlSkillProvider::createPropertyDefinitions()
    {
        armarx::PropertyDefinitionsPtr defs = new ComponentPropertyDefinitions(getConfigIdentifier());

        tcpControlSkillContext.defineProperties(defs, "tcpControlSkills.");

        return defs;
    }

    std::string TCPControlSkillProvider::getDefaultName() const
    {
        return "TCPControlSkillProvider";
    }

    void TCPControlSkillProvider::onInitComponent()
    {
        tcpControlSkillContext.onInit(*this);
    }

    void TCPControlSkillProvider::onConnectComponent()
    {
        tcpControlSkillContext.onConnected(*this);

        auto& mns = memoryNameSystem();

        // Add move skill
        addSkill(std::make_unique<MoveTCPToTargetPose>(mns, arviz, tcpControlSkillContext));
    }

    void TCPControlSkillProvider::onDisconnectComponent()
    {

    }

    void TCPControlSkillProvider::onExitComponent()
    {}
}
