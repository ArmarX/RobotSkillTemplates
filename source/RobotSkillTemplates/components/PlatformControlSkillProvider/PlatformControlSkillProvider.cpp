

#include "PlatformControlSkillProvider.h"

#include <RobotAPI/libraries/aron/core/type/variant/container/Object.h>
#include <RobotAPI/libraries/aron/core/type/variant/primitive/String.h>
#include <RobotAPI/libraries/aron/converter/json/NLohmannJSONConverter.h>

//#include <RobotAPI/libraries/armem/client/MemoryNameSystem.h>
#include <RobotAPI/libraries/armem/client.h>

namespace armarx::skills::provider
{
    armarx::PropertyDefinitionsPtr PlatformControlSkillProvider::createPropertyDefinitions()
    {
        armarx::PropertyDefinitionsPtr defs = new ComponentPropertyDefinitions(getConfigIdentifier());

        platformControlSkillContext.defineProperties(defs, "platformControlSkills.");

        return defs;
    }

    std::string PlatformControlSkillProvider::getDefaultName() const
    {
        return "PlatformControlSkillProvider";
    }

    void PlatformControlSkillProvider::onInitComponent()
    {
        platformControlSkillContext.onInit(*this);
    }

    void PlatformControlSkillProvider::onConnectComponent()
    {
        platformControlSkillContext.onConnected(*this);

        auto& mns = memoryNameSystem();

        // Add move skill
        addSkill(std::make_unique<MovePlatformToLandmark>(mns, arviz, platformControlSkillContext));
        addSkill(std::make_unique<MovePlatformToPose>(mns, arviz, platformControlSkillContext));

    }

    void PlatformControlSkillProvider::onDisconnectComponent()
    {

    }

    void PlatformControlSkillProvider::onExitComponent()
    {}
}
