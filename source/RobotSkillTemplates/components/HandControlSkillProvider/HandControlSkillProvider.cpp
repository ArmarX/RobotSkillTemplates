#include "HandControlSkillProvider.h"

#include <RobotAPI/libraries/aron/core/type/variant/container/Object.h>
#include <RobotAPI/libraries/aron/core/type/variant/primitive/String.h>
#include <RobotAPI/libraries/aron/converter/json/NLohmannJSONConverter.h>

//#include <RobotAPI/libraries/armem/client/MemoryNameSystem.h>
#include <RobotAPI/libraries/armem/client.h>

namespace armarx::skills::provider
{
    armarx::PropertyDefinitionsPtr HandControlSkillProvider::createPropertyDefinitions()
    {
        armarx::PropertyDefinitionsPtr defs = new ComponentPropertyDefinitions(getConfigIdentifier());

        handControlSkillContext.defineProperties(defs, "handControlSkills.");

        return defs;
    }

    std::string HandControlSkillProvider::getDefaultName() const
    {
        return "HandControlSkillProvider";
    }

    void HandControlSkillProvider::onInitComponent()
    {
        handControlSkillContext.onInit(*this);
    }

    void HandControlSkillProvider::onConnectComponent()
    {
        handControlSkillContext.onConnected(*this);

        // Add move skill
        addSkill(std::make_unique<OpenHand>(handControlSkillContext));
        addSkill(std::make_unique<CloseHand>(handControlSkillContext));

    }

    void HandControlSkillProvider::onDisconnectComponent()
    {

    }

    void HandControlSkillProvider::onExitComponent()
    {}
}
