#include "VisualSearchSkillProvider.h"

#include <RobotAPI/libraries/aron/core/type/variant/container/Object.h>
#include <RobotAPI/libraries/aron/core/type/variant/primitive/String.h>
#include <RobotAPI/libraries/aron/converter/json/NLohmannJSONConverter.h>

//#include <RobotAPI/libraries/armem/client/MemoryNameSystem.h>
#include <RobotAPI/libraries/armem/client.h>

namespace armarx::skills::provider
{
    armarx::PropertyDefinitionsPtr VisualSearchSkillProvider::createPropertyDefinitions()
    {
        armarx::PropertyDefinitionsPtr defs = new ComponentPropertyDefinitions(getConfigIdentifier());

        visualSearchSkillContext.defineProperties(defs, "visualSearchSkills.");

        return defs;
    }

    std::string VisualSearchSkillProvider::getDefaultName() const
    {
        return "VisualSearchSkillProvider";
    }

    void VisualSearchSkillProvider::onInitComponent()
    {
        visualSearchSkillContext.onInit(*this);
    }

    void VisualSearchSkillProvider::onConnectComponent()
    {
        visualSearchSkillContext.onConnected(*this);

        auto& mns = memoryNameSystem();

        // Add move skill
        addSkill(std::make_unique<WhatCanYouSeeNow>(mns, arviz, visualSearchSkillContext));
    }

    void VisualSearchSkillProvider::onDisconnectComponent()
    {

    }

    void VisualSearchSkillProvider::onExitComponent()
    {}
}
