#include "BringObjectSkillProvider.h"

namespace armarx::skills::provider
{
    armarx::PropertyDefinitionsPtr BringObjectSkillProvider::createPropertyDefinitions()
    {
        armarx::PropertyDefinitionsPtr defs = new ComponentPropertyDefinitions(getConfigIdentifier());

        bringObjectSkillContext.defineProperties(defs, "bringObject.");

        return defs;
    }

    std::string BringObjectSkillProvider::getDefaultName() const
    {
        return "BringObjectSkillProvider";
    }

    void BringObjectSkillProvider::onInitComponent()
    {
        bringObjectSkillContext.onInit(*this);
    }

    void BringObjectSkillProvider::onConnectComponent()
    {
        bringObjectSkillContext.onConnected(*this);

        // Add move skill
        addSkill(std::make_unique<BringObjectToLocation>(bringObjectSkillContext));
        //addSkill(std::make_unique<BringObjectToHuman>(mns, arviz, graspObjectSkillContext));
    }

    void BringObjectSkillProvider::onDisconnectComponent()
    {

    }

    void BringObjectSkillProvider::onExitComponent()
    {}
}
