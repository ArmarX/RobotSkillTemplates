/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2014-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::HandGroup::HandGroupRemoteStateOfferer
 * @author     Simon Ottenhaus ( simon dot ottenhaus at kit dot edu )
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "HandGroupRemoteStateOfferer.h"

using namespace armarx;
using namespace HandGroup;

// DO NOT EDIT NEXT LINE
HandGroupRemoteStateOfferer::SubClassRegistry HandGroupRemoteStateOfferer::Registry(HandGroupRemoteStateOfferer::GetName(), &HandGroupRemoteStateOfferer::CreateInstance);



HandGroupRemoteStateOfferer::HandGroupRemoteStateOfferer(StatechartGroupXmlReaderPtr reader) :
    XMLRemoteStateOfferer < HandGroupStatechartContext > (reader)
{
}

void HandGroupRemoteStateOfferer::onInitXMLRemoteStateOfferer()
{

}

void HandGroupRemoteStateOfferer::onConnectXMLRemoteStateOfferer()
{

}

void HandGroupRemoteStateOfferer::onExitXMLRemoteStateOfferer()
{

}

// DO NOT EDIT NEXT FUNCTION
std::string HandGroupRemoteStateOfferer::GetName()
{
    return "HandGroupRemoteStateOfferer";
}

// DO NOT EDIT NEXT FUNCTION
XMLStateOffererFactoryBasePtr HandGroupRemoteStateOfferer::CreateInstance(StatechartGroupXmlReaderPtr reader)
{
    return XMLStateOffererFactoryBasePtr(new HandGroupRemoteStateOfferer(reader));
}



