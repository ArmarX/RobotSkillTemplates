/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2014-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::HandGroup
 * @author     Simon Ottenhaus ( simon dot ottenhaus at kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "CloseThumb.h"
#include "HandGroupStatechartContext.h"

using namespace armarx;
using namespace HandGroup;

// DO NOT EDIT NEXT LINE
CloseThumb::SubClassRegistry CloseThumb::Registry(CloseThumb::GetName(), &CloseThumb::CreateInstance);



CloseThumb::CloseThumb(const XMLStateConstructorParams& stateData) :
    XMLStateTemplate<CloseThumb>(stateData),  CloseThumbGeneratedBase<CloseThumb>(stateData)
{
}

void CloseThumb::onEnter()
{
    HandUnitInterfacePrx handUnit = getContext<HandGroupStatechartContext>()->getHandUnit(in.getHandName());
    handUnit->setShape(in.getShapeName());
    setTimeoutEvent(in.getWaitTimeMs(), this->createEventDone());
}

void CloseThumb::run()
{
    // nop
}

void CloseThumb::onBreak()
{
    // nop
}

void CloseThumb::onExit()
{
    // nop
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr CloseThumb::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new CloseThumb(stateData));
}

