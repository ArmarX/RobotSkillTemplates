/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2014-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::HandGroup
 * @author     Simon Ottenhaus ( simon dot ottenhaus at kit dot edu )
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "HandGroupStatechartContext.h"
#include "ShapeHand.h"

using namespace armarx;
using namespace HandGroup;

// DO NOT EDIT NEXT LINE
ShapeHand::SubClassRegistry ShapeHand::Registry(ShapeHand::GetName(), &ShapeHand::CreateInstance);



ShapeHand::ShapeHand(XMLStateConstructorParams stateData) :
    XMLStateTemplate<ShapeHand>(stateData),  ShapeHandGeneratedBase<ShapeHand>(stateData)
{
}

void ShapeHand::onEnter()
{
    setTimeoutEvent(in.getWaitTimeMs(), this->createEventDone());
}

void ShapeHand::run()
{
    ARMARX_INFO << "Shaping hand " << in.getHandName() << " to shape " << in.getShapeName();
    switchToPosControl = in.getSwitchPosControlAfterShaping();
    HandUnitInterfacePrx handUnit = getContext<HandGroupStatechartContext>()->getHandUnit(in.getHandName());

    if (in.getUseCollisionChecking() && in.isObjectInstanceNameSet())
    {
        std::string objectInstanceName = in.getObjectInstanceName();
        handUnit->setShapeWithObjectInstance(in.getShapeName(), objectInstanceName);
    }
    else
    {
        handUnit->setShape(in.getShapeName());
    }

    bool notify = false;

    if (in.isNotifyReleasedDuringShapingSet() && in.getNotifyReleasedDuringShaping())
    {
        notify = true;
    }

    if (notify)
    {
        handUnit->setObjectReleased(in.getNotifyReleasedObjectName());
    }
}

void ShapeHand::onBreak()
{
    // nop
}

void ShapeHand::onExit()
{
    if (switchToPosControl)
    {
        try
        {
            HandUnitInterfacePrx handUnit = getContext<HandGroupStatechartContext>()->getHandUnit(in.getHandName());
            NameValueMap jv = handUnit->getCurrentJointValues();
            handUnit->setJointAngles(jv);
        }
        catch (...) {}
    }
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr ShapeHand::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new ShapeHand(stateData));
}

