/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2014-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::HandGroup
 * @author     Simon Ottenhaus ( simon dot ottenhaus at kit dot edu )
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once

#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/system/ImportExportComponent.h>
#include <ArmarXCore/statechart/StatechartContext.h>
#include <RobotAPI/components/units/HandUnit.h>

#include <RobotAPI/libraries/core/RobotAPIObjectFactories.h>
#include <MemoryX/core/MemoryXCoreObjectFactories.h>

// Custom Includes
//#include <RobotAPI/interface/units/KinematicUnitInterface.h>
//#include <RobotAPI/interface/units/TCPControlUnit.h>
//#include <RobotAPI/interface/observers/KinematicUnitObserverInterface.h>
#include <RobotAPI/interface/core/RobotState.h>
#include <VirtualRobot/VirtualRobot.h>

namespace armarx::HandGroup
{

    struct HandGroupStatechartContextProperties : StatechartContextPropertyDefinitions
    {
        HandGroupStatechartContextProperties(std::string prefix):
            StatechartContextPropertyDefinitions(prefix)
        {
            //            defineRequiredProperty<std::string>("KinematicUnitName", "Name of the kinematic unit that should be used");
            //            defineRequiredProperty<std::string>("KinematicUnitObserverName", "Name of the kinematic unit observer that should be used");
            defineOptionalProperty<std::string>("RobotStateComponentName", "RobotStateComponent", "Name of the robot state component that should be used");
            //            defineOptionalProperty<std::string>("TCPControlUnitName", "TCPControlUnit", "Name of the tcp control unit component that should be used");
            defineRequiredProperty<std::string>("HandUnitNames", "Comma separated list of hand unit to use");
        }
    };

    /**
     * @class HandGroupStatechartContext is a custom implementation of the StatechartContext
     * for a statechart
     */
    class ARMARXCOMPONENT_IMPORT_EXPORT HandGroupStatechartContext :
        virtual public XMLStatechartContext
    {
    public:
        // inherited from Component
        std::string getDefaultName() const override
        {
            return "HandGroupStatechartContext";
        }
        void onInitStatechartContext() override;
        void onConnectStatechartContext() override;


        const VirtualRobot::RobotPtr getRobot()
        {
            return remoteRobot;
        }
        RobotStateComponentInterfacePrx getRobotStateComponent()
        {
            return robotStateComponent;
        }
        //        KinematicUnitInterfacePrx getKinematicUnit() { return kinematicUnitPrx; }
        //        std::string getKinematicUnitObserverName() { return getProperty<std::string>("KinematicUnitObserverName").getValue(); }
        //        TCPControlUnitInterfacePrx getTCPControlUnit() { return tcpControlPrx;}

        const HandUnitInterfacePrx getHandUnit(const std::string& handName)
        {
            std::stringstream handUnitNamesSS;

            for (std::pair<std::string, HandUnitInterfacePrx> pair : handUnits)
            {
                if (pair.first == handName)
                {
                    return pair.second;
                }

                handUnitNamesSS << pair.first << "; ";
            }

            throw LocalException("Could not find HandUnit Proxy for hand name '" + handName + "'. Available Hand Units: " + handUnitNamesSS.str());
        }

    private:


        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        PropertyDefinitionsPtr createPropertyDefinitions() override;


        RobotStateComponentInterfacePrx robotStateComponent;
        //        KinematicUnitInterfacePrx kinematicUnitPrx;
        //        KinematicUnitObserverInterfacePrx kinematicUnitObserverPrx;
        //        TCPControlUnitInterfacePrx tcpControlPrx;
        VirtualRobot::RobotPtr remoteRobot;
        std::map<std::string, HandUnitInterfacePrx> handUnits;
        std::vector<std::string> handUnitNames;

    };

}
