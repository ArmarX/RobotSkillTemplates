/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2014-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::HandGroup
 * @author     Simon Ottenhaus ( simon dot ottenhaus at kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "GetIndexToThumbDistanceRightHand.h"
#include "HandGroupStatechartContext.h"

#include <RobotAPI/libraries/core/remoterobot/RemoteRobot.h>
#include <RobotAPI/libraries/core/Pose.h>

using namespace armarx;
using namespace HandGroup;

// DO NOT EDIT NEXT LINE
GetIndexToThumbDistanceRightHand::SubClassRegistry GetIndexToThumbDistanceRightHand::Registry(GetIndexToThumbDistanceRightHand::GetName(), &GetIndexToThumbDistanceRightHand::CreateInstance);



GetIndexToThumbDistanceRightHand::GetIndexToThumbDistanceRightHand(const XMLStateConstructorParams& stateData) :
    XMLStateTemplate<GetIndexToThumbDistanceRightHand>(stateData),  GetIndexToThumbDistanceRightHandGeneratedBase<GetIndexToThumbDistanceRightHand>(stateData)
{
}

void GetIndexToThumbDistanceRightHand::onEnter()
{
    HandGroupStatechartContext* context = getContext<HandGroupStatechartContext>();
    VirtualRobot::RobotNodePtr indexRFingertipPtr = context->getRobot()->getRobotNode("Index R Fingertip");
    VirtualRobot::RobotNodePtr thumbRFingertipPtr = context->getRobot()->getRobotNode("Thumb R Fingertip");

    // object: distance = 95
    // open:   distance = 155
    // close (empty) distance = 27

    Eigen::Vector3f indexRFingertipPos = indexRFingertipPtr->getGlobalPose().block<3, 1>(0, 3);
    Eigen::Vector3f thumbRFingertipPos = thumbRFingertipPtr->getGlobalPose().block<3, 1>(0, 3);
    float distance = (indexRFingertipPos - thumbRFingertipPos).norm();
    out.setDistance(distance);
    emitDone();
}

void GetIndexToThumbDistanceRightHand::run()
{
    // put your user code for the execution-phase here
    // runs in seperate thread, thus can do complex operations
    // should check constantly whether isRunningTaskStopped() returns true

    // uncomment this if you need a continous run function. Make sure to use sleep or use blocking wait to reduce cpu load.
    //    while (!isRunningTaskStopped()) // stop run function if returning true
    //    {
    //        // do your calculations
    //    }

}

void GetIndexToThumbDistanceRightHand::onBreak()
{
    // put your user code for the breaking point here
    // execution time should be short (<100ms)
}

void GetIndexToThumbDistanceRightHand::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)

}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr GetIndexToThumbDistanceRightHand::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new GetIndexToThumbDistanceRightHand(stateData));
}

