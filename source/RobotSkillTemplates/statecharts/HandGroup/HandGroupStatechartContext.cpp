/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2014-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::HandGroup
 * @author     Simon Ottenhaus ( simon dot ottenhaus at kit dot edu )
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "HandGroupStatechartContext.h"

#include <RobotAPI/libraries/core/remoterobot/RemoteRobot.h>
#include <RobotAPI/libraries/core/Pose.h>

#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/system/ImportExportComponent.h>
#include <ArmarXCore/statechart/Statechart.h>

#include <SimoxUtility/algorithm/string/string_tools.h>

using namespace armarx;

namespace armarx::HandGroup
{
    void HandGroupStatechartContext::onInitStatechartContext()
    {
        // Register dependencies
        //        usingProxy(getProperty<std::string>("KinematicUnitName").getValue());
        //        usingProxy(getProperty<std::string>("KinematicUnitObserverName").getValue());
        usingProxy(getProperty<std::string>("RobotStateComponentName").getValue());
        //        usingProxy(getProperty<std::string>("TCPControlUnitName").getValue());

        const std::string handUnitNamesStr = getProperty<std::string>("HandUnitNames").getValue();
        handUnitNames = simox::alg::split(handUnitNamesStr, ",");

        for (std::string handUnitName : handUnitNames)
        {
            usingProxy(handUnitName);
        }
    }


    void HandGroupStatechartContext::onConnectStatechartContext()
    {

        // retrieve proxies
        robotStateComponent = getProxy<RobotStateComponentInterfacePrx>(getProperty<std::string>("RobotStateComponentName").getValue());
        //        kinematicUnitPrx = getProxy<KinematicUnitInterfacePrx>(getProperty<std::string>("KinematicUnitName").getValue());
        //        kinematicUnitObserverPrx = getProxy<KinematicUnitObserverInterfacePrx>(getProperty<std::string>("KinematicUnitObserverName").getValue());
        //        tcpControlPrx = getProxy<TCPControlUnitInterfacePrx>(getProperty<std::string>("TCPControlUnitName").getValue());

        // initialize remote robot
        remoteRobot.reset(new RemoteRobot(robotStateComponent->getSynchronizedRobot()));

        for (std::string handUnitName : handUnitNames)
        {
            HandUnitInterfacePrx prx = getProxy<HandUnitInterfacePrx>(handUnitName);
            handUnits.insert(std::make_pair(prx->getHandName(), prx));
        }
    }

    PropertyDefinitionsPtr HandGroupStatechartContext::createPropertyDefinitions()
    {
        return PropertyDefinitionsPtr(new HandGroupStatechartContextProperties(
                                          getConfigIdentifier()));
    }
}


