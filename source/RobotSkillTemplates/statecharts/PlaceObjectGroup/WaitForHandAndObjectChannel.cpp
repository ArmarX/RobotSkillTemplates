/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2014-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::PlaceObjectGroup
 * @author     David ( david dot schiebener at kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "WaitForHandAndObjectChannel.h"

#include <ArmarXCore/core/time/TimeUtil.h>
#include <RobotAPI/interface/components/ViewSelectionInterface.h>


using namespace armarx;
using namespace PlaceObjectGroup;

// DO NOT EDIT NEXT LINE
WaitForHandAndObjectChannel::SubClassRegistry WaitForHandAndObjectChannel::Registry(WaitForHandAndObjectChannel::GetName(), &WaitForHandAndObjectChannel::CreateInstance);



WaitForHandAndObjectChannel::WaitForHandAndObjectChannel(const XMLStateConstructorParams& stateData) :
    XMLStateTemplate<WaitForHandAndObjectChannel>(stateData),  WaitForHandAndObjectChannelGeneratedBase<WaitForHandAndObjectChannel>(stateData)
{
}

void WaitForHandAndObjectChannel::onEnter()
{
    PlaceObjectGroupStatechartContextBase* context = getContext<PlaceObjectGroupStatechartContextBase>();

    if (context->getObjectMemoryObserver()->getObjectInstancesByClass(in.getObjectName()).size() == 0)
    {
        ARMARX_VERBOSE << "Requesting object " << in.getObjectName();
        ChannelRefBasePtr objectMemoryChannel = context->getObjectMemoryObserver()->requestObjectClassRepeated(in.getObjectName(), 200, armarx::DEFAULT_VIEWTARGET_PRIORITY);

        if (objectMemoryChannel)
        {
            local.setObjectMemoryChannel(ChannelRefPtr::dynamicCast(objectMemoryChannel));
        }
    }

    if (context->getObjectMemoryObserver()->getObjectInstancesByClass(in.getHandNameInMemory()).size() == 0)
    {
        ARMARX_VERBOSE << "Requesting hand " << in.getHandNameInMemory();
        ChannelRefBasePtr handMemoryChannel = context->getObjectMemoryObserver()->requestObjectClassRepeated(in.getHandNameInMemory(), 300, armarx::DEFAULT_VIEWTARGET_PRIORITY);

        if (handMemoryChannel)
        {
            //handMemoryChannel->validate();
            local.setHandMemoryChannel(ChannelRefPtr::dynamicCast(handMemoryChannel));
        }
    }
}

void WaitForHandAndObjectChannel::run()
{
    int waitSec = in.getWaitUntilProcessSec();
    if (waitSec > 0)
    {
        TimeUtil::MSSleep(waitSec * 1000);
    }
    else
    {
        TimeUtil::MSSleep(1000);
    }

    PlaceObjectGroupStatechartContextBase* context = getContext<PlaceObjectGroupStatechartContextBase>();
    bool bothAvailable = false;
    memoryx::ChannelRefBaseSequence objectList;
    armarx::ChannelRefPtr objChannel;
    memoryx::ChannelRefBaseSequence handList;
    armarx::ChannelRefPtr handChannel;

    while (!isRunningTaskStopped() && !bothAvailable) // stop run function if returning true
    {
        handList = context->getObjectMemoryObserver()->getObjectInstancesByClass(in.getHandNameInMemory());

        if (handList.size() > 0)
        {
            handChannel = ChannelRefPtr::dynamicCast(handList.at(0));
        }
        else
        {
            ARMARX_VERBOSE << "Hand " << in.getHandNameInMemory() << " not yet localized";
            TimeUtil::MSSleep(10);
        }

        objectList = context->getObjectMemoryObserver()->getObjectInstancesByClass(in.getObjectName());

        if (objectList.size() > 0)
        {
            objChannel = ChannelRefPtr::dynamicCast(objectList.at(0));
        }
        else
        {
            ARMARX_VERBOSE << "Object " << in.getObjectName() << " not localized";
            TimeUtil::MSSleep(10);
        }


        if (handChannel)
        {
            // sometimes the object cannot be localized, so we are happy with the hand channel...
            if (objChannel)
            {
                out.setObjectChannelRef(objChannel);
            }

            out.setHandChannelRef(handChannel);
            out.setObjectClassName(in.getObjectName());
            bothAvailable = true;
            emitHandAndObjectLocalized();
        }
    }
}

void WaitForHandAndObjectChannel::onBreak()
{
    // put your user code for the breaking point here
    // execution time should be short (<100ms)
}

void WaitForHandAndObjectChannel::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr WaitForHandAndObjectChannel::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new WaitForHandAndObjectChannel(stateData));
}

