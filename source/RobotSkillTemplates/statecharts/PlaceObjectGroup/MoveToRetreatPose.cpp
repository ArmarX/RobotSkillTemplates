/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2014-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::PlaceObjectGroup
 * @author     David ( david dot schiebener at kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "MoveToRetreatPose.h"

using namespace armarx;
using namespace PlaceObjectGroup;

// DO NOT EDIT NEXT LINE
MoveToRetreatPose::SubClassRegistry MoveToRetreatPose::Registry(MoveToRetreatPose::GetName(), &MoveToRetreatPose::CreateInstance);



MoveToRetreatPose::MoveToRetreatPose(const XMLStateConstructorParams& stateData) :
    XMLStateTemplate<MoveToRetreatPose>(stateData),  MoveToRetreatPoseGeneratedBase<MoveToRetreatPose>(stateData)
{
}

void MoveToRetreatPose::onEnter()
{
    PlaceObjectGroupStatechartContextBase* context = getContext<PlaceObjectGroupStatechartContextBase>();
    local.setStartTimeRef(ChannelRefPtr::dynamicCast(context->getSystemObserver()->startTimer("MoveToPrePlacePoseStartTime")));

    auto retreatPose = in.getRetreatPose();
    context->getDebugDrawerTopic()->setPoseVisu("PlacingPoses", "RetreatTarget", retreatPose);

    context->getTCPControlUnit()->request();
}


void MoveToRetreatPose::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
    PlaceObjectGroupStatechartContextBase* context = getContext<PlaceObjectGroupStatechartContextBase>();
    context->getTCPControlUnit()->release();
    context->getDebugDrawerTopic()->removePoseVisu("PlacingPoses", "RetreatTarget");


}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr MoveToRetreatPose::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new MoveToRetreatPose(stateData));
}

