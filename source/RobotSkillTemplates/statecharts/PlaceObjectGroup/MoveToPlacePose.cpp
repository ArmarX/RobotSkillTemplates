/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2014-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::PlaceObjectGroup
 * @author     David ( david dot schiebener at kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "MoveToPlacePose.h"
#include <RobotAPI/interface/observers/ObserverFilters.h>
#include <ArmarXCore/core/time/TimeUtil.h>
#include <MemoryX/libraries/motionmodels/MotionModelStaticObject.h>

using namespace armarx;
using namespace PlaceObjectGroup;

// DO NOT EDIT NEXT LINE
MoveToPlacePose::SubClassRegistry MoveToPlacePose::Registry(MoveToPlacePose::GetName(), &MoveToPlacePose::CreateInstance);



MoveToPlacePose::MoveToPlacePose(const XMLStateConstructorParams& stateData) :
    XMLStateTemplate<MoveToPlacePose>(stateData),  MoveToPlacePoseGeneratedBase<MoveToPlacePose>(stateData)
{
}

void MoveToPlacePose::onEnter()
{
    PlaceObjectGroupStatechartContextBase* context = getContext<PlaceObjectGroupStatechartContextBase>();
    local.setStartTimeRef(ChannelRefPtr::dynamicCast(context->getSystemObserver()->startTimer("MoveToPlacePoseStartTime")));

    ARMARX_VERBOSE << VAROUT(in.getForceSensorDatafieldName());
    local.setForceDatafield(DatafieldRefPtr::dynamicCast(getForceTorqueObserver()->createNulledDatafield(getForceTorqueObserver()->getForceDatafield(in.getForceSensorDatafieldName()))));

    ARMARX_VERBOSE << VAROUT(in.getForceThreshold());
    Literal forceCheck(local.getForceDatafield()->getDataFieldIdentifier(), checks::magnitudelarger, {(float)in.getForceThreshold()});

    installConditionForHighForceDetected(forceCheck);
    context->getTCPControlUnit()->request();
}

void MoveToPlacePose::run()
{
    while (!isRunningTaskStopped())
    {
        ARMARX_INFO  << deactivateSpam(0.2) << "Current force: " << local.getForceDatafield()->get<FramedDirection>()->toEigen().norm() << " Threshold: " << in.getForceThreshold();
        TimeUtil::MSSleep(20);
    }
}



void MoveToPlacePose::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
    // Not working
    //    if(getTriggeredEndstateEvent() && getTriggeredEndstateEvent()->eventName == createEventHighForceDetected()->eventName)
    //    {
    //        ARMARX_WARNING << "High force detected: " << local.getForceDatafield()->get<FramedDirection>()->toEigen().norm();
    //    }
    //    else
    {
        ARMARX_INFO << "Force at end: " << local.getForceDatafield()->get<FramedDirection>()->toEigen().norm();
    }

    getForceTorqueObserver()->removeFilteredDatafield(local.getForceDatafield());
    PlaceObjectGroupStatechartContextBase* context = getContext<PlaceObjectGroupStatechartContextBase>();
    context->getTCPControlUnit()->release();
    context->getSystemObserver()->removeTimer(local.getStartTimeRef());

    auto retreatPose = in.getRetreatPose();
    retreatPose->changeToGlobal(getRobot());
    retreatPose->position->z += 100;
    out.setRetreatPose(retreatPose);

    auto instances = context->getWorkingMemory()->getObjectInstancesSegment()->getObjectInstancesByClass(in.getObjectClassName());

    if (instances.size() > 0)
    {
        memoryx::ObjectInstanceBasePtr obj = instances.front();
        ARMARX_CHECK_EXPRESSION(obj);

        memoryx::MotionModelStaticObjectPtr newMotionModel = new memoryx::MotionModelStaticObject(context->getRobotStateComponent());
        getWorkingMemory()->getObjectInstancesSegment()->setNewMotionModel(obj->getId(), newMotionModel);
        auto pose = in.getObjectTargetPose();
        pose->changeFrame(context->getRobot(), context->getRobot()->getRootNode()->getName());
        getWorkingMemory()->getObjectInstancesSegment()->setObjectPose(obj->getId(),
                new LinkedPose(pose->toEigen(), pose->getFrame(),
                               context->getRobotStateComponent()->getRobotSnapshot(context->getRobot()->getName())));
        ARMARX_INFO << "Setting object in WM to pose: " << *in.getObjectTargetPose();
    }
    else
    {
        ARMARX_WARNING << "No object instance with name '" << in.getObjectClassName() << "' found! Could not change working memory position and motion model of object.";
    }

}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr MoveToPlacePose::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new MoveToPlacePose(stateData));
}

