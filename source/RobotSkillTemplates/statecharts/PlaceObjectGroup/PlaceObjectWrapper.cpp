/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2014-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::PlaceObjectGroup
 * @author     David ( david dot schiebener at kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "PlaceObjectWrapper.h"

using namespace armarx;
using namespace PlaceObjectGroup;

// DO NOT EDIT NEXT LINE
PlaceObjectWrapper::SubClassRegistry PlaceObjectWrapper::Registry(PlaceObjectWrapper::GetName(), &PlaceObjectWrapper::CreateInstance);



PlaceObjectWrapper::PlaceObjectWrapper(const XMLStateConstructorParams& stateData) :
    XMLStateTemplate<PlaceObjectWrapper>(stateData),  PlaceObjectWrapperGeneratedBase<PlaceObjectWrapper>(stateData)
{
}

void PlaceObjectWrapper::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
}

void PlaceObjectWrapper::run()
{
    // put your user code for the execution-phase here
    // runs in seperate thread, thus can do complex operations
    // should check constantly whether isRunningTaskStopped() returns true

    // uncomment this if you need a continous run function. Make sure to use sleep or use blocking wait to reduce cpu load.
    //    while (!isRunningTaskStopped()) // stop run function if returning true
    //    {
    //        // do your calculations
    //    }

}

void PlaceObjectWrapper::onBreak()
{
    // put your user code for the breaking point here
    // execution time should be short (<100ms)
}

void PlaceObjectWrapper::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)

}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr PlaceObjectWrapper::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new PlaceObjectWrapper(stateData));
}

