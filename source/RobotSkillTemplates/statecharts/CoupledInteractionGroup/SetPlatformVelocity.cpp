/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2014-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::SetPlatformVelocityGroup
 * @author     [Author Name] ( [Author Email] )
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "SetPlatformVelocity.h"
#include <RobotAPI/libraries/core/remoterobot/RemoteRobot.h>


using namespace armarx;
using namespace CoupledInteractionGroup;

// DO NOT EDIT NEXT LINE
SetPlatformVelocity::SubClassRegistry SetPlatformVelocity::Registry(SetPlatformVelocity::GetName(), &SetPlatformVelocity::CreateInstance);



SetPlatformVelocity::SetPlatformVelocity(XMLStateConstructorParams stateData) :
    XMLStateTemplate < SetPlatformVelocity > (stateData), SetPlatformVelocityGeneratedBase < SetPlatformVelocity > (stateData)
{
}

void SetPlatformVelocity::onEnter()
{
    CoupledInteractionGroupStatechartContext* context = getContext<CoupledInteractionGroupStatechartContext>();
    Vector3Ptr platformVelocity = in.getPlatformVelocity();
    PlatformUnitInterfacePrx platformPrx = context->getPlatformUnitProxy();
    ARMARX_INFO << "Moving Platform with " << platformVelocity->x << " " << platformVelocity->y << " " << platformVelocity->z;
    platformPrx->move(platformVelocity->x, platformVelocity->y, platformVelocity->z);
    emitEvDone();


}


void SetPlatformVelocity::onExit()
{

    // put your user code for the exit point here
    // execution time should be short (<100ms)

}

// DO NOT EDIT NEXT FUNCTION
std::string SetPlatformVelocity::GetName()
{
    return "SetPlatformVelocity";
}

// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr SetPlatformVelocity::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new SetPlatformVelocity(stateData));
}

