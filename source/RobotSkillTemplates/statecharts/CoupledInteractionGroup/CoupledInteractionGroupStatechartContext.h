/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2014-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::CoupledInteractionGroup
 * @author     [Author Name] ( [Author Email] )
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once

#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/system/ImportExportComponent.h>
#include <ArmarXCore/statechart/StatechartContext.h>


// Custom Includes
// Custom Includes
#include <RobotAPI/interface/units/KinematicUnitInterface.h>
#include <RobotAPI/interface/units/TCPControlUnit.h>
#include <RobotAPI/interface/observers/KinematicUnitObserverInterface.h>
#include <RobotAPI/interface/core/RobotState.h>
#include <RobotAPI/libraries/core/remoterobot/RemoteRobot.h>
#include <RobotAPI/interface/units/PlatformUnitInterface.h>
#include <RobotAPI/interface/observers/PlatformUnitObserverInterface.h>
#include <RobotAPI/components/DebugDrawer/DebugDrawerComponent.h>
#include <RobotAPI/components/units/ForceTorqueObserver.h>

#include <VirtualRobot/VirtualRobot.h>

#include <MemoryX/interface/components/WorkingMemoryInterface.h>
#include <MemoryX/interface/observers/ObjectMemoryObserverInterface.h>
#include <MemoryX/interface/components/PriorKnowledgeInterface.h>
#include <MemoryX/libraries/memorytypes/entity/AgentInstance.h>
#include <MemoryX/libraries/memorytypes/MemoryXTypesObjectFactories.h>
#include <MemoryX/core/MemoryXCoreObjectFactories.h>
#include <RobotAPI/interface/components/ViewSelectionInterface.h>
//#include <ArmarXSimulation/interface/simulator/SimulatorInterface.h>
#include <ArmarXCore/observers/variant/DatafieldRef.h>

namespace armarx::CoupledInteractionGroup
{

    struct CoupledInteractionGroupStatechartContextProperties : StatechartContextPropertyDefinitions
    {
        CoupledInteractionGroupStatechartContextProperties(std::string prefix):
            StatechartContextPropertyDefinitions(prefix)
        {
            //            defineRequiredProperty<std::string>("KinematicUnitName", "Name of the kinematic unit that should be used");
            //            defineRequiredProperty<std::string>("KinematicUnitObserverName", "Name of the kinematic unit observer that should be used");
            //            defineOptionalProperty<std::string>("RobotStateComponentName", "RobotStateComponent", "Name of the robot state component that should be used");
            //            defineOptionalProperty<std::string>("TCPControlUnitName", "TCPControlUnit", "Name of the tcp control unit component that should be used");
            defineRequiredProperty<std::string>("KinematicUnitName", "Name of the kinematic unit that should be used");
            defineRequiredProperty<std::string>("KinematicUnitObserverName", "Name of the kinematic unit observer that should be used");
            defineOptionalProperty<std::string>("RobotStateComponentName", "RobotStateComponent", "Name of the robot state component that should be used");
            defineOptionalProperty<std::string>("TCPControlUnitName", "TCPControlUnit", "Name of the tcp control unit component that should be used");
            defineOptionalProperty<std::string>("WorkingMemoryName", "WorkingMemory", "Name of the WorkingMemory component that should be used");
            defineOptionalProperty<std::string>("ObjectMemoryObserverName", "ObjectMemoryObserver", "Name of the ObjectMemoryObserver component that should be used");
            defineOptionalProperty<std::string>("PriorKnowledgeName", "PriorKnowledge", "Name of the PriorKnowledge component that should be used");
            defineOptionalProperty<std::string>("PlatformUnitName", "PlatformUnitDynamicSimulation", "Name of the PlatformUnit to use");
            defineOptionalProperty<std::string>("PlatformUnitObserverName", "PlatformUnitObserver", "Name of the PlatformUnitObserver to use");
            defineOptionalProperty<std::string>("ForceTorqueObserverName", "ForceTorqueObserver", "Name of the ForceTorqueObserver to use");
            defineOptionalProperty<std::string>("TCPControlUnitObserverName", "TCPControlUnitObserver", "Name of the tcp control unit observer that should be used");
            defineOptionalProperty<std::string>("ViewSelectionName", "ViewSelection", "Name of the ViewSelection component that should be used");
        }
    };

    /**
     * @class CoupledInteractionGroupStatechartContext is a custom implementation of the StatechartContext
     * for a statechart
     */
    class ARMARXCOMPONENT_IMPORT_EXPORT CoupledInteractionGroupStatechartContext :
        virtual public XMLStatechartContext
    {
    public:
        // inherited from Component
        std::string getDefaultName() const override
        {
            return "CoupledInteractionGroupStatechartContext";
        }
        void onInitStatechartContext() override;
        void onConnectStatechartContext() override;


        const VirtualRobot::RobotPtr getRobot()
        {
            return remoteRobot;
        }
        RobotStateComponentInterfacePrx getRobotStateComponent()
        {
            return robotStateComponent;
        }
        KinematicUnitInterfacePrx getKinematicUnit()
        {
            return kinematicUnitPrx;
        }
        std::string getKinematicUnitObserverName()
        {
            return getProperty<std::string>("KinematicUnitObserverName").getValue();
        }
        TCPControlUnitInterfacePrx getTCPControlUnit()
        {
            return tcpControlPrx;
        }
        TCPControlUnitObserverInterfacePrx getTCPControlUnitObserver()
        {
            return tcpControlUnitObserverPrx;
        }
        memoryx::WorkingMemoryInterfacePrx getWorkingMemoryProxy()
        {
            return workingMemoryProxy;
        }
        memoryx::ObjectMemoryObserverInterfacePrx getObjectMemoryObserverProxy()
        {
            return objectMemoryObserverProxy;
        }
        memoryx::PriorKnowledgeInterfacePrx getPriorKnowledgeProxy()
        {
            return priorKnowledgeProxy;
        }
        PlatformUnitInterfacePrx getPlatformUnitProxy()
        {
            return platformUnitPrx;
        }
        PlatformUnitObserverInterfacePrx getPlatformUnitObserverProxy()
        {
            return platformUnitObserverPrx;
        }

        armarx::DebugDrawerInterfacePrx getDebugDrawerTopicProxy()
        {
            return debugDrawerTopicProxy;
        }
        //SimulatorInterfacePrx getSimulatorProxy() { return simulatorPrx; }

        std::string getPlatformUnitObserverName()
        {
            return platformUnitObserverName;
        }
        Eigen::Matrix4f getRobotPose();
        ViewSelectionInterfacePrx getViewSelection()
        {
            return viewSelectionProxy;
        }
        ForceTorqueUnitObserverInterfacePrx getForceTorqueUnitObserver()
        {
            return ftUnitObserverPrx;
        }
        //SimpleLocationInterfacePrx getSimpleLocation() { return simpleLocationPrx;}
        std::string getForceTorqueObserverName()
        {
            return getProperty<std::string>("ForceTorqueObserverName").getValue();
        }
    private:


        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        PropertyDefinitionsPtr createPropertyDefinitions() override;


        RobotStateComponentInterfacePrx robotStateComponent;
        KinematicUnitInterfacePrx kinematicUnitPrx;
        KinematicUnitObserverInterfacePrx kinematicUnitObserverPrx;
        TCPControlUnitInterfacePrx tcpControlPrx;
        TCPControlUnitObserverInterfacePrx tcpControlUnitObserverPrx;
        VirtualRobot::RobotPtr remoteRobot;

        memoryx::WorkingMemoryInterfacePrx workingMemoryProxy;
        memoryx::ObjectMemoryObserverInterfacePrx objectMemoryObserverProxy;
        memoryx::PriorKnowledgeInterfacePrx priorKnowledgeProxy;

        //SimulatorInterfacePrx simulatorPrx;
        armarx::DebugDrawerInterfacePrx debugDrawerTopicProxy;
        ViewSelectionInterfacePrx viewSelectionProxy;
        memoryx::AgentInstancePtr agent;
        PlatformUnitInterfacePrx platformUnitPrx;
        PlatformUnitObserverInterfacePrx platformUnitObserverPrx;
        std::string platformUnitName;
        std::string platformUnitObserverName;
        //SimpleLocationInterfacePrx simpleLocationPrx;
        ForceTorqueUnitObserverInterfacePrx ftUnitObserverPrx;
    };

}


