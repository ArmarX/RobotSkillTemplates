/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2014-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::CarryTableGroup
 * @author     [Author Name] ( [Author Email] )
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "CarryTable.h"
#include <RobotAPI/libraries/core/remoterobot/RemoteRobot.h>


#include <RobotAPI/interface/components/ViewSelectionInterface.h>

using namespace armarx;
using namespace CoupledInteractionGroup;

// DO NOT EDIT NEXT LINE
CarryTable::SubClassRegistry CarryTable::Registry(CarryTable::GetName(), &CarryTable::CreateInstance);



CarryTable::CarryTable(XMLStateConstructorParams stateData) :
    XMLStateTemplate < CarryTable > (stateData), CarryTableGeneratedBase < CarryTable > (stateData)
{
}

ChannelRefPtr CarryTable::getChannelRef(std::string objectName)
{

    CoupledInteractionGroupStatechartContext* context = getContext<CoupledInteractionGroupStatechartContext>();
    ChannelRefBasePtr memoryChannel = context->getObjectMemoryObserverProxy()->requestObjectClassRepeated(objectName, 250, armarx::DEFAULT_VIEWTARGET_PRIORITY);


    if (memoryChannel)
    {
        memoryChannel->validate();
        return ChannelRefPtr::dynamicCast(memoryChannel);
    }
    else
    {
        return nullptr;
    }
}

void CarryTable::onEnter()
{
    std::string leftHandNameInMemory = in.getLeftHandNameInMemory();
    local.setLeftHandMemoryChannelRef(getChannelRef(leftHandNameInMemory));
    ARMARX_CHECK_EXPRESSION(local.getLeftHandMemoryChannelRef());

    std::string rightHandNameInMemory = in.getRightHandNameInMemory();
    local.setRightHandMemoryChannelRef(getChannelRef(rightHandNameInMemory));
    ARMARX_CHECK_EXPRESSION(local.getRightHandMemoryChannelRef());
    CoupledInteractionGroupStatechartContext* context = getContext<CoupledInteractionGroupStatechartContext>();
    armarx::ChannelRefPtr poseRef = context->getChannelRef(context->getPlatformUnitObserverName(), "platformPose");
    Eigen::Vector3f tableStartPose;
    tableStartPose(0) = poseRef->getDataField("positionX")->getFloat();
    tableStartPose(1) = poseRef->getDataField("positionY")->getFloat();
    tableStartPose(2) = poseRef->getDataField("rotation")->getFloat();

    Eigen::Vector3f tableTargetPose = in.getTableTargetPose_2()->toEigen();
    local.setTablePose(FramedDirection(tableStartPose, "Global", "Armar3"));
    local.setTableTargetPose(FramedDirection(tableTargetPose, "Global", "Armar3"));

}



void CarryTable::onExit()
{

    // put your user code for the exit point here
    // execution time should be short (<100ms)

}

// DO NOT EDIT NEXT FUNCTION
std::string CarryTable::GetName()
{
    return "CarryTable";
}

// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr CarryTable::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new CarryTable(stateData));
}

