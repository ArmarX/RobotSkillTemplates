/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2014-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::LocalizeTableGroup
 * @author     [Author Name] ( [Author Email] )
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "LocalizeTable.h"
#include <RobotAPI/libraries/core/remoterobot/RemoteRobot.h>

#include <RobotAPI/interface/components/ViewSelectionInterface.h>

#include <ArmarXCore/core/time/TimeUtil.h>

#include <MemoryX/libraries/memorytypes/entity/ObjectInstance.h>


using namespace armarx;
using namespace CoupledInteractionGroup;

// DO NOT EDIT NEXT LINE
LocalizeTable::SubClassRegistry LocalizeTable::Registry(LocalizeTable::GetName(), &LocalizeTable::CreateInstance);



LocalizeTable::LocalizeTable(XMLStateConstructorParams stateData) :
    XMLStateTemplate < LocalizeTable > (stateData), LocalizeTableGeneratedBase < LocalizeTable > (stateData)
{
}

void LocalizeTable::onEnter()
{
    CoupledInteractionGroupStatechartContext* context = getContext<CoupledInteractionGroupStatechartContext>();

    ARMARX_LOG << "deactivating automatic view selection";
    ViewSelectionInterfacePrx viewSelection = context->getViewSelection();
    viewSelection->deactivateAutomaticViewSelection();

    memoryx::WorkingMemoryInterfacePrx workingMemory = context->getWorkingMemoryProxy();
    memoryx::ObjectInstanceMemorySegmentBasePrx objectInstancesSegment = workingMemory->getObjectInstancesSegment();
    memoryx::ObjectInstanceList objectInstanceList = objectInstancesSegment->getObjectInstancesByClass(in.getObjectNameTable());

    for (auto& o : objectInstanceList)
    {
        memoryx::ObjectInstancePtr objectInstance = memoryx::ObjectInstancePtr::dynamicCast(o);

        //        objectInstance->setExistenceCertainty(0.0);
        //        objectInstancesSegment->updateEntity(objectInstance->getId(), objectInstance);
        //        ARMARX_LOG << "setting existance certainty to zero for " << in.getobjectName();

        ARMARX_LOG << "removing entity";
        objectInstancesSegment->removeEntity(objectInstance->getId());
    }

    ARMARX_LOG << "requesting object ";
    memoryx::ObjectMemoryObserverInterfacePrx objectMemoryObserver = context->getObjectMemoryObserverProxy();
    objectMemoryObserver->requestObjectClassRepeated(in.getObjectNameTable(), 2000, armarx::DEFAULT_VIEWTARGET_PRIORITY);
}

void LocalizeTable::run()
{
    CoupledInteractionGroupStatechartContext* context = getContext< CoupledInteractionGroupStatechartContext>();
    memoryx::ObjectMemoryObserverInterfacePrx objectMemoryObserver = context->getObjectMemoryObserverProxy();

    while (!isRunningTaskStopped())
    {

        memoryx::ChannelRefBaseSequence objectInstanceList = objectMemoryObserver->getObjectInstancesByClass(in.getObjectNameTable());
        ARMARX_LOG << "found " << objectInstanceList.size() << " instances for object " << in.getObjectNameTable();

        for (auto& o : objectInstanceList)
        {
            ChannelRefPtr channelRef = ChannelRefPtr::dynamicCast(o);

            FramedPositionPtr position = channelRef->get<FramedPosition>("position");
            FramedOrientationPtr orientation = channelRef->get<FramedOrientation>("orientation");

            ARMARX_LOG << "pos " << *position;
            ARMARX_LOG << "orientation " << *orientation;
            FramedPosePtr tablePose = new FramedPose(position, orientation, position->frame, position->agent);
            Eigen::Vector3f platformPose;
            //            Eigen::Matrix3f platformOrientation = orientation->toEigen();
            platformPose(0) = position->x;
            platformPose(1) = position->y;
            platformPose(2) = getYawAngle(tablePose);
            //            ARMARX_INFO << "Pose vec: " << platformPose;
            //            float anglex = acos(std::min(std::max(platformOrientation(0,1),-1.0f),1.0f));
            //            if (anglex < M_PI_2)
            //                platformPose(2) = -platformPose(2);
            out.setTablePoseBase(tablePose);
            ARMARX_LOG << "Before emitting Event ";
            emitEvTableLocalized();
            break;
            ARMARX_LOG << "After emitting Event ";
        }

        ARMARX_LOG << "Before sleep ";
        TimeUtil::MSSleep(1000);
    }


}


float LocalizeTable::getYawAngle(const armarx::PoseBasePtr& pose) const
{
    Eigen::Vector3f rpy;
    armarx::PosePtr p = armarx::PosePtr::dynamicCast(pose);
    VirtualRobot::MathTools::eigen4f2rpy(p->toEigen(), rpy);
    return rpy[2];
}

void LocalizeTable::onExit()
{

    // put your user code for the exit point here
    // execution time should be short (<100ms)

}

// DO NOT EDIT NEXT FUNCTION
std::string LocalizeTable::GetName()
{
    return "LocalizeTable";
}

// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr LocalizeTable::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new LocalizeTable(stateData));
}

