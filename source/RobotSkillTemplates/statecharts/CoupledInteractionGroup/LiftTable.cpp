/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2014-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::LiftTableGroup
 * @author     [Author Name] ( [Author Email] )
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "LiftTable.h"
#include <RobotAPI/libraries/core/remoterobot/RemoteRobot.h>
#include <MemoryX/libraries/memorytypes/entity/ObjectInstance.h>

#include <MemoryX/libraries/motionmodels/MotionModelAttachedToOtherObject.h>

using namespace armarx;
using namespace CoupledInteractionGroup;

// DO NOT EDIT NEXT LINE
LiftTable::SubClassRegistry LiftTable::Registry(LiftTable::GetName(), &LiftTable::CreateInstance);



LiftTable::LiftTable(XMLStateConstructorParams stateData) :
    XMLStateTemplate < LiftTable > (stateData), LiftTableGeneratedBase < LiftTable > (stateData)
{
}

void LiftTable::onEnter()
{
    CoupledInteractionGroupStatechartContext* context = getContext<CoupledInteractionGroupStatechartContext>();
    context->getTCPControlUnit()->request();
    Eigen::Matrix4f leftTcpPoseBase = context->getRobot()->getRobotNode(in.getLeftHandName())->getPoseInRootFrame();
    Eigen::Matrix4f rightTcpPoseBase = context->getRobot()->getRobotNode(in.getRightHandName())->getPoseInRootFrame();
    ARMARX_INFO << "LeftHandName in Liftable " << in.getLeftHandName();
    ARMARX_INFO << "RightHandName in Liftable " << in.getRightHandName();
    ARMARX_INFO << "Lift Offset " << in.getLiftOffset();
    FramedPose leftTcpPose = FramedPose(leftTcpPoseBase, std::string("Armar3"), std::string("Armar3_Base"));
    FramedPose rightTcpPose = FramedPose(rightTcpPoseBase, std::string("Armar3"), std::string("Armar3_Base"));
    local.setInitialLeftTcpPose(leftTcpPose);
    local.setInitialRightTcpPose(rightTcpPose);
}

void LiftTable::run()
{
    // put your user code for the execution-phase here
    // runs in seperate thread, thus can do complex operations
    // should check constantly whether isRunningTaskStopped() returns true


}

void LiftTable::onBreak()
{
    // put your user code for the breaking point here
    // execution time should be short (<100ms)
}

void LiftTable::onExit()
{

    // put your user code for the exit point here
    // execution time should be short (<100ms)
    CoupledInteractionGroupStatechartContext* context = getContext<CoupledInteractionGroupStatechartContext>();
    context->getTCPControlUnit()->release();

    // we attach table to left hand
    // this code should go to a dedicated state, since it is also executed on falure!

    // set motion model AttachedToOtherObject
    //if (in.isObjectInstanceChannelSet())
    {
        // todo: get objetc name
        std::string objName = "lighttable";//in.getObjectInstanceChannel()->getDataField("className")->getString();
        ARMARX_VERBOSE << "Attch to hand... Object Name:" << objName << ", hand name:" << in.getLeftHandMemoryChannel()->getDataField("className")->getString();
        auto handInstances = context->getObjectMemoryObserverProxy()->getObjectInstancesByClass(in.getLeftHandMemoryChannel()->getDataField("className")->getString());

        ARMARX_CHECK_EXPRESSION(handInstances.size() != 0);
        ChannelRefBasePtr handChannel = handInstances.front();

        memoryx::MotionModelAttachedToOtherObjectPtr newMotionModel = new memoryx::MotionModelAttachedToOtherObject(context->getRobotStateComponent(), ChannelRefPtr::dynamicCast(handChannel));
        memoryx::ObjectInstancePtr object = memoryx::ObjectInstancePtr::dynamicCast(context->getWorkingMemoryProxy()->getObjectInstancesSegment()->getEntityByName(objName));

        context->getWorkingMemoryProxy()->getObjectInstancesSegment()->setNewMotionModel(object->getId(), newMotionModel);
        ARMARX_IMPORTANT << "Attached " << objName << " to " << ChannelRefPtr::dynamicCast(handChannel)->getDataField("className")->getString();
    }

}

// DO NOT EDIT NEXT FUNCTION
std::string LiftTable::GetName()
{
    return "LiftTable";
}

// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr LiftTable::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new LiftTable(stateData));
}

