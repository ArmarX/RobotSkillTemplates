/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2014-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::PlaceTableGroup
 * @author     [Author Name] ( [Author Email] )
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "PlaceTable.h"
#include <RobotAPI/libraries/core/remoterobot/RemoteRobot.h>


using namespace armarx;
using namespace CoupledInteractionGroup;

// DO NOT EDIT NEXT LINE
PlaceTable::SubClassRegistry PlaceTable::Registry(PlaceTable::GetName(), &PlaceTable::CreateInstance);



PlaceTable::PlaceTable(XMLStateConstructorParams stateData) :
    XMLStateTemplate < PlaceTable > (stateData), PlaceTableGeneratedBase < PlaceTable > (stateData)
{
}

void PlaceTable::onEnter()
{
    CoupledInteractionGroupStatechartContext* context = getContext<CoupledInteractionGroupStatechartContext>();
    context->getTCPControlUnit()->request();

    // put your user code for the enter-point here
    // execution time should be short (<100ms)
    //std::string leftTcpName("Wrist 2 L");
    //std::string rightTcpName("Wrist 2 R");
    std::string leftTcpName = in.getLeftHandName();
    std::string rightTcpName = in.getRightHandName();
    //leftTcpName = std::string("Wrist 2 L");
    //rightTcpName = std::string("Wrist 2 R");
    //else
    //    duration = IceUtil::Time::milliSecondsDouble(0);

    DatafieldRefPtr forceRefLeft = DatafieldRefPtr::dynamicCast(context->getForceTorqueUnitObserver()->getForceDatafield(leftTcpName));
    DatafieldRefPtr torqueRefLeft = DatafieldRefPtr::dynamicCast(context->getForceTorqueUnitObserver()->getTorqueDatafield(leftTcpName));
    DatafieldRefPtr forceRefRight = DatafieldRefPtr::dynamicCast(context->getForceTorqueUnitObserver()->getForceDatafield(rightTcpName));
    DatafieldRefPtr torqueRefRight = DatafieldRefPtr::dynamicCast(context->getForceTorqueUnitObserver()->getTorqueDatafield(rightTcpName));
    forceRefLeft = DatafieldRefPtr::dynamicCast(context->getForceTorqueUnitObserver()->createNulledDatafield(forceRefLeft));
    forceRefRight = DatafieldRefPtr::dynamicCast(context->getForceTorqueUnitObserver()->createNulledDatafield(forceRefRight));
    torqueRefLeft = DatafieldRefPtr::dynamicCast(context->getForceTorqueUnitObserver()->createNulledDatafield(torqueRefLeft));
    torqueRefRight = DatafieldRefPtr::dynamicCast(context->getForceTorqueUnitObserver()->createNulledDatafield(torqueRefRight));

    FramedDirectionPtr curForceLeft = forceRefLeft->getDataField()->get<FramedDirection>();
    curForceLeft = FramedDirection::ChangeFrame(context->getRobot(), *curForceLeft, "Armar3_Base");
    FramedDirectionPtr curForceRight = forceRefRight->getDataField()->get<FramedDirection>();
    curForceRight = FramedDirection::ChangeFrame(context->getRobot(), *curForceRight, "Armar3_Base");
    FramedDirectionPtr curTorqueLeft = torqueRefLeft->getDataField()->get<FramedDirection>();
    curTorqueLeft = FramedDirection::ChangeFrame(context->getRobot(), *curTorqueLeft, "Armar3_Base");
    FramedDirectionPtr curTorqueRight = torqueRefRight->getDataField()->get<FramedDirection>();
    curTorqueRight = FramedDirection::ChangeFrame(context->getRobot(), *curTorqueRight, "Armar3_Base");
    Eigen::Vector3f zeroVelVec;
    zeroVelVec << 0.0, 0.0, 0.0;
    FramedDirectionPtr curVelLeft = new FramedDirection(zeroVelVec, "Armar3_Base", "Armar3");
    FramedDirectionPtr curVelRight = new FramedDirection(zeroVelVec, "Armar3_Base", "Armar3");
    FramedDirectionPtr curVelOriLeft = new FramedDirection(zeroVelVec, "Armar3_Base", "Armar3");
    FramedDirectionPtr curVelOriRight = new FramedDirection(zeroVelVec, "Armar3_Base", "Armar3");

    Eigen::Matrix4f leftTcpPoseBase = context->getRobot()->getRobotNode(in.getLeftHandName())->getPoseInRootFrame();
    Eigen::Matrix4f rightTcpPoseBase = context->getRobot()->getRobotNode(in.getRightHandName())->getPoseInRootFrame();

    Eigen::Vector3f leftTcpPos;
    leftTcpPos << leftTcpPoseBase(0, 3), leftTcpPoseBase(1, 3), leftTcpPoseBase(2, 3);
    Eigen::Vector3f rightTcpPos;
    rightTcpPos << rightTcpPoseBase(0, 3), rightTcpPoseBase(1, 3), rightTcpPoseBase(2, 3);

    local.setCurrentRightHandPosition(FramedPosition(rightTcpPos, "Armar3_Base", "Armar3"));
    local.setCurrentLeftHandPosition(FramedPosition(leftTcpPos, "Armar3_Base", "Armar3"));
    float velInMillimeterSecond = 20.0;
    //Eigen::Vector3f curTorqueRightVec = curTorqueRight->toEigen();
    //Eigen::Vector3f curTorqueLeftVec = curTorqueLeft->toEigen();
    local.setCurrentLeftHandForces(curForceLeft);
    local.setCurrentRightHandForces(curForceRight);
    local.setCurrentRightHandTorques(curTorqueRight);
    local.setCurrentLeftHandTorques(curTorqueLeft);
    local.setVelocityInMillimetersSecond(velInMillimeterSecond);
    local.setCurrentLeftTcpOriVelocity(curVelOriLeft);
    local.setCurrentRightTcpOriVelocity(curVelOriRight);
    local.setCurrentLeftTcpPosVelocity(curVelLeft);
    local.setCurrentRightTcpPosVelocity(curVelRight);
    local.setLeftHandStopped(false);
    local.setRightHandStopped(false);
    local.setLeftHandChecked(false);
    local.setRightHandChecked(false);
    //Set initial forces an velocities
}

void PlaceTable::run()
{
    // put your user code for the execution-phase here
    // runs in seperate thread, thus can do complex operations
    // should check constantly whether isRunningTaskStopped() returns true

}

void PlaceTable::onBreak()
{
    // put your user code for the breaking point here
    // execution time should be short (<100ms)
}

void PlaceTable::onExit()
{
    CoupledInteractionGroupStatechartContext* context = getContext<CoupledInteractionGroupStatechartContext>();
    context->getTCPControlUnit()->release();
    //std::string leftTcpName("Wrist 2 L");
    //std::string rightTcpName("Wrist 2 R");
    std::string leftTcpName = in.getLeftHandName();
    std::string rightTcpName = in.getRightHandName();
    DatafieldRefPtr forceRefLeft = DatafieldRefPtr::dynamicCast(context->getForceTorqueUnitObserver()->getForceDatafield(leftTcpName));
    DatafieldRefPtr torqueRefLeft = DatafieldRefPtr::dynamicCast(context->getForceTorqueUnitObserver()->getTorqueDatafield(leftTcpName));
    DatafieldRefPtr forceRefRight = DatafieldRefPtr::dynamicCast(context->getForceTorqueUnitObserver()->getForceDatafield(rightTcpName));
    DatafieldRefPtr torqueRefRight = DatafieldRefPtr::dynamicCast(context->getForceTorqueUnitObserver()->getTorqueDatafield(rightTcpName));
    context->getForceTorqueUnitObserver()->removeFilteredDatafield(forceRefLeft);
    context->getForceTorqueUnitObserver()->removeFilteredDatafield(forceRefRight);
    context->getForceTorqueUnitObserver()->removeFilteredDatafield(torqueRefLeft);
    context->getForceTorqueUnitObserver()->removeFilteredDatafield(torqueRefRight);

}

// DO NOT EDIT NEXT FUNCTION
std::string PlaceTable::GetName()
{
    return "PlaceTable";
}

// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr PlaceTable::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new PlaceTable(stateData));
}

