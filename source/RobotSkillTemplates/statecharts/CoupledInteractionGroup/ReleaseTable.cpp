/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2014-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::ReleaseTableGroup
 * @author     [Author Name] ( [Author Email] )
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "ReleaseTable.h"
#include <RobotAPI/libraries/core/remoterobot/RemoteRobot.h>
#include <MemoryX/libraries/motionmodels/MotionModelStaticObject.h>
#include <MemoryX/libraries/memorytypes/entity/ObjectInstance.h>

using namespace armarx;
using namespace CoupledInteractionGroup;

// DO NOT EDIT NEXT LINE
ReleaseTable::SubClassRegistry ReleaseTable::Registry(ReleaseTable::GetName(), &ReleaseTable::CreateInstance);



ReleaseTable::ReleaseTable(XMLStateConstructorParams stateData) :
    XMLStateTemplate < ReleaseTable > (stateData), ReleaseTableGeneratedBase < ReleaseTable > (stateData)
{
}

void ReleaseTable::onEnter()
{
    //CoupledInteractionGroupStatechartContext* context = getContext<CoupledInteractionGroupStatechartContext>();
    float goBackDistance = in.getGoBackDistance();
    Eigen::Vector3f targetPoseRelativeVec;
    targetPoseRelativeVec(0) = 0.0;
    targetPoseRelativeVec(1) = -goBackDistance;
    targetPoseRelativeVec(2) = 0.0;
    std::vector<std::string> jointNames;
    jointNames.push_back("Shoulder 2 L"); // -> Pitch
    jointNames.push_back("Shoulder 2 R"); // -> Pitch
    jointNames.push_back("Upperarm L"); // -> Pitch
    jointNames.push_back("Upperarm R"); // -> Pitch
    jointNames.push_back("Elbow L"); // -> Pitch
    jointNames.push_back("Elbow R"); // -> Pitch
    local.setGoBackJointNames(jointNames);


    std::vector<float> targetJointValues;
    targetJointValues.push_back(0.5);
    targetJointValues.push_back(0.5);
    targetJointValues.push_back(0.0);
    targetJointValues.push_back(0.0);
    targetJointValues.push_back(-0.5);
    targetJointValues.push_back(-0.5);
    local.setGoBackTargetJointValues(targetJointValues);
    local.setPlatformTargetPose(Vector3(targetPoseRelativeVec));
}


void ReleaseTable::onExit()
{
    CoupledInteractionGroupStatechartContext* context = getContext<CoupledInteractionGroupStatechartContext>();

    // detach table from hand
    //  is always executed, even if release failed (but that should be ok)

    // todo: get object class name from input
    std::string objClassName = "lighttable"; // in.getObjectClassName();
    auto instances = context->getWorkingMemoryProxy()->getObjectInstancesSegment()->getObjectInstancesByClass(objClassName);

    if (instances.size() > 0)
    {
        memoryx::ObjectInstanceBasePtr obj = instances.front();
        ARMARX_CHECK_EXPRESSION(obj);
        memoryx::ObjectInstancePtr obj2 = memoryx::ObjectInstancePtr::dynamicCast(obj);
        FramedPosePtr pose = obj2->getPose();
        ARMARX_VERBOSE << "Detaching " << objClassName << " from hand. Setting pose to:\n" << pose->output();

        memoryx::MotionModelStaticObjectPtr newMotionModel = new memoryx::MotionModelStaticObject(context->getRobotStateComponent());
        context->getWorkingMemoryProxy()->getObjectInstancesSegment()->setObjectPose(obj->getId(),
                new LinkedPose(*pose, context->getRobotStateComponent()->getRobotSnapshot(context->getRobot()->getName())));
        context->getWorkingMemoryProxy()->getObjectInstancesSegment()->setNewMotionModel(obj->getId(), newMotionModel);
    }
    else
    {
        ARMARX_WARNING << "No object instance with name '" << objClassName << "' found! Could not change working memory position and motion model of object.";
    }

}

// DO NOT EDIT NEXT FUNCTION
std::string ReleaseTable::GetName()
{
    return "ReleaseTable";
}

// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr ReleaseTable::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new ReleaseTable(stateData));
}

