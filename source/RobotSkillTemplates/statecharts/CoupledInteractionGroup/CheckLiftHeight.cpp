/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2014-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::CheckLiftHeightGroup
 * @author     [Author Name] ( [Author Email] )
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "CheckLiftHeight.h"
#include <RobotAPI/libraries/core/remoterobot/RemoteRobot.h>


using namespace armarx;
using namespace CoupledInteractionGroup;

// DO NOT EDIT NEXT LINE
CheckLiftHeight::SubClassRegistry CheckLiftHeight::Registry(CheckLiftHeight::GetName(), &CheckLiftHeight::CreateInstance);



CheckLiftHeight::CheckLiftHeight(XMLStateConstructorParams stateData) :
    XMLStateTemplate < CheckLiftHeight > (stateData), CheckLiftHeightGeneratedBase < CheckLiftHeight > (stateData)
{
}

void CheckLiftHeight::onEnter()
{
    CoupledInteractionGroupStatechartContext* context = getContext<CoupledInteractionGroupStatechartContext>();
    float liftOffset = in.getLiftOffset();
    Eigen::Matrix4f leftTcpPoseBase = context->getRobot()->getRobotNode(in.getLeftHandName())->getPoseInRootFrame();
    Eigen::Matrix4f rightTcpPoseBase = context->getRobot()->getRobotNode(in.getRightHandName())->getPoseInRootFrame();
    FramedPosePtr initialLeftTcpPoseBase = in.getInitialLeftTcpPose();
    FramedPosePtr initialRightTcpPoseBase = in.getInitialRightTcpPose();
    //leftTcpPoseBase->toEigen();
    Eigen::Matrix4f initLeftTcpPoseBase = initialLeftTcpPoseBase->toEigen();
    Eigen::Matrix4f initRightTcpPoseBase = initialRightTcpPoseBase->toEigen();
    Eigen::Vector3f leftOrientationVel;
    leftOrientationVel(0) = 0.0;
    leftOrientationVel(1) = 0.0;
    leftOrientationVel(2) = 0.0;
    Eigen::Vector3f rightOrientationVel;
    rightOrientationVel(0) = 0.0;
    rightOrientationVel(1) = 0.0;
    rightOrientationVel(2) = 0.0;
    Eigen::Vector3f leftPositionVel;
    leftPositionVel(0) = 0.0;
    leftPositionVel(1) = 0.0;
    leftPositionVel(2) = liftOffset + initLeftTcpPoseBase(2, 3) - leftTcpPoseBase(2, 3);
    ARMARX_INFO << "in check lift height init and current" << initLeftTcpPoseBase(2, 3) << " " << leftTcpPoseBase(2, 3);
    Eigen::Vector3f rightPositionVel;
    rightPositionVel(0) = 0.0;
    rightPositionVel(1) = 0.0;
    rightPositionVel(2) = liftOffset + initRightTcpPoseBase(2, 3) - rightTcpPoseBase(2, 3);

    if (fabs(rightPositionVel(2)) < 30.0 || fabs(leftPositionVel(2)) < 30.0)
    {
        leftPositionVel(2) = 0;
        rightPositionVel(2) = 0;
        out.setCurrentLeftOrientationVel(FramedDirection(leftOrientationVel, "Armar3_Base", "Armar3"));
        out.setCurrentRightOrientationVel(FramedDirection(rightOrientationVel, "Armar3_Base", "Armar3"));
        out.setCurrentLeftPositionVel(FramedDirection(leftPositionVel, "Armar3_Base", "Armar3"));
        out.setCurrentRightPositionVel(FramedDirection(rightPositionVel, "Armar3_Base", "Armar3"));
        emitEvLiftHeightReached();
    }
    else
    {
        out.setCurrentLeftOrientationVel(FramedDirection(leftOrientationVel, "Armar3_Base", "Armar3"));
        out.setCurrentRightOrientationVel(FramedDirection(rightOrientationVel, "Armar3_Base", "Armar3"));
        out.setCurrentLeftPositionVel(FramedDirection(leftPositionVel, "Armar3_Base", "Armar3"));
        out.setCurrentRightPositionVel(FramedDirection(rightPositionVel, "Armar3_Base", "Armar3"));
        emitEvLiftHeightNotReached();
    }
}

void CheckLiftHeight::run()
{
    // put your user code for the execution-phase here
    // runs in seperate thread, thus can do complex operations
    // should check constantly whether isRunningTaskStopped() returns true

}

void CheckLiftHeight::onBreak()
{
    // put your user code for the breaking point here
    // execution time should be short (<100ms)
}

void CheckLiftHeight::onExit()
{

    // put your user code for the exit point here
    // execution time should be short (<100ms)

}

// DO NOT EDIT NEXT FUNCTION
std::string CheckLiftHeight::GetName()
{
    return "CheckLiftHeight";
}

// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr CheckLiftHeight::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new CheckLiftHeight(stateData));
}

