/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2014-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::MoveTableImpedanceControlGroup
 * @author     [Author Name] ( [Author Email] )
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "MoveTableImpedanceControl.h"
#include <RobotAPI/libraries/core/remoterobot/RemoteRobot.h>


using namespace armarx;
using namespace CoupledInteractionGroup;

// DO NOT EDIT NEXT LINE
MoveTableImpedanceControl::SubClassRegistry MoveTableImpedanceControl::Registry(MoveTableImpedanceControl::GetName(), &MoveTableImpedanceControl::CreateInstance);



MoveTableImpedanceControl::MoveTableImpedanceControl(XMLStateConstructorParams stateData) :
    XMLStateTemplate < MoveTableImpedanceControl > (stateData), MoveTableImpedanceControlGeneratedBase < MoveTableImpedanceControl > (stateData)
{
}

void MoveTableImpedanceControl::onEnter()
{
    CoupledInteractionGroupStatechartContext* context = getContext<CoupledInteractionGroupStatechartContext>();


    Eigen::Vector3f zeroVelVec;
    zeroVelVec << 0.0, 0.0, 0.0;
    std::string leftTcpName = in.getLeftHandName();
    std::string rightTcpName = in.getRightHandName();
    //leftTcpName = std::string("Wrist 2 L");
    //rightTcpName = std::string("Wrist 2 R");
    //std::string leftTcpName("Wrist 2 L");
    //std::string rightTcpName("Wrist 2 R");
    DatafieldRefPtr forceRefLeft = DatafieldRefPtr::dynamicCast(context->getForceTorqueUnitObserver()->getForceDatafield(leftTcpName));
    DatafieldRefPtr torqueRefLeft = DatafieldRefPtr::dynamicCast(context->getForceTorqueUnitObserver()->getTorqueDatafield(leftTcpName));
    DatafieldRefPtr forceRefRight = DatafieldRefPtr::dynamicCast(context->getForceTorqueUnitObserver()->getForceDatafield(rightTcpName));
    DatafieldRefPtr torqueRefRight = DatafieldRefPtr::dynamicCast(context->getForceTorqueUnitObserver()->getTorqueDatafield(rightTcpName));
    forceRefLeft = DatafieldRefPtr::dynamicCast(context->getForceTorqueUnitObserver()->createNulledDatafield(forceRefLeft));
    forceRefRight = DatafieldRefPtr::dynamicCast(context->getForceTorqueUnitObserver()->createNulledDatafield(forceRefRight));
    torqueRefLeft = DatafieldRefPtr::dynamicCast(context->getForceTorqueUnitObserver()->createNulledDatafield(torqueRefLeft));
    torqueRefRight = DatafieldRefPtr::dynamicCast(context->getForceTorqueUnitObserver()->createNulledDatafield(torqueRefRight));
    FramedDirectionPtr curForceLeft = forceRefLeft->getDataField()->get<FramedDirection>();
    curForceLeft = FramedDirection::ChangeFrame(context->getRobot(), *curForceLeft, "Armar3_Base");
    FramedDirectionPtr curForceRight = forceRefRight->getDataField()->get<FramedDirection>();
    curForceRight = FramedDirection::ChangeFrame(context->getRobot(), *curForceRight, "Armar3_Base");
    FramedDirectionPtr curTorqueLeft = torqueRefLeft->getDataField()->get<FramedDirection>();
    curTorqueLeft = FramedDirection::ChangeFrame(context->getRobot(), *curTorqueLeft, "Armar3_Base");
    FramedDirectionPtr curTorqueRight = torqueRefRight->getDataField()->get<FramedDirection>();
    curTorqueRight = FramedDirection::ChangeFrame(context->getRobot(), *curTorqueRight, "Armar3_Base");
    Eigen::Matrix4f leftTcpPoseBase = context->getRobot()->getRobotNode(in.getLeftHandName())->getPoseInRootFrame();
    Eigen::Matrix4f rightTcpPoseBase = context->getRobot()->getRobotNode(in.getRightHandName())->getPoseInRootFrame();

    Eigen::Vector3f desiredLeftRPY;
    VirtualRobot::MathTools::eigen4f2rpy(leftTcpPoseBase, desiredLeftRPY);
    Eigen::Vector3f desiredRightRPY;
    VirtualRobot::MathTools::eigen4f2rpy(rightTcpPoseBase, desiredRightRPY);
    local.setDesiredLeftTcpPose(FramedPose(leftTcpPoseBase, "Armar3_Base", "Armar3"));
    local.setDesiredRightTcpPose(FramedPose(rightTcpPoseBase, "Armar3_Base", "Armar3"));
    local.setDesiredLeftTcpOrientation(FramedDirection(desiredLeftRPY, "Armar3_Base", "Armar3"));
    local.setDesiredRightTcpOrientation(FramedDirection(desiredRightRPY, "Armar3_Base", "Armar3"));
    local.setCurrentLeftTcpOriVelocity(FramedDirection(zeroVelVec, "Armar3_Base", "Armar3"));
    local.setCurrentLeftTcpPosVelocity(FramedDirection(zeroVelVec, "Armar3_Base", "Armar3"));
    local.setCurrentRightTcpOriVelocity(FramedDirection(zeroVelVec, "Armar3_Base", "Armar3"));
    local.setCurrentRightTcpPosVelocity(FramedDirection(zeroVelVec, "Armar3_Base", "Armar3"));
    local.setCurrentPlatformVelocity(Vector3(zeroVelVec));
    local.setTimestamp(TimestampVariant::nowPtr());
    local.setinitialForcesReset(true);
    local.setcounter(0);
    int timeout = in.getTimeout();

    if (timeout > 0)
    {
        setTimeoutEvent(timeout, createEventTimeout());
    }

}



void MoveTableImpedanceControl::onExit()
{
    CoupledInteractionGroupStatechartContext* context = getContext<CoupledInteractionGroupStatechartContext>();
    //std::string leftTcpName("Wrist 2 L");
    //std::string rightTcpName("Wrist 2 R");
    std::string leftTcpName = in.getLeftHandName();
    std::string rightTcpName = in.getRightHandName();
    DatafieldRefPtr forceRefLeft = DatafieldRefPtr::dynamicCast(context->getForceTorqueUnitObserver()->getForceDatafield(leftTcpName));
    DatafieldRefPtr torqueRefLeft = DatafieldRefPtr::dynamicCast(context->getForceTorqueUnitObserver()->getTorqueDatafield(leftTcpName));
    DatafieldRefPtr forceRefRight = DatafieldRefPtr::dynamicCast(context->getForceTorqueUnitObserver()->getForceDatafield(rightTcpName));
    DatafieldRefPtr torqueRefRight = DatafieldRefPtr::dynamicCast(context->getForceTorqueUnitObserver()->getTorqueDatafield(rightTcpName));
    context->getForceTorqueUnitObserver()->removeFilteredDatafield(forceRefLeft);
    context->getForceTorqueUnitObserver()->removeFilteredDatafield(forceRefRight);
    context->getForceTorqueUnitObserver()->removeFilteredDatafield(torqueRefLeft);
    context->getForceTorqueUnitObserver()->removeFilteredDatafield(torqueRefRight);

    // put your user code for the exit point here
    // execution time should be short (<100ms)

}

// DO NOT EDIT NEXT FUNCTION
std::string MoveTableImpedanceControl::GetName()
{
    return "MoveTableImpedanceControl";
}

// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr MoveTableImpedanceControl::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new MoveTableImpedanceControl(stateData));
}

