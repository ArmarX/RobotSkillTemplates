/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2014-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::CalculateForceControlRobotPoseGroup
 * @author     [Author Name] ( [Author Email] )
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "CalculateForceControlRobotPose.h"
#include <RobotAPI/libraries/core/remoterobot/RemoteRobot.h>
#include <ArmarXCore/core/time/TimeUtil.h>


using namespace armarx;
using namespace CoupledInteractionGroup;

// DO NOT EDIT NEXT LINE
CalculateForceControlRobotPose::SubClassRegistry CalculateForceControlRobotPose::Registry(CalculateForceControlRobotPose::GetName(), &CalculateForceControlRobotPose::CreateInstance);



CalculateForceControlRobotPose::CalculateForceControlRobotPose(XMLStateConstructorParams stateData) :
    XMLStateTemplate < CalculateForceControlRobotPose > (stateData), CalculateForceControlRobotPoseGeneratedBase < CalculateForceControlRobotPose > (stateData)
{

}

void CalculateForceControlRobotPose::onEnter()
{

    CoupledInteractionGroupStatechartContext* context = getContext<CoupledInteractionGroupStatechartContext>();
    int kernelSize = 50;
    std::vector<Vector3Ptr> platformVelMeanFilter;

    if (local.isPlatformVelMeanFilterSet())
    {
        platformVelMeanFilter = local.getPlatformVelMeanFilter();
    }

    // put your user code for the enter-point here
    // execution time should be short (<100ms)

    std::string leftTcpName = in.getLeftHandName();
    std::string rightTcpName = in.getRightHandName();
    //leftTcpName = std::string("Wrist 2 L");
    //rightTcpName = std::string("Wrist 2 R");
    auto now = TimeUtil::GetTime();
    IceUtil::Time duration;
    duration = now - in.getCurrentTimestamp()->toTime();
    int counter = in.getcounter();
    counter++;
    //else
    FramedPosePtr desiredLeftTcpPose = in.getDesiredLeftTcpPose();
    FramedPosePtr desiredRightTcpPose = in.getDesiredRightTcpPose();
    float leftForceAmplifier = 2.0;
    float rightForceAmplifier = 1.2;
    float forceThreshold = in.getForceThreshhold();
    float desiredDistance = (desiredLeftTcpPose->toEigen() - desiredRightTcpPose->toEigen()).norm();
    float accGain = 120.0;
    float decGain = 1.0;
    float maxVelocity = 50.0;
    float torqueThreshold = in.getTorqueThreshhold();
    float oriAccGain = 4.0;
    float oriDccGain = 1.0;
    float maxOriAcc = 1.0;
    float maxOriVelocity = 0.1;
    float stiffness = 0.5 * accGain;
    float damping = 1.0 / (2.0 * sqrt(stiffness));

    if (torqueThreshold == 0.0)
    {
        oriAccGain = 0.0;
    }

    if (forceThreshold == 0.0)
    {
        accGain = 0.0;
    }

    //    duration = IceUtil::Time::milliSecondsDouble(0);

    Eigen::Matrix4f leftTcpPoseBase = context->getRobot()->getRobotNode(in.getLeftHandName())->getPoseInRootFrame();
    Eigen::Matrix4f rightTcpPoseBase = context->getRobot()->getRobotNode(in.getRightHandName())->getPoseInRootFrame();

    Eigen::Vector3f currentLeftRPY;
    VirtualRobot::MathTools::eigen4f2rpy(leftTcpPoseBase, currentLeftRPY);
    Eigen::Vector3f currentRightRPY;
    VirtualRobot::MathTools::eigen4f2rpy(rightTcpPoseBase, currentRightRPY);



    Eigen::Matrix4f deltaLeftTcpPose = desiredLeftTcpPose->toEigen() * leftTcpPoseBase.inverse();
    Eigen::Matrix4f deltaRightTcpPose = desiredRightTcpPose->toEigen() * rightTcpPoseBase.inverse();

    Eigen::Vector3f deltaLeftRPY;
    VirtualRobot::MathTools::eigen4f2rpy(deltaLeftTcpPose, deltaLeftRPY);
    Eigen::Vector3f deltaRightRPY;
    VirtualRobot::MathTools::eigen4f2rpy(deltaRightTcpPose, deltaRightRPY);

    DatafieldRefPtr forceRefLeft = DatafieldRefPtr::dynamicCast(context->getForceTorqueUnitObserver()->getForceDatafield(leftTcpName));
    DatafieldRefPtr torqueRefLeft = DatafieldRefPtr::dynamicCast(context->getForceTorqueUnitObserver()->getTorqueDatafield(leftTcpName));
    DatafieldRefPtr forceRefRight = DatafieldRefPtr::dynamicCast(context->getForceTorqueUnitObserver()->getForceDatafield(rightTcpName));
    DatafieldRefPtr torqueRefRight = DatafieldRefPtr::dynamicCast(context->getForceTorqueUnitObserver()->getTorqueDatafield(rightTcpName));
    /*forceRefLeft = DatafieldRefPtr::dynamicCast(context->getForceTorqueUnitObserver()->createNulledDatafield(forceRefLeft));
    forceRefRight = DatafieldRefPtr::dynamicCast(context->getForceTorqueUnitObserver()->createNulledDatafield(forceRefRight));
    torqueRefLeft = DatafieldRefPtr::dynamicCast(context->getForceTorqueUnitObserver()->createNulledDatafield(torqueRefLeft));
    torqueRefRight = DatafieldRefPtr::dynamicCast(context->getForceTorqueUnitObserver()->createNulledDatafield(torqueRefRight));*/
    FramedDirectionPtr curForceLeft = forceRefLeft->getDataField()->get<FramedDirection>();
    curForceLeft = FramedDirection::ChangeFrame(context->getRobot(), *curForceLeft, "Armar3_Base");
    FramedDirectionPtr curForceRight = forceRefRight->getDataField()->get<FramedDirection>();
    curForceRight = FramedDirection::ChangeFrame(context->getRobot(), *curForceRight, "Armar3_Base");
    FramedDirectionPtr curTorqueLeft = torqueRefLeft->getDataField()->get<FramedDirection>();
    curTorqueLeft = FramedDirection::ChangeFrame(context->getRobot(), *curTorqueLeft, "Armar3_Base");
    FramedDirectionPtr curTorqueRight = torqueRefRight->getDataField()->get<FramedDirection>();
    curTorqueRight = FramedDirection::ChangeFrame(context->getRobot(), *curTorqueRight, "Armar3_Base");
    FramedDirectionPtr curVelLeft = in.getCurrentLeftTcpPositionVelocity();
    curVelLeft = FramedDirection::ChangeFrame(context->getRobot(), *curVelLeft, "Armar3_Base");
    FramedDirectionPtr curVelRight = in.getCurrentRightTcpPositionVelocity();
    curVelRight = FramedDirection::ChangeFrame(context->getRobot(), *curVelRight, "Armar3_Base");
    FramedDirectionPtr curVelOriLeft = in.getCurrentLeftTcpPositionVelocity();
    curVelOriLeft = FramedDirection::ChangeFrame(context->getRobot(), *curVelOriLeft, "Armar3_Base");
    FramedDirectionPtr curVelOriRight = in.getCurrentRightTcpPositionVelocity();
    curVelOriRight = FramedDirection::ChangeFrame(context->getRobot(), *curVelOriRight, "Armar3_Base");

    ARMARX_INFO << "Measuring forces in frame: " << curForceLeft->frame;
    Eigen::Vector3f newVelLeft(3);
    Eigen::Vector3f newAccLeft(3);
    Eigen::Vector3f newVelRight(3);
    Eigen::Vector3f newAccRight(3);
    Eigen::Vector3f curForceRightVec = curForceRight->toEigen();
    Eigen::Vector3f curForceLeftVec = curForceLeft->toEigen();
    Eigen::Vector3f curTorqueRightVec = curTorqueRight->toEigen();
    Eigen::Vector3f curTorqueLeftVec = curTorqueLeft->toEigen();
    ARMARX_INFO << "current Force right hand " << curForceRightVec(0) << " "  << curForceRightVec(1) << " "  << curForceRightVec(2);

    curForceRightVec = curForceRightVec * rightForceAmplifier;
    curForceLeftVec = curForceLeftVec * leftForceAmplifier;

    ARMARX_INFO << "current Force right hand wo init " << curForceRightVec(0) << " "  << curForceRightVec(1) << " "  << curForceRightVec(2);
    ARMARX_INFO << "current Force left hand wo init " << curForceLeftVec(0) << " "  << curForceLeftVec(1) << " "  << curForceLeftVec(2);
    /*if (counter > 300)
    {
        curForceLeftVec(1) = 0.0;
        curForceLeftVec(0) = 0.0;
        curForceRightVec(1) = 0.0;
        curForceRightVec(0) = 0.0;
    }
    else
    {
        curForceLeftVec(1) += 20.0;
        curForceLeftVec(0) += 20.0;
        //curForceRightVec(1) += 20.0;current Force right hand wo init 10.3537 7.50225 1.28756
    current Force right hand wo init -3.64839 7.4992 4.22474
        //curForceRightVec(0) += 20.0;
    }*/

    out.setcounter(counter);
    //Eigen::Vector3f curTorqueRightVec = curTorqueRight->toEigen();
    //Eigen::Vector3f curTorqueLeftVec = curTorqueLeft->toEigen();
    curForceRightVec(1) = 2 * curForceRightVec(1);
    curForceLeftVec(1) = 2 * curForceLeftVec(1);

    curForceRightVec(2) = 0.0;//2*curForceRightVec(2);
    curForceLeftVec(2) = 0.0;//2*curForceLeftVec(2);
    Eigen::Vector3f leftToRightVec;
    leftToRightVec(0) = rightTcpPoseBase(0, 3) - leftTcpPoseBase(0, 3);
    leftToRightVec(1) = rightTcpPoseBase(1, 3) - leftTcpPoseBase(1, 3);
    leftToRightVec(2) = rightTcpPoseBase(2, 3) - leftTcpPoseBase(2, 3);
    Eigen::Vector3f forceLeftToRightVec;
    forceLeftToRightVec(0) = 2.0 * forceThreshold + curForceRightVec(0) - curForceLeftVec(0);
    forceLeftToRightVec(1) = curForceRightVec(1) - curForceLeftVec(1);
    forceLeftToRightVec(2) = 0.0;//curForceRightVec(2)-curForceLeftVec(2);
    Eigen::Vector3f meanForceVec = 0.5 * (curForceLeftVec + curForceRightVec);
    Eigen::Vector3f platformAccVec;
    Eigen::Vector3f curPlatformVel = in.getCurrentPlatformVelocity()->toEigen();
    bool useCurrentVel = false;
    Eigen::Vector3f meanForceVec2 = meanForceVec;
    meanForceVec2(2) = 0;

    if (meanForceVec2.norm() < forceThreshold)// || forceLeftToRightVec.norm() < 2.1*forceThreshold)
    {
        //if (platformAccVec.norm() > 0)
        //   newAccLeft = -decGain*curVelLeft->toEigen().normalized()  + centerForceLeftVec;

        if (curPlatformVel.norm() > 0)
        {
            platformAccVec(0) = -0.5 * curPlatformVel(0) / (duration.toMilliSecondsDouble() * 0.001);
            platformAccVec(1) = -0.5 * curPlatformVel(1) / (duration.toMilliSecondsDouble() * 0.001);
            platformAccVec(2) = -0.5 * curPlatformVel(2) / (duration.toMilliSecondsDouble() * 0.001);
            useCurrentVel = true;
        }
        else
        {
            platformAccVec(0) = 0.0;
            platformAccVec(1) = 0.0;
            platformAccVec(2) = 0.0;

        }

        if (!in.getinitialForcesReset())
        {
            /*forceRefLeft = DatafieldRefPtr::dynamicCast(context->getForceTorqueUnitObserver()->createNulledDatafield(forceRefLeft));
            forceRefRight = DatafieldRefPtr::dynamicCast(context->getForceTorqueUnitObserver()->createNulledDatafield(forceRefRight));
            torqueRefLeft = DatafieldRefPtr::dynamicCast(context->getForceTorqueUnitObserver()->createNulledDatafield(torqueRefLeft));
            torqueRefRight = DatafieldRefPtr::dynamicCast(context->getForceTorqueUnitObserver()->createNulledDatafield(torqueRefRight));*/
        }

        out.setinitialForcesReset(true);

    }
    else
    {

        ARMARX_INFO << "forceLeftToRightVec " << forceThreshold << " " << forceLeftToRightVec(0) << " "  << forceLeftToRightVec(1) << " "  << forceLeftToRightVec(2);
        float xangle = std::min(std::max(forceLeftToRightVec(0) / std::sqrt(forceLeftToRightVec(0) * forceLeftToRightVec(0) + forceLeftToRightVec(1) * forceLeftToRightVec(1)), -1.f), 1.f);
        float yangle = std::min(std::max(forceLeftToRightVec(1) / std::sqrt(forceLeftToRightVec(0) * forceLeftToRightVec(0) + forceLeftToRightVec(1) * forceLeftToRightVec(1)), -1.f), 1.f);
        float angle = acos(xangle);

        if (acos(yangle) > 0.5 * M_PI)
        {
            angle = -angle;
        }

        float forceAccGain = 1.0; //TODO make dependent on timestep
        ARMARX_INFO << "Platform rotating around " <<  angle;
        platformAccVec(0) = meanForceVec(0) * forceAccGain * accGain;
        platformAccVec(1) = meanForceVec(1) * forceAccGain * accGain;
        platformAccVec(2) = angle * forceAccGain * oriAccGain;

        out.setinitialForcesReset(false);
    }

    Eigen::Vector3f platformVelVec = platformAccVec * duration.toMilliSecondsDouble() * 0.001;

    if (useCurrentVel)
    {
        platformVelVec += curPlatformVel;
    }

    armarx::Vector3 platformVelocity(platformVelVec);

    float velForceGain = 1.0; //TODO make dependent on timestep
    //
    curForceLeftVec = curForceLeftVec - velForceGain * meanForceVec;
    curForceRightVec = curForceRightVec - velForceGain * meanForceVec;
    //TODO implement arm center Force here
    //Spring like system

    stiffness = 1.5 * accGain;
    damping = (2.0 * sqrt(stiffness));
    Eigen::Vector3f centerForceLeftVec;
    centerForceLeftVec(0) = stiffness * ((desiredLeftTcpPose->toEigen())(0, 3) - leftTcpPoseBase(0, 3)) - damping * curVelLeft->x;
    centerForceLeftVec(1) = stiffness * ((desiredLeftTcpPose->toEigen())(1, 3) - leftTcpPoseBase(1, 3)) - damping * curVelLeft->y;
    centerForceLeftVec(2) = stiffness * ((desiredLeftTcpPose->toEigen())(2, 3) - leftTcpPoseBase(2, 3)) - damping * curVelLeft->z;


    Eigen::Vector3f centerForceRightVec;
    centerForceRightVec(0) = stiffness * ((desiredRightTcpPose->toEigen())(0, 3) - rightTcpPoseBase(0, 3)) - damping * curVelRight->x; //250.0 Table length half
    centerForceRightVec(1) = stiffness * ((desiredRightTcpPose->toEigen())(1, 3) - rightTcpPoseBase(1, 3)) - damping * curVelRight->y;
    centerForceRightVec(2) = stiffness * ((desiredRightTcpPose->toEigen())(2, 3) - rightTcpPoseBase(2, 3)) - damping * curVelRight->z; //1100.0 Table length half

    //Rotatrotate first then move arms

    stiffness = accGain;
    damping = (2.0 * sqrt(stiffness));
    Eigen::Vector3f coupledForceLeftVec;
    coupledForceLeftVec(0) = stiffness * ((leftToRightVec(0) / leftToRightVec.norm()) * (leftToRightVec.norm() - desiredDistance)) - damping * curVelLeft->x;; //-0.5*tableWidth-leftTcpPoseBase(0,3) - damping*curVelLeft->x;
    coupledForceLeftVec(1) = stiffness * ((leftToRightVec(0) / leftToRightVec.norm()) * (leftToRightVec.norm() - desiredDistance)) - damping * curVelLeft->y; //600.0-leftTcpPoseBase(1,3)- damping*curVelLeft->y;
    coupledForceLeftVec(2) = stiffness * ((leftToRightVec(0) / leftToRightVec.norm()) * (leftToRightVec.norm() - desiredDistance)) - damping * curVelLeft->z; //1100.0-leftTcpPoseBase(2,3)- damping*curVelLeft->z;

    Eigen::Vector3f coupledForceRightVec;

    coupledForceRightVec(0) = stiffness * ((-leftToRightVec(0) / leftToRightVec.norm()) * (leftToRightVec.norm() - desiredDistance)) - damping * curVelRight->x;
    coupledForceRightVec(1) = stiffness * ((-leftToRightVec(1) / leftToRightVec.norm()) * (leftToRightVec.norm() - desiredDistance)) - damping * curVelRight->y;;
    coupledForceRightVec(2) = stiffness * ((-leftToRightVec(2) / leftToRightVec.norm()) * (leftToRightVec.norm() - desiredDistance)) - damping * curVelRight->z;;
    centerForceRightVec = centerForceRightVec + coupledForceRightVec;
    centerForceLeftVec = centerForceLeftVec + coupledForceLeftVec;

    ARMARX_INFO << "desired left tcp pose "  << *desiredLeftTcpPose;
    ARMARX_INFO << "desired right tcp pose "  << *desiredRightTcpPose;

    ARMARX_INFO << "Forces pulling TCP L to center " << centerForceLeftVec(0) << " " << centerForceLeftVec(1) << " " << centerForceLeftVec(2);
    ARMARX_INFO << "Forces pulling TCP R to center " << centerForceRightVec(0) << " " << centerForceRightVec(1) << " " << centerForceRightVec(2);
    ARMARX_INFO << "Forces acting on TCP L " << curForceLeftVec(0) * accGain << " " << curForceLeftVec(1) * accGain << " " << curForceLeftVec(2) * accGain;
    ARMARX_INFO << "Forces acting on TCP R " << curForceRightVec(0) * accGain << " " << curForceRightVec(1) * accGain << " " << curForceRightVec(2) * accGain;

    if (curForceLeftVec.norm() > forceThreshold)
    {
        newAccLeft = curForceLeftVec * accGain + centerForceLeftVec;
    }
    else if (curVelLeft->toEigen().norm() > 0)
    {
        ARMARX_IMPORTANT << deactivateSpam(1) << "Deccelerating";
        newAccLeft = -decGain * curVelLeft->toEigen().normalized()  + centerForceLeftVec;
    }
    else
    {
        newAccLeft = Eigen::Vector3f::Zero()  + centerForceLeftVec;
    }

    if (curForceRightVec.norm() > forceThreshold)
    {
        newAccRight = curForceRightVec * accGain + centerForceRightVec;
    }
    else if (curVelRight->toEigen().norm() > 0)
    {
        ARMARX_IMPORTANT << deactivateSpam(1) << "Deccelerating";
        newAccRight = -decGain * curVelRight->toEigen().normalized() + centerForceRightVec;
    }
    else
    {
        newAccRight = Eigen::Vector3f::Zero() + centerForceRightVec;
    }

    //ARMARX_IMPORTANT << deactivateSpam(1) << "current curForce: " << curForce->output();
    //ARMARX_IMPORTANT << deactivateSpam(1) << "current velocity: " << vel->output();
    newVelLeft = curVelLeft->toEigen() + newAccLeft * duration.toMilliSecondsDouble() * 0.001;

    if (newVelLeft.norm() > maxVelocity)
    {
        newVelLeft = newVelLeft.normalized() * maxVelocity;
    }

    newVelRight = curVelRight->toEigen() + newAccRight * duration.toMilliSecondsDouble() * 0.001;

    if (newVelRight.norm() > maxVelocity)
    {
        newVelRight = newVelRight.normalized() * maxVelocity;
    }


    Eigen::Vector3f newOriVelLeft(3);
    Eigen::Vector3f newOriAccLeft(3);

    //ARMARX_INFO << deactivateSpam(1) << "force magnitude: " << curToruqe->toEigen().norm();
    if (curTorqueLeftVec.norm() > torqueThreshold)
    {
        newOriAccLeft = curTorqueLeftVec * oriAccGain;
    }
    else if (curVelOriLeft->toEigen().norm() > 0)
    {
        //ARMARX_IMPORTANT << deactivateSpam(1) << "Deccelerating";
        newOriAccLeft = -oriDccGain * curVelOriLeft->toEigen().normalized();
    }
    else
    {
        newOriAccLeft = Eigen::Vector3f::Zero();
    }

    //ARMARX_IMPORTANT << deactivateSpam(1) << "current curTorque: " << curTorque->output();
    //ARMARX_IMPORTANT << deactivateSpam(1) << "current velocity: " << velOriLeft->output();
    Eigen::Vector3f velOriLeftDelta =  newOriAccLeft * duration.toMilliSecondsDouble() * 0.001;

    if (velOriLeftDelta.norm() > maxOriAcc)
    {
        velOriLeftDelta = velOriLeftDelta.normalized() * maxOriAcc;
    }

    newOriVelLeft = curVelOriLeft->toEigen() + velOriLeftDelta;

    if (newOriVelLeft.norm() > maxOriVelocity)
    {
        newOriVelLeft = newOriVelLeft.normalized() * maxOriVelocity;
    }

    //   ARMARX_IMPORTANT << deactivateSpam(1) << "current newVel: " << newOriVel;
    //    vel->x = 0;
    //    vel->y = 0;



    //float maxSensitivity = 2.0f;

    Eigen::Vector3f newOriVelRight(3);
    Eigen::Vector3f newOriAccRight(3);

    if (curTorqueRightVec.norm() > torqueThreshold)
    {
        ARMARX_INFO << deactivateSpam(1) << "zero force";
        newOriAccRight = curTorqueRightVec * oriAccGain;
    }
    else if (curVelOriRight->toEigen().norm() > 0)
    {
        ARMARX_IMPORTANT << deactivateSpam(1) << "Deccelerating";
        newOriAccRight = -oriDccGain * curVelOriRight->toEigen().normalized();
    }
    else
    {
        newOriAccRight = Eigen::Vector3f::Zero();
    }

    ARMARX_INFO << "New Ori Acc Right " << newOriAccRight(0) << " " << newOriAccRight(1) << " " << newOriAccRight(2);
    //ARMARX_IMPORTANT << deactivateSpam(1) << "current curTorque: " << curTorque->output();
    //ARMARX_IMPORTANT << deactivateSpam(1) << "current velocity: " << velOriRight->output();
    Eigen::Vector3f velOriRightDelta =  newOriAccRight * duration.toMilliSecondsDouble() * 0.001;

    if (velOriRightDelta.norm() > maxOriAcc)
    {
        velOriRightDelta = velOriRightDelta.normalized() * maxOriAcc;
    }

    newOriVelRight = curVelOriRight->toEigen() + velOriRightDelta;

    if (newOriVelRight.norm() > maxOriVelocity)
    {
        newOriVelRight = newOriVelRight.normalized() * maxOriVelocity;
    }

    ARMARX_INFO << "New Ori Vel Right " << newOriVelRight(0) << " " << newOriVelRight(1) << " " << newOriVelRight(2);
    //platformVelocity.x = platformGain*0.5*(newVelLeft(0)+newVelRight(0));
    //platformVelocity.y = platformGain*0.5*(newVelLeft(1)+newVelRight(1));
    //platformVelocity.z = platformGain*0.5*(newOriVelLeft(2)+newOriVelRight(2));
    float maxVelocityTrans = 60.0;
    float maxVelocityRot = 0.2;
    //float minVelocityTrans = 30.0;  //unused variable
    //float minVelocityRot = 0.05;  //unused variable
    Vector3Ptr platformVelPtr = new Vector3(platformVelocity.toEigen());

    assert(kernelSize >= 0);
    if (platformVelMeanFilter.size() == static_cast<std::size_t>(kernelSize))
    {
        platformVelMeanFilter.erase(platformVelMeanFilter.begin(), platformVelMeanFilter.begin() + 1);
    }

    platformVelMeanFilter.push_back(platformVelPtr);

    platformVelocity.x = 0.0;
    platformVelocity.y = 0.0;
    platformVelocity.z = 0.0;

    for (std::size_t i = 0; i < platformVelMeanFilter.size(); i++)
    {
        platformVelocity.x += platformVelMeanFilter[i]->x;
        platformVelocity.y += platformVelMeanFilter[i]->y;
        platformVelocity.z += platformVelMeanFilter[i]->z;
    }

    platformVelocity.x /= float(platformVelMeanFilter.size());
    platformVelocity.y /= float(platformVelMeanFilter.size());
    platformVelocity.z /= float(platformVelMeanFilter.size());
    platformVelocity.x = std::max(std::min(platformVelocity.x, maxVelocityTrans), -maxVelocityTrans);
    platformVelocity.y = std::max(std::min(platformVelocity.y, maxVelocityTrans), -maxVelocityTrans);
    platformVelocity.z = std::max(std::min(platformVelocity.z, maxVelocityRot), -maxVelocityRot);
    Eigen::Matrix4f platformTransformation;
    platformTransformation << cos(platformVelocity.z), -sin(platformVelocity.z), 0.0, 0.0,
                           sin(platformVelocity.z), cos(platformVelocity.z), 0.0, 0.0,
                           0.0, 0.0, 1.0, 0.0,
                           0.0, 0.0, 0.0, 1.0;
    Eigen::Vector4f temp;

    for (int i = 0; i < 3; i++)
    {
        temp(i) = newVelLeft(i);
    }

    temp(3) = 1.0;
    temp = platformTransformation.inverse() * temp;

    for (int i = 0; i < 3; i++)
    {
        newVelLeft(i) = temp(i);
    }

    for (int i = 0; i < 3; i++)
    {
        temp(i) = newOriVelLeft(i);
    }

    temp(3) = 1.0;
    temp = platformTransformation.inverse() * temp;

    for (int i = 0; i < 3; i++)
    {
        newOriVelLeft(i) = oriAccGain * deltaLeftRPY(i);    //((in.getDesiredLeftTcpOrientation()->toEigen())(i)-currentLeftRPY(i))*10.0;//duration.toMilliSecondsDouble()*0.001;//temp(i);
    }

    for (int i = 0; i < 3; i++)
    {
        temp(i) = newVelRight(i);
    }

    temp(3) = 1.0;
    temp = platformTransformation.inverse() * temp;

    for (int i = 0; i < 3; i++)
    {
        newVelRight(i) = temp(i);
    }

    for (int i = 0; i < 3; i++)
    {
        temp(i) = newOriVelRight(i);
    }

    temp(3) = 1.0;
    temp = platformTransformation.inverse() * temp;

    for (int i = 0; i < 3; i++)
    {
        newOriVelRight(i) = oriAccGain * deltaRightRPY(i);    //((in.getDesiredRightTcpOrientation()->toEigen())(i)-currentRightRPY(i))*10.0;//duration.toMilliSecondsDouble()*0.001;//temp(i);
    }

    ARMARX_INFO << "New Vel right pos " << newVelRight(0) << " " << newVelRight(1) << " " << newVelRight(2);
    ARMARX_INFO << "New Vel right ori " << newOriVelRight(0) << " " << newOriVelRight(1) << " " << newOriVelRight(2);
    ARMARX_INFO << "New Vel right pos " << newVelRight(0) << " " << newVelRight(1) << " " << newVelRight(2);
    ARMARX_INFO << "New Vel left ori " << newOriVelLeft(0) << " " << newOriVelLeft(1) << " " << newOriVelLeft(2);
    Eigen::Matrix4f targetLeft;
    VirtualRobot::MathTools::rpy2eigen4f(in.getDesiredLeftTcpOrientation()->toEigen()(0),
                                         in.getDesiredLeftTcpOrientation()->toEigen()(1),
                                         in.getDesiredLeftTcpOrientation()->toEigen()(2), targetLeft);
    targetLeft(0, 3) = leftTcpPoseBase(0, 3);
    targetLeft(1, 3) = leftTcpPoseBase(1, 3);
    targetLeft(2, 3) = leftTcpPoseBase(2, 3);
    Eigen::Matrix4f targetright;
    VirtualRobot::MathTools::rpy2eigen4f(in.getDesiredRightTcpOrientation()->toEigen()(0),
                                         in.getDesiredRightTcpOrientation()->toEigen()(1),
                                         in.getDesiredRightTcpOrientation()->toEigen()(2), targetright);
    targetright(0, 3) = rightTcpPoseBase(0, 3);
    targetright(1, 3) = rightTcpPoseBase(1, 3);
    targetright(2, 3) = rightTcpPoseBase(2, 3);
    ARMARX_INFO << "target RPY right " <<   currentRightRPY(0) << " " << currentRightRPY(1) << " " << currentRightRPY(2);
    ARMARX_INFO << "target RPY left " <<   currentLeftRPY(0) << " " << currentLeftRPY(1) << " " << currentLeftRPY(2);
    ARMARX_INFO << "target RPY right " << (in.getDesiredRightTcpOrientation()->toEigen())(0) << " " << (in.getDesiredRightTcpOrientation()->toEigen())(1) << " " << (in.getDesiredRightTcpOrientation()->toEigen())(2);
    ARMARX_INFO << "target RPY left " << (in.getDesiredLeftTcpOrientation()->toEigen())(0) << " " << (in.getDesiredLeftTcpOrientation()->toEigen())(1) << " " << (in.getDesiredLeftTcpOrientation()->toEigen())(2);
    auto debugDrawer = context->getDebugDrawerTopicProxy();
    debugDrawer->setPoseVisu("leftHandTCP", "leftHandTCP", FramedPose(leftTcpPoseBase, "Armar3_Base", "Armar3").toGlobal(context->getRobot()));
    debugDrawer->setPoseVisu("leftTargetTCP", "leftTargetTCP", FramedPose(targetLeft, "Armar3_Base", "Armar3").toGlobal(context->getRobot()));
    debugDrawer->setPoseVisu("rightHandTCP", "rightHandTCP", FramedPose(rightTcpPoseBase, "Armar3_Base", "Armar3").toGlobal(context->getRobot()));
    debugDrawer->setPoseVisu("rightTargetTCP", "rightTargetTCP", FramedPose(targetright, "Armar3_Base", "Armar3").toGlobal(context->getRobot()));
    //*********************Output**********************
    out.setTimestamp(new TimestampVariant(now));
    //out.setcurrentSensitivity(in.getcurrentSensitivity());

    out.setLeftTcpTargetPosVelocity(new FramedDirection(newVelLeft, curVelLeft->frame, curVelLeft->agent));
    out.setLeftTcpTargetOriVelocity(new FramedDirection(newOriVelLeft, "Armar3_Base", "Armar3"));
    out.setRightTcpTargetPosVelocity(new FramedDirection(newVelRight, curVelRight->frame, curVelRight->agent));
    out.setRightTcpTargetOriVelocity(new FramedDirection(newOriVelRight, "Armar3_Base", "Armar3"));
    //TODO make platform vel smaller
    ARMARX_INFO << "Computing Platform with " << platformVelocity.x << " " << platformVelocity.y << " " << platformVelocity.z;
    out.setPlatformVelocity(platformVelocity);
    local.setPlatformVelMeanFilter(platformVelMeanFilter);
    //FramedDirectionPtr newFramedVel = new FramedDirection(newVel, vel->frame,vel->agent);//
    //FramedDirectionPtr newFramedRotVel = new FramedDirection(newRotVel, velRot->frame,velRot->agent);


    /*newFramedVel = FramedDirection::ChangeFrame(c->getRobot(), *newFramedVel, c->getRobot()->getRootNode()->getName());
    ARMARX_IMPORTANT << deactivateSpam(1) << "current newVel in platform: " << newFramedVel->output();
    newFramedRotVel = FramedDirection::ChangeFrame(c->getRobot(), *newFramedRotVel, c->getRobot()->getRootNode()->getName());
    ARMARX_IMPORTANT << deactivateSpam(1) << "current newVel in platform: " << newFramedRotVel->output();*/
    bool noChange = false;

    if (noChange)
    {
        //TODO set to zero all velocities
        emitEvNoChange();
    }
    else
    {
        emitEvDone();
    }

}




void CalculateForceControlRobotPose::onExit()
{
    /*CoupledInteractionGroupStatechartContext* context =*/ getContext<CoupledInteractionGroupStatechartContext>();  //unused variable
    /*std::string leftTcpName("Wrist 2 L");
    std::string rightTcpName("Wrist 2 R");
    DatafieldRefPtr forceRefLeft = DatafieldRefPtr::dynamicCast(context->getForceTorqueUnitObserver()->getForceDatafield(leftTcpName));
    DatafieldRefPtr torqueRefLeft = DatafieldRefPtr::dynamicCast(context->getForceTorqueUnitObserver()->getTorqueDatafield(leftTcpName));
    DatafieldRefPtr forceRefRight = DatafieldRefPtr::dynamicCast(context->getForceTorqueUnitObserver()->getForceDatafield(rightTcpName));
    DatafieldRefPtr torqueRefRight = DatafieldRefPtr::dynamicCast(context->getForceTorqueUnitObserver()->getTorqueDatafield(rightTcpName));
    context->getForceTorqueUnitObserver()->removeFilteredDatafield(forceRefLeft);
    context->getForceTorqueUnitObserver()->removeFilteredDatafield(forceRefRight);
    context->getForceTorqueUnitObserver()->removeFilteredDatafield(torqueRefLeft);
    context->getForceTorqueUnitObserver()->removeFilteredDatafield(torqueRefRight);*/
    // put your user code for the exit point here
    // execution time should be short (<100ms)

}

// DO NOT EDIT NEXT FUNCTION
std::string CalculateForceControlRobotPose::GetName()
{
    return "CalculateForceControlRobotPose";
}

// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr CalculateForceControlRobotPose::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new CalculateForceControlRobotPose(stateData));
}

