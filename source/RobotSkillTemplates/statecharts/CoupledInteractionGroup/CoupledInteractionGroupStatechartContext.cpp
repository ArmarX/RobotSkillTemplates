/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2014-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::CoupledInteractionGroup
 * @author     [Author Name] ( [Author Email] )
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "CoupledInteractionGroupStatechartContext.h"

//#include <RobotAPI/libraries/core/remoterobot/RemoteRobot.h>
//#include <RobotAPI/libraries/core/Pose.h>

#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/system/ImportExportComponent.h>
#include <ArmarXCore/statechart/Statechart.h>

#define DEFAULT_SETTINGS_AGENT_INSTANCES_SEGMENT_NAME "agentInstances"

using namespace memoryx;


namespace armarx::CoupledInteractionGroup
{
    void CoupledInteractionGroupStatechartContext::onInitStatechartContext()
    {
        // Register dependencies
        //        usingProxy(getProperty<std::string>("KinematicUnitName").getValue());
        //        usingProxy(getProperty<std::string>("KinematicUnitObserverName").getValue());
        //        usingProxy(getProperty<std::string>("RobotStateComponentName").getValue());
        //        usingProxy(getProperty<std::string>("TCPControlUnitName").getValue());
        usingProxy(getProperty<std::string>("KinematicUnitName").getValue());
        usingProxy(getProperty<std::string>("KinematicUnitObserverName").getValue());
        usingProxy(getProperty<std::string>("RobotStateComponentName").getValue());
        usingProxy(getProperty<std::string>("TCPControlUnitName").getValue());
        usingProxy(getProperty<std::string>("TCPControlUnitObserverName").getValue());
        usingProxy(getProperty<std::string>("WorkingMemoryName").getValue());
        usingProxy(getProperty<std::string>("ObjectMemoryObserverName").getValue());
        usingProxy(getProperty<std::string>("PriorKnowledgeName").getValue());
        usingProxy(getProperty<std::string>("ViewSelectionName").getValue());
        usingProxy(getProperty<std::string>("ForceTorqueObserverName").getValue());
        platformUnitObserverName = getProperty<std::string>("PlatformUnitObserverName").getValue();
        platformUnitName = getProperty<std::string>("PlatformUnitName").getValue();
        ARMARX_INFO << "Using platform unit:" << platformUnitName << ", platform observer:" << platformUnitObserverName;

        usingProxy(platformUnitName);
        usingProxy(platformUnitObserverName);

        offeringTopic("DebugDrawerUpdates");
    }


    void CoupledInteractionGroupStatechartContext::onConnectStatechartContext()
    {

        // retrieve proxies
        //        robotStateComponent = getProxy<RobotStateComponentInterfacePrx>(getProperty<std::string>("RobotStateComponentName").getValue());
        //        kinematicUnitPrx = getProxy<KinematicUnitInterfacePrx>(getProperty<std::string>("KinematicUnitName").getValue());
        //        kinematicUnitObserverPrx = getProxy<KinematicUnitObserverInterfacePrx>(getProperty<std::string>("KinematicUnitObserverName").getValue());
        //        tcpControlPrx = getProxy<TCPControlUnitInterfacePrx>(getProperty<std::string>("TCPControlUnitName").getValue());

        //        // initialize remote robot
        //        remoteRobot.reset(new RemoteRobot(robotStateComponent->getSynchronizedRobot()));
        // retrieve proxies
        robotStateComponent = getProxy<RobotStateComponentInterfacePrx>(getProperty<std::string>("RobotStateComponentName").getValue());
        kinematicUnitPrx = getProxy<KinematicUnitInterfacePrx>(getProperty<std::string>("KinematicUnitName").getValue());
        kinematicUnitObserverPrx = getProxy<KinematicUnitObserverInterfacePrx>(getProperty<std::string>("KinematicUnitObserverName").getValue());
        tcpControlPrx = getProxy<TCPControlUnitInterfacePrx>(getProperty<std::string>("TCPControlUnitName").getValue());
        tcpControlUnitObserverPrx = getProxy<TCPControlUnitObserverInterfacePrx>(getProperty<std::string>("TCPControlUnitObserverName").getValue());
        workingMemoryProxy = getProxy<memoryx::WorkingMemoryInterfacePrx>(getProperty<std::string>("WorkingMemoryName").getValue());
        objectMemoryObserverProxy = getProxy<memoryx::ObjectMemoryObserverInterfacePrx>(getProperty<std::string>("ObjectMemoryObserverName").getValue());
        priorKnowledgeProxy = getProxy<memoryx::PriorKnowledgeInterfacePrx>(getProperty<std::string>("PriorKnowledgeName").getValue());

        // retrieve proxies
        platformUnitPrx = getProxy<PlatformUnitInterfacePrx>(platformUnitName);
        platformUnitObserverPrx = getProxy<PlatformUnitObserverInterfacePrx>(platformUnitObserverName);
        viewSelectionProxy = getProxy<ViewSelectionInterfacePrx>(getProperty<std::string>("ViewSelectionName").getValue());
        ftUnitObserverPrx = getProxy<ForceTorqueUnitObserverInterfacePrx>(getProperty<std::string>("ForceTorqueObserverName").getValue());

        // initialize remote robot
        remoteRobot.reset(new RemoteRobot(robotStateComponent->getSynchronizedRobot()));

        debugDrawerTopicProxy = getTopic<armarx::DebugDrawerInterfacePrx>("DebugDrawerUpdates");
        /*if (this->getIceManager()->isObjectReachable("Simulator"))
        {
            simulatorPrx = getProxy<SimulatorInterfacePrx>("Simulator");
        }
        else
        {
            simulatorPrx = NULL;
        }*/


        AgentInstancesSegmentBasePrx agentInstancesProxy = AgentInstancesSegmentBasePrx::uncheckedCast(workingMemoryProxy->getSegment(DEFAULT_SETTINGS_AGENT_INSTANCES_SEGMENT_NAME));

        if (agentInstancesProxy)
        {
            agent = AgentInstancePtr::dynamicCast(agentInstancesProxy->getAgentInstanceByName("Armar3"));
        }
        else
        {
            agent = nullptr;
        }
    }

    Eigen::Matrix4f CoupledInteractionGroupStatechartContext::getRobotPose()
    {
        Eigen::Matrix4f robotPose = Eigen::Matrix4f::Identity();

        if (!agent)
        {
            ARMARX_ERROR << deactivateSpam() << "no agent with name Armar3";
        }
        else
        {

            FramedPositionPtr agentPosition = agent->getPosition();

            if (agentPosition)
            {
                robotPose.block(0, 3, 3, 1) = agentPosition->toEigen();
            }

            FramedOrientationPtr agentOrientation = agent->getOrientation();

            if (agentOrientation)
            {
                robotPose.block(0, 0, 3, 3) = agentOrientation->toEigen();
            }
        }

        return robotPose;
    }

    PropertyDefinitionsPtr CoupledInteractionGroupStatechartContext::createPropertyDefinitions()
    {
        return PropertyDefinitionsPtr(new CoupledInteractionGroupStatechartContextProperties(
                                          getConfigIdentifier()));
    }
}
