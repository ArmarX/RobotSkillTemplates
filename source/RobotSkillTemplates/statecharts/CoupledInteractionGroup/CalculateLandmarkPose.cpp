/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2014-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::CalculateLandmarkPoseGroup
 * @author     [Author Name] ( [Author Email] )
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "CalculateLandmarkPose.h"
#include <RobotAPI/libraries/core/remoterobot/RemoteRobot.h>

#include <ArmarXCore/core/logging/Logging.h>

using namespace armarx;
using namespace CoupledInteractionGroup;

// DO NOT EDIT NEXT LINE
CalculateLandmarkPose::SubClassRegistry CalculateLandmarkPose::Registry(CalculateLandmarkPose::GetName(), &CalculateLandmarkPose::CreateInstance);



CalculateLandmarkPose::CalculateLandmarkPose(XMLStateConstructorParams stateData) :
    XMLStateTemplate < CalculateLandmarkPose > (stateData), CalculateLandmarkPoseGeneratedBase < CalculateLandmarkPose > (stateData)
{
}

void CalculateLandmarkPose::onEnter()
{

    Vector3 landmarkVector;

    CoupledInteractionGroupStatechartContext* context = getContext<CoupledInteractionGroupStatechartContext>();
    auto graphSegment = context->getPriorKnowledgeProxy()->getGraphSegment();

    //read scene name
    std::string sceneName = in.getSceneName();

    //does the scene exist
    if (!graphSegment->hasScene(sceneName))
    {
        ARMARX_WARNING << "Target scene " << sceneName << " does not exist in menory" << flush;
        out.setLandmarkPose(landmarkVector);
        emitEvLandmarkPoseCalculationFailed();
        return;
    }

    //does the target landmark exist
    std::string landmark = in.getLandmarkName();
    auto landmarkNode = graphSegment->getNodeFromSceneByName(sceneName, landmark);

    if (!graphSegment->hasNodeWithName(sceneName, landmark))
    {
        ARMARX_WARNING << "Target landmark '" << landmark << "' doesn't exist in graph " << sceneName << flush;
        auto nodes = graphSegment->getNodesByScene(sceneName);

        for (auto& node : nodes)
        {
            ARMARX_INFO << "node: " << node->getName();
        }

        out.setLandmarkPose(landmarkVector);
        emitEvLandmarkPoseCalculationFailed();
        return;
    }

    if (landmark != landmarkNode->getName())
    {
        ARMARX_IMPORTANT << "Resolved " << landmark << " to " << landmarkNode->getName();
    }

    FramedPosePtr landmarkPose = FramedPosePtr::dynamicCast(landmarkNode->getPose());

    ARMARX_DEBUG << "CalcPath::onEnter(): Looking for path" << flush;

    Eigen::Vector3f pose = landmarkPose->getPosition()->toEigen();
    landmarkVector = Vector3(pose);
    out.setLandmarkPose(landmarkVector);
    emitEvLandmarkPoseCalculated();

}


void CalculateLandmarkPose::onExit()
{

    // put your user code for the exit point here
    // execution time should be short (<100ms)

}

// DO NOT EDIT NEXT FUNCTION
std::string CalculateLandmarkPose::GetName()
{
    return "CalculateLandmarkPose";
}

// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr CalculateLandmarkPose::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new CalculateLandmarkPose(stateData));
}

