/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2014-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::TestLocalizeTableGroup
 * @author     [Author Name] ( [Author Email] )
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "TestLocalizeTable.h"
#include <RobotAPI/libraries/core/remoterobot/RemoteRobot.h>


#include <MemoryX/libraries/observers/ObjectMemoryObserver.h>

using namespace armarx;
using namespace CoupledInteractionGroup;

// DO NOT EDIT NEXT LINE
TestLocalizeTable::SubClassRegistry TestLocalizeTable::Registry(TestLocalizeTable::GetName(), &TestLocalizeTable::CreateInstance);



TestLocalizeTable::TestLocalizeTable(XMLStateConstructorParams stateData) :
    XMLStateTemplate < TestLocalizeTable > (stateData), TestLocalizeTableGeneratedBase < TestLocalizeTable > (stateData)
{
}

void TestLocalizeTable::onEnter()
{

    CoupledInteractionGroupStatechartContext* context = getContext<CoupledInteractionGroupStatechartContext>();
    context->getViewSelection()->deactivateAutomaticViewSelection();
    std::string objectName = in.getObjectNameTable();
    float localizationFrequencyWaitingTime = 300;
    ChannelRefBasePtr objectMemoryChannel = context->getObjectMemoryObserverProxy()->requestObjectClassRepeated(objectName, localizationFrequencyWaitingTime, armarx::DEFAULT_VIEWTARGET_PRIORITY);

    //objectMemoryChannel = context->getObjectMemoryObserverProxy()->requestObjectClassOnce(objectName);
    if (objectMemoryChannel)
    {
        out.setObjectMemoryChannel(ChannelRefPtr::dynamicCast(objectMemoryChannel));
        emitEvMemoryChannelSet();
    }
    else
    {
        emitEvMemoryChannelNotSet();
    }

}


void TestLocalizeTable::onExit()
{
    //CoupledInteractionGroupStatechartContext* context = getContext<CoupledInteractionGroupStatechartContext>();
    //if(local.isObjectMemoryChannelSet())
    //   context->getObjectMemoryObserverProxy()->releaseObjectClass(local.getObjectMemoryChannel());
    // put your user code for the exit point here
    // execution time should be short (<100ms)

}

// DO NOT EDIT NEXT FUNCTION
std::string TestLocalizeTable::GetName()
{
    return "TestLocalizeTable";
}

// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr TestLocalizeTable::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new TestLocalizeTable(stateData));
}

