/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2014-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::CheckTableMovingGroup
 * @author     [Author Name] ( [Author Email] )
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "CheckTableMoving.h"
#include <RobotAPI/libraries/core/remoterobot/RemoteRobot.h>


using namespace armarx;
using namespace CoupledInteractionGroup;

// DO NOT EDIT NEXT LINE
CheckTableMoving::SubClassRegistry CheckTableMoving::Registry(CheckTableMoving::GetName(), &CheckTableMoving::CreateInstance);



CheckTableMoving::CheckTableMoving(XMLStateConstructorParams stateData) :
    XMLStateTemplate < CheckTableMoving > (stateData), CheckTableMovingGeneratedBase < CheckTableMoving > (stateData)
{
}

void CheckTableMoving::onEnter()
{
    //TODO Fix at localizer
    emitEvTableNotMoving();
}



void CheckTableMoving::onExit()
{

    // put your user code for the exit point here
    // execution time should be short (<100ms)

}

// DO NOT EDIT NEXT FUNCTION
std::string CheckTableMoving::GetName()
{
    return "CheckTableMoving";
}

// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr CheckTableMoving::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new CheckTableMoving(stateData));
}

