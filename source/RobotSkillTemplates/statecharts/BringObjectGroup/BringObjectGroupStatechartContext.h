/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2014-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::BringObjectGroup
 * @author     Mirko Waechter ( mirko dot waechter at kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once

#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/system/ImportExportComponent.h>
#include <ArmarXCore/statechart/StatechartContext.h>


// Custom Includes
//#include <RobotAPI/interface/units/KinematicUnitInterface.h>
//#include <RobotAPI/interface/units/TCPControlUnit.h>
//#include <RobotAPI/interface/observers/KinematicUnitObserverInterface.h>
#include <RobotAPI/interface/core/FramedPoseBase.h>
#include <RobotAPI/interface/visualization/DebugDrawerInterface.h>
#include <VirtualRobot/VirtualRobot.h>

#include <MemoryX/interface/components/WorkingMemoryInterface.h>
#include <MemoryX/interface/observers/ObjectMemoryObserverInterface.h>
#include <MemoryX/interface/components/PriorKnowledgeInterface.h>
//#include <ArmarXSimulation/interface/simulator/SimulatorInterface.h>
#include <MemoryX/libraries/updater/ObjectLocalization/MemoryXUpdaterObjectFactories.h>

#include <RobotComponents/interface/components/PathPlanner.h>
#include <RobotAPI/interface/components/ViewSelectionInterface.h>


namespace armarx::BringObjectGroup
{

    struct BringObjectGroupStatechartContextProperties : StatechartContextPropertyDefinitions
    {
        BringObjectGroupStatechartContextProperties(std::string prefix):
            StatechartContextPropertyDefinitions(prefix)
        {
            //            defineRequiredProperty<std::string>("KinematicUnitName", "Name of the kinematic unit that should be used");
            //            defineRequiredProperty<std::string>("KinematicUnitObserverName", "Name of the kinematic unit observer that should be used");
            defineOptionalProperty<std::string>("RobotStateComponentName", "RobotStateComponent", "Name of the robot state component that should be used");
            //            defineOptionalProperty<std::string>("TCPControlUnitName", "TCPControlUnit", "Name of the tcp control unit component that should be used");

            defineOptionalProperty<std::string>("WorkingMemoryName", "WorkingMemory", "Name of the WorkingMemory component that should be used");
            defineOptionalProperty<std::string>("ObjectMemoryObserverName", "ObjectMemoryObserver", "Name of the ObjectMemoryObserver component that should be used");
            defineOptionalProperty<std::string>("PriorKnowledgeName", "PriorKnowledge", "Name of the PriorKnowledge component that should be used");
            defineOptionalProperty<std::string>("PathPlannerName", "PathPlanner", "Name of the Path Planner component that should be used");
            defineOptionalProperty<std::string>("ViewSelectionName", "ViewSelection", "Name of the ViewSelection component that should be used");

        }
    };

    /**
     * @class BringObjectGroupStatechartContext is a custom implementation of the StatechartContext
     * for a statechart
     */
    class ARMARXCOMPONENT_IMPORT_EXPORT BringObjectGroupStatechartContext :
        virtual public XMLStatechartContext
    {
    public:
        // inherited from Component
        std::string getDefaultName() const override
        {
            return "BringObjectGroupStatechartContext";
        }
        void onInitStatechartContext() override;
        void onConnectStatechartContext() override;

        memoryx::WorkingMemoryInterfacePrx getWorkingMemoryProxy()
        {
            return workingMemoryProxy;
        }
        memoryx::ObjectMemoryObserverInterfacePrx getObjectMemoryObserverProxy()
        {
            return objectMemoryObserverProxy;
        }
        memoryx::PriorKnowledgeInterfacePrx getPriorKnowledgeProxy()
        {
            return priorKnowledgeProxy;
        }

        armarx::DebugDrawerInterfacePrx getDebugDrawerTopicProxy()
        {
            return debugDrawerTopicProxy;
        }
        const VirtualRobot::RobotPtr getRobot()
        {
            return remoteRobot;
        }
        RobotStateComponentInterfacePrx getRobotStateComponent()
        {
            return robotStateComponent;
        }
        //        KinematicUnitInterfacePrx getKinematicUnit() { return kinematicUnitPrx; }
        //        std::string getKinematicUnitObserverName() { return getProperty<std::string>("KinematicUnitObserverName").getValue(); }
        //        TCPControlUnitInterfacePrx getTCPControlUnit() { return tcpControlPrx;}
        //SimulatorInterfacePrx getSimulatorProxy() { return simulatorPrx; }
        PathPlannerBasePrx getPathPlanner() const
        {
            return pathPlannerPrx;
        }
        ViewSelectionInterfacePrx getViewSelection() const
        {
            return viewSelection;
        }
    private:


        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        PropertyDefinitionsPtr createPropertyDefinitions() override;


        RobotStateComponentInterfacePrx robotStateComponent;
        //        KinematicUnitInterfacePrx kinematicUnitPrx;
        //        KinematicUnitObserverInterfacePrx kinematicUnitObserverPrx;
        //        TCPControlUnitInterfacePrx tcpControlPrx;
        VirtualRobot::RobotPtr remoteRobot;

        memoryx::WorkingMemoryInterfacePrx workingMemoryProxy;
        memoryx::ObjectMemoryObserverInterfacePrx objectMemoryObserverProxy;
        memoryx::PriorKnowledgeInterfacePrx priorKnowledgeProxy;

        armarx::DebugDrawerInterfacePrx debugDrawerTopicProxy;
        //SimulatorInterfacePrx simulatorPrx;
        PathPlannerBasePrx pathPlannerPrx;
        ViewSelectionInterfacePrx viewSelection;
    };

}


