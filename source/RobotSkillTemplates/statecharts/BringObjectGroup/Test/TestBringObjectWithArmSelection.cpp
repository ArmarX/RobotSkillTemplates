/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2014-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::BringObjectGroup
 * @author     Mirko Waechter ( mirko dot waechter at kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "TestBringObjectWithArmSelection.h"
#include "../BringObjectGroupStatechartContext.h"
#include <MemoryX/interface/memorytypes/MemoryEntities.h>
#include <RobotAPI/interface/core/FramedPoseBase.h>
#include <MemoryX/libraries/memorytypes/MemoryXTypesObjectFactories.h>
#include <MemoryX/core/MemoryXCoreObjectFactories.h>
using namespace armarx;
using namespace BringObjectGroup;

// DO NOT EDIT NEXT LINE
TestBringObjectWithArmSelection::SubClassRegistry TestBringObjectWithArmSelection::Registry(TestBringObjectWithArmSelection::GetName(), &TestBringObjectWithArmSelection::CreateInstance);



TestBringObjectWithArmSelection::TestBringObjectWithArmSelection(XMLStateConstructorParams stateData) :
    XMLStateTemplate<TestBringObjectWithArmSelection>(stateData),  TestBringObjectWithArmSelectionGeneratedBase<TestBringObjectWithArmSelection>(stateData)
{
}

void TestBringObjectWithArmSelection::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
    //BringObjectGroupStatechartContext* c = getContext<BringObjectGroupStatechartContext>();

    ARMARX_IMPORTANT << "FIXME: No direct access to the simulator here, you can start a scenario that adds an object (" << in.getObjectName() << ") via the SceneModifier component (see ArmarXSimulation)";
    /*if(c->getSimulatorProxy())
    {
        std::string objectClassName = in.getObjectName();
        memoryx::PersistentObjectClassSegmentBasePrx classesSegmentPrx = c->getPriorKnowledgeProxy()->getObjectClassesSegment();

        memoryx::EntityBasePtr classesEntity = classesSegmentPrx->getEntityByName(objectClassName);
        if (!classesEntity)
        {
            ARMARX_ERROR_S << "No memory entity found with name " << objectClassName;
        }
        memoryx::ObjectClassBasePtr objectClass = memoryx::ObjectClassBasePtr::dynamicCast(classesEntity);
        if (!objectClass)
        {
            ARMARX_ERROR_S << "Could not cast entitiy to object class, name: " << objectClassName;
        }

        PosePtr pose = in.getObjectPose();
        srand(IceUtil::Time::now().toMilliSeconds());
    //        pose->position->y += -200 + (double)(rand()) / RAND_MAX * 400;
        if (!c->getSimulatorProxy()->hasObject(objectClassName))
        {
            ARMARX_IMPORTANT_S << "Adding object " << objectClassName << " at pose:" << *pose;
            c->getSimulatorProxy()->addObject(objectClass, objectClassName, pose, false);
        }
    }*/
}



void TestBringObjectWithArmSelection::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)

}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr TestBringObjectWithArmSelection::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new TestBringObjectWithArmSelection(stateData));
}

