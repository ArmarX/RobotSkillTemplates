/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2014-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::BringObjectGroup
 * @author     Mirko Waechter ( mirko dot waechter at kit dot edu )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include <ArmarXCore/core/time/TimeUtil.h>
#include "VerifyObjectPosition.h"

using namespace armarx;
using namespace BringObjectGroup;

// DO NOT EDIT NEXT LINE
VerifyObjectPosition::SubClassRegistry VerifyObjectPosition::Registry(VerifyObjectPosition::GetName(), &VerifyObjectPosition::CreateInstance);



void VerifyObjectPosition::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)

    // put your user code for the enter-point here
    // execution time should be short (<100ms)

}

void VerifyObjectPosition::run()
{
    float uncertainty = in.getObjectInstanceChannel()->get<float>("uncertaintyOfPosition");
    IceUtil::Time start = TimeUtil::GetTime();
    ARMARX_INFO << "Current position uncertainty " << uncertainty << " (Maximum: " << in.getMaximumPositionUncertainty() << ") ";

    while ((TimeUtil::GetTime() - start).toMilliSeconds() < in.getTimeout() &&
           !isRunningTaskStopped() && uncertainty > in.getMaximumPositionUncertainty()
          )
    {
        TimeUtil::MSSleep(10);
        uncertainty = in.getObjectInstanceChannel()->get<float>("uncertaintyOfPosition");
        ARMARX_INFO << deactivateSpam(0.3) << "Current position uncertainty: " << uncertainty;
    }

    if (uncertainty > in.getMaximumPositionUncertainty())
    {
        emitFailure();
    }
    else
    {
        emitPositionCertain();
    }
}

//void VerifyObjectPosition::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void VerifyObjectPosition::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr VerifyObjectPosition::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new VerifyObjectPosition(stateData));
}

