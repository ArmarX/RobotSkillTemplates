/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2014-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::BringObjectGroup
 * @author     Mirko Waechter ( waechter at kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include <ArmarXCore/core/time/TimeUtil.h>
#include "VerifyObjectExistenceCertaintyCalc.h"

using namespace armarx;
using namespace BringObjectGroup;

// DO NOT EDIT NEXT LINE
VerifyObjectExistenceCertaintyCalc::SubClassRegistry VerifyObjectExistenceCertaintyCalc::Registry(VerifyObjectExistenceCertaintyCalc::GetName(), &VerifyObjectExistenceCertaintyCalc::CreateInstance);



VerifyObjectExistenceCertaintyCalc::VerifyObjectExistenceCertaintyCalc(const XMLStateConstructorParams& stateData) :
    XMLStateTemplate<VerifyObjectExistenceCertaintyCalc>(stateData),  VerifyObjectExistenceCertaintyCalcGeneratedBase<VerifyObjectExistenceCertaintyCalc>(stateData)
{
}

void VerifyObjectExistenceCertaintyCalc::run()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
    float certainty = in.getObjectInstanceChannel()->get<float>("existenceCertainty");
    IceUtil::Time start = TimeUtil::GetTime();
    ARMARX_INFO << "Current certainty " << certainty << " (Minimum: " << in.getMinimumObjectExistenceCertainty() << ") ";

    while ((TimeUtil::GetTime() - start).toMilliSeconds() < in.getTimeout() &&
           !isRunningTaskStopped() &&
           in.getObjectInstanceChannel()->get<float>("existenceCertainty") < in.getMinimumObjectExistenceCertainty())
    {
        TimeUtil::MSSleep(10);
        ARMARX_INFO << deactivateSpam(0.3) << "existenceCertainty: " << in.getObjectInstanceChannel()->get<float>("existenceCertainty");
    }

    if (certainty >= in.getMinimumObjectExistenceCertainty())
    {
        emitObjectCertain();
    }
    else
    {
        emitObjectUncertain();
    }

}


void VerifyObjectExistenceCertaintyCalc::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)

}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr VerifyObjectExistenceCertaintyCalc::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new VerifyObjectExistenceCertaintyCalc(stateData));
}

