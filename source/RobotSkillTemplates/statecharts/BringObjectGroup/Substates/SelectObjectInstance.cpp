/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2014-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::BringObjectGroup
 * @author     Mirko Waechter ( mirko dot waechter at kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "SelectObjectInstance.h"

using namespace armarx;
using namespace BringObjectGroup;

// DO NOT EDIT NEXT LINE
SelectObjectInstance::SubClassRegistry SelectObjectInstance::Registry(SelectObjectInstance::GetName(), &SelectObjectInstance::CreateInstance);



SelectObjectInstance::SelectObjectInstance(XMLStateConstructorParams stateData) :
    XMLStateTemplate<SelectObjectInstance>(stateData),  SelectObjectInstanceGeneratedBase<SelectObjectInstance>(stateData)
{
}

void SelectObjectInstance::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
    auto channels = in.getAvailableObjectInstances();
    auto className = in.getRequestedObjectClassName();

    for (ChannelRefPtr& channel : channels)
    {
        if (className  == channel->getDataField("className")->getString())
        {
            out.setSelectObjectInstanceChannel(channel);
            emitObjectSelected();
            return;
        }
    }

    emitRequestedObjectNotFound();
}



void SelectObjectInstance::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)

}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr SelectObjectInstance::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new SelectObjectInstance(stateData));
}

