/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2014-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::BringObjectGroup
 * @author     Mirko Waechter ( mirko dot waechter at kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "SelectArmAndGraspObject.h"
#include "BringObjectGroupStatechartContext.h"

#include <RobotAPI/libraries/core/FramedPose.h>
#include <ArmarXCore/core/time/TimeUtil.h>

using namespace armarx;
using namespace BringObjectGroup;

// DO NOT EDIT NEXT LINE
SelectArmAndGraspObject::SubClassRegistry SelectArmAndGraspObject::Registry(SelectArmAndGraspObject::GetName(), &SelectArmAndGraspObject::CreateInstance);



SelectArmAndGraspObject::SelectArmAndGraspObject(XMLStateConstructorParams stateData) :
    XMLStateTemplate<SelectArmAndGraspObject>(stateData),  SelectArmAndGraspObjectGeneratedBase<SelectArmAndGraspObject>(stateData)
{
}

void SelectArmAndGraspObject::onEnter()
{


    BringObjectGroupStatechartContext* c = getContext<BringObjectGroupStatechartContext>();
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
    FramedPositionPtr pos = in.getObjectInstanceChannel()->getDataField("position")->get<FramedPosition>();
    c->getViewSelection()->addManualViewTarget(pos);
    TimeUtil::MSSleep(2000);

    //    if(!in.isObjectInstanceChannelSet())
    //    local.setObjectName(in.getObjectInstanceChannel()->getDataField("className")->getString());

    // get memory channel for the object
    //    std::string objectName = in.getObjectName();
    //    ChannelRefBasePtr objectClassMemoryChannel = c->getObjectMemoryObserverProxy()->requestObjectClassOnce(objectName);
    //    if (objectClassMemoryChannel)
    //    {
    //        local.setObjectClassChannel(ChannelRefPtr::dynamicCast(objectClassMemoryChannel));
    //    }
    //    else
    //    {
    ////        emitFailure();
    //        ARMARX_WARNING << "Unknown Object Class: " << objectName;
    //    }
}





void SelectArmAndGraspObject::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)


}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr SelectArmAndGraspObject::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new SelectArmAndGraspObject(stateData));
}

