/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2014-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::BringObjectGroup
 * @author     Mirko Waechter ( mirko dot waechter at kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "BringObjectGroupStatechartContext.h"

#include <RobotAPI/libraries/core/remoterobot/RemoteRobot.h>
#include <RobotAPI/interface/core/FramedPoseBase.h>
#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/system/ImportExportComponent.h>
#include <ArmarXCore/statechart/Statechart.h>



namespace armarx::BringObjectGroup
{
    void BringObjectGroupStatechartContext::onInitStatechartContext()
    {
        // Register dependencies
        //        usingProxy(getProperty<std::string>("KinematicUnitName").getValue());
        //        usingProxy(getProperty<std::string>("KinematicUnitObserverName").getValue());
        usingProxy(getProperty<std::string>("RobotStateComponentName").getValue());
        //        usingProxy(getProperty<std::string>("TCPControlUnitName").getValue());
        usingProxy(getProperty<std::string>("WorkingMemoryName").getValue());
        usingProxy(getProperty<std::string>("ObjectMemoryObserverName").getValue());
        usingProxy(getProperty<std::string>("PriorKnowledgeName").getValue());
        usingProxy(getProperty<std::string>("ViewSelectionName").getValue());
        //            usingProxy(getProperty<std::string>("PathPlannerName").getValue());

        offeringTopic("DebugDrawerUpdates");

    }


    void BringObjectGroupStatechartContext::onConnectStatechartContext()
    {

        // retrieve proxies
        robotStateComponent = getProxy<RobotStateComponentInterfacePrx>(getProperty<std::string>("RobotStateComponentName").getValue());
        //        kinematicUnitPrx = getProxy<KinematicUnitInterfacePrx>(getProperty<std::string>("KinematicUnitName").getValue());
        //        kinematicUnitObserverPrx = getProxy<KinematicUnitObserverInterfacePrx>(getProperty<std::string>("KinematicUnitObserverName").getValue());
        //        tcpControlPrx = getProxy<TCPControlUnitInterfacePrx>(getProperty<std::string>("TCPControlUnitName").getValue());

        workingMemoryProxy = getProxy<memoryx::WorkingMemoryInterfacePrx>(getProperty<std::string>("WorkingMemoryName").getValue());
        objectMemoryObserverProxy = getProxy<memoryx::ObjectMemoryObserverInterfacePrx>(getProperty<std::string>("ObjectMemoryObserverName").getValue());
        priorKnowledgeProxy = getProxy<memoryx::PriorKnowledgeInterfacePrx>(getProperty<std::string>("PriorKnowledgeName").getValue());
        viewSelection = getProxy<ViewSelectionInterfacePrx>(getProperty<std::string>("ViewSelectionName").getValue());
        //            pathPlannerPrx = getProxy<PathPlannerBasePrx>(getProperty<std::string>("PriorPathPlannerNameKnowledgeName").getValue());


        //        // initialize remote robot
        remoteRobot.reset(new RemoteRobot(robotStateComponent->getSynchronizedRobot()));

        debugDrawerTopicProxy = getTopic<armarx::DebugDrawerInterfacePrx>("DebugDrawerUpdates");
        /*try
        {
            simulatorPrx = getProxy<SimulatorInterfacePrx>("Simulator");
        }
        catch(...)
        {
            ARMARX_WARNING << "Could not get simulator proxy";
        }*/
    }

    PropertyDefinitionsPtr BringObjectGroupStatechartContext::createPropertyDefinitions()
    {
        return PropertyDefinitionsPtr(new BringObjectGroupStatechartContextProperties(
                                          getConfigIdentifier()));
    }
}


