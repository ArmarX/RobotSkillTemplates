armarx_component_set_name("TrackingGroup")

set(COMPONENT_LIBS
#   RobotAPIInterfaces RobotAPIRemoteRobot
    ArmarXCoreInterfaces ArmarXCore ArmarXCoreStatechart ArmarXCoreObservers)

# Sources

set(SOURCES
TrackingGroupRemoteStateOfferer.cpp
TrackingGroupStatechartContext.cpp
./TrackObject.cpp
substates/DetectObject.cpp
substates/LookAtObject.cpp
#@TEMPLATE_LINE@@COMPONENT_PATH@/@COMPONENT_NAME@.cpp
)

set(HEADERS
TrackingGroupRemoteStateOfferer.h
TrackingGroup.scgxml
TrackingGroupStatechartContext.h
./TrackObject.h
substates/DetectObject.h
substates/LookAtObject.h
#@TEMPLATE_LINE@@COMPONENT_PATH@/@COMPONENT_NAME@.h
#@TEMPLATE_LINE@@COMPONENT_PATH@/@COMPONENT_NAME@.xml
./TrackObject.xml
substates/DetectObject.xml
substates/LookAtObject.xml
)

armarx_add_component("${SOURCES}" "${HEADERS}")
