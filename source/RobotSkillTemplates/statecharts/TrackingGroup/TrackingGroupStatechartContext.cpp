/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2014-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::TrackingGroup
 * @author     Manfred Kroehnert ( Manfred dot Kroehnert at kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "TrackingGroupStatechartContext.h"

//#include <RobotAPI/libraries/core/remoterobot/RemoteRobot.h>
//#include <RobotAPI/libraries/core/Pose.h>

#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/system/ImportExportComponent.h>
#include <ArmarXCore/statechart/Statechart.h>



namespace armarx::TrackingGroup
{
    void TrackingGroupStatechartContext::onInitStatechartContext()
    {
        // Register dependencies
        //        usingProxy(getProperty<std::string>("KinematicUnitName").getValue());
        //        usingProxy(getProperty<std::string>("KinematicUnitObserverName").getValue());
        //        usingProxy(getProperty<std::string>("RobotStateComponentName").getValue());
        //        usingProxy(getProperty<std::string>("TCPControlUnitName").getValue());
    }


    void TrackingGroupStatechartContext::onConnectStatechartContext()
    {

        // retrieve proxies
        //        robotStateComponent = getProxy<RobotStateComponentInterfacePrx>(getProperty<std::string>("RobotStateComponentName").getValue());
        //        kinematicUnitPrx = getProxy<KinematicUnitInterfacePrx>(getProperty<std::string>("KinematicUnitName").getValue());
        //        kinematicUnitObserverPrx = getProxy<KinematicUnitObserverInterfacePrx>(getProperty<std::string>("KinematicUnitObserverName").getValue());
        //        tcpControlPrx = getProxy<TCPControlUnitInterfacePrx>(getProperty<std::string>("TCPControlUnitName").getValue());

        //        // initialize remote robot
        //        remoteRobot.reset(new RemoteRobot(robotStateComponent->getSynchronizedRobot()));
    }

    PropertyDefinitionsPtr TrackingGroupStatechartContext::createPropertyDefinitions()
    {
        return PropertyDefinitionsPtr(new TrackingGroupStatechartContextProperties(
                                          getConfigIdentifier()));
    }
}
