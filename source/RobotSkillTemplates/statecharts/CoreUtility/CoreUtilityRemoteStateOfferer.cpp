/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::CoreUtility::CoreUtilityRemoteStateOfferer
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "CoreUtilityRemoteStateOfferer.h"

using namespace armarx;
using namespace CoreUtility;

// DO NOT EDIT NEXT LINE
CoreUtilityRemoteStateOfferer::SubClassRegistry CoreUtilityRemoteStateOfferer::Registry(CoreUtilityRemoteStateOfferer::GetName(), &CoreUtilityRemoteStateOfferer::CreateInstance);



CoreUtilityRemoteStateOfferer::CoreUtilityRemoteStateOfferer(StatechartGroupXmlReaderPtr reader) :
    XMLRemoteStateOfferer < CoreUtilityStatechartContext > (reader)
{
}

void CoreUtilityRemoteStateOfferer::onInitXMLRemoteStateOfferer()
{

}

void CoreUtilityRemoteStateOfferer::onConnectXMLRemoteStateOfferer()
{

}

void CoreUtilityRemoteStateOfferer::onExitXMLRemoteStateOfferer()
{

}

// DO NOT EDIT NEXT FUNCTION
std::string CoreUtilityRemoteStateOfferer::GetName()
{
    return "CoreUtilityRemoteStateOfferer";
}

// DO NOT EDIT NEXT FUNCTION
XMLStateOffererFactoryBasePtr CoreUtilityRemoteStateOfferer::CreateInstance(StatechartGroupXmlReaderPtr reader)
{
    return XMLStateOffererFactoryBasePtr(new CoreUtilityRemoteStateOfferer(reader));
}



