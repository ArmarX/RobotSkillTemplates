/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2014-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::GraspObjectGroup
 * @author     David ( david dot schiebener at kit dot edu )
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "VisualServoTowardsObject.h"

#include "VisualServoGroupStatechartContext.generated.h"


// MemoryX
#include <MemoryX/core/GridFileManager.h>
#include <MemoryX/core/MemoryXCoreObjectFactories.h>
#include <MemoryX/libraries/memorytypes/entity/ObjectClass.h>
#include <MemoryX/libraries/helpers/VirtualRobotHelpers/SimoxObjectWrapper.h>
#include <MemoryX/libraries/memorytypes/MemoryXTypesObjectFactories.h>

// Simox
#include <VirtualRobot/ManipulationObject.h>
#include <VirtualRobot/Grasping/Grasp.h>
#include <VirtualRobot/Grasping/GraspSet.h>
#include <VirtualRobot/XML/ObjectIO.h>



using namespace armarx;
using namespace VisualServoGroup;


// DO NOT EDIT NEXT LINE
VisualServoTowardsObject::SubClassRegistry VisualServoTowardsObject::Registry(VisualServoTowardsObject::GetName(), &VisualServoTowardsObject::CreateInstance);



VisualServoTowardsObject::VisualServoTowardsObject(XMLStateConstructorParams stateData) :
    XMLStateTemplate < VisualServoTowardsObject > (stateData),
    VisualServoTowardsObjectGeneratedBase < VisualServoTowardsObject > (stateData)
{
}


void VisualServoTowardsObject::onEnter()
{
    VisualServoGroupStatechartContext* context = getContext<VisualServoGroupStatechartContext>();
    local.setStartTimeRef(ChannelRefPtr::dynamicCast(context->systemObserverPrx->startTimer("VisualServoTowardsObjectStartTime")));

    // get grasp definition
    std::string objectName;
    memoryx::PersistentObjectClassSegmentBasePrx classesSegmentPrx = context->getPriorKnowledge()->getObjectClassesSegment();
    memoryx::CommonStorageInterfacePrx databasePrx = context->getPriorKnowledge()->getCommonStorage();
    memoryx::GridFileManagerPtr fileManager(new memoryx::GridFileManager(databasePrx));
    memoryx::EntityBasePtr entity;
    if (in.isPreselectedObjectInstanceChannelSet())
    {
        ARMARX_INFO << "Using preselected instance " << in.getPreselectedObjectInstanceChannel()->output();
        objectName = in.getPreselectedObjectInstanceChannel()->getDataField("className")->getString();
    }
    else
    {
        objectName = in.getObjectMemoryChannel()->getDataField("className")->getString();
    }
    entity = classesSegmentPrx->getEntityByName(objectName);
    auto chainName = in.getKinematicChainName();

    memoryx::ObjectClassPtr objectClass = memoryx::ObjectClassPtr::dynamicCast(entity);
    memoryx::EntityWrappers::SimoxObjectWrapperPtr simoxWrapper = objectClass->addWrapper(new memoryx::EntityWrappers::SimoxObjectWrapper(fileManager));
    VirtualRobot::ManipulationObjectPtr mo = simoxWrapper->getManipulationObject();

    auto tcpObjectClass = classesSegmentPrx->getObjectClassByName(in.getHandNameInMemory());
    if (tcpObjectClass)
    {
        context->getEntityDrawerTopic()->setObjectVisu("VisualServoHandVisu", "tcpTarget_" + chainName, tcpObjectClass, new Pose());
        context->getEntityDrawerTopic()->updateObjectTransparency("VisualServoHandVisu", "tcpTarget_" + chainName, 0.3);
    }
    else
    {
        ARMARX_IMPORTANT << "Could not get objectclass for tcp " << in.getHandNameInMemory() << " for visu purposes";
    }
    Eigen::Matrix4f transformationFromObjectToTCPPose = Eigen::Matrix4f::Identity();

    std::string graspSetName = getInput<std::string>("GraspSetName");

    if (mo->getGraspSet(graspSetName))
    {
        std::string graspName = getInput<std::string>("GraspDefinitionName");
        VirtualRobot::GraspPtr grasp = mo->getGraspSet(graspSetName)->getGrasp(graspName);

        if (!grasp)
        {
            ARMARX_ERROR << "No grasp with name " << graspName << " found! ";
            return;
        }
        else
        {
            transformationFromObjectToTCPPose = grasp->getTransformation().inverse();
            ARMARX_IMPORTANT << "Successfully loaded " << graspName << "";
            ARMARX_VERBOSE << "Grasp " << graspName << " has value:\n" << grasp->getTransformation();
        }
    }
    else
    {
        ARMARX_ERROR << "No grasp set with name " << graspSetName << " found! ";
        return;
    }

    Eigen::Matrix4f desiredTcpOffsetToObjectEigen = transformationFromObjectToTCPPose;
    PosePtr desiredTcpOffsetToObject = new Pose(desiredTcpOffsetToObjectEigen);
    setLocal("DesiredTcpOffsetToObject", *desiredTcpOffsetToObject);

    // request IK units
    ARMARX_VERBOSE << "Requesting TCP control unit";
    context->getTCPControlUnit()->request();
    ARMARX_VERBOSE << "Requested TCP control unit";
}







void VisualServoTowardsObject::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)

    VisualServoGroupStatechartContext* context = getContext<VisualServoGroupStatechartContext>();
    ARMARX_VERBOSE << "Releasing TCP control unit";
    context->getTCPControlUnit()->release();
    ARMARX_VERBOSE << "Released TCP control unit";

    // this pose was set in GetObjectPose
    context->getDebugDrawerTopic()->removePoseDebugLayerVisu("objectPose");
    context->systemObserverPrx->removeTimer(local.getStartTimeRef());
    context->getEntityDrawerTopic()->removeLayer("VisualServoHandVisu");
}



// DO NOT EDIT NEXT FUNCTION
std::string VisualServoTowardsObject::GetName()
{
    return "VisualServoTowardsObject";
}



// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr VisualServoTowardsObject::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new VisualServoTowardsObject(stateData));
}

