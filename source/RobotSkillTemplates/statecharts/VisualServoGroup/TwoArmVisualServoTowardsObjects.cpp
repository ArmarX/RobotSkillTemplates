/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2014-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::VisualServoGroup
 * @author     Markus Grotz ( markus dot grotz at kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "TwoArmVisualServoTowardsObjects.h"



// MemoryX
#include <MemoryX/core/GridFileManager.h>
#include <MemoryX/core/MemoryXCoreObjectFactories.h>
#include <MemoryX/libraries/memorytypes/entity/ObjectClass.h>
#include <MemoryX/libraries/helpers/VirtualRobotHelpers/SimoxObjectWrapper.h>
#include <MemoryX/libraries/memorytypes/MemoryXTypesObjectFactories.h>

// Simox
#include <VirtualRobot/Grasping/Grasp.h>
#include <VirtualRobot/Grasping/GraspSet.h>
#include <VirtualRobot/XML/ObjectIO.h>

using namespace armarx;
using namespace VisualServoGroup;

// DO NOT EDIT NEXT LINE
TwoArmVisualServoTowardsObjects::SubClassRegistry TwoArmVisualServoTowardsObjects::Registry(TwoArmVisualServoTowardsObjects::GetName(), &TwoArmVisualServoTowardsObjects::CreateInstance);



TwoArmVisualServoTowardsObjects::TwoArmVisualServoTowardsObjects(XMLStateConstructorParams stateData) :
    XMLStateTemplate<TwoArmVisualServoTowardsObjects>(stateData),  TwoArmVisualServoTowardsObjectsGeneratedBase<TwoArmVisualServoTowardsObjects>(stateData)
{
}


VirtualRobot::ManipulationObjectPtr TwoArmVisualServoTowardsObjects::getObjectPtr(std::string objectName, VisualServoGroupStatechartContext* context)
{

    memoryx::PersistentObjectClassSegmentBasePrx classesSegmentPrx = context->getPriorKnowledge()->getObjectClassesSegment();
    memoryx::CommonStorageInterfacePrx databasePrx = context->getPriorKnowledge()->getCommonStorage();
    memoryx::GridFileManagerPtr fileManager(new memoryx::GridFileManager(databasePrx));
    memoryx::EntityBasePtr entity = classesSegmentPrx->getEntityByName(objectName);
    memoryx::ObjectClassPtr objectClass = memoryx::ObjectClassPtr::dynamicCast(entity);
    memoryx::EntityWrappers::SimoxObjectWrapperPtr simoxWrapper = objectClass->addWrapper(new memoryx::EntityWrappers::SimoxObjectWrapper(fileManager));

    return simoxWrapper->getManipulationObject();
}



PosePtr TwoArmVisualServoTowardsObjects::getDesiredTcpOffsetToObject(std::string graspSetName, std::string graspName, VirtualRobot::ManipulationObjectPtr mo)
{

    Eigen::Matrix4f transformationFromObjectToTCPPose = Eigen::Matrix4f::Identity();

    if (mo->getGraspSet(graspSetName))
    {
        VirtualRobot::GraspPtr grasp = mo->getGraspSet(graspSetName)->getGrasp(graspName);

        if (!grasp)
        {
            ARMARX_WARNING << "No grasp with name " << graspName << " found! ";
        }
        else
        {
            transformationFromObjectToTCPPose = grasp->getTransformation().inverse();
            ARMARX_IMPORTANT << "Successfully loaded " << graspName << "";
            ARMARX_VERBOSE << "Grasp " << graspName << " has value:\n" << grasp->getTransformation();
        }
    }
    else
    {
        ARMARX_WARNING << "No grasp set with name " << graspSetName << " found! ";
    }

    Eigen::Matrix4f desiredTcpOffsetToObjectEigen = transformationFromObjectToTCPPose;


    return new Pose(desiredTcpOffsetToObjectEigen);
}


void TwoArmVisualServoTowardsObjects::onEnter()
{
    // get grasp definition
    VisualServoGroupStatechartContext* context = getContext<VisualServoGroupStatechartContext>();

    VirtualRobot::ManipulationObjectPtr leftObjectPtr = getObjectPtr(in.getLeftObjectName(), context);
    VirtualRobot::ManipulationObjectPtr rightObjectPtr = getObjectPtr(in.getRightObjectName(), context);

    PosePtr leftDesiredTcpOffsetToObject = getDesiredTcpOffsetToObject(in.getLeftGraspSetName(), in.getLeftGraspDefinitionName(), leftObjectPtr);
    PosePtr rightDesiredTcpOffsetToObject = getDesiredTcpOffsetToObject(in.getRightGraspSetName(), in.getRightGraspDefinitionName(), rightObjectPtr);

    local.setLeftDesiredTcpOffsetToObject(leftDesiredTcpOffsetToObject);
    local.setRightDesiredTcpOffsetToObject(rightDesiredTcpOffsetToObject);

    // request IK units
    //ARMARX_VERBOSE << "Requesting HeadIK unit";
    //context->getHeadIKUnit()->request();
    ARMARX_VERBOSE << "Requesting TCP control unit";
    context->getTCPControlUnit()->request();
    ARMARX_VERBOSE << "Requested TCP control unit";
}


void TwoArmVisualServoTowardsObjects::onExit()
{

    VisualServoGroupStatechartContext* context = getContext<VisualServoGroupStatechartContext>();
    ARMARX_VERBOSE << "Releasing TCP control unit";
    context->getTCPControlUnit()->release();
    ARMARX_VERBOSE << "Released TCP control unit";
    //ARMARX_VERBOSE << "Releasing HeadIK unit";
    //context->getHeadIKUnit()->release();
    //ARMARX_VERBOSE << "Released HeadIK unit";

}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr TwoArmVisualServoTowardsObjects::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new TwoArmVisualServoTowardsObjects(stateData));
}

