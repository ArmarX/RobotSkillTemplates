/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2014-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::GraspObjectGroup
 * @author     David ( david dot schiebener at kit dot edu )
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "LookBetweenHandAndObject.h"

#include "VisualServoGroupStatechartContext.generated.h"


using namespace armarx;
using namespace VisualServoGroup;


// DO NOT EDIT NEXT LINE
LookBetweenHandAndObject::SubClassRegistry LookBetweenHandAndObject::Registry(LookBetweenHandAndObject::GetName(), &LookBetweenHandAndObject::CreateInstance);



LookBetweenHandAndObject::LookBetweenHandAndObject(XMLStateConstructorParams stateData) :
    XMLStateTemplate < LookBetweenHandAndObject > (stateData)
{
}



void LookBetweenHandAndObject::onEnter()
{
    //    VisualServoGroupStatechartContext* context = getContext<VisualServoGroupStatechartContext>();

    //    FramedPosePtr objectPose = getInput<FramedPose>("TcpTargetPose");
    //    FramedPosePtr handPose = getInput<FramedPose>("HandPose");

    //    Eigen::Vector3f objectPosition, handPosition, viewTarget;
    //    objectPosition << objectPose->position->x, objectPose->position->y, objectPose->position->z;
    //    handPosition << handPose->position->x, handPose->position->y, handPose->position->z;
    //    if (objectPose->frame.compare(handPose->frame) != 0)
    //    {
    //        ARMARX_WARNING << "Hand and object pose are in different frames. Add code here to handle this!";
    //    }
    //    const float interpolationFactor = 0.4f;
    //    viewTarget = interpolationFactor*objectPosition + (1-interpolationFactor)*handPosition;

    //    FramedDirectionPtr viewTargetPositionPtr = new FramedDirection(viewTarget, objectPose->frame, objectPose->agent);
    //    std::string headIKKinematicChainName = getInput<std::string>("HeadIKKinematicChainName");

    //    //context->getHeadIKUnitProxy()->setHeadTarget(headIKKinematicChainName, viewTargetPositionPtr);
    //    FramedPosePtr viewPose = new FramedPose(Eigen::Matrix3f::Identity(), viewTargetPositionPtr->toEigen(),viewTargetPositionPtr->frame, objectPose->agent);
    //    PoseBasePtr globalViewTarget = context->getWorkingMemoryProxy()->getAgentInstancesSegment()->convertToWorldPose("Armar3", viewPose);
    //    context->getDebugDrawerTopicProxy()->setPoseDebugLayerVisu("viewTarget", globalViewTarget);


    //        FramedPosition viewTargetPosition(viewTarget, objectPose->frame);
    //        setLocal("target", viewTargetPosition);
    //        setLocal("jointTargetTolerance", 0.2f);
    //        setLocal("timeoutInMs", 3000);

    sendEvent<HeadTargetSet>();
}






void LookBetweenHandAndObject::onBreak()
{
    // put your user code for the breaking point here
    // execution time should be short (<100ms)
}



void LookBetweenHandAndObject::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)

}



// DO NOT EDIT NEXT FUNCTION
std::string LookBetweenHandAndObject::GetName()
{
    return "LookBetweenHandAndObject";
}



// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr LookBetweenHandAndObject::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new LookBetweenHandAndObject(stateData));
}

