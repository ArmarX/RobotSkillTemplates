/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2014-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::VisualServoGroup
 * @author     Markus Grotz ( markus dot grotz at kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "TwoArmVisualServoTowardsTargetPose.h"
#include "VisualServoGroupStatechartContext.generated.h"

#include <ArmarXCore/core/time/TimeUtil.h>
#include <RobotAPI/interface/observers/ObserverFilters.h>

#include <IceUtil/UUID.h>

using namespace armarx;
using namespace VisualServoGroup;

// DO NOT EDIT NEXT LINE
TwoArmVisualServoTowardsTargetPose::SubClassRegistry TwoArmVisualServoTowardsTargetPose::Registry(TwoArmVisualServoTowardsTargetPose::GetName(), &TwoArmVisualServoTowardsTargetPose::CreateInstance);



TwoArmVisualServoTowardsTargetPose::TwoArmVisualServoTowardsTargetPose(XMLStateConstructorParams stateData) :
    XMLStateTemplate<TwoArmVisualServoTowardsTargetPose>(stateData),  TwoArmVisualServoTowardsTargetPoseGeneratedBase<TwoArmVisualServoTowardsTargetPose>(stateData)
{
}

void TwoArmVisualServoTowardsTargetPose::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)

    if (in.isStartTimeRefSet())
    {
        ARMARX_INFO << "Using existing StartTime for Two Arm Visual Servo.";
        local.setTimerCreated(false);
    }
    else
    {
        ARMARX_INFO << "Creating new StartTime for Two Arm Visual Servo.";
        in.setStartTimeRef(getContext<StatechartContext>()->systemObserverPrx->startTimer("TwoArmVisualServoTimer_" + IceUtil::generateUUID()));
        local.setTimerCreated(true);
    }

    VisualServoGroupStatechartContext* context = getContext<VisualServoGroupStatechartContext>();
    if (context->getTCPControlUnit()->isRequested())
    {
        ARMARX_INFO << "TCPControlUnit is already requested.";
        local.setTCPControlUnitRequested(false);
    }
    else
    {
        ARMARX_INFO << "Requesting TCPControlUnit.";
        context->getTCPControlUnit()->request();
        local.setTCPControlUnitRequested(true);
    }

    std::string leftTcpName = context->getRobot()->getRobotNodeSet(in.getLeftHandKinematicChainName())->getTCP()->getName();
    local.setLeftTcpName(leftTcpName);

    std::string rightTcpName = context->getRobot()->getRobotNodeSet(in.getRightHandKinematicChainName())->getTCP()->getName();
    local.setRightTcpName(rightTcpName);

    local.setLeftTargetReached(false);
    local.setRightTargetReached(false);

    memoryx::ChannelRefBaseSequence instancesLeft = context->getObjectMemoryObserver()->getObjectInstances(in.getLeftHandMemoryChannel());
    memoryx::ChannelRefBaseSequence instancesRight = context->getObjectMemoryObserver()->getObjectInstances(in.getRightHandMemoryChannel());
    DataFieldIdentifierPtr leftPosId;
    DataFieldIdentifierPtr rightPosId;

    if (instancesLeft.size() == 0)
    {
        ARMARX_WARNING << "No instances of the hand in the memory: " << in.getLeftHandMemoryChannel()->get<std::string>("className");
        auto list = context->getObjectMemoryObserver()->getObjectInstancesByClass(in.getLeftHandMemoryChannel()->get<std::string>("className"));

        for (auto& entry : list)
        {
            ARMARX_INFO << "obj: " << ChannelRefPtr::dynamicCast(entry)->getDataField("className")->getString();
        }
        return;
    }
    else
    {
        leftPosId = ChannelRefPtr::dynamicCast(instancesLeft.front())->getDataFieldIdentifier("position");
    }

    if (instancesRight.size() == 0)
    {
        ARMARX_WARNING << "No instances of the hand in the memory: " << in.getRightHandMemoryChannel()->get<std::string>("className");
        auto list = context->getObjectMemoryObserver()->getObjectInstancesByClass(in.getRightHandMemoryChannel()->get<std::string>("className"));

        for (auto& entry : list)
        {
            ARMARX_INFO << "obj: " << ChannelRefPtr::dynamicCast(entry)->getDataField("className")->getString();
        }
        return;
    }
    else
    {
        rightPosId = ChannelRefPtr::dynamicCast(instancesRight.front())->getDataFieldIdentifier("position");
    }

    //    auto leftPosId = in.getLeftHandMemoryChannel()->getDataFieldIdentifier("position");
    //    auto rightPosId = in.getRightHandMemoryChannel()->getDataFieldIdentifier("position");

    local.setPoseUpdateTimerChannelRef(ChannelRefPtr::dynamicCast(context->systemObserverPrx->startTimer("objectPoseUpdatedTimer")));
    auto elapsed = local.getPoseUpdateTimerChannelRef()->getDataFieldIdentifier("elapsedMs");
    if (in.getWatchObjectPoseUpdates())
    {
        Literal left(leftPosId, checks::updated);
        Literal right(rightPosId, checks::updated);
        Literal minDelay(elapsed, checks::larger, {100});
        installConditionForObjectPoseUpdated((left || right) && minDelay);
    }

    if (in.isRightHandForceFieldRefSet() && in.isLeftHandForceFieldRefSet())
    {
        Literal rightForceLiteral(in.getRightHandForceFieldRef()->getDataFieldIdentifier(), checks::magnitudelarger, Literal::createParameterList(in.getForceThreshold()));
        Literal leftForceLiteral(in.getLeftHandForceFieldRef()->getDataFieldIdentifier(), checks::magnitudelarger, Literal::createParameterList(in.getForceThreshold()));

        installConditionForForceThresholdExceeded(rightForceLiteral || leftForceLiteral);
    }
}

void TwoArmVisualServoTowardsTargetPose::run()
{
    while (!isRunningTaskStopped())
    {
        if (in.isRightHandForceFieldRefSet() && in.isLeftHandForceFieldRefSet())
        {
            ARMARX_INFO << deactivateSpam(0.5) << "Right Forces: " << in.getRightHandForceFieldRef()->get<FramedDirection>()->toEigen().norm();
            ARMARX_INFO << deactivateSpam(0.5) << "Left Forces: " << in.getLeftHandForceFieldRef()->get<FramedDirection>()->toEigen().norm();
        }
        else
        {
            ARMARX_INFO << deactivateSpam(5) << "No Force refs set!";
        }
        TimeUtil::MSSleep(100);
    }
}



void TwoArmVisualServoTowardsTargetPose::onExit()
{
    VisualServoGroupStatechartContext* context = getContext<VisualServoGroupStatechartContext>();
    context->getTCPControlUnit()->release();

    context->systemObserverPrx->removeTimer(local.getPoseUpdateTimerChannelRef());

    if (local.getTimerCreated())
    {
        ARMARX_INFO << "Removing timer.";
        getContext<StatechartContext>()->systemObserverPrx->removeTimer(in.getStartTimeRef());
    }
    if (local.getTCPControlUnitRequested())
    {
        ARMARX_INFO << "Releasing TCPControlUnit.";
        getContext<VisualServoGroupStatechartContext>()->getTCPControlUnit()->release();
    }
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr TwoArmVisualServoTowardsTargetPose::CreateInstance(XMLStateConstructorParams stateData)
{

    return XMLStateFactoryBasePtr(new TwoArmVisualServoTowardsTargetPose(stateData));
}

