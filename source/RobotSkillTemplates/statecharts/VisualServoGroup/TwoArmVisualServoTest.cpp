/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2014-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::VisualServoGroup
 * @author     Markus Grotz ( markus dot grotz at kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "TwoArmVisualServoTest.h"


#include <MemoryX/libraries/memorytypes/entity/ObjectClass.h>
#include <MemoryX/libraries/memorytypes/MemoryXTypesObjectFactories.h>
#include <MemoryX/core/MemoryXCoreObjectFactories.h>

#include <RobotAPI/interface/components/ViewSelectionInterface.h>


using namespace armarx;
using namespace VisualServoGroup;

#include <MemoryX/libraries/observers/ObjectMemoryObserver.h>

// DO NOT EDIT NEXT LINE
TwoArmVisualServoTest::SubClassRegistry TwoArmVisualServoTest::Registry(TwoArmVisualServoTest::GetName(), &TwoArmVisualServoTest::CreateInstance);



TwoArmVisualServoTest::TwoArmVisualServoTest(XMLStateConstructorParams stateData) :
    XMLStateTemplate<TwoArmVisualServoTest>(stateData),  TwoArmVisualServoTestGeneratedBase<TwoArmVisualServoTest>(stateData)
{
}


ChannelRefPtr TwoArmVisualServoTest::getChannelRef(std::string objectName)
{

    VisualServoGroupStatechartContext* context = getContext<VisualServoGroupStatechartContext>();
    ChannelRefBasePtr memoryChannel = context->getObjectMemoryObserver()->requestObjectClassRepeated(objectName, 250, armarx::DEFAULT_VIEWTARGET_PRIORITY);

    if (memoryChannel)
    {
        memoryChannel->validate();
        return ChannelRefPtr::dynamicCast(memoryChannel);
    }
    else
    {
        ARMARX_ERROR << "unable to get channel ref";
        return nullptr;
    }
}

void TwoArmVisualServoTest::onEnter()
{

    std::string leftObjectName = in.getLeftObjectName();
    local.setLeftObjectMemoryChannel(getChannelRef(leftObjectName));

    std::string rightObjectName = in.getRightObjectName();
    if (leftObjectName != rightObjectName)
    {
        local.setRightObjectMemoryChannel(getChannelRef(rightObjectName));
    }
    else
    {
        local.setRightObjectMemoryChannel(local.getLeftObjectMemoryChannel());
    }


    std::string leftHandNameInMemory = in.getLeftHandNameInMemory();
    local.setLeftHandMemoryChannel(getChannelRef(leftHandNameInMemory));

    std::string rightHandNameInMemory = in.getRightHandNameInMemory();
    local.setRightHandMemoryChannel(getChannelRef(rightHandNameInMemory));

    VisualServoGroupStatechartContext* context = getContext<VisualServoGroupStatechartContext>();


    // set first view directions manually for initial localization of hands and objects
    FramedPositionPtr viewTarget;
    std::string frameName = context->getRobotStateComponent()->getSynchronizedRobot()->getRootNode()->getName();
    std::string agentName = context->getRobotStateComponent()->getSynchronizedRobot()->getName();
    Eigen::Vector3f targetPos;
    targetPos(0) = 0;
    targetPos(1) = 700;
    targetPos(2) = 1100;
    viewTarget = new FramedPosition(targetPos, frameName, agentName);
    context->getViewSelection()->addManualViewTarget(viewTarget);
    targetPos(0) = 300;
    targetPos(1) = 500;
    targetPos(2) = 1100;
    viewTarget = new FramedPosition(targetPos, frameName, agentName);
    context->getViewSelection()->addManualViewTarget(viewTarget);
    targetPos(0) = 0;
    targetPos(1) = 700;
    targetPos(2) = 1100;
    viewTarget = new FramedPosition(targetPos, frameName, agentName);
    context->getViewSelection()->addManualViewTarget(viewTarget);
    targetPos(0) = -300;
    targetPos(1) = 500;
    targetPos(2) = 1100;
    viewTarget = new FramedPosition(targetPos, frameName, agentName);
    context->getViewSelection()->addManualViewTarget(viewTarget);

}



// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr TwoArmVisualServoTest::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new TwoArmVisualServoTest(stateData));
}

