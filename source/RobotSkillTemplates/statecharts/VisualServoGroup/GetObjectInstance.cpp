/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2014-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::VisualServoGroup
 * @author     Mirko Waechter ( waechter at kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "GetObjectInstance.h"
#include "VisualServoGroupStatechartContext.generated.h"

#include <ArmarXCore/core/time/TimeUtil.h>

using namespace armarx;
using namespace VisualServoGroup;

// DO NOT EDIT NEXT LINE
GetObjectInstance::SubClassRegistry GetObjectInstance::Registry(GetObjectInstance::GetName(), &GetObjectInstance::CreateInstance);



GetObjectInstance::GetObjectInstance(const XMLStateConstructorParams& stateData) :
    XMLStateTemplate<GetObjectInstance>(stateData),  GetObjectInstanceGeneratedBase<GetObjectInstance>(stateData)
{
}

void GetObjectInstance::onEnter()
{
    VisualServoGroupStatechartContext* context = getContext<VisualServoGroupStatechartContext>();


    ChannelRefPtr objectInstance;

    if (in.isObjectInstanceChannelSet())
    {
        objectInstance = in.getObjectInstanceChannel();
    }
    else if (in.isObjectClassChannelSet())
    {
        ChannelRefPtr objectMemoryChannel = in.getObjectClassChannel();

        if (!context->getObjectMemoryObserver())
        {
            ARMARX_WARNING << "getObjectMemoryObserver is NULL";
            setTimeoutEvent(500, createEventObjectNotYetLocalized());
            return;
        }

        memoryx::ChannelRefBaseSequence instances = context->getObjectMemoryObserver()->getObjectInstances(objectMemoryChannel);

        if (instances.size() == 0)
        {
            ARMARX_WARNING << "No instances of the object in the memory - obj channel: " << objectMemoryChannel->output();
            setTimeoutEvent(500, createEventObjectNotYetLocalized());
            return;
        }

        objectInstance = ChannelRefPtr::dynamicCast(instances.front());

    }
    else
    {
        throw LocalException("Either ObjectInstanceChannel or ObjectMemoryChannel must be set in " +  getStateClassName());
    }




    ARMARX_CHECK_EXPRESSION(objectInstance);
    out.setObjectInstanceChannel(objectInstance);
    ARMARX_INFO << "Checking existenceCertainty: " << objectInstance->getDataField("existenceCertainty")->getFloat() << " > " << in.getMinimumExistenceCertainty();
    ARMARX_INFO << "Checking positionUncertainty: " << objectInstance->get<float>("uncertaintyOfPosition") << " < " << in.getMaximalPositionUncertainty();

    float existenceCertainty = objectInstance->getDataField("existenceCertainty")->getFloat();
    if (existenceCertainty >= in.getMinimumExistenceCertainty())
    {
        float positionUncertainty = objectInstance->get<float>("uncertaintyOfPosition");

        if (existenceCertainty >= in.getGoodExistenceCertainty() && positionUncertainty <= in.getMaximalPositionUncertainty())
        {
            if (in.getRequestTCPControlUnit())
            {
                getTcpControlUnit()->request();
            }
            emitObjectInstanceChannelFound();
        }
        else
        {
            ARMARX_VERBOSE << "Waiting for better localization. Existence certainty: " << existenceCertainty << " (min: " << in.getGoodExistenceCertainty()
                           << "), position uncertainty: " << positionUncertainty << " (max: " << in.getMaximalPositionUncertainty() << ")";
            if (in.getRequestTCPControlUnit())
            {
                getTcpControlUnit()->release();
            }
            TimeUtil::MSSleep(100);
            emitWaitForBetterLocalization();
        }
    }
    else
    {
        ARMARX_WARNING << "Object was found earlier but is lost now: Existence certainty is only " << existenceCertainty;
        emitObjectNotFound();
    }

}

void GetObjectInstance::run()
{
    // put your user code for the execution-phase here
    // runs in seperate thread, thus can do complex operations
    // should check constantly whether isRunningTaskStopped() returns true

    // uncomment this if you need a continous run function. Make sure to use sleep or use blocking wait to reduce cpu load.
    //    while (!isRunningTaskStopped()) // stop run function if returning true
    //    {
    //        // do your calculations
    //    }

}

void GetObjectInstance::onBreak()
{
    // put your user code for the breaking point here
    // execution time should be short (<100ms)
}

void GetObjectInstance::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)

}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr GetObjectInstance::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new GetObjectInstance(stateData));
}

