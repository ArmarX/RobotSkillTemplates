/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2014-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::GraspObjectGroup
 * @author     David ( david dot schiebener at kit dot edu )
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "GetHandPose.h"

#include "VisualServoGroupStatechartContext.generated.h"

#include <ArmarXCore/core/time/TimeUtil.h>

using namespace armarx;
using namespace VisualServoGroup;


// DO NOT EDIT NEXT LINE
GetHandPose::SubClassRegistry GetHandPose::Registry(GetHandPose::GetName(), &GetHandPose::CreateInstance);



GetHandPose::GetHandPose(XMLStateConstructorParams stateData) :
    XMLStateTemplate < GetHandPose > (stateData),
    GetHandPoseGeneratedBase < GetHandPose > (stateData)
{
}



void GetHandPose::onEnter()
{
    VisualServoGroupStatechartContext* context = getContext<VisualServoGroupStatechartContext>();
    bool handLocalized = false;
    bool handPoseUncertain = false;

    // get hand pose from kinematic model
    FramedPosePtr handPoseFromKinematicModel = new FramedPose(context->getRobot()->getRobotNode(in.getTcpNameInRobotModel())->getPoseInRootFrame(),
            context->getRobot()->getRootNode()->getName(),
            context->getRobot()->getName());

    ARMARX_VERBOSE << "Hand pose from kinematic model: " << *handPoseFromKinematicModel;

    // get hand pose from MemoryX
    FramedPosePtr handPoseFromMemoryX = handPoseFromKinematicModel;
    ChannelRefPtr handMemoryChannel = in.getHandMemoryChannel();
    memoryx::ChannelRefBaseSequence instances = context->getObjectMemoryObserver()->getObjectInstances(handMemoryChannel);

    if (instances.size() == 0)
    {
        ARMARX_WARNING << "No instances of the hand in the memory: " << handMemoryChannel->get<std::string>("className");
        auto list = context->getObjectMemoryObserver()->getObjectInstancesByClass(handMemoryChannel->get<std::string>("className"));

        for (auto& entry : list)
        {
            ARMARX_INFO << "obj: " << ChannelRefPtr::dynamicCast(entry)->getDataField("className")->getString();
        }
    }
    else
    {
        ARMARX_VERBOSE << "Getting hand pose from memory";
        handLocalized = true;

        FramedPositionPtr position = ChannelRefPtr::dynamicCast(instances.front())->get<FramedPosition>("position");
        FramedOrientationPtr orientation = ChannelRefPtr::dynamicCast(instances.front())->get<FramedOrientation>("orientation");
        handPoseFromMemoryX = new FramedPose(orientation->toEigen(), position->toEigen(), position->getFrame(), position->agent);

        handPoseFromMemoryX->changeFrame(context->getRobot(), context->getRobot()->getRootNode()->getName());

        context->getDebugDrawerTopic()->setPoseVisu("VisualServo", in.getTcpNameInRobotModel(), handPoseFromMemoryX->toGlobal(context->getRobot()));

        float uncertainty = ChannelRefPtr::dynamicCast(instances.front())->get<float>("uncertaintyOfPosition");
        ARMARX_VERBOSE << "Uncertainty of hand position: " << uncertainty;

        if (uncertainty > 100.0f)
        {
            handPoseUncertain = true;
        }
    }

    setOutput("HandPose", handPoseFromMemoryX);
    setOutput("HandPoseFromKinematicModel", handPoseFromKinematicModel);

    if (handLocalized)
    {
        if (handPoseUncertain)
        {
            TimeUtil::MSSleep(100);
            sendEvent<HandPoseUncertain>();
        }
        else
        {
            sendEvent<HandPoseAvailable>();
        }
    }
    else
    {
        TimeUtil::MSSleep(100);
        sendEvent<HandNotYetLocalized>();
    }
}






void GetHandPose::onBreak()
{
    // put your user code for the breaking point here
    // execution time should be short (<100ms)
}



void GetHandPose::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)

}



// DO NOT EDIT NEXT FUNCTION
std::string GetHandPose::GetName()
{
    return "GetHandPose";
}



// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr GetHandPose::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new GetHandPose(stateData));
}

