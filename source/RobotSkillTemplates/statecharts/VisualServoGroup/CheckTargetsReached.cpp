/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2014-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::VisualServoGroup
 * @author     Markus Grotz ( markus dot grotz at kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "CheckTargetsReached.h"

using namespace armarx;
using namespace VisualServoGroup;

// DO NOT EDIT NEXT LINE
CheckTargetsReached::SubClassRegistry CheckTargetsReached::Registry(CheckTargetsReached::GetName(), &CheckTargetsReached::CreateInstance);



CheckTargetsReached::CheckTargetsReached(const XMLStateConstructorParams& stateData) :
    XMLStateTemplate<CheckTargetsReached>(stateData),  CheckTargetsReachedGeneratedBase<CheckTargetsReached>(stateData)
{
}

void CheckTargetsReached::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)

    ARMARX_INFO << "LeftTargetReached: " << in.getLeftTargetReached();
    ARMARX_INFO << "RightTargetReached: " << in.getRightTargetReached();

    if (in.getLeftTargetReached() &&
        in.getRightTargetReached())
    {
        emitTargetsReached();
    }
    else
    {
        emitTargetsNotReached();
    }
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr CheckTargetsReached::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new CheckTargetsReached(stateData));
}

