/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2014-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::GraspObjectGroup
 * @author     David ( david dot schiebener at kit dot edu )
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "VisualServoTowardsTargetPoseWrapper.h"

#include "VisualServoGroupStatechartContext.generated.h"


using namespace armarx;
using namespace VisualServoGroup;


// DO NOT EDIT NEXT LINE
VisualServoTowardsTargetPoseWrapper::SubClassRegistry VisualServoTowardsTargetPoseWrapper::Registry(VisualServoTowardsTargetPoseWrapper::GetName(), &VisualServoTowardsTargetPoseWrapper::CreateInstance);



VisualServoTowardsTargetPoseWrapper::VisualServoTowardsTargetPoseWrapper(XMLStateConstructorParams stateData) :
    XMLStateTemplate < VisualServoTowardsTargetPoseWrapper > (stateData),
    VisualServoTowardsTargetPoseWrapperGeneratedBase < VisualServoTowardsTargetPoseWrapper > (stateData)
{
}



void VisualServoTowardsTargetPoseWrapper::onEnter()
{
    // install event ObjectPoseUpdated
    VisualServoGroupStatechartContext* context = getContext<VisualServoGroupStatechartContext>();
    //    FramedPositionPtr objPos = in.getObjectInstanceChannel()->getDataField("position")->get<FramedPosition>();

    ChannelRefPtr handMemoryChannel = in.getHandMemoryChannel();
    FramedPositionPtr handPosFromMemoryX;
    memoryx::ChannelRefBaseSequence instances = context->getObjectMemoryObserver()->getObjectInstances(handMemoryChannel);

    if (instances.size() == 0)
    {
        ARMARX_WARNING << "No instances of the hand in the memory";
    }
    else
    {
        ARMARX_VERBOSE << "Getting hand pose from memory";

        handPosFromMemoryX = ChannelRefPtr::dynamicCast(instances.front())->get<FramedPosition>("position");
    }

    if (handPosFromMemoryX && (handPosFromMemoryX->toGlobal(context->getRobot())->toEigen()
                               - in.getTcpTargetPose()->toGlobal(context->getRobot())->toEigen().block<3, 1>(0, 3)).norm() > in.getNoObjectUpdateDistance())
        //    if (!handPosFromMemoryX || (handPosFromMemoryX->toGlobal(context->getRobot())->toEigen()
        //                                - in.getTcpTargetPose()->toGlobal(context->getRobot())->toEigen().block<3, 1>(0, 3)).norm() > in.getNoObjectUpdateDistance())
    {
        ChannelRefPtr objectInstanceChannel = in.getObjectInstanceChannel();
        Literal poseUpdated(objectInstanceChannel->observerName + "." + objectInstanceChannel->channelName + ".position", "updated", {});
        installCondition<ObjectPoseUpdated>(poseUpdated);
    }



}






void VisualServoTowardsTargetPoseWrapper::onBreak()
{
    // put your user code for the breaking point here
    // execution time should be short (<100ms)
}



void VisualServoTowardsTargetPoseWrapper::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}



// DO NOT EDIT NEXT FUNCTION
std::string VisualServoTowardsTargetPoseWrapper::GetName()
{
    return "VisualServoTowardsTargetPoseWrapper";
}



// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr VisualServoTowardsTargetPoseWrapper::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new VisualServoTowardsTargetPoseWrapper(stateData));
}

