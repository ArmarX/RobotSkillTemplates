/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2014-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::GraspObjectGroup
 * @author     David ( david dot schiebener at kit dot edu )
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "CalculateTcpTarget.h"

#include "VisualServoGroupStatechartContext.generated.h"

#include <VirtualRobot/RobotNodeSet.h>


using namespace armarx;
using namespace VisualServoGroup;


// DO NOT EDIT NEXT LINE
CalculateTcpTarget::SubClassRegistry CalculateTcpTarget::Registry(CalculateTcpTarget::GetName(), &CalculateTcpTarget::CreateInstance);



CalculateTcpTarget::CalculateTcpTarget(XMLStateConstructorParams stateData) :
    XMLStateTemplate < CalculateTcpTarget > (stateData), CalculateTcpTargetGeneratedBase < CalculateTcpTarget > (stateData)
{
}



void CalculateTcpTarget::onEnter()
{
    VisualServoGroupStatechartContext* context = getContext<VisualServoGroupStatechartContext>();
    FramedPosePtr objectPose = in.getObjectPose();
    PosePtr desiredTcpOffsetToObject = in.getDesiredTcpOffsetToObject();

    Eigen::Matrix4f tcpTargetPoseEigen = objectPose->toEigen() * desiredTcpOffsetToObject->toEigen();
    FramedPose tcpTargetPose(tcpTargetPoseEigen, objectPose->frame, objectPose->agent);
    auto nodeset = context->getRobotStateComponent()->getSynchronizedRobot()->getRobotNodeSet(in.getKinematicChainName());
    auto tcpTargetPoseChainRoot = FramedPose(tcpTargetPose.toEigen(), tcpTargetPose.frame, tcpTargetPose.agent);
    tcpTargetPoseChainRoot.changeFrame(context->getRobot(), nodeset->names.at(0));
    float distance = tcpTargetPoseChainRoot.toEigen().block<3, 1>(0, 3).norm();

    ARMARX_VERBOSE << "TcpTargetPose: " << tcpTargetPose;
    ARMARX_VERBOSE << "Extension of TCP to target pose: " << distance;
    out.setTcpTargetPose(tcpTargetPose);

    if (in.getUseReachabilityMaps() && context->getRobotIK()->hasReachabilitySpace(in.getKinematicChainName()))
    {
        if (!context->getRobotIK()->hasReachabilitySpace(in.getKinematicChainName()))
        {
            ARMARX_ERROR << "No loaded reachability space available for kinematic chain '" << in.getKinematicChainName() << "'. Falling back to distance-based reachability check";
        }

        // Use reachability maps provided by RobotIK component
        FramedPoseBasePtr p(new FramedPose(tcpTargetPose));

        if (context->getRobotIK()->isFramedPoseReachable(in.getKinematicChainName(), p))
        {
            ARMARX_INFO << "Reachability check for target pose: Succeeded";
            emitTcpTargetCalculated();
        }
        else
        {
            ARMARX_WARNING << "Reachability check for target pose: Failed";
            emitOutOfReach();
        }
    }
    else
    {
        // Fall back to simple distance calculation
        if (distance > in.getMaxTCPExtension())
        {
            ARMARX_WARNING << "Object is too far away (distance: " << distance << ", max: " << in.getMaxTCPExtension();
            emitOutOfReach();
        }
        else
        {
            emitTcpTargetCalculated();
        }
    }
}






void CalculateTcpTarget::onBreak()
{
    // put your user code for the breaking point here
    // execution time should be short (<100ms)
}



void CalculateTcpTarget::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)

}



// DO NOT EDIT NEXT FUNCTION
std::string CalculateTcpTarget::GetName()
{
    return "CalculateTcpTarget";
}



// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr CalculateTcpTarget::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new CalculateTcpTarget(stateData));
}

