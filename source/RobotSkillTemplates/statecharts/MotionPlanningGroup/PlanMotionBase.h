/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::MotionPlanningGroup
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <mutex>
#include <RobotSkillTemplates/statecharts/MotionPlanningGroup/PlanMotionBase.generated.h>
#include <RobotComponents/components/MotionPlanning/Tasks/RandomShortcutPostprocessor/Task.h>

namespace armarx::MotionPlanningGroup
{
    class PlanMotionBase :
        public PlanMotionBaseGeneratedBase < PlanMotionBase >
    {
    public:
        PlanMotionBase(const XMLStateConstructorParams& stateData):
            XMLStateTemplate < PlanMotionBase > (stateData), PlanMotionBaseGeneratedBase < PlanMotionBase > (stateData)
        {
        }

        PlanMotionBase(const PlanMotionBase& other):
            IceUtil::Shared(other),
            StateIceBase(other),
            StateBase(other),
            StateController(other),
            State(other),
            XMLState(other),
            XMLStateTemplate < PlanMotionBase >(other),
            PlanMotionBaseGeneratedBase < PlanMotionBase >(other)
        {
            //no need to copy the handle or mutex
        }

        // inherited from StateBase
        void onEnter() override
        {
            resetTask();
        }
        void run() override;
        void onBreak() override
        {
            resetTask();
        }
        void onExit() override
        {
            resetTask();
        }

        void resetTask();

        // static functions for AbstractFactory Method
        static XMLStateFactoryBasePtr CreateInstance(XMLStateConstructorParams stateData);
        static SubClassRegistry Registry;

        // DO NOT INSERT ANY CLASS MEMBERS,
        // use stateparameters instead,
        // if classmember are neccessary nonetheless, reset them in onEnter
        RandomShortcutPostprocessorTaskHandle task;
        std::mutex taskMutex;
    };
}


