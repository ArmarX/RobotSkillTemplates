/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::MotionPlanningGroup
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "GenerateNextConfig.h"

#include <VirtualRobot/MathTools.h>

#include <random>

#include <VirtualRobot/MathTools.h>

using namespace armarx;
using namespace MotionPlanningGroup;

// DO NOT EDIT NEXT LINE
GenerateNextConfig::SubClassRegistry GenerateNextConfig::Registry(GenerateNextConfig::GetName(), &GenerateNextConfig::CreateInstance);



void GenerateNextConfig::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
}

void GenerateNextConfig::run()
{
    // put your user code for the execution-phase here
    // runs in seperate thread, thus can do complex operations
    // should check constantly whether isRunningTaskStopped() returns true

    //constants
    out.setCol({"PlatformTorsoColModel", "RightArmColModel", "LeftArmColModel"});
    out.setJoints({"Shoulder 2 L", "Shoulder 2 R"}); //don't set x,y and alpha, since they are no robot joints
    out.setPlatfMin(new Vector3{1000, 1000, -M_PI});
    out.setPlatfMax(new Vector3{5000, 10000, M_PI});
    out.setTime(60);
    //calc
    StringFloatDictionary cfg;
    VirtualRobot::RobotPtr robot = getRobot();
    cfg["Shoulder 2 L"] = robot->getRobotNode("Shoulder 2 L")->getJointValue();
    cfg["Shoulder 2 R"] = robot->getRobotNode("Shoulder 2 R")->getJointValue();
    auto globalPose = robot->getGlobalPose();
    Eigen::Vector3f rpy;
    VirtualRobot::MathTools::eigen4f2rpy(globalPose, rpy);
    ARMARX_VERBOSE_S << "Agents global pose:\n" << globalPose;
    cfg["x"] = globalPose(0, 3);
    cfg["y"] = globalPose(1, 3);
    cfg["alpha"] = rpy(2);
    out.setConfigStart(cfg);
    ARMARX_IMPORTANT_S << "move from " << cfg;

    std::mt19937 gen{std::random_device{}()};
    cfg["Shoulder 2 L"] = in.getArmsUp() ? 1.6f : 0.f;
    cfg["Shoulder 2 R"] = in.getArmsUp() ? 1.6f : 0.f;
    cfg["x"] = std::uniform_real_distribution<float> {1000, 5000}(gen);
    cfg["y"] = std::uniform_real_distribution<float> {1000, 10000}(gen);
    cfg["alpha"] = std::uniform_real_distribution<float> {-M_PI, M_PI}(gen);
    out.setConfigStop(cfg);
    ARMARX_IMPORTANT_S << "move to " << cfg;
    //other out
    out.setArmsUp(!in.getArmsUp());
    emitSuccess();
}

//void GenerateNextConfig::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void GenerateNextConfig::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr GenerateNextConfig::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new GenerateNextConfig(stateData));
}

