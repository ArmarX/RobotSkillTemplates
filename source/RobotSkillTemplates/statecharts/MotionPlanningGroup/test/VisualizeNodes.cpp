/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::MotionPlanningGroup
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "VisualizeNodes.h"
#include <IceUtil/UUID.h>

#include <RobotAPI/libraries/core/Pose.h>

using namespace armarx;
using namespace MotionPlanningGroup;

// DO NOT EDIT NEXT LINE
VisualizeNodes::SubClassRegistry VisualizeNodes::Registry(VisualizeNodes::GetName(), &VisualizeNodes::CreateInstance);



void VisualizeNodes::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
}

void VisualizeNodes::run()
{
    out.setTrajectory(in.getTrajectory());
    TrajectoryPtr t = in.getTrajectory();

    ARMARX_INFO_S << "trajectory size = " << t->size();
    ARMARX_INFO_S << "trajectory dims = " << t->dim();
    ARMARX_INFO_S << "Dimension names" << t->getDimensionNames();
    ARMARX_INFO_S << "trajectory " << t->output();

    Vector3Ptr poseOld;
    Vector3Ptr poseNew;
    for (const auto& elem : *t)
    {
        auto elemX = static_cast<float>(elem.getPosition(0));
        auto elemY = static_cast<float>(elem.getPosition(1));
        poseNew = new Vector3{elemX, elemY, 50.f};
        if (poseOld)
        {
            getDebugDrawerTopic()->setLineVisu("RandomKitchenWalk", IceUtil::generateUUID(), poseOld, poseNew, 0, {0, 0, 1, 1});
        }
        poseOld = poseNew;
    }

    emitSuccess();
}

//void VisualizeNodes::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void VisualizeNodes::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr VisualizeNodes::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new VisualizeNodes(stateData));
}

