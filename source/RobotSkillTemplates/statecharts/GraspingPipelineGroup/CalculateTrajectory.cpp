/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::GraspingPipelineGroup
 * @author     Stefan Reither ( stef dot reither at web dot de )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "CalculateTrajectory.h"

#include <ArmarXCore/util/json/JSONObject.h>

using namespace armarx;
using namespace GraspingPipelineGroup;

// DO NOT EDIT NEXT LINE
CalculateTrajectory::SubClassRegistry CalculateTrajectory::Registry(CalculateTrajectory::GetName(), &CalculateTrajectory::CreateInstance);




TrajectoryPtr CalculateTrajectory::balanceTimestamps(TrajectoryPtr t)
{
    ARMARX_INFO << t->output();
    double length = t->getLength(0);
    if (length == 0.0f)
    {
        return t;
    }
    ARMARX_INFO << VAROUT(length);
    double timelength = t->getTimeLength();
    ARMARX_INFO << VAROUT(timelength);
    auto timestamps = t->getTimestamps();
    ARMARX_INFO << VAROUT(timestamps);
    Ice::DoubleSeq newTimestamps;
    newTimestamps.push_back(0);
    for (size_t var = 0; var < timestamps.size() - 1; ++var)
    {
        double tBefore = timestamps.at(var);
        double tAfter = (timestamps.at(var + 1));
        ARMARX_INFO << VAROUT(tBefore) << VAROUT(tAfter);
        double partLength = t->getLength(0, tBefore, tAfter);
        double lengthPortion = partLength / length;
        ARMARX_INFO << VAROUT(partLength) << VAROUT(lengthPortion);
        newTimestamps.push_back(*newTimestamps.rbegin() + timelength * lengthPortion);
    }
    ARMARX_INFO << VAROUT(newTimestamps);
    TrajectoryPtr newTraj = new Trajectory();
    for (size_t d = 0; d < t->dim(); ++d)
    {
        newTraj->addDimension(t->getDimensionData(d), newTimestamps, t->getDimensionName(d));
    }
    newTraj->setLimitless(t->getLimitless());
    ARMARX_INFO << newTraj->output();

    return newTraj;
}

void CalculateTrajectory::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)

    graspingManager = getGraspingManager();
}

void CalculateTrajectory::run()
{
    // put your user code for the execution-phase here
    // runs in seperate thread, thus can do complex operations
    // should check constantly whether isRunningTaskStopped() returns true
    memoryx::WorkingMemoryInterfacePrx workingMemory = getWorkingMemory();
    memoryx::ObjectInstanceBasePtr objectInstance;
    std::string objectName;
    ARMARX_INFO << "Run() of CalculateTrajectory started";
    if (in.isObjectInstanceChannelSet())
    {
        objectName = in.getObjectInstanceChannel()->getDataField("className")->getString();
        auto id = in.getObjectInstanceChannel()->getDataField("id")->getString();
        objectInstance = workingMemory->getObjectInstancesSegment()->getObjectInstanceById(id);

    }
    else if (in.isObjectNameSet())
    {
        objectName = in.getObjectName();
        objectInstance = workingMemory->getObjectInstancesSegment()->getObjectInstanceByName(objectName);
    }
    else
    {
        ARMARX_ERROR << "Either ObjectName or ObjectInstanceChannel must bet set!";
        emitFailure();
        return;
    }
    if (!objectInstance)
    {
        ARMARX_WARNING << "Could not find ObjectInstance with name " + objectName;
        emitFailure();
        return;
    }


    GraspingTrajectory gt;
    int maxTries = 5;

    for (int i = 0; i < maxTries; i++)
    {
        ARMARX_INFO << "Try " << i << " of " << maxTries;
        try
        {
            gt = graspingManager->generateGraspingTrajectory(objectInstance->getId());
        }
        catch (armarx::LocalException&)
        {
            handleExceptions();
        }

        if (gt.poseTrajectory && gt.configTrajectory && !gt.rnsToUse.empty())
        {
            //            gt.configTrajectory = balanceTimestamps(TrajectoryPtr::dynamicCast(gt.configTrajectory));
            out.setKinematicChainName(gt.rnsToUse);
            out.setHandName(gt.endeffector);
            //            gt.poseTrajectory = balanceTimestamps(TrajectoryPtr::dynamicCast(gt.poseTrajectory));

            JSONObjectPtr json = new JSONObject();
            json->setVariant("posetrajectory", new Variant(gt.poseTrajectory));
            ARMARX_INFO << "Json: " << json->asString(true);

            out.setPlatformTrajectory(gt.poseTrajectory);
            out.setJointTrajectory(gt.configTrajectory);
            out.setGraspPose(gt.grasp.framedPose);
            out.setPreGraspPose(gt.grasp.framedPrePose);
            std::vector<Vector3Ptr> platformPointList;
            TrajectoryPtr platformTraj = TrajectoryPtr ::dynamicCast(gt.poseTrajectory);
            for (const Trajectory::TrajData& point : *platformTraj)
            {
                platformPointList.push_back(new Vector3(point.getPosition(0),
                                                        point.getPosition(1),
                                                        point.getPosition(2)));
            }

            out.setPlatformPointList(platformPointList);
            emitSuccess();
            return;
        }
        else
        {

            ARMARX_INFO << "GraspingManger could not generate a graspingTrajectory. Trying again....";
        }
    }
    ARMARX_WARNING << "Stopped trying to find a solution after " << maxTries << " trys.";
    emitFailure();

}

//void CalculateTrajectory::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void CalculateTrajectory::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr CalculateTrajectory::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new CalculateTrajectory(stateData));
}

