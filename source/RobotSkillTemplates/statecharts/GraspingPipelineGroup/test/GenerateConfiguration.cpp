/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::GraspingPipelineGroup
 * @author     Stefan Reither ( stef dot reither at web dot de )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "GenerateConfiguration.h"

#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/ManagedIceObject.h>
#include <MemoryX/core/MemoryXCoreObjectFactories.h>
#include <MemoryX/libraries/memorytypes/MemoryXTypesObjectFactories.h>
#include <MemoryX/interface/components/WorkingMemoryInterface.h>
#include <MemoryX/interface/components/PriorKnowledgeInterface.h>
#include <RobotComponents/components/GraspingManager/GraspingManager.h>
#include <RobotAPI/libraries/core/LinkedPose.h>
#include <MemoryX/libraries/motionmodels/MotionModelStaticObject.h>

using namespace armarx;
using namespace GraspingPipelineGroup;

// DO NOT EDIT NEXT LINE
GenerateConfiguration::SubClassRegistry GenerateConfiguration::Registry(GenerateConfiguration::GetName(), &GenerateConfiguration::CreateInstance);



void GenerateConfiguration::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)


}

void GenerateConfiguration::run()
{
    // put your user code for the execution-phase here
    // runs in seperate thread, thus can do complex operations
    // should check constantly whether isRunningTaskStopped() returns true

    //    memoryx::WorkingMemoryInterfacePrx wm = getWorkingMemory();
    //    RobotStateComponentInterfacePrx robot = getRobotStateComponent();
    //    auto vitalis = wm->getObjectInstancesSegment()->getEntityByName("vitaliscereal");
    //    if (vitalis)
    //    {
    //        wm->getObjectInstancesSegment()->removeEntity(vitalis->getId());
    //    }

    //    Eigen::Quaternionf q(
    //        0.5174515247344971,
    //        -0.5286158323287964,
    //        0.4808708131313324,
    //        -0.4707148373126984);
    //    wm->getObjectInstancesSegment()->addObjectInstance("vitaliscereal", "vitaliscereal",
    //            new LinkedPose(q.toRotationMatrix(),
    //                           Eigen::Vector3f(4130.22705078125,
    //                                           7142.34765625,
    //                                           1170.102416992188), GlobalFrame, robot->getSynchronizedRobot()),
    //            new memoryx::MotionModelStaticObject(robot));


    emitSuccess();


}

//void GenerateConfiguration::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void GenerateConfiguration::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr GenerateConfiguration::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new GenerateConfiguration(stateData));
}

