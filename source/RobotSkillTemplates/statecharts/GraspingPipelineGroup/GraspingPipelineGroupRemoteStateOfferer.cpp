/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::GraspingPipelineGroup::GraspingPipelineGroupRemoteStateOfferer
 * @author     Stefan Reither ( stef dot reither at web dot de )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "GraspingPipelineGroupRemoteStateOfferer.h"

using namespace armarx;
using namespace GraspingPipelineGroup;

// DO NOT EDIT NEXT LINE
GraspingPipelineGroupRemoteStateOfferer::SubClassRegistry GraspingPipelineGroupRemoteStateOfferer::Registry(GraspingPipelineGroupRemoteStateOfferer::GetName(), &GraspingPipelineGroupRemoteStateOfferer::CreateInstance);



GraspingPipelineGroupRemoteStateOfferer::GraspingPipelineGroupRemoteStateOfferer(StatechartGroupXmlReaderPtr reader) :
    XMLRemoteStateOfferer < GraspingPipelineGroupStatechartContext > (reader)
{
}

void GraspingPipelineGroupRemoteStateOfferer::onInitXMLRemoteStateOfferer()
{

}

void GraspingPipelineGroupRemoteStateOfferer::onConnectXMLRemoteStateOfferer()
{

}

void GraspingPipelineGroupRemoteStateOfferer::onExitXMLRemoteStateOfferer()
{

}

// DO NOT EDIT NEXT FUNCTION
std::string GraspingPipelineGroupRemoteStateOfferer::GetName()
{
    return "GraspingPipelineGroupRemoteStateOfferer";
}

// DO NOT EDIT NEXT FUNCTION
XMLStateOffererFactoryBasePtr GraspingPipelineGroupRemoteStateOfferer::CreateInstance(StatechartGroupXmlReaderPtr reader)
{
    return XMLStateOffererFactoryBasePtr(new GraspingPipelineGroupRemoteStateOfferer(reader));
}



