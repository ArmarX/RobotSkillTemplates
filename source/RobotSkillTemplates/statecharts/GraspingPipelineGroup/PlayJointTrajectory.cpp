/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::GraspingPipelineGroup
 * @author     Stefan Reither ( stef dot reither at web dot de )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "PlayJointTrajectory.h"
#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/ManagedIceObject.h>
#include <ArmarXCore/util/json/JSONObject.h>

using namespace armarx;
using namespace GraspingPipelineGroup;

// DO NOT EDIT NEXT LINE
PlayJointTrajectory::SubClassRegistry PlayJointTrajectory::Registry(PlayJointTrajectory::GetName(), &PlayJointTrajectory::CreateInstance);



void PlayJointTrajectory::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
}

void PlayJointTrajectory::run()
{
    // put your user code for the execution-phase here
    // runs in seperate thread, thus can do complex operations
    // should check constantly whether isRunningTaskStopped() returns true

    //    while (!isRunningTaskStopped()) // stop run function if returning true
    //    {
    //        // do your calculations
    //    }

    TrajectoryPlayerInterfacePrx trajectoryPlayer = getTrajectoryPlayer();


    TrajectoryPtr trajectory = TrajectoryPtr::dynamicCast(in.getTrajectory());
    if (trajectory->size() == 0)
    {
        emitFailure();
        return;
    }

    ARMARX_INFO << VAROUT(trajectory->output());
    ARMARX_INFO << trajectory->getTimestamps();
    JSONObjectPtr json = new JSONObject();
    json->setVariant("trajectory", new Variant(trajectory));
    ARMARX_INFO << "Json: " << json->asString(true);
    trajectoryPlayer->resetTrajectoryPlayer(false);
    trajectoryPlayer->loadJointTraj(trajectory);
    trajectoryPlayer->loadBasePoseTraj(trajectory);
    trajectoryPlayer->setIsVelocityControl(true);

    if (!in.getIsTimeOptimal())
    {
        size_t dim = trajectory->dim();

        double length = 0.0;
        for (size_t i = 0; i < dim; i++)
        {
            double tempLength = trajectory->getLength(i, 0);
            if (tempLength > length)
            {
                length = tempLength;
            }
        }
        double speed = length * in.getPlayTimePer1Rad();

        ARMARX_INFO << trajectoryPlayer->getEndTime();
        trajectoryPlayer->setEndTime(trajectory->begin()->getTimestamp() + trajectory->getTimeLength() * speed);
        ARMARX_INFO << trajectoryPlayer->getEndTime();
    }

    trajectoryPlayer->startTrajectoryPlayer();

    ARMARX_INFO << VAROUT(trajectory->output());
    ARMARX_INFO << trajectory->getTimestamps();

    while (!isRunningTaskStopped()) // stop run function if returning true
    {
        if (this->getTrajectoryPlayer()->getCurrentTime() >= this->getTrajectoryPlayer()->getEndTime())
        {
            ARMARX_LOG << "emitSuccess()";
            this->emitSuccess();
            return;
        }
        //Wait 50ms
        usleep(50000);
    }



}

//void PlayJointTrajectory::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void PlayJointTrajectory::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr PlayJointTrajectory::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new PlayJointTrajectory(stateData));
}

