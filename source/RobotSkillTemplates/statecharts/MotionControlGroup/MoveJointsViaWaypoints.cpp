/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::MotionControlGroup
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "MoveJointsViaWaypoints.h"

//#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>

using namespace armarx;
using namespace MotionControlGroup;

// DO NOT EDIT NEXT LINE
MoveJointsViaWaypoints::SubClassRegistry MoveJointsViaWaypoints::Registry(MoveJointsViaWaypoints::GetName(), &MoveJointsViaWaypoints::CreateInstance);

void MoveJointsViaWaypoints::onEnter()
{
    local.setIndex(in.getStartIndex());
    if (!in.isStopIndexSet())
    {
        in.setStopIndex(in.getJointValueWaypoints().size());
    }
}

// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr MoveJointsViaWaypoints::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new MoveJointsViaWaypoints(stateData));
}

