/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2014-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::MotionControlGroup
 * @author     Simon Ottenhaus ( simon dot ottenhaus at kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "CartesianMultipleRelativePositionsControl.h"
#include "MotionControlGroupStatechartContext.h"

using namespace armarx;
using namespace MotionControlGroup;

// DO NOT EDIT NEXT LINE
CartesianMultipleRelativePositionsControl::SubClassRegistry CartesianMultipleRelativePositionsControl::Registry(CartesianMultipleRelativePositionsControl::GetName(), &CartesianMultipleRelativePositionsControl::CreateInstance);



CartesianMultipleRelativePositionsControl::CartesianMultipleRelativePositionsControl(const XMLStateConstructorParams& stateData) :
    XMLStateTemplate<CartesianMultipleRelativePositionsControl>(stateData),  CartesianMultipleRelativePositionsControlGeneratedBase<CartesianMultipleRelativePositionsControl>(stateData)
{
}

void CartesianMultipleRelativePositionsControl::onEnter()
{
    if (!in.isTcpRelativeOrientationsSet() && !in.isTcpRelativePositionsSet())
    {
        ARMARX_ERROR << "TcpRelativeOrientations and/or TcpRelativePositions has to be set, but none has been set.";
        emitFailure();
        return;
    }
    local.setIndex(0);

    MotionControlGroupStatechartContext* context = getContext<MotionControlGroupStatechartContext>();
    auto robot = context->getRobot();
    Eigen::Matrix4f initialTcpPose = robot->getRobotNodeSet(in.getKinematicChainName())->getTCP()->getPoseInRootFrame();
    std::string rootFrameName = robot->getRootNode()->getName();
    FramedPositionPtr initialTcpPositionPtr = new FramedPosition(initialTcpPose, rootFrameName, robot->getName());
    FramedOrientationPtr initialTcpOrientationPtr = new FramedOrientation(initialTcpPose, rootFrameName, robot->getName());

    local.setInitialTcpOrientation(initialTcpOrientationPtr);
    local.setInitialTcpPosition(initialTcpPositionPtr);
    local.setTcpOrientation(initialTcpOrientationPtr);
    local.setTcpPosition(initialTcpPositionPtr);
}

void CartesianMultipleRelativePositionsControl::run()
{
    /*while (!isRunningTaskStopped()) // stop run function if returning true
    {
        if (local.isIndexSet())
        {
            ARMARX_IMPORTANT << "index: " << local.getIndex();
        }
        else
        {
            ARMARX_WARNING << "index not set!";
        }
        usleep(100000);
    }*/

}

void CartesianMultipleRelativePositionsControl::onBreak()
{
    // put your user code for the breaking point here
    // execution time should be short (<100ms)
}

void CartesianMultipleRelativePositionsControl::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)

}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr CartesianMultipleRelativePositionsControl::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new CartesianMultipleRelativePositionsControl(stateData));
}

