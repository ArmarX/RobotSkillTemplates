/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2014-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::MotionControlGroup
 * @author     Peter Kaiser ( peter dot kaiser at kit dot edu )
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "CartesianVelocityControl.h"
#include "MotionControlGroupStatechartContext.h"

#include <RobotAPI/interface/units/TCPControlUnit.h>

using namespace armarx;
using namespace MotionControlGroup;

// DO NOT EDIT NEXT LINE
CartesianVelocityControl::SubClassRegistry CartesianVelocityControl::Registry(CartesianVelocityControl::GetName(), &CartesianVelocityControl::CreateInstance);



CartesianVelocityControl::CartesianVelocityControl(XMLStateConstructorParams stateData) :
    XMLStateTemplate<CartesianVelocityControl>(stateData),
    CartesianVelocityControlGeneratedBase<CartesianVelocityControl>(stateData)
{
}

void CartesianVelocityControl::onEnter()
{
    MotionControlGroupStatechartContext* context = getContext<MotionControlGroupStatechartContext>();

    std::string kinematicChain = in.getKinematicChainName();

    if (!context->getRobot()->hasRobotNodeSet(kinematicChain))
    {
        ARMARX_WARNING << "Kinematic Chain '" << kinematicChain << "' not available" << flush;
        emitFailure();
    }

    std::string endEffectorName = context->getRobot()->getRobotNodeSet(kinematicChain)->getTCP()->getName();

    FramedDirectionPtr orientationVelocity(new FramedDirection);
    orientationVelocity->frame = endEffectorName;
    orientationVelocity->x = orientationVelocity->y = orientationVelocity->z = 0;

    FramedDirectionPtr positionVelocity(new FramedDirection);
    positionVelocity->frame = endEffectorName;
    positionVelocity->x = positionVelocity->y = positionVelocity->z = 0;

    if (isInputParameterSet("TargetTcpOrientationVelocity"))
    {
        orientationVelocity = in.getTargetTcpOrientationVelocity();
    }

    if (isInputParameterSet("TargetTcpPositionVelocity"))
    {
        positionVelocity = in.getTargetTcpPositionVelocity();
    }

    context->getTCPControlUnit()->setCycleTime(in.getCycleTime());

    // Call request without release to keep the velocities applied after the state is left. Calling release would stop the joints
    // It is no problem to call request a second time in another unit.
    context->getTCPControlUnit()->request();
    context->getTCPControlUnit()->setTCPVelocity(in.getKinematicChainName(), endEffectorName, positionVelocity, orientationVelocity);

    // wait a custom amount of time until going to next state
    setTimeoutEvent(in.getWaitTimeUntilTransition(), createEventSuccess());
}


void CartesianVelocityControl::onBreak()
{
}

void CartesianVelocityControl::onExit()
{
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr CartesianVelocityControl::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new CartesianVelocityControl(stateData));
}

