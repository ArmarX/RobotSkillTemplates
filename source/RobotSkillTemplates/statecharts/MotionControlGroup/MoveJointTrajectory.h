/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::MotionControlGroup
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <chrono>

#include <RobotAPI/libraries/core/RobotAPIObjectFactories.h>
#include <ArmarXCore/statechart/xmlstates/XMLState.h>

#include <RobotSkillTemplates/statecharts/MotionControlGroup/MoveJointTrajectory.generated.h>

#include "MotionControlGroupStatechartContext.h"

namespace armarx::MotionControlGroup
{
    class MoveJointTrajectory :
        public MoveJointTrajectoryGeneratedBase < MoveJointTrajectory >
    {
    public:



        MoveJointTrajectory(const XMLStateConstructorParams& stateData):
            XMLStateTemplate < MoveJointTrajectory > (stateData), MoveJointTrajectoryGeneratedBase < MoveJointTrajectory > (stateData)
        {
        }

        // inherited from StateBase
        void onEnter() override;
        void run() override;
        void onBreak() override;
        void onExit() override;

        // static functions for AbstractFactory Method
        static XMLStateFactoryBasePtr CreateInstance(XMLStateConstructorParams stateData);
        static SubClassRegistry Registry;


    };
}
