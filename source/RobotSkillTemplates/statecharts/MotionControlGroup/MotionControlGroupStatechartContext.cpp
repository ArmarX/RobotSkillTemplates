/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2014-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::MotionControlGroup
 * @author     Peter Kaiser ( peter dot kaiser at kit dot edu )
 * @author     Manfred Kroehnert ( Manfred dot Kroehnert at kit dot edu )
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "MotionControlGroupStatechartContext.h"

//#include <RobotAPI/libraries/core/remoterobot/RemoteRobot.h>
//#include <RobotAPI/libraries/core/Pose.h>

#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/system/ArmarXDataPath.h>
#include <ArmarXCore/core/system/ImportExportComponent.h>
#include <ArmarXCore/core/system/cmake/CMakePackageFinder.h>
#include <ArmarXCore/statechart/Statechart.h>

using namespace armarx;

namespace armarx::MotionControlGroup
{
    void MotionControlGroupStatechartContext::onInitStatechartContext()
    {
        // Register dependencies
        ARMARX_INFO << "KinematicUnitName         " << getProperty<std::string>("KinematicUnitName").getValue();
        ARMARX_INFO << "KinematicUnitObserverName " << getProperty<std::string>("KinematicUnitObserverName").getValue();
        ARMARX_INFO << "RobotStateComponentName   " << getProperty<std::string>("RobotStateComponentName").getValue();
        ARMARX_INFO << "TCPControlUnitName        " << getProperty<std::string>("TCPControlUnitName").getValue();

        usingProxy(getProperty<std::string>("KinematicUnitName").getValue());
        usingProxy(getProperty<std::string>("KinematicUnitObserverName").getValue());
        usingProxy(getProperty<std::string>("RobotStateComponentName").getValue());
        usingProxy(getProperty<std::string>("TCPControlUnitName").getValue());

        offeringTopic("DebugDrawerUpdates");
    }

    void MotionControlGroupStatechartContext::onConnectStatechartContext()
    {
        // retrieve proxies
        robotStateComponent = getProxy<RobotStateComponentInterfacePrx>(getProperty<std::string>("RobotStateComponentName").getValue());
        kinematicUnitPrx = getProxy<KinematicUnitInterfacePrx>(getProperty<std::string>("KinematicUnitName").getValue());
        kinematicUnitObserverPrx = getProxy<KinematicUnitObserverInterfacePrx>(getProperty<std::string>("KinematicUnitObserverName").getValue());
        tcpControlPrx = getProxy<TCPControlUnitInterfacePrx>(getProperty<std::string>("TCPControlUnitName").getValue());

        // initialize remote robot
        remoteRobot.reset(new RemoteRobot(robotStateComponent->getSynchronizedRobot()));
        std::string robotfilepath = robotStateComponent->getRobotFilename();
        if (!ArmarXDataPath::getAbsolutePath(robotfilepath, robotfilepath, {}, false))
        {
            auto packages = robotStateComponent->getArmarXPackages();
            for (auto& p : packages)
            {
                CMakePackageFinder finder(p);
                ArmarXDataPath::addDataPaths(finder.getDataDir());
            }
            if (!ArmarXDataPath::getAbsolutePath(robotfilepath, robotfilepath))
            {
                ARMARX_WARNING << "Could not get absolute robot model filepath for relative path : " << robotfilepath << " - using network clone";
                robotfilepath = "";
            }
        }
        localrobot = RemoteRobot::createLocalClone(robotStateComponent, robotfilepath);
        localCollisionRobot = RemoteRobot::createLocalClone(robotStateComponent, robotfilepath, {}, VirtualRobot::RobotIO::eCollisionModel);
        localStructureRobot = RemoteRobot::createLocalClone(robotStateComponent, robotfilepath, {}, VirtualRobot::RobotIO::eStructure);
        robotPool.reset(new RobotPool(localCollisionRobot));
        debugDrawerTopicProxy = getTopic<DebugDrawerInterfacePrx>("DebugDrawerUpdates");
        debugObserverProxy = getProxy<DebugObserverInterfacePrx>("DebugObserver", false, "", false);
    }

    PropertyDefinitionsPtr MotionControlGroupStatechartContext::createPropertyDefinitions()
    {
        return PropertyDefinitionsPtr(new MotionControlGroupStatechartContextProperties(
                                          getConfigIdentifier()));
    }
}

