/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2014-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::MotionControlGroup
 * @author     Peter Kaiser ( peter dot kaiser at kit dot edu )
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "JointPositionControlApply.h"
#include "../MotionControlGroupStatechartContext.h"

using namespace armarx;
using namespace MotionControlGroup;

// DO NOT EDIT NEXT LINE
JointPositionControlApply::SubClassRegistry JointPositionControlApply::Registry(JointPositionControlApply::GetName(), &JointPositionControlApply::CreateInstance);



JointPositionControlApply::JointPositionControlApply(XMLStateConstructorParams stateData) :
    XMLStateTemplate<JointPositionControlApply>(stateData),
    JointPositionControlApplyGeneratedBase<JointPositionControlApply>(stateData)
{
}

void JointPositionControlApply::onEnter()
{
    MotionControlGroupStatechartContext* context = getContext<MotionControlGroupStatechartContext>();

    auto targetValues = in.getJointTargetValues();

    float jointVelocity = 0;

    if (isInputParameterSet("JointVelocity"))
    {
        jointVelocity = in.getJointVelocity();
    }

    // Set default velocity and control mode values
    NameValueMap targetVelocities;
    NameControlModeMap jointControlModes;

    for (auto& pair : targetValues)
    {
        targetVelocities[pair.first] = jointVelocity;
        jointControlModes[pair.first] = ePositionControl;
    }

    // Override velocity values with specifically given speed values (if available)
    if (isInputParameterSet("JointVelocities"))
    {
        auto givenVelocities = in.getJointVelocities();

        for (auto& pair : givenVelocities)
        {
            targetVelocities[pair.first] = pair.second;
        }
    }

    std::set<std::string> blacklist;

    float tolerance = in.getJointTargetTolerance();

    for (auto& pair : targetValues)
    {
        DataFieldIdentifierBasePtr dataFieldIdentifier = new DataFieldIdentifier(context->getKinematicUnitObserverName(), "jointangles", pair.first);

        if (!dataFieldIdentifier)
        {
            ARMARX_WARNING << "Could not generate DataFieldIdentifier from '" << pair.first << "'";
            blacklist.insert(pair.first);
            continue;
        }

        VariantBasePtr dataField = context->getKinematicUnitObserver()->getDataField(dataFieldIdentifier);

        if (!dataField || !dataField->getInitialized())
        {
            ARMARX_WARNING << "Could not obtain DataField of '" << pair.first << "'";
            blacklist.insert(pair.first);
            continue;
        }

        float currentValue = dataField->getFloat();

        if (currentValue >= pair.second - tolerance && currentValue <= pair.second + tolerance)
        {
            // Target is reached. Set velocity to zero
            targetVelocities[pair.first] = 0;
        }
        else
        {
            targetVelocities[pair.first] = fabs(targetVelocities[pair.first]);
        }
    }

    // Create and install success condition
    Term condition;
    bool stillWaitingForJoint = false;

    for (auto& pair : targetValues)
    {
        if (blacklist.find(pair.first) != blacklist.end())
        {
            // We failed to get the current value of this joint.
            // It is probably a virtual joint that is not handled by the kinematic unit.
            // Those joints are sometimes part of some RobotNodeSets.
            continue;
        }

        if (targetVelocities[pair.first] == 0)
        {
            // Target already reached, omit check
            continue;
        }

        Literal approx(DataFieldIdentifier(context->getKinematicUnitObserverName(), "jointangles", pair.first), checks::approx, {pair.second, tolerance});
        condition = condition || approx;

        stillWaitingForJoint = true;
    }

    if (stillWaitingForJoint)
    {
        installConditionForJointReachedTarget(condition);
    }

    // Create and install timeout condition
    int timeout = in.getTimeout();

    if (timeout > 0)
    {
        setTimeoutEvent(timeout, createEventTimeout());
    }

    context->getKinematicUnit()->switchControlMode(jointControlModes);
    context->getKinematicUnit()->setJointAngles(targetValues);

    // Position control with velocity targets is currently not supported by the kinematic unit.
    // However, once it is supported uncommenting the following line will enable it
    //context->getKinematicUnit()->setJointVelocities(targetVelocities);

    if (!stillWaitingForJoint)
    {
        // All joints reached their target
        emitSuccess();
    }
}

void JointPositionControlApply::run()
{
}

void JointPositionControlApply::onBreak()
{
}

void JointPositionControlApply::onExit()
{
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr JointPositionControlApply::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new JointPositionControlApply(stateData));
}

