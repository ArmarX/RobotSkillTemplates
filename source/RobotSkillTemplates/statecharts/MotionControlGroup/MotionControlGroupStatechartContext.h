/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2014-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::MotionControlGroup
 * @author     Peter Kaiser ( peter dot kaiser at kit dot edu )
 * @author     Manfred Kroehnert ( Manfred dot Kroehnert at kit dot edu )
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once

#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/system/ImportExportComponent.h>
#include <ArmarXCore/statechart/StatechartContext.h>


// Custom Includes
#include <RobotAPI/interface/units/KinematicUnitInterface.h>
#include <RobotAPI/interface/units/TCPControlUnit.h>
#include <RobotAPI/interface/observers/KinematicUnitObserverInterface.h>
#include <RobotAPI/interface/core/RobotState.h>
#include <RobotAPI/libraries/core/remoterobot/RemoteRobot.h>
#include <RobotAPI/components/DebugDrawer/DebugDrawerComponent.h>
#include <RobotAPI/libraries/core/RobotPool.h>

//#include <ArmarXSimulation/interface/simulator/SimulatorInterface.h>

#include <VirtualRobot/VirtualRobot.h>

namespace armarx::MotionControlGroup
{

    struct MotionControlGroupStatechartContextProperties : StatechartContextPropertyDefinitions
    {
        MotionControlGroupStatechartContextProperties(std::string prefix):
            StatechartContextPropertyDefinitions(prefix)
        {
            defineRequiredProperty<std::string>("KinematicUnitName", "Name of the kinematic unit that should be used");
            defineRequiredProperty<std::string>("KinematicUnitObserverName", "Name of the kinematic unit observer that should be used");
            defineOptionalProperty<std::string>("RobotStateComponentName", "RobotStateComponent", "Name of the robot state component that should be used");
            defineOptionalProperty<std::string>("TCPControlUnitName", "TCPControlUnit", "Name of the tcp control unit component that should be used");
        }
    };

    /**
             * @class MotionControlGroupStatechartContext is a custom implementation of the StatechartContext
             * for a statechart
             */
    class ARMARXCOMPONENT_IMPORT_EXPORT MotionControlGroupStatechartContext :
        virtual public XMLStatechartContext
    {
    public:
        // inherited from Component
        std::string getDefaultName() const override
        {
            return "MotionControlGroupStatechartContext";
        }
        void onInitStatechartContext() override;
        void onConnectStatechartContext() override;


        const std::shared_ptr<RemoteRobot> getRobot()
        {
            return remoteRobot;
        }
        const VirtualRobot::RobotPtr getLocalRobot()
        {
            return localrobot;
        }
        const VirtualRobot::RobotPtr getLocalCollisionRobot()
        {
            return localCollisionRobot;
        }
        const VirtualRobot::RobotPtr getLocalStructureRobot()
        {
            return localStructureRobot;
        }

        RobotStateComponentInterfacePrx getRobotStateComponent()
        {
            return robotStateComponent;
        }
        KinematicUnitInterfacePrx getKinematicUnit()
        {
            return kinematicUnitPrx;
        }
        KinematicUnitObserverInterfacePrx getKinematicUnitObserver()
        {
            return kinematicUnitObserverPrx;
        }
        std::string getKinematicUnitObserverName()
        {
            return getProperty<std::string>("KinematicUnitObserverName").getValue();
        }
        TCPControlUnitInterfacePrx getTCPControlUnit()
        {
            return tcpControlPrx;
        }
        DebugDrawerInterfacePrx getDebugDrawerTopicProxy()
        {
            return debugDrawerTopicProxy;
        }

        DebugObserverInterfacePrx getDebugObserverProxy() const
        {
            return debugObserverProxy;
        }

        /**
         * @brief Robot Pool containing collision robots
         * @return
         */
        const RobotPoolPtr& getRobotPool() const
        {
            return robotPool;
        }

    private:


        /**
             * @see PropertyUser::createPropertyDefinitions()
             */
        PropertyDefinitionsPtr createPropertyDefinitions() override;
        VirtualRobot::RobotPtr localrobot;
        VirtualRobot::RobotPtr localCollisionRobot;
        VirtualRobot::RobotPtr localStructureRobot;
        std::shared_ptr<RemoteRobot> remoteRobot;
        RobotStateComponentInterfacePrx robotStateComponent;
        KinematicUnitInterfacePrx kinematicUnitPrx;
        KinematicUnitObserverInterfacePrx kinematicUnitObserverPrx;
        TCPControlUnitInterfacePrx tcpControlPrx;
        DebugDrawerInterfacePrx debugDrawerTopicProxy;
        DebugObserverInterfacePrx debugObserverProxy;
        RobotPoolPtr robotPool;
    };

}

