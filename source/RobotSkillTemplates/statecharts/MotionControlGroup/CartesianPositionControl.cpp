/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2014-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::MotionControlGroup
 * @author     Peter Kaiser ( peter dot kaiser at kit dot edu )
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "CartesianPositionControl.h"
#include "MotionControlGroupStatechartContext.h"

#include <RobotAPI/libraries/core/Pose.h>

using namespace armarx;
using namespace MotionControlGroup;

// DO NOT EDIT NEXT LINE
CartesianPositionControl::SubClassRegistry CartesianPositionControl::Registry(CartesianPositionControl::GetName(), &CartesianPositionControl::CreateInstance);



CartesianPositionControl::CartesianPositionControl(XMLStateConstructorParams stateData) :
    XMLStateTemplate<CartesianPositionControl>(stateData),
    CartesianPositionControlGeneratedBase<CartesianPositionControl>(stateData)
{
}

void CartesianPositionControl::onEnter()
{
    // Create and install timeout condition
    int timeout = in.getTimeout();

    if (timeout > 0)
    {
        setTimeoutEvent(timeout, createEventTimeout());
    }

    // Check if position and orientation are given in the same frame
    if (isInputParameterSet("TcpTargetPosition") && isInputParameterSet("TcpTargetOrientation"))
    {
        if (in.getTcpTargetPosition()->frame != in.getTcpTargetOrientation()->frame)
        {
            ARMARX_WARNING << "Frame mismatch between target position and target orientation" << flush;
            emitFailure();
        }
    }

    // Check if at least one type of target is given
    if (!isInputParameterSet("TcpTargetPosition") && !isInputParameterSet("TcpTargetOrientation"))
    {
        ARMARX_WARNING << "Neither position target nor orientation target given for IK" << flush;
        emitFailure();
    }
}

void CartesianPositionControl::onBreak()
{
}

void CartesianPositionControl::onExit()
{
    if (in.isTcpTargetPositionSet())
    {
        MotionControlGroupStatechartContext* context = getContext<MotionControlGroupStatechartContext>();

        auto robot = context->getRobot()->clone();
        Eigen::Vector3f target = in.getTcpTargetPosition()->toRootEigen(robot);
        Eigen::Vector3f currentTcpPosition = robot->getRobotNodeSet(in.getKinematicChainName())->getTCP()->getPoseInRootFrame().block<3, 1>(0, 3);

        ARMARX_IMPORTANT << "Remaining error: " << (currentTcpPosition - target).norm() << ", current TCP position: " << currentTcpPosition << ", target: " << target << "(in root frame)";
    }
}

// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr CartesianPositionControl::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new CartesianPositionControl(stateData));
}

