/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::MotionControlGroup
 * @author     Mirko Waechter ( mirko dot waechter at kit dot edu )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "CollisionFreeCartesianControl.h"

//#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>

using namespace armarx;
using namespace MotionControlGroup;

// DO NOT EDIT NEXT LINE
CollisionFreeCartesianControl::SubClassRegistry CollisionFreeCartesianControl::Registry(CollisionFreeCartesianControl::GetName(), &CollisionFreeCartesianControl::CreateInstance);



void CollisionFreeCartesianControl::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
    if (!in.isTargetPoseSet() && !in.isTargetPositionSet() && !in.isTargetOrientationSet())
    {
        ARMARX_WARNING << "input parameters of pose, position and orientation are not set!";
        emitFailure();
        //        cancelSubstates();
        return;
    }
    if (in.isTargetPoseSet())
    {
        local.setProcessedTargetPosition(new FramedPosition(in.getTargetPose()->toEigen(), in.getTargetPose()->getFrame(), in.getTargetPose()->agent));
        local.setProcessedTargetOrientation(new FramedOrientation(in.getTargetPose()->toEigen(), in.getTargetPose()->getFrame(), in.getTargetPose()->agent));
    }
    else
    {
        if (in.isTargetPositionSet())
        {
            local.setProcessedTargetPosition(in.getTargetPosition());
        }
        if (in.isTargetOrientationSet())
        {
            local.setProcessedTargetOrientation(in.getTargetOrientation());
        }

    }
}

//void CollisionFreeCartesianControl::run()
//{
//    // put your user code for the execution-phase here
//    // runs in seperate thread, thus can do complex operations
//    // should check constantly whether isRunningTaskStopped() returns true
//
//// uncomment this if you need a continous run function. Make sure to use sleep or use blocking wait to reduce cpu load.
//    while (!isRunningTaskStopped()) // stop run function if returning true
//    {
//        // do your calculations
//    }
//}

//void CollisionFreeCartesianControl::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void CollisionFreeCartesianControl::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr CollisionFreeCartesianControl::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new CollisionFreeCartesianControl(stateData));
}



