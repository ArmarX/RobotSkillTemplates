/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2014-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::MotionControlGroup
 * @author     Valerij Wittenbeck ( valerij dot wittenbeck at student dot kit dot edu )
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "CalculateJointAngleConfiguration.h"

// Core
#include <RobotAPI/libraries/core/remoterobot/RemoteRobot.h>

// Virtual Robot
#include <ArmarXCore/statechart/standardstates/FinalState.h>
#include <VirtualRobot/IK/GenericIKSolver.h>
#include <VirtualRobot/LinkedCoordinate.h>

using namespace armarx;
using namespace MotionControlGroup;

// DO NOT EDIT NEXT LINE
CalculateJointAngleConfiguration::SubClassRegistry CalculateJointAngleConfiguration::Registry(CalculateJointAngleConfiguration::GetName(), &CalculateJointAngleConfiguration::CreateInstance);



CalculateJointAngleConfiguration::CalculateJointAngleConfiguration(XMLStateConstructorParams stateData) :
    XMLStateTemplate < CalculateJointAngleConfiguration > (stateData)
{
}

void CalculateJointAngleConfiguration::run()
{
    //RobotStatechartContext* context = getContext<RobotStatechartContext>();
    //VirtualRobot::RobotPtr robot(new RemoteRobot(context->robotStateComponent->getRobotSnapshot("CalculateTCPPoseTime")));

    // TODO: with the following line, the IK doesn't find a solution, so this terrible hack must be used. Fix it!!!
    //VirtualRobot::RobotPtr robot = robotPtr->clone("CalculateTCPPoseClone");
    //std::string robotModelFile;
    //ArmarXDataPath::getAbsolutePath("Armar4/Armar4/ArmarIV.xml", robotModelFile);
    //ArmarXDataPath::getAbsolutePath("Armar3/robotmodel/ArmarIII.xml", robotModelFile);
    //VirtualRobot::RobotPtr robot = VirtualRobot::RobotIO::loadRobot(robotModelFile.c_str());

    std::string kinChainName = getInput<std::string>("kinematicChainName");
    float maxError = getInput<float>("targetPositionDistanceTolerance");
    float maxErrorRot = getInput<float>("targetOrientationDistanceTolerance");
    bool withOrientation = getInput<bool>("ikWithOrientation");
    VirtualRobot::IKSolver::CartesianSelection ikType = VirtualRobot::IKSolver::All;

    if (!withOrientation)
    {
        ikType = VirtualRobot::IKSolver::Position;
    }


    RobotStatechartContext* context = getContext<RobotStatechartContext>();

    if (!context)
    {
        ARMARX_WARNING << "Need a RobotStatechartContext" << flush;
        sendEvent<Failure>();
    }

    if (!context->robotStateComponent)
    {
        ARMARX_WARNING << "No RobotStatechartContext->robotStateComponent" << flush;
        sendEvent<Failure>();
    }

    VirtualRobot::RobotPtr robot = RemoteRobot::createLocalClone(context->robotStateComponent);

    if (!robot)
    {
        ARMARX_WARNING << "Could not create a local clone of RemoteRobot" << flush;
        sendEvent<Failure>();
    }

    if (!robot->hasRobotNodeSet(kinChainName))
    {
        ARMARX_WARNING << "Robot does not know kinematic chain with name " << kinChainName << flush;
        sendEvent<Failure>();
    }

    ARMARX_INFO << "Setting up ik solver for kin chain: " << kinChainName << ", max position error:" << maxError << ", max orientation erorr " << maxErrorRot << ", useOrientation:" << withOrientation << armarx::flush;
    VirtualRobot::GenericIKSolverPtr ikSolver(new VirtualRobot::GenericIKSolver(robot->getRobotNodeSet(kinChainName), VirtualRobot::JacobiProvider::eSVDDamped));
    ikSolver->setVerbose(true);
    ikSolver->setMaximumError(maxError, maxErrorRot);
    ikSolver->setupJacobian(0.6f, 10);

    VirtualRobot::LinkedCoordinate target = FramedPose::createLinkedCoordinate(robot, getInput<FramedPosition>("targetTCPPosition"), getInput<FramedOrientation>("targetTCPOrientation"));
    ARMARX_INFO << "IK target: " << target.getPose() << armarx::flush;
    Eigen::Matrix4f goalInRoot = target.getInFrame(robot->getRootNode());

    //    // test
    //    VirtualRobot::RobotNodePtr rn = robot->getRobotNode("LeftTCP");
    //    Eigen::Matrix4f goalTmpTCP = rn->getPoseInRootFrame();
    Eigen::Matrix4f goalGlobal = robot->getRootNode()->toGlobalCoordinateSystem(goalInRoot);
    ARMARX_INFO << "GOAL in root: " << goalInRoot << armarx::flush;
    ARMARX_INFO << "GOAL global: " << goalGlobal << armarx::flush;
    //    ARMARX_INFO << "GOAL TCP:" << goalTmpTCP << endl;

    ARMARX_INFO << "Calculating IK" << flush;

    //if (!ikSolver->solve(goalGlobal, VirtualRobot::IKSolver::Position, 50))
    if (!ikSolver->solve(goalGlobal, ikType, 5))
    {
        ARMARX_WARNING << "IKSolver found no solution! " << flush;
        sendEvent<Failure>();
    }
    else
    {

        SingleTypeVariantList jointNames = SingleTypeVariantList(VariantType::String);
        SingleTypeVariantList targetJointValues = SingleTypeVariantList(VariantType::Float);
        VirtualRobot::RobotNodeSetPtr kinematicChain = robot->getRobotNodeSet(kinChainName);

        for (int i = 0; i < (signed int)kinematicChain->getSize(); i++)
        {
            jointNames.addVariant(Variant(kinematicChain->getNode(i)->getName()));
            targetJointValues.addVariant(Variant(kinematicChain->getNode(i)->getJointValue()));
            ARMARX_LOG << " joint: " << kinematicChain->getNode(i)->getName() << "   value: " << kinematicChain->getNode(i)->getJointValue() << flush;
        }

        ARMARX_LOG << "number of joints to be set: " << jointNames.getSize() << flush;
        ARMARX_LOG << "setting output: jointNames" << flush;
        setOutput("jointNames", jointNames);
        ARMARX_LOG << "setting output: targetJointValues" << flush;
        setOutput("targetJointValues", targetJointValues);
        sendEvent<EvCalculationDone>();
    }
}

// DO NOT EDIT NEXT FUNCTION
std::string CalculateJointAngleConfiguration::GetName()
{
    return "CalculateJointAngleConfiguration";
}

// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr CalculateJointAngleConfiguration::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new CalculateJointAngleConfiguration(stateData));
}

