/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::MotionControlGroup
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "SetJointZeroVelocity.h"
#include "MotionControlGroupStatechartContext.h"

using namespace armarx;
using namespace MotionControlGroup;

// DO NOT EDIT NEXT LINE
SetJointZeroVelocity::SubClassRegistry SetJointZeroVelocity::Registry(SetJointZeroVelocity::GetName(), &SetJointZeroVelocity::CreateInstance);

void SetJointZeroVelocity::onEnter()
{
    MotionControlGroupStatechartContext* c = getContext<MotionControlGroupStatechartContext>();
    KinematicUnitInterfacePrx ku = c->getKinematicUnit();
    NameControlModeMap cmodes;
    NameValueMap targets;
    for (const auto& elem : in.getJoints())
    {
        cmodes[elem] = eVelocityControl;
        targets[elem] = 0;
    }
    ku->switchControlMode(cmodes);
    ku->setJointVelocities(targets);
    emitSuccess();
}

// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr SetJointZeroVelocity::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new SetJointZeroVelocity(stateData));
}

