/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::MotionControlGroup
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include <chrono>
#include <thread>

#include "MoveJointTrajectory.h"

#include <ArmarXCore/core/time/CycleUtil.h>
#include <ArmarXCore/core/time/TimeUtil.h>

#include <RobotAPI/interface/units/KinematicUnitInterface.h>

#include <RobotAPI/interface/components/TrajectoryPlayerInterface.h>
#include <RobotAPI/libraries/core/RobotStatechartContext.h>
#include <RobotAPI/libraries/core/TrajectoryController.h>
#include <VirtualRobot/RobotConfig.h>

using namespace armarx;
using namespace MotionControlGroup;

// DO NOT EDIT NEXT LINE
MoveJointTrajectory::SubClassRegistry MoveJointTrajectory::Registry(MoveJointTrajectory::GetName(), &MoveJointTrajectory::CreateInstance);

void MoveJointTrajectory::onEnter()
{
}

void MoveJointTrajectory::run()
{
    MotionControlGroupStatechartContext* context = getContext<MotionControlGroupStatechartContext>();
    auto robot = context->getLocalStructureRobot();

    auto combinedTraj = in.getTrajectory();
    ARMARX_INFO << VAROUT(combinedTraj->output());
    if (in.getUseTrajectoryPlayerComponent())
    {
        TrajectoryPlayerInterfacePrx trajPlayer = context->getProxy<TrajectoryPlayerInterfacePrx>(in.getTrajectoryPlayerName());
        trajPlayer->resetTrajectoryPlayer(false);
        trajPlayer->considerConstraints(false);
        trajPlayer->setLoopPlayback(false);
        trajPlayer->loadJointTraj(combinedTraj);
        trajPlayer->startTrajectoryPlayer();
        auto endTime = trajPlayer->getEndTime();
        double currentTime;;
        do
        {
            ARMARX_INFO << deactivateSpam(1) << "CurrentTime: " << currentTime << " endTime: " << endTime;
            usleep(10000);
            currentTime = trajPlayer->getCurrentTime();
        }
        while (currentTime < endTime && !isRunningTaskStopped());
        if (!isRunningTaskStopped() && currentTime >= endTime)
        {
            if (in.getWaitTimeAfterExecution() > 0)
            {
                ARMARX_INFO << "Waiting for " << in.getWaitTimeAfterExecution();
                TimeUtil::MSSleep(in.getWaitTimeAfterExecution());
            }
            trajPlayer->pauseTrajectoryPlayer();
            emitSuccess();
        }
        else if (isRunningTaskStopped())
        {
            trajPlayer->pauseTrajectoryPlayer();
        }
    }
    else
    {
        TrajectoryController ctrl(combinedTraj, in.getKp());
        auto duration = combinedTraj->getTimeLength();
        auto jointNames = combinedTraj->getDimensionNames();
        auto rns = VirtualRobot::RobotNodeSet::createRobotNodeSet(robot, "tempset", jointNames, robot->getRootNode()->getName());




        auto timestep = in.getTimeStep();
        CycleUtil cycle(timestep * 1000);
        NameControlModeMap modes;
        NameValueMap targetVelocities;

        for (auto& jointName : jointNames)
        {
            modes[jointName] = eVelocityControl;
        }
        context->getKinematicUnit()->switchControlMode(modes);
        // uncomment this if you need a continous run function. Make sure to use sleep or use blocking wait to reduce cpu load.
        while (!isRunningTaskStopped()) // stop run function if returning true
        {
            // do your calculations
            RemoteRobot::synchronizeLocalClone(robot, context->getRobotStateComponent());

            Eigen::VectorXf velocityTargets = ctrl.update(timestep, rns->getJointValuesEigen());
            int i = 0;
            for (auto& jointName : jointNames)
            {
                targetVelocities[jointName] = velocityTargets(i);
                i++;
            }
            context->getKinematicUnit()->setJointVelocities(targetVelocities);
            //        context->getDebugDrawerTopicProxy()->setSphereVisu("trajectory", "timestep" + std::to_string(), new )
            ARMARX_INFO << deactivateSpam(2) << "timestep: " << ctrl.getCurrentTimestamp() << " Duration: " << duration;
            if (ctrl.getCurrentTimestamp() >= duration)
            {
                ARMARX_INFO << "Reached Goal!";
                emitSuccess();
                break;
            }
            cycle.waitForCycleDuration();
        }
        for (auto& jointName : jointNames)
        {
            targetVelocities[jointName] = 0;
        }
        context->getKinematicUnit()->setJointVelocities(targetVelocities);
    }
    //    RemoteRobot::synchronizeLocalClone(robot, context->getRobotStateComponent());

    //    for (auto goalConfigMapPtr : in.getGoalConfig())
    //    {
    //        auto goalConfigMap = goalConfigMapPtr->toStdMap<float>();
    //        NameValueMap errors;
    //        for (auto& pair : goalConfigMap)
    //        {
    //            errors[pair.first] = robot->getRobotNode(pair.first)->getJointValue();
    //        }
    //        ARMARX_INFO << VAROUT(errors) << VAROUT(goalConfigMap);
    //    }

}

void MoveJointTrajectory::onExit()
{

}
void MoveJointTrajectory::onBreak()
{

}



// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr MoveJointTrajectory::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new MoveJointTrajectory(stateData));
}



