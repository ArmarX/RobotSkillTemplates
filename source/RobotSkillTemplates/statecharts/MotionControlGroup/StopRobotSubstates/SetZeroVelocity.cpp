/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2014-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::MotionControlGroup
 * @author     Mirko Waechter ( mirko dot waechter at kit dot edu )
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "SetZeroVelocity.h"
#include "../MotionControlGroupStatechartContext.h"

#include <ArmarXCore/core/time/TimeUtil.h>
#include <VirtualRobot/RobotConfig.h>

using namespace armarx;
using namespace MotionControlGroup;

// DO NOT EDIT NEXT LINE
SetZeroVelocity::SubClassRegistry SetZeroVelocity::Registry(SetZeroVelocity::GetName(), &SetZeroVelocity::CreateInstance);



SetZeroVelocity::SetZeroVelocity(XMLStateConstructorParams stateData) :
    XMLStateTemplate<SetZeroVelocity>(stateData),  SetZeroVelocityGeneratedBase<SetZeroVelocity>(stateData)
{
}

void SetZeroVelocity::onEnter()
{

    MotionControlGroupStatechartContext* context = getContext<MotionControlGroupStatechartContext>();

    if (context->getTCPControlUnit()->isRequested())
    {
        context->getTCPControlUnit()->release();
        usleep(100000);
    }
    int decelerationTime = in.getDecelerationTime();

    if (decelerationTime != 0)
    {
        setTimeoutEvent(decelerationTime, createEventTimeout());
    }

    std::string kinematicChain = "";

    if (in.isKinematicChainNameSet())
    {
        kinematicChain = in.getKinematicChainName();
    }

    Ice::StringSeq nodeNames, realJointNames;

    if (kinematicChain.empty())
    {
        nodeNames = context->getLocalRobot()->getRobotNodeNames();
    }
    else
    {
        VirtualRobot::RobotNodeSetPtr nodeSet = context->getLocalRobot()->getRobotNodeSet(kinematicChain);

        for (size_t i = 0; i < nodeSet->getSize(); ++i)
        {
            nodeNames.push_back(nodeSet->getNode(i)->getName());
        }
    }


    Term zeroVeloCondition;

    auto channels = context->getKinematicUnitObserver()->getAvailableChannels(false);
    ChannelRegistryEntry& channelEntry = channels["jointvelocities"];
    NameValueMap jointVelocities;

    for (auto& nodeName : nodeNames)
    {
        if (channelEntry.dataFields.count(nodeName) == 0 || !channelEntry.dataFields[nodeName].value->getInitialized())
        {
            continue;
        }

        jointVelocities[nodeName] = channelEntry.dataFields[nodeName].value->getFloat();
        zeroVeloCondition = zeroVeloCondition && Literal(context->getKinematicUnitObserverName() + ".jointvelocities." + nodeName, "inrange", Literal::createParameterList(-0.01f, 0.01f));
        realJointNames.push_back(nodeName);
    }


    installConditionForZeroVelocityReached(zeroVeloCondition);
    out.setJointNames(realJointNames);

}

void SetZeroVelocity::run()
{
    MotionControlGroupStatechartContext* context = getContext<MotionControlGroupStatechartContext>();


    std::string kinematicChain = "";

    if (in.isKinematicChainNameSet())
    {
        kinematicChain = in.getKinematicChainName();
    }

    NameValueMap jointMap = context->getRobot()->getSharedRobot()->getConfig();

    //    std::vector<VirtualRobot::RobotNodePtr> nodes;
    //    if(kinematicChain.empty())
    //    {
    //        context->getRobot()->getRobotNodes(nodes);
    //    }
    //    else
    //    {
    //        VirtualRobot::RobotNodeSetPtr nodeSet = context->getRobot()->getRobotNodeSet(kinematicChain);
    //        nodes = nodeSet->getAllRobotNodes();
    //    }

    auto channels = context->getKinematicUnitObserver()->getAvailableChannels(false);
    ChannelRegistryEntry& channelEntry = channels["jointvelocities"];
    NameValueMap jointVelocities;
    Ice::StringSeq validJoints;

    for (auto& node : jointMap)
    {
        if (channelEntry.dataFields.count(node.first) == 0 || !channelEntry.dataFields[node.first].value->getInitialized())
        {
            continue;
        }

        jointVelocities[node.first] = channelEntry.dataFields[node.first].value->getFloat();
        validJoints.push_back(node.first);
    }


    while (!isRunningTaskStopped())
    {

        for (auto& jointName : validJoints)
        {
            jointVelocities[jointName] *= 0.5;
        }

        context->getKinematicUnit()->setJointVelocities(jointVelocities);
        TimeUtil::MSSleep(5);
    }
}



void SetZeroVelocity::onExit()
{
    MotionControlGroupStatechartContext* context = getContext<MotionControlGroupStatechartContext>();
    NameValueMap config = context->getRobot()->getSharedRobot()->getConfig();
    auto nodes = out.getJointNames();
    Ice::StringSeq activeJoints;
    std::vector<float> jointTargets;
    ARMARX_VERBOSE << config;
    for (auto nodeName : nodes)
    {
        auto it = config.find(nodeName);

        if (it != config.end())
        {

            jointTargets.push_back(it->second);
            activeJoints.push_back(it->first);
        }
    }

    out.setJointNames(activeJoints);
    out.setCurrentJointPositions(jointTargets);
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr SetZeroVelocity::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new SetZeroVelocity(stateData));
}

