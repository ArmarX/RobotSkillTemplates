/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::MotionControlGroup
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "SetJointVelocity.h"
#include "MotionControlGroupStatechartContext.h"

using namespace armarx;
using namespace MotionControlGroup;

// DO NOT EDIT NEXT LINE
SetJointVelocity::SubClassRegistry SetJointVelocity::Registry(SetJointVelocity::GetName(), &SetJointVelocity::CreateInstance);

void SetJointVelocity::onEnter()
{
    MotionControlGroupStatechartContext* c = getContext<MotionControlGroupStatechartContext>();
    KinematicUnitInterfacePrx ku = c->getKinematicUnit();
    NameControlModeMap cmodes;
    for (const auto& elem : in.getVelocities())
    {
        cmodes[elem.first] = eVelocityControl;
    }
    ku->switchControlMode(cmodes);
    ku->setJointVelocities(in.getVelocities());
    emitSuccess();
}

// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr SetJointVelocity::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new SetJointVelocity(stateData));
}

