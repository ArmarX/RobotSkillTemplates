/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::MotionControlGroup
 * @author     Mirko Waechter ( mirko dot waechter at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "CollisionFreeJointControl.h"

#include <ArmarXCore/core/util/algorithm.h>
#include <ArmarXCore/core/time/TimeUtil.h>
#include <RobotAPI/interface/components/TrajectoryPlayerInterface.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>

#include <MotionPlanning/Planner/BiRrt.h>

#include "MotionControlGroupStatechartContext.h"

#include <MotionPlanning/PostProcessing/ShortcutProcessor.h>

#include <MotionPlanning/CSpace/CSpacePath.h>

#include <VirtualRobot/TimeOptimalTrajectory/TimeOptimalTrajectory.h>
#include <VirtualRobot/RobotNodeSet.h>

#include <RobotAPI/libraries/core/Trajectory.h>
#include <RobotAPI/libraries/core/TrajectoryController.h>

#include <ArmarXCore/core/time/CycleUtil.h>

using namespace armarx;
using namespace MotionControlGroup;

// DO NOT EDIT NEXT LINE
CollisionFreeJointControl::SubClassRegistry CollisionFreeJointControl::Registry(CollisionFreeJointControl::GetName(), &CollisionFreeJointControl::CreateInstance);



void CollisionFreeJointControl::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)

}

void CollisionFreeJointControl::run()
{
    // put your user code for the execution-phase here
    // runs in seperate thread, thus can do complex operations
    // should check constantly whether isRunningTaskStopped() returns true
    TIMING_START(FullCalc);

    MotionControlGroupStatechartContext* context = getContext<MotionControlGroupStatechartContext>();
    std::set<std::string> jointNameSet;
    if (!in.isSingleGoalConfigSet() && !in.isGoalConfigSet())
    {
        ARMARX_WARNING << "Either GoalConfig or SingleGoalConfig input parameter must be set!";
        emitFailure();
        return;
    }
    ARMARX_INFO << VAROUT(in.isGoalConfigSet()) << VAROUT(in.isSingleGoalConfigSet());
    if (in.isSingleGoalConfigSet())
    {
        ARMARX_INFO << VAROUT(in.getSingleGoalConfig());
    }
    std::vector<StringValueMapPtr> goalConfig;
    if (in.isGoalConfigSet())
    {
        goalConfig = in.getGoalConfig();
    }
    else
    {
        auto mapPtr = StringValueMap::FromStdMap(in.getSingleGoalConfig());
        ARMARX_CHECK_EXPRESSION(mapPtr);
        goalConfig.push_back(mapPtr);
    }
    // check for joint conflict
    ARMARX_INFO << VAROUT(goalConfig.size());
    for (auto goalConfigMapPtr : goalConfig)
    {
        ARMARX_CHECK_EXPRESSION(goalConfigMapPtr);
        auto goalConfigMap = goalConfigMapPtr->toStdMap<float>();
        for (auto pair : goalConfigMap)
        {
            if (jointNameSet.count(pair.first) != 0)
            {
                ARMARX_ERROR << "joint " << pair.first << " is used twice!";
                emitFailure();
                return;
            }
            jointNameSet.insert(pair.first);
        }

    }

    float timeStep = in.getTimestep();
    std::vector<TrajectoryPtr> solutions;
    TIMING_START(robotCloning);

    auto robot = context->getRobotPool()->getRobot(in.getSafetyMargin());
    TIMING_END(robotCloning);
    //    TIMING_START(robotInflation);
    //    for (auto& node : robot->getRobotNodes())
    //    {
    //        if (node->getCollisionModel())
    //        {
    //            node->getCollisionModel()->inflateModel(30);
    //        }
    //    }
    //    //    robot->inflateCollisionModel(30);
    //    TIMING_END(robotInflation);
    if (!in.isCollisionSetNamesSet() && !in.isCollisionSetPairsSet())
    {
        throw LocalException() << "You need to either specify CollisionSetNames or CollisionSetPairs!";
    }

    VirtualRobot::CDManagerPtr cdm(new VirtualRobot::CDManager(robot->getCollisionChecker()));


    auto rnsList = in.isCollisionSetNamesSet() ? in.getCollisionSetNames() : Ice::StringSeq();
    for (size_t i = 0; i < rnsList.size(); ++i)
    {
        auto rns1 = robot->getRobotNodeSet(rnsList.at(i));
        ARMARX_CHECK_NOT_NULL(rns1);
        cdm->addCollisionModel(rns1);
        //        for (size_t j = i + 1; j < rnsList.size(); ++j)
        //        {
        //            auto rns1 = robot->getRobotNodeSet(rnsList.at(i));
        //            auto rns2 = robot->getRobotNodeSet(rnsList.at(j));
        //            ARMARX_CHECK_NOT_NULL(rns1);
        //            ARMARX_CHECK_NOT_NULL(rns2);
        //            ARMARX_INFO << "Adding: " << rns1->getName() << " and " << rns2->getName();
        //            cdm->addCollisionModelPair(rns1, rns2);
        //        }
    }

    auto rnsPairList = in.getCollisionSetPairs();
    for (auto& subList : rnsPairList)
    {
        ARMARX_CHECK_EQUAL(subList->getSize(), 2);
        auto getRns = [&](const std::string & name)
        {
            VirtualRobot::RobotNodeSetPtr rns;
            if (robot->hasRobotNodeSet(name))
            {
                rns = robot->getRobotNodeSet(name);
            }
            else
            {
                if (robot->hasRobotNode(name))
                {
                    auto node = robot->getRobotNode(name);
                    rns = VirtualRobot::RobotNodeSet::createRobotNodeSet(robot, node->getName(), {node});
                }

            }
            ARMARX_CHECK_EXPRESSION(rns) << name;
            return rns;
        };

        auto rns1 = getRns(subList->getVariant(0)->getString());
        auto rns2 = getRns(subList->getVariant(1)->getString());

        cdm->addCollisionModelPair(rns1, rns2);
    }

    for (auto goalConfigMapPtr : goalConfig)
    {
        if (isRunningTaskStopped())
        {
            break;
        }
        auto goalConfigMap = goalConfigMapPtr->toStdMap<float>();
        Ice::StringSeq jointNames = getMapKeys(goalConfigMap);


        auto rns = VirtualRobot::RobotNodeSet::createRobotNodeSet(robot, "tempset", jointNames, robot->getRootNode()->getName());
        //    auto rns = robot->getRobotNodeSet(in.getKinematicChain());
        //    auto jointNames = rns->getNodeNames();
        Saba::CSpaceSampledPtr cspace(new Saba::CSpaceSampled(robot, cdm, rns, 1000000));

        Saba::BiRrt rrt(cspace, Saba::Rrt::eExtend);
        rrt.setPlanningTimeout(in.getTimeoutMs());
        RemoteRobot::synchronizeLocalClone(robot, context->getRobotStateComponent());

        Eigen::VectorXf goalConfig(rns->getSize());
        for (std::size_t i = 0; i < rns->getSize(); ++i)
        {
            goalConfig(i) = goalConfigMap.at(rns->getNode(i)->getName());
        }
        rrt.setStart(rns->getJointValuesEigen());
        rrt.setGoal(goalConfig);
        TIMING_START(Planning);
        auto planningOk = rrt.plan();
        TIMING_END(Planning);
        Saba::CSpacePathPtr solutionOptimized;
        if (planningOk)
        {
            ARMARX_INFO << " Planning succeeded ";
            auto solution = rrt.getSolution();
            TIMING_START(ShortCutting);

            //            Saba::ElasticBandProcessorPtr postProcessing(new Saba::ElasticBandProcessor(solution, cspace, false));
            Saba::ShortcutProcessorPtr postProcessing(new Saba::ShortcutProcessor(solution, cspace, false));
            solutionOptimized = postProcessing->shortenSolutionRandom(in.getOptimizationSteps(), in.getOptimizationMaxStepSize());
            TIMING_END(ShortCutting);
        }
        else
        {
            ARMARX_INFO << " Planning failed";
            emitPlanningFailed();
            return;
        }




        auto waypoints = solutionOptimized->getPoints();
        ARMARX_INFO << "Waypoint Count: " << waypoints.size();
        TrajectoryPtr waypointTraj = new Trajectory();
        LimitlessStateSeq limitlessStates;
        for (std::size_t i = 0; i < rns->getSize(); ++i)
        {
            auto node = rns->getNode(i);
            limitlessStates.push_back(LimitlessState {node->isLimitless(), node->getJointLimitLow(), node->getJointLimitHigh()});
        }
        waypointTraj->setDimensionNames(jointNames);
        waypointTraj->setLimitless(limitlessStates);
        int t = 0;
        for (auto waypoint : waypoints)
        {
            for (std::size_t i = 0; i < rns->getSize(); ++i)
            {
                waypointTraj->setPositionEntry(t, i, waypoint(i));
            }
            t++;
        }
        TrajectoryController::UnfoldLimitlessJointPositions(waypointTraj);

        Eigen::VectorXd maxVelocities = Eigen::VectorXd::Constant(rns->getSize(), (double)in.getMaxVelocity());
        Eigen::VectorXd maxAcceleration = Eigen::VectorXd::Constant(rns->getSize(), (double)in.getMaxAcceleration());
        if (in.isJointScaleSet())
        {
            auto scalingMap = in.getJointScale();
            for (size_t i = 0; i < rns->getSize(); ++i)
            {
                if (scalingMap.at(rns->getNode(i)->getName()) > 0)
                {
                    auto scale = scalingMap.at(rns->getNode(i)->getName());
                    maxVelocities(i) *= scale;
                    maxAcceleration(i) *= scale;
                }
            }
        }
        auto newTraj = waypointTraj->calculateTimeOptimalTrajectory(maxVelocities, maxAcceleration, in.getMaxDeviation(), IceUtil::Time::secondsDouble(timeStep));
        TrajectoryController::FoldLimitlessJointPositions(newTraj);
        solutions.push_back(newTraj);
    }
    TrajectoryPtr combinedTraj(new Trajectory());
    Ice::StringSeq jointNames;
    int fullTrajDim = 0;
    for (auto& sol : solutions)
    {
        size_t dims = sol->dim();
        Ice::DoubleSeq timestamps = sol->getTimestamps();

        for (size_t d = 0; d < dims; ++d)
        {
            combinedTraj->addPositionsToDimension(fullTrajDim, sol->getDimensionData(d), timestamps);
            jointNames.push_back(sol->getDimensionName(d));
            fullTrajDim++;
        }
    }
    combinedTraj->setDimensionNames(jointNames);
    double duration = combinedTraj->getTimeLength();
    auto rns = VirtualRobot::RobotNodeSet::createRobotNodeSet(robot, "tempset", jointNames, robot->getRootNode()->getName());
    LimitlessStateSeq limitlessStates;
    for (std::size_t i = 0; i < rns->getSize(); ++i)
    {
        auto node = rns->getNode(i);
        limitlessStates.push_back(LimitlessState {node->isLimitless(), node->getJointLimitLow(), node->getJointLimitHigh()});
    }
    combinedTraj->setLimitless(limitlessStates);

    TIMING_END(FullCalc);
    ARMARX_INFO << "traj: " << combinedTraj->output();

    NameValueMap targetVelocities;

    if (in.getUseTrajectoryPlayerComponent())
    {
        TrajectoryPlayerInterfacePrx trajPlayer = context->getProxy<TrajectoryPlayerInterfacePrx>(in.getTrajectoryPlayerName());
        trajPlayer->resetTrajectoryPlayer(false);
        trajPlayer->considerConstraints(false);
        trajPlayer->setLoopPlayback(false);
        trajPlayer->loadJointTraj(combinedTraj);
        trajPlayer->startTrajectoryPlayer();
        auto endTime = trajPlayer->getEndTime();
        double currentTime;;
        do
        {
            ARMARX_INFO << deactivateSpam(1) << "CurrentTime: " << currentTime << " endTime: " << endTime;
            usleep(10000);
            currentTime = trajPlayer->getCurrentTime();
        }
        while (currentTime < endTime && !isRunningTaskStopped());
        if (!isRunningTaskStopped() && currentTime >= endTime)
        {
            if (in.getWaitTimeAfterExecution() > 0)
            {
                ARMARX_INFO << "Waiting for " << in.getWaitTimeAfterExecution();
                TimeUtil::MSSleep(in.getWaitTimeAfterExecution());
            }
            trajPlayer->pauseTrajectoryPlayer();
            emitSuccess();
        }
        else if (isRunningTaskStopped())
        {
            trajPlayer->pauseTrajectoryPlayer();
        }
    }
    else
    {
        TrajectoryController ctrl(combinedTraj, in.getKp());

        CycleUtil cycle(timeStep * 1000);
        NameControlModeMap modes;

        for (auto& jointName : jointNames)
        {
            modes[jointName] = eVelocityControl;
        }
        context->getKinematicUnit()->switchControlMode(modes);
        // uncomment this if you need a continous run function. Make sure to use sleep or use blocking wait to reduce cpu load.
        while (!isRunningTaskStopped()) // stop run function if returning true
        {
            // do your calculations
            RemoteRobot::synchronizeLocalClone(robot, context->getRobotStateComponent());

            Eigen::VectorXf velocityTargets = ctrl.update(timeStep, rns->getJointValuesEigen());
            int i = 0;
            for (auto& jointName : jointNames)
            {
                targetVelocities[jointName] = velocityTargets(i);
                i++;
            }
            context->getKinematicUnit()->setJointVelocities(targetVelocities);
            //        context->getDebugDrawerTopicProxy()->setSphereVisu("trajectory", "timestep" + std::to_string(), new )
            ARMARX_INFO << deactivateSpam(2) << "timestep: " << ctrl.getCurrentTimestamp() << " Duration: " << duration;
            if (ctrl.getCurrentTimestamp() >= duration)
            {
                ARMARX_INFO << "Reached Goal!";
                emitSuccess();
                break;
            }
            cycle.waitForCycleDuration();
        }
        for (auto& jointName : jointNames)
        {
            targetVelocities[jointName] = 0;
        }
        context->getKinematicUnit()->setJointVelocities(targetVelocities);
    }
    RemoteRobot::synchronizeLocalClone(robot, context->getRobotStateComponent());

    for (auto goalConfigMapPtr : goalConfig)
    {
        auto goalConfigMap = goalConfigMapPtr->toStdMap<float>();
        NameValueMap errors;
        for (auto& pair : goalConfigMap)
        {
            errors[pair.first] = robot->getRobotNode(pair.first)->getJointValue();
        }
        ARMARX_INFO << VAROUT(errors) << VAROUT(goalConfigMap);
    }


}

void CollisionFreeJointControl::onBreak()
{
    // put your user code for the breaking point here
    // execution time should be short (<100ms)
    if (in.getUseTrajectoryPlayerComponent())
    {
        MotionControlGroupStatechartContext* context = getContext<MotionControlGroupStatechartContext>();
        TrajectoryPlayerInterfacePrx trajPlayer = context->getProxy<TrajectoryPlayerInterfacePrx>(in.getTrajectoryPlayerName());
        trajPlayer->stopTrajectoryPlayer();
    }
}

void CollisionFreeJointControl::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr CollisionFreeJointControl::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new CollisionFreeJointControl(stateData));
}

