/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2014-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::MotionControlGroup
 * @author     Simon Ottenhaus ( simon dot ottenhaus at kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "CartesianRelativePositionControl.h"
#include "MotionControlGroupStatechartContext.h"

using namespace armarx;
using namespace MotionControlGroup;

// DO NOT EDIT NEXT LINE
CartesianRelativePositionControl::SubClassRegistry CartesianRelativePositionControl::Registry(CartesianRelativePositionControl::GetName(), &CartesianRelativePositionControl::CreateInstance);



CartesianRelativePositionControl::CartesianRelativePositionControl(const XMLStateConstructorParams& stateData) :
    XMLStateTemplate<CartesianRelativePositionControl>(stateData),  CartesianRelativePositionControlGeneratedBase<CartesianRelativePositionControl>(stateData)
{
}

void CartesianRelativePositionControl::onEnter()
{
    if (!in.isTcpRelativeOrientationSet() && !in.isTcpRelativePositionSet())
    {
        ARMARX_ERROR << "TcpRelativeOrientation and/or TcpRelativePosition has to be set, but none has been set.";
        emitFailure();
        return;
    }

    MotionControlGroupStatechartContext* context = getContext<MotionControlGroupStatechartContext>();
    auto robot = context->getRobot();
    Eigen::Matrix4f currentTcpPose = robot->getRobotNodeSet(in.getKinematicChainName())->getTCP()->getPoseInRootFrame();
    std::string rootFrameName = robot->getRootNode()->getName();
    std::string agentName = robot->getName();

    ARMARX_IMPORTANT << "currentTcpPose: " << currentTcpPose;

    auto localRobot = robot->clone();
    if (in.isTcpRelativePositionSet())
    {
        Eigen::Vector3f positionOffset = in.getTcpRelativePosition()->toRootEigen(localRobot);
        Eigen::Vector3f targetPosition = currentTcpPose.block<3, 1>(0, 3) + positionOffset;
        FramedPositionPtr targetPositionPtr = new FramedPosition(targetPosition, rootFrameName, agentName);
        local.setTcpTargetPosition(targetPositionPtr);
        ARMARX_IMPORTANT << "targetPosition: " << targetPosition;
    }
    if (in.isTcpRelativeOrientationSet())
    {
        Eigen::Matrix3f orientationOffset = in.getTcpRelativeOrientation()->toRootEigen(localRobot);
        Eigen::Matrix3f targetOrientation = currentTcpPose.block<3, 3>(0, 0) * orientationOffset;
        FramedOrientationPtr targetOrientationPtr = new FramedOrientation(targetOrientation, rootFrameName, agentName);
        local.setTcpTargetOrientation(targetOrientationPtr);
    }
}

void CartesianRelativePositionControl::run()
{
    // put your user code for the execution-phase here
    // runs in seperate thread, thus can do complex operations
    // should check constantly whether isRunningTaskStopped() returns true

    // uncomment this if you need a continous run function. Make sure to use sleep or use blocking wait to reduce cpu load.
    //    while (!isRunningTaskStopped()) // stop run function if returning true
    //    {
    //        // do your calculations
    //    }

}

void CartesianRelativePositionControl::onBreak()
{
    // put your user code for the breaking point here
    // execution time should be short (<100ms)
}

void CartesianRelativePositionControl::onExit()
{
    MotionControlGroupStatechartContext* context = getContext<MotionControlGroupStatechartContext>();
    auto robot = context->getRobot();
    Eigen::Matrix4f currentTcpPose = robot->getRobotNodeSet(in.getKinematicChainName())->getTCP()->getPoseInRootFrame();

    ARMARX_IMPORTANT << "final Tcp pose: " << currentTcpPose;
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr CartesianRelativePositionControl::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new CartesianRelativePositionControl(stateData));
}

