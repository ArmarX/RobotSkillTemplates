/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::MotionControlGroup
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "Select.h"

//#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>

using namespace armarx;
using namespace MotionControlGroup;

// DO NOT EDIT NEXT LINE
Select::SubClassRegistry Select::Registry(Select::GetName(), &Select::CreateInstance);

void Select::onEnter()
{
    ARMARX_CHECK_GREATER_EQUAL(in.getIndex(), 0);

    out.setIndex(in.getIndex() + 1);
    out.setHoldPositionAfterwards(in.getHoldPositionAfterwardsForWaypoints());
    out.setSetVelocitiesToZeroAtEnd(false);

    if (in.getIndex() < in.getStopIndex())
    {
        out.setJointValues(in.getJointValueWaypoints().at(in.getIndex())->toStdMap<float>());

        if (!(out.getIndex() < in.getStopIndex()))
        {
            ARMARX_INFO << "last waypoint " << in.getIndex() << ": " << out.getJointValues();
            out.setHoldPositionAfterwards(in.getHoldPositionAfterwardsForLast());
            out.setSetVelocitiesToZeroAtEnd(in.getSetVelocitiesToZeroAtEnd());
        }
        else
        {
            ARMARX_INFO << "waypoint " << in.getIndex() << ": " << out.getJointValues();
        }
        emitNext();
    }
    else
    {
        ARMARX_INFO << "all waypoints are done";
        emitDone();
    }
}

// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr Select::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new Select(stateData));
}

