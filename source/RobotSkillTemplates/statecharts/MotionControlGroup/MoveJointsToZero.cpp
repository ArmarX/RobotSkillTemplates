/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2014-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::MotionControlGroup
 * @author     Peter Kaiser ( peter dot kaiser at kit dot edu )
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "MoveJointsToZero.h"
#include "MotionControlGroupStatechartContext.h"

#include <RobotAPI/interface/units/KinematicUnitInterface.h>

using namespace armarx;
using namespace MotionControlGroup;

// DO NOT EDIT NEXT LINE
MoveJointsToZero::SubClassRegistry MoveJointsToZero::Registry(MoveJointsToZero::GetName(), &MoveJointsToZero::CreateInstance);



MoveJointsToZero::MoveJointsToZero(XMLStateConstructorParams stateData) :
    XMLStateTemplate<MoveJointsToZero>(stateData),
    MoveJointsToZeroGeneratedBase<MoveJointsToZero>(stateData)
{
}

void MoveJointsToZero::onEnter()
{
    MotionControlGroupStatechartContext* context = getContext<MotionControlGroupStatechartContext>();

    // Set all zero target for all joints of the kinematic chain
    NameValueMap jointTargets;
    std::string kinematicChain = in.getKinematicChainName();

    for (auto& node : context->getRobot()->getRobotNodeSet(kinematicChain)->getAllRobotNodes())
    {
        jointTargets[node->getName()] = 0;
    }

    local.setJointTargetValues(jointTargets);
}


void MoveJointsToZero::onBreak()
{
}

void MoveJointsToZero::onExit()
{
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr MoveJointsToZero::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new MoveJointsToZero(stateData));
}

