/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2014-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::MotionControlGroup
 * @author     Simon Ottenhaus ( simon dot ottenhaus at kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "CalculateRelativePosition.h"

#include <MotionControlGroupStatechartContext.h>

using namespace armarx;
using namespace MotionControlGroup;

// DO NOT EDIT NEXT LINE
CalculateRelativePosition::SubClassRegistry CalculateRelativePosition::Registry(CalculateRelativePosition::GetName(), &CalculateRelativePosition::CreateInstance);



CalculateRelativePosition::CalculateRelativePosition(const XMLStateConstructorParams& stateData) :
    XMLStateTemplate<CalculateRelativePosition>(stateData),  CalculateRelativePositionGeneratedBase<CalculateRelativePosition>(stateData)
{
}

void CalculateRelativePosition::onEnter()
{
    if (!in.isTcpRelativeOrientationsSet() && !in.isTcpRelativePositionsSet())
    {
        ARMARX_ERROR << "TcpRelativeOrientations and/or TcpRelativePositions has to be set, but none has been set.";
        emitFailure();
        return;
    }
    int index = in.getIndex();
    ARMARX_IMPORTANT << "relative position control index: " << index;

    MotionControlGroupStatechartContext* context = getContext<MotionControlGroupStatechartContext>();
    auto robot = context->getRobot();
    std::string rootFrameName = robot->getRootNode()->getName();
    std::string agentName = robot->getName();

    Eigen::Vector3f positionOffset = Eigen::Vector3f::Zero();
    Eigen::Matrix3f orientationOffset = Eigen::Matrix3f::Identity();

    bool doPos = in.isTcpRelativePositionsSet();
    bool doOri = in.isTcpRelativeOrientationsSet();
    out.setIndex(index + 1);

    auto localRobot = robot->clone();
    if (doPos)
    {
        std::vector<FramedDirectionPtr> relativePositions = in.getTcpRelativePositions();
        assert(index >= 0);
        if (static_cast<std::size_t>(index) >= relativePositions.size())
        {
            emitDone();
            return;
        }
        positionOffset = relativePositions.at(index)->toRootEigen(localRobot);
    }
    if (doOri)
    {
        std::vector<FramedOrientationPtr> relativeOrientations = in.getTcpRelativeOrientations();
        assert(index >= 0);
        if (static_cast<std::size_t>(index) >= relativeOrientations.size())
        {
            emitDone();
            return;
        }
        orientationOffset = relativeOrientations.at(index)->toRootEigen(localRobot);
    }

    Eigen::Vector3f referencePosition;
    Eigen::Matrix3f referenceOrientation;

    if (in.getRelativeMode() == "Initial")
    {
        referencePosition = in.getInitalTcpPosition()->toRootEigen(localRobot);
        referenceOrientation = in.getInitailTcpOrientation()->toRootEigen(localRobot);
    }
    else if (in.getRelativeMode() == "TCP")
    {
        Eigen::Matrix4f currentTcpPose = robot->getRobotNodeSet(in.getKinematicChainName())->getTCP()->getPoseInRootFrame();
        referencePosition = currentTcpPose.block<3, 1>(0, 3);
        referenceOrientation = currentTcpPose.block<3, 3>(0, 0);
    }
    else if (in.getRelativeMode() == "Previous")
    {
        referencePosition = doPos ? in.getTcpPosition()->toRootEigen(localRobot) : Eigen::Vector3f::Zero();
        referenceOrientation = doOri ? in.getTcpOrientation()->toRootEigen(localRobot) : Eigen::Matrix3f::Identity();
    }
    else
    {
        ARMARX_ERROR << "Invalid RelativeMode '" << in.getRelativeMode() << "'. Valid options: 'Initial' 'TCP' 'Previous'";
        emitFailure();
        return;
    }

    if (doPos)
    {
        Eigen::Vector3f targetPosition = referencePosition + positionOffset;
        FramedPositionPtr targetPositionPtr = new FramedPosition(targetPosition, rootFrameName, agentName);
        out.setTcpPosition(targetPositionPtr);
    }

    if (doOri)
    {
        Eigen::Matrix3f targetOrientation = referenceOrientation * orientationOffset;
        FramedOrientationPtr targetOrientationPtr = new FramedOrientation(targetOrientation, rootFrameName, agentName);
        out.setTcpOrientation(targetOrientationPtr);
    }

    emitPositionControl();
}

void CalculateRelativePosition::run()
{
    // put your user code for the execution-phase here
    // runs in seperate thread, thus can do complex operations
    // should check constantly whether isRunningTaskStopped() returns true

    // uncomment this if you need a continous run function. Make sure to use sleep or use blocking wait to reduce cpu load.
    //    while (!isRunningTaskStopped()) // stop run function if returning true
    //    {
    //        // do your calculations
    //    }

}

void CalculateRelativePosition::onBreak()
{
    // put your user code for the breaking point here
    // execution time should be short (<100ms)
}

void CalculateRelativePosition::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)

}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr CalculateRelativePosition::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new CalculateRelativePosition(stateData));
}

