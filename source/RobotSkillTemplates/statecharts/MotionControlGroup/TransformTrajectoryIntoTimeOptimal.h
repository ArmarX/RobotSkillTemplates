/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::MotionControlGroup
 * @author     Stefan Reither ( stef dot reither at web dot de )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <RobotSkillTemplates/statecharts/MotionControlGroup/TransformTrajectoryIntoTimeOptimal.generated.h>

namespace armarx::MotionControlGroup
{
    class TransformTrajectoryIntoTimeOptimal :
        public TransformTrajectoryIntoTimeOptimalGeneratedBase < TransformTrajectoryIntoTimeOptimal >
    {
    public:
        TransformTrajectoryIntoTimeOptimal(const XMLStateConstructorParams& stateData):
            XMLStateTemplate < TransformTrajectoryIntoTimeOptimal > (stateData), TransformTrajectoryIntoTimeOptimalGeneratedBase < TransformTrajectoryIntoTimeOptimal > (stateData)
        {
        }

        TrajectoryPtr balanceTimestamps(TrajectoryPtr t);


        // inherited from StateBase
        void onEnter() override;
        void run() override;
        // void onBreak() override;
        void onExit() override;

        // static functions for AbstractFactory Method
        static XMLStateFactoryBasePtr CreateInstance(XMLStateConstructorParams stateData);
        static SubClassRegistry Registry;

        // DO NOT INSERT ANY CLASS MEMBERS,
        // use stateparameters instead,
        // if classmember are neccessary nonetheless, reset them in onEnter
    };
}


