/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2014-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::MotionControlGroup
 * @author     Peter Kaiser ( peter dot kaiser at kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "Wait.h"

using namespace armarx;
using namespace MotionControlGroup;

// DO NOT EDIT NEXT LINE
Wait::SubClassRegistry Wait::Registry(Wait::GetName(), &Wait::CreateInstance);



Wait::Wait(const XMLStateConstructorParams& stateData) :
    XMLStateTemplate<Wait>(stateData),  WaitGeneratedBase<Wait>(stateData)
{
}

void Wait::onEnter()
{
    int duration = in.getDurationMs();

    if (duration < 0)
    {
        ARMARX_ERROR << "Invalid duration: " << duration;
        duration = 0;
    }

    ARMARX_INFO << "Waiting for " << duration << " ms";
    setTimeoutEvent(duration, createEventTimeout());
}

void Wait::onBreak()
{
}

void Wait::onExit()
{
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr Wait::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new Wait(stateData));
}

