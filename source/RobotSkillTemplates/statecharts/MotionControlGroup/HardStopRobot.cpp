/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2014-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::MotionControlGroup
 * @author     Mirko Waechter ( waechter at kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "HardStopRobot.h"
#include "MotionControlGroupStatechartContext.h"

using namespace armarx;
using namespace MotionControlGroup;

// DO NOT EDIT NEXT LINE
HardStopRobot::SubClassRegistry HardStopRobot::Registry(HardStopRobot::GetName(), &HardStopRobot::CreateInstance);



HardStopRobot::HardStopRobot(const XMLStateConstructorParams& stateData) :
    XMLStateTemplate<HardStopRobot>(stateData),  HardStopRobotGeneratedBase<HardStopRobot>(stateData)
{
}

void HardStopRobot::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)

    MotionControlGroupStatechartContext* context = getContext<MotionControlGroupStatechartContext>();

    std::string kinematicChain = "";

    if (in.isKinematicChainNameSet())
    {
        kinematicChain = in.getKinematicChainName();
    }



    Ice::StringSeq nodeNames;

    if (kinematicChain.empty())
    {
        auto start = IceUtil::Time::now();
        nodeNames = context->getRobot()->getSharedRobot()->getRobotNodes();
        ARMARX_INFO << "getting nodes took " << (IceUtil::Time::now() - start).toMilliSecondsDouble() << "ms";
    }
    else
    {
        VirtualRobot::RobotNodeSetPtr nodeSet = context->getRobot()->getRobotNodeSet(kinematicChain);

        for (size_t i = 0; i < nodeSet->getSize(); ++i)
        {
            nodeNames.push_back(nodeSet->getNode(i)->getName());
        }
    }

    ARMARX_INFO << "Stopping joints, kin chain:" << kinematicChain << ", nodes:" << nodeNames;





    NameValueMap zeroJointVelocities;
    NameControlModeMap modes;

    for (auto& name : nodeNames)
    {
        zeroJointVelocities[name] = 0.0f;
        modes[name] = eVelocityControl;
    }

    auto start = IceUtil::Time::now();

    context->getKinematicUnit()->setJointVelocities(zeroJointVelocities);
    context->getKinematicUnit()->switchControlMode(modes);
    ARMARX_INFO << "setting joint velos took " << (IceUtil::Time::now() - start).toMilliSecondsDouble() << "ms";
    start = IceUtil::Time::now();

    NameValueMap jointMap = context->getRobot()->getSharedRobot()->getConfig();
    ARMARX_INFO << "getting robot config took " << (IceUtil::Time::now() - start).toMilliSecondsDouble() << "ms";
    NameValueMap jointPositions;
    modes.clear();

    for (auto& name : nodeNames)
    {
        auto it = jointMap.find(name);

        if (it != jointMap.end())
        {
            jointPositions[name] = it->second;
            modes[name] = ePositionControl;
        }
    }

    ARMARX_INFO << "new joint angles: " << jointPositions;
    context->getKinematicUnit()->setJointAngles(jointPositions);
    context->getKinematicUnit()->switchControlMode(modes);

    emitStopped();

}



void HardStopRobot::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)

}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr HardStopRobot::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new HardStopRobot(stateData));
}

