/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2014-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::MotionControlGroup
 * @author     Mirko Waechter ( mirko dot waechter at kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "CartesianGazeControl.h"
#include "MotionControlGroupStatechartContext.h"

using namespace armarx;
using namespace MotionControlGroup;

// DO NOT EDIT NEXT LINE
CartesianGazeControl::SubClassRegistry CartesianGazeControl::Registry(CartesianGazeControl::GetName(), &CartesianGazeControl::CreateInstance);



CartesianGazeControl::CartesianGazeControl(XMLStateConstructorParams stateData) :
    XMLStateTemplate<CartesianGazeControl>(stateData),  CartesianGazeControlGeneratedBase<CartesianGazeControl>(stateData)
{
}

void CartesianGazeControl::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
}


void CartesianGazeControl::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
    MotionControlGroupStatechartContext* context = getContext<MotionControlGroupStatechartContext>();
    context->getDebugDrawerTopicProxy()->removePoseDebugLayerVisu("gazeTarget");

}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr CartesianGazeControl::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new CartesianGazeControl(stateData));
}

