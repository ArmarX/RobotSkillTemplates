/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::MotionControlGroup
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "NextTrajectory.h"

#include <thread>

using namespace armarx;
using namespace MotionControlGroup;

// DO NOT EDIT NEXT LINE
NextTrajectory::SubClassRegistry NextTrajectory::Registry(NextTrajectory::GetName(), &NextTrajectory::CreateInstance);



void NextTrajectory::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
    auto trajId = in.getTrajectoryId();
    out.setSkipFirstTimepoint(trajId % 2);
    out.setUseNativePositionControl(trajId % 2);

    out.setTrajectoryId(trajId + 1);
    std::string side = trajId % 2 ? "R" : "L";

    TrajectoryPtr t = new Trajectory;

    Ice::DoubleSeq sr = {0, 1, 1.6, 1, 0};
    Ice::DoubleSeq ur = {0, 0, 1.2, 0.1, 0};
    if (!trajId % 2)
    {
        sr = {1, 1.6, 1, 0};
        ur = {0, 1.2, 0.1, 0};
    }
    t->addDimension(sr, {}, "Shoulder 2 " + side);
    t->addDimension(ur, {}, "Upperarm " + side);

    out.setTrajectory(t);
    ARMARX_INFO_S << "next trajectory " << t->output();
    ARMARX_INFO_S << "trajId= " << trajId << (trajId % 2 ? "skip and use native" : "\t don't skip or use native");
    emitSuccess();
}

//void NextTrajectory::run()
//{
//    // put your user code for the execution-phase here
//    // runs in seperate thread, thus can do complex operations
//    // should check constantly whether isRunningTaskStopped() returns true
//
//// uncomment this if you need a continous run function. Make sure to use sleep or use blocking wait to reduce cpu load.
//    while (!isRunningTaskStopped()) // stop run function if returning true
//    {
//        // do your calculations
//    }
//}

//void NextTrajectory::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void NextTrajectory::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr NextTrajectory::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new NextTrajectory(stateData));
}

