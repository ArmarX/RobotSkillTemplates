/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::MotionControlGroup
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "MoveJointTrajectoryTest.h"

using namespace armarx;
using namespace MotionControlGroup;

// DO NOT EDIT NEXT LINE
MoveJointTrajectoryTest::SubClassRegistry MoveJointTrajectoryTest::Registry(MoveJointTrajectoryTest::GetName(), &MoveJointTrajectoryTest::CreateInstance);



void MoveJointTrajectoryTest::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
    local.setHoldPositionAfterwards(true);
    local.setJointMaxAccel(1);
    local.setJointMaxSpeed(0.8);
    local.setJointTargetTolerance(0.03);
    local.setTimeoutInMs(600000);
    local.setMoveToZeroChain("TorsoBothArms");
    local.setNextConfigTolerance(/*10.f / 180.f * M_PI*/0.04);
    local.setTrajectoryId(0);
}

//void MoveJointTrajectoryTest::run()
//{
//    // put your user code for the execution-phase here
//    // runs in seperate thread, thus can do complex operations
//    // should check constantly whether isRunningTaskStopped() returns true
//
//// uncomment this if you need a continous run function. Make sure to use sleep or use blocking wait to reduce cpu load.
//    while (!isRunningTaskStopped()) // stop run function if returning true
//    {
//        // do your calculations
//    }
//}

//void MoveJointTrajectoryTest::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void MoveJointTrajectoryTest::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr MoveJointTrajectoryTest::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new MoveJointTrajectoryTest(stateData));
}

