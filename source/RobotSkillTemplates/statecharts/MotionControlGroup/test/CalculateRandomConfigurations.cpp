/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::MotionControlGroup
 * @author     Mirko Waechter ( mirko dot waechter at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include <random>

#include "CalculateRandomConfigurations.h"
#include "MotionControlGroupStatechartContext.h"
#include <ArmarXCore/core/util/algorithm.h>
#include <ArmarXCore/observers/variant/SingleTypeVariantList.h>
#include <ArmarXCore/observers/variant/StringValueMap.h>
//#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>

using namespace armarx;
using namespace MotionControlGroup;

// DO NOT EDIT NEXT LINE
CalculateRandomConfigurations::SubClassRegistry CalculateRandomConfigurations::Registry(CalculateRandomConfigurations::GetName(), &CalculateRandomConfigurations::CreateInstance);



void CalculateRandomConfigurations::onEnter()
{
    struct Config
    {
        bool used = false;
        Eigen::VectorXf config;
    };
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
    MotionControlGroupStatechartContext* context = getContext<MotionControlGroupStatechartContext>();
    auto nextConfig = in.getSeedConfig();
    std::vector< ::armarx::StringValueMapPtr> newConfigList;
    auto robot = context->getLocalRobot();
    std::random_device rd; // obtain a random number from hardware
    std::mt19937 eng(rd()); // seed the generator
    //    std::vector< ::armarx::SingleTypeVariantListPtr>  configs;

    std::vector<Config> configs;
    std::vector<Eigen::VectorXf> orderedConfigs;

    //    auto goalConfigMap = goalConfigMapPtr->toStdMap<float>();
    auto keys = armarx::getMapKeys(in.getSeedConfig());
    int count = in.getConfigrationCount();
    for (int i = 0; i < count; ++i)
    {
        Eigen::VectorXf config(keys.size());
        int j = 0;
        for (auto& key : keys)
        {
            auto node = robot->getRobotNode(key);
            ARMARX_CHECK_EXPRESSION(node) << key;
            std::uniform_real_distribution<> distr(node->getJointLimitLow(), node->getJointLimitHigh()); // define the range
            //                newConfigMap->addVariant(key, Variant((float)distr(eng)));
            config(j) = distr(eng);
            j++;
        }
        Config c;
        c.config = config;
        configs.push_back(c);
    }
    auto findNearest = [&](const Eigen::VectorXf & config)
    {
        float minDistance = std::numeric_limits<float>::max();
        int index = -1;
        int i = 0;
        for (auto& c : configs)
        {
            if (!c.used)
            {
                float distance = (c.config - config).norm();
                if (distance < minDistance)
                {
                    index = i;
                    minDistance = distance;
                }
            }
            i++;
        }
        ARMARX_CHECK_GREATER(index, -1);
        return index;
    };
    //        for (size_t i = 1; i < configs.size(); ++i)
    ARMARX_CHECK_GREATER(configs.size(), 0);
    orderedConfigs.push_back(configs.begin()->config);
    configs.begin()->used = true;
    int nearestIndex = 0;
    while (orderedConfigs.size() < configs.size())
    {
        nearestIndex = findNearest(configs.at(nearestIndex).config);
        configs.at(nearestIndex).used = true;
        orderedConfigs.push_back(configs.at(nearestIndex).config);
    }
    //    newConfigList.push_back(Variant(newConfigMap));
    std::vector< ::armarx::StringValueMapPtr>  resultConfigs;
    for (auto& c : orderedConfigs)
    {
        StringValueMapPtr newConfigMap(new StringValueMap());
        int i = 0;
        for (auto& key : keys)
        {
            newConfigMap->addVariant(key, c(i));
            i++;
        }
        resultConfigs.push_back(newConfigMap);
    }
    out.setConfigurations(resultConfigs);
    emitSuccess();
}

//void CalculateRandomConfigurations::run()
//{
//    // put your user code for the execution-phase here
//    // runs in seperate thread, thus can do complex operations
//    // should check constantly whether isRunningTaskStopped() returns true
//
//// uncomment this if you need a continous run function. Make sure to use sleep or use blocking wait to reduce cpu load.
//    while (!isRunningTaskStopped()) // stop run function if returning true
//    {
//        // do your calculations
//    }
//}

//void CalculateRandomConfigurations::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void CalculateRandomConfigurations::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr CalculateRandomConfigurations::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new CalculateRandomConfigurations(stateData));
}

