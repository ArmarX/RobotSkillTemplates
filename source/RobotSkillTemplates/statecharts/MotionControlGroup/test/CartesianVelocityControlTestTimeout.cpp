/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2014-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::MotionControlGroup
 * @author     Peter Kaiser ( peter dot kaiser at kit dot edu )
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "CartesianVelocityControlTestTimeout.h"

using namespace armarx;
using namespace MotionControlGroup;

// DO NOT EDIT NEXT LINE
CartesianVelocityControlTestTimeout::SubClassRegistry CartesianVelocityControlTestTimeout::Registry(CartesianVelocityControlTestTimeout::GetName(), &CartesianVelocityControlTestTimeout::CreateInstance);



CartesianVelocityControlTestTimeout::CartesianVelocityControlTestTimeout(XMLStateConstructorParams stateData) :
    XMLStateTemplate<CartesianVelocityControlTestTimeout>(stateData),
    CartesianVelocityControlTestTimeoutGeneratedBase<CartesianVelocityControlTestTimeout>(stateData)
{
}

void CartesianVelocityControlTestTimeout::onEnter()
{
    setTimeoutEvent(in.getTimeout(), createEventTimeout());
}


void CartesianVelocityControlTestTimeout::onBreak()
{
}

void CartesianVelocityControlTestTimeout::onExit()
{
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr CartesianVelocityControlTestTimeout::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new CartesianVelocityControlTestTimeout(stateData));
}

