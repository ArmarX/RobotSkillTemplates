/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::MotionControlGroup
 * @author     Mirko Waechter ( mirko dot waechter at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include <random>

#include "CalculateRandomConfiguration.h"

#include "MotionControlGroupStatechartContext.h"
#include <ArmarXCore/core/util/algorithm.h>
#include <ArmarXCore/observers/variant/SingleTypeVariantList.h>
#include <ArmarXCore/observers/variant/StringValueMap.h>
#include <VirtualRobot/BoundingBox.h>
#include <VirtualRobot/EndEffector/EndEffector.h>
//#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>

using namespace armarx;
using namespace MotionControlGroup;

// DO NOT EDIT NEXT LINE
CalculateRandomConfiguration::SubClassRegistry CalculateRandomConfiguration::Registry(CalculateRandomConfiguration::GetName(), &CalculateRandomConfiguration::CreateInstance);



void CalculateRandomConfiguration::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
    MotionControlGroupStatechartContext* context = getContext<MotionControlGroupStatechartContext>();
    auto nextConfig = in.getSeedConfig();
    std::vector< ::armarx::StringValueMapPtr> newConfigList;
    VirtualRobot::RobotPtr robot = context->getLocalRobot();
    VirtualRobot::RobotPtr boundingBoxRobot = robot->clone("BondingBoxRobot");
    std::random_device rd; // obtain a random number from hardware
    std::mt19937 eng(rd()); // seed the generator

    VirtualRobot::BoundingBox tcpBoundingBox;
    if (in.isTCPBoundingBoxSet() && in.isTCPNameSet())
    {
        std::map<std::string, float> bounds = in.getTCPBoundingBox();
        float min_x = bounds.at("min_x");
        float max_x = bounds.at("max_x");
        float min_y = bounds.at("min_y");
        float max_y = bounds.at("max_y");
        float min_z = bounds.at("min_z");
        float max_z = bounds.at("max_z");

        std::vector<Eigen::Vector3f> points;
        points.push_back(Eigen::Vector3f(min_x, min_y, min_z));
        points.push_back(Eigen::Vector3f(min_x, min_y, max_z));
        points.push_back(Eigen::Vector3f(min_x, max_y, min_z));
        points.push_back(Eigen::Vector3f(min_x, max_y, max_z));
        points.push_back(Eigen::Vector3f(max_x, min_y, min_z));
        points.push_back(Eigen::Vector3f(max_x, min_y, max_z));
        points.push_back(Eigen::Vector3f(max_x, max_y, min_z));
        points.push_back(Eigen::Vector3f(max_x, max_y, max_z));

        tcpBoundingBox = VirtualRobot::BoundingBox(points);
    }

    for (auto goalConfigMapPtr : nextConfig)
    {
        StringValueMapPtr newConfigMap(new StringValueMap());
        do
        {
            newConfigMap->clear();
            auto goalConfigMap = goalConfigMapPtr->toStdMap<float>();
            auto keys = armarx::getMapKeys(goalConfigMap);
            for (auto& key : keys)
            {
                auto node = robot->getRobotNode(key);
                ARMARX_CHECK_EXPRESSION(node) << key;
                std::uniform_real_distribution<> distr(node->getJointLimitLow(), node->getJointLimitHigh()); // define the range
                newConfigMap->addVariant(key, Variant((float)distr(eng)));
            }
            boundingBoxRobot->setJointValues(newConfigMap->toStdMap<float>());
        }
        while (!checkBoundingBox(boundingBoxRobot, tcpBoundingBox, in.isTCPBoundingBoxSet() && in.isTCPNameSet()));

        newConfigList.push_back(newConfigMap);
    }
    out.setRandomConfig(newConfigList);
    emitSuccess();
}

bool CalculateRandomConfiguration::checkBoundingBox(VirtualRobot::RobotPtr& robot, VirtualRobot::BoundingBox& box, bool active) const
{
    if (!active)
    {
        return true;
    }
    else
    {
        return box.isInside(robot->getRobotNode(in.getTCPName())->getPoseInRootFrame().block<3, 1>(0, 3));
    }
}

//void CalculateRandomConfiguration::run()
//{
//    // put your user code for the execution-phase here
//    // runs in seperate thread, thus can do complex operations
//    // should check constantly whether isRunningTaskStopped() returns true
//
//// uncomment this if you need a continous run function. Make sure to use sleep or use blocking wait to reduce cpu load.
//    while (!isRunningTaskStopped()) // stop run function if returning true
//    {
//        // do your calculations
//    }
//}

//void CalculateRandomConfiguration::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void CalculateRandomConfiguration::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr CalculateRandomConfiguration::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new CalculateRandomConfiguration(stateData));
}

