/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2014-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::MotionControlGroup
 * @author     Peter Kaiser ( peter dot kaiser at kit dot edu )
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "JointPositionControl.h"
#include "MotionControlGroupStatechartContext.h"

#include <RobotAPI/interface/units/KinematicUnitInterface.h>
#include <RobotAPI/libraries/core/RobotStatechartContext.h>

using namespace armarx;
using namespace MotionControlGroup;

// DO NOT EDIT NEXT LINE
JointPositionControl::SubClassRegistry JointPositionControl::Registry(JointPositionControl::GetName(), &JointPositionControl::CreateInstance);



JointPositionControl::JointPositionControl(XMLStateConstructorParams stateData) :
    XMLStateTemplate<JointPositionControl>(stateData),
    JointPositionControlGeneratedBase<JointPositionControl>(stateData)
{
}

void JointPositionControl::onEnter()
{
}


void JointPositionControl::onExit()
{
    MotionControlGroupStatechartContext* context = getContext<MotionControlGroupStatechartContext>();

    auto targetValues = in.getJointTargetValues();
    std::map<std::string, float> distances;

    for (auto& pair : targetValues)
    {
        DataFieldIdentifierBasePtr dataFieldIdentifier = new DataFieldIdentifier(context->getKinematicUnitObserverName(), "jointangles", pair.first);

        if (!dataFieldIdentifier)
        {
            continue;
        }

        VariantBasePtr dataField = context->getKinematicUnitObserver()->getDataField(dataFieldIdentifier);

        if (!dataField || !dataField->getInitialized())
        {
            continue;
        }

        float currentValue = dataField->getFloat();
        distances[pair.first] = pair.second - currentValue;
    }

    out.setJointFinalDistancesToTarget(distances);
}

// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr JointPositionControl::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new JointPositionControl(stateData));
}

