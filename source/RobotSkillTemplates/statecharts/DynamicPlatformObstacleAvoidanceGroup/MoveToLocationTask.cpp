/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::DynamicPlatformObstacleAvoidanceGroup
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "MoveToLocationTask.h"

//#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>

namespace armarx::DynamicPlatformObstacleAvoidanceGroup
{
    // DO NOT EDIT NEXT LINE
    MoveToLocationTask::SubClassRegistry MoveToLocationTask::Registry(MoveToLocationTask::GetName(), &MoveToLocationTask::CreateInstance);

    void MoveToLocationTask::onEnter()
    {
        std::string locationName;
        if (in.isargsSet() && in.getargs().size() > 0)
        {
            locationName = in.getargs().at(0)->entityName;
        }
        else if (in.isLandmarkNameSet())
        {
            locationName = in.getLandmarkName();
        }
        else
        {
            throw LocalException() << "You need to set either LandmarkName or args";
        }
        ARMARX_IMPORTANT << "Moving to location: " << locationName;

        try
        {
            if (in.getSendLocationToTTS() && in.getTextReply().count(locationName))
            {
                std::string const textReply = in.getTextReply().at(locationName);
                getTextToSpeech()->reportText(textReply);
            }
        }
        catch (std::exception const& ex)
        {
            ARMARX_WARNING << "Text output failed: " << ex.what();
        }

        local.settargetLandmark(locationName);
    }

    //void MoveToLocationTask::run()
    //{
    //    // put your user code for the execution-phase here
    //    // runs in seperate thread, thus can do complex operations
    //    // should check constantly whether isRunningTaskStopped() returns true
    //
    //    // get a private kinematic instance for this state of the robot (tick "Robot State Component" proxy checkbox in statechart group)
    //    VirtualRobot::RobotPtr robot = getLocalRobot();
    //
    //// uncomment this if you need a continous run function. Make sure to use sleep or use blocking wait to reduce cpu load.
    //    while (!isRunningTaskStopped()) // stop run function if returning true
    //    {
    //        // do your calculations
    //        // synchronize robot clone to most recent state
    //        RemoteRobot::synchronizeLocalClone(robot, getRobotStateComponent());
    //    }
    //}

    //void MoveToLocationTask::onBreak()
    //{
    //    // put your user code for the breaking point here
    //    // execution time should be short (<100ms)
    //}

    void MoveToLocationTask::onExit()
    {
        // put your user code for the exit point here
        // execution time should be short (<100ms)
    }


    // DO NOT EDIT NEXT FUNCTION
    XMLStateFactoryBasePtr MoveToLocationTask::CreateInstance(XMLStateConstructorParams stateData)
    {
        return XMLStateFactoryBasePtr(new MoveToLocationTask(stateData));
    }
}
