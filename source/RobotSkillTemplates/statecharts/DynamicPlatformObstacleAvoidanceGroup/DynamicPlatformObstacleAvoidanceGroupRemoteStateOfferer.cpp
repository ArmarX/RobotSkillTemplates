/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::DynamicPlatformObstacleAvoidanceGroup::DynamicPlatformObstacleAvoidanceGroupRemoteStateOfferer
 * @author     Mirko Wächter ( mirko dot waechter at kit dot edu )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "DynamicPlatformObstacleAvoidanceGroupRemoteStateOfferer.h"

using namespace armarx;
using namespace DynamicPlatformObstacleAvoidanceGroup;

// DO NOT EDIT NEXT LINE
DynamicPlatformObstacleAvoidanceGroupRemoteStateOfferer::SubClassRegistry DynamicPlatformObstacleAvoidanceGroupRemoteStateOfferer::Registry(DynamicPlatformObstacleAvoidanceGroupRemoteStateOfferer::GetName(), &DynamicPlatformObstacleAvoidanceGroupRemoteStateOfferer::CreateInstance);



DynamicPlatformObstacleAvoidanceGroupRemoteStateOfferer::DynamicPlatformObstacleAvoidanceGroupRemoteStateOfferer(StatechartGroupXmlReaderPtr reader) :
    XMLRemoteStateOfferer < DynamicPlatformObstacleAvoidanceGroupStatechartContext > (reader)
{
}

void DynamicPlatformObstacleAvoidanceGroupRemoteStateOfferer::onInitXMLRemoteStateOfferer()
{

}

void DynamicPlatformObstacleAvoidanceGroupRemoteStateOfferer::onConnectXMLRemoteStateOfferer()
{

}

void DynamicPlatformObstacleAvoidanceGroupRemoteStateOfferer::onExitXMLRemoteStateOfferer()
{

}

// DO NOT EDIT NEXT FUNCTION
std::string DynamicPlatformObstacleAvoidanceGroupRemoteStateOfferer::GetName()
{
    return "DynamicPlatformObstacleAvoidanceGroupRemoteStateOfferer";
}

// DO NOT EDIT NEXT FUNCTION
XMLStateOffererFactoryBasePtr DynamicPlatformObstacleAvoidanceGroupRemoteStateOfferer::CreateInstance(StatechartGroupXmlReaderPtr reader)
{
    return XMLStateOffererFactoryBasePtr(new DynamicPlatformObstacleAvoidanceGroupRemoteStateOfferer(reader));
}



