/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2014-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::ScanLocationGroup
 * @author     Manfred Kroehnert ( Manfred dot Kroehnert at kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "DetectObjects.h"

#include <RobotAPI/interface/components/ViewSelectionInterface.h>

using namespace armarx;
using namespace ScanLocationGroup;

// DO NOT EDIT NEXT LINE
DetectObjects::SubClassRegistry DetectObjects::Registry(DetectObjects::GetName(), &DetectObjects::CreateInstance);



DetectObjects::DetectObjects(XMLStateConstructorParams stateData) :
    XMLStateTemplate<DetectObjects>(stateData),  DetectObjectsGeneratedBase<DetectObjects>(stateData)
{
}

void DetectObjects::onEnter()
{
    classBaseChannels.clear();

    std::vector<std::string> objectClasses = in.getObjectclassesToLocalize();

    if (objectClasses.empty())
    {
        emitFinished();
        return;
    }

    memoryx::ObjectMemoryObserverInterfacePrx memoryObserver = getObjectMemoryObserver();

    Term classesLocalized;

    for (std::string className : objectClasses)
    {
        ChannelRefBasePtr channel = memoryObserver->requestObjectClassOnce(className, armarx::DEFAULT_VIEWTARGET_PRIORITY);

        if (!channel)
        {
            continue;
        }

        classBaseChannels.push_back(channel);
        ChannelRefPtr classChannel = ChannelRefPtr::dynamicCast(channel);

        if (classChannel)
        {
            Literal localizationFinished(classChannel->getDataFieldIdentifier("localizationFinished"), "equals", Literal::createParameterList(true));
            classesLocalized = classesLocalized && localizationFinished;
        }
    }

    if (classBaseChannels.empty())
    {
        emitFinished();
    }
    else
    {
        installCondition(classesLocalized, createEventFinished());
        setTimeoutEvent(in.getLocationTimeout() * 1000.0, createEventFinished());
    }
}

void DetectObjects::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
    memoryx::ObjectMemoryObserverInterfacePrx memoryObserver = getObjectMemoryObserver();

    for (auto& c : classBaseChannels)
    {
        memoryObserver->releaseObjectClass(c);
    }

    classBaseChannels.clear();
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr DetectObjects::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new DetectObjects(stateData));
}

