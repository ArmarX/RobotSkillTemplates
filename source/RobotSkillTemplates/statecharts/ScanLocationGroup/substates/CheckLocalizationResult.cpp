/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::ScanLocationGroup
 * @author     Markus Grotz ( markus dot grotz at kit dot edu )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "CheckLocalizationResult.h"


//#include <ArmarXCore/observers/variant/DatafieldRef.h>


#include <RobotAPI/libraries/core/FramedPose.h>
#include <MemoryX/libraries/memorytypes/entity/ObjectInstance.h>

#include <MemoryX/interface/components/WorkingMemoryInterface.h>
#include <MemoryX/interface/observers/ObjectMemoryObserverInterface.h>
#include <RobotAPI/libraries/ArmarXObjects/ObjectPose.h>


#include <ArmarXCore/observers/variant/ChannelRef.h>


using namespace armarx;
using namespace ScanLocationGroup;

// DO NOT EDIT NEXT LINE
CheckLocalizationResult::SubClassRegistry CheckLocalizationResult::Registry(CheckLocalizationResult::GetName(), &CheckLocalizationResult::CreateInstance);



void CheckLocalizationResult::onEnter()
{

    TimestampVariantPtr startTime = in.getStartTime();

    memoryx::ObjectMemoryObserverInterfacePrx objectMemoryObserver = getObjectMemoryObserver();

    if (in.getObjects().empty())
    {
        ARMARX_WARNING << "Object list is empty.";
        emitFailure();
        return;
    }
    bool all_found = true;
    for (std::string& objectName : in.getObjects())
    {
        bool found = false;
        if (objectName.find("/") != std::string::npos)
        {
            // TODO: Check in ObjectPoseStorage whether object was found
            //       Check timestamp is recently

            // It's an ObjectID.
            ObjectID id(objectName);
            objpose::ObjectPoseSeq objectPoses = objpose::fromIce(getObjectPoseStorage()->getObjectPoses());
            for (const auto& p : objectPoses)
            {
                if (p.objectID == id)
                {
                    ARMARX_IMPORTANT << "Found object in ObjectPoseStorage: " << id;
                    // out.setObjectChannel(nullptr); // Optional
                    // Miss-use instance ID for our ObjectID.
                    found = true;
                    break;
                }
            }
            if (!found)
            {
                ARMARX_WARNING << "Object instance ID '" << objectName << "' looks like an armarx::ObjectID, "
                               << "but no such object was found in ObjectPoseStorage (offering " << objectPoses.size() << " objects)."
                               << "Trying to interpret it as memory instance ID.";
            }
        }
        else
        {
            std::vector<ChannelRefBasePtr> objectInstanceList = objectMemoryObserver->getObjectInstancesByClass(objectName);

            for (ChannelRefBasePtr objectInstanceChannel : objectInstanceList)
            {

                ChannelRefPtr channelRef = ChannelRefPtr::dynamicCast(objectInstanceChannel);
                if (!channelRef || !channelRef->hasDatafield("pose"))
                {
                    continue;
                }

                TimestampVariantPtr localizationTimestamp = channelRef->get<TimestampVariant>("timestamp");

                if (localizationTimestamp->toTime() > startTime->toTime())
                {
                    found = true;
                    break;
                }

            }
        }
        if (!found)
        {
            all_found = false;
            emitIncomplete();
            return;
        }
    }

    if (all_found)
    {
        emitComplete();
    }
    else
    {
        emitIncomplete();
    }
}

//void CheckLocalizationResult::run()
//{
//    // put your user code for the execution-phase here
//    // runs in seperate thread, thus can do complex operations
//    // should check constantly whether isRunningTaskStopped() returns true
//
//// uncomment this if you need a continous run function. Make sure to use sleep or use blocking wait to reduce cpu load.
//    while (!isRunningTaskStopped()) // stop run function if returning true
//    {
//        // do your calculations
//    }
//}

//void CheckLocalizationResult::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void CheckLocalizationResult::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr CheckLocalizationResult::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new CheckLocalizationResult(stateData));
}

