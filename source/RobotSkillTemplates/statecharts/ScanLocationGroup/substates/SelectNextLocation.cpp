/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2014-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::ScanLocationGroup
 * @author     Manfred Kroehnert ( Manfred dot Kroehnert at kit dot edu )
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "SelectNextLocation.h"

using namespace armarx;
using namespace ScanLocationGroup;

// DO NOT EDIT NEXT LINE
SelectNextLocation::SubClassRegistry SelectNextLocation::Registry(SelectNextLocation::GetName(), &SelectNextLocation::CreateInstance);



SelectNextLocation::SelectNextLocation(XMLStateConstructorParams stateData) :
    XMLStateTemplate < SelectNextLocation >(stateData),
    SelectNextLocationGeneratedBase< SelectNextLocation >(stateData)
{
}

void SelectNextLocation::onEnter()
{
    ChannelRefPtr counterId = in.getLocationCounterId();
    std::vector<armarx::FramedPositionPtr> locationList = in.getLocationList();

    int nextLocationIndex = counterId->getDataField("value")->getInt();
    getSystemObserver()->incrementCounter(counterId);

    if (nextLocationIndex < (int)locationList.size())
    {
        out.setnextLocation(locationList.at(nextLocationIndex));
        emitNextLocation();

        {
            objpose::SignalHeadMovementInput input;
            input.action = objpose::HeadMovementAction_Starting;
            input.discardUpdatesIntervalMilliSeconds = in.getObjectPoseStorageDiscardUpdatesIntervalMS();
            getObjectPoseStorage()->signalHeadMovement(input);
        }
    }
    else
    {
        out.setnextLocation(locationList.back());
        emitNoLocation();
    }
}


void SelectNextLocation::onBreak()
{
    // put your user code for the breaking point here
    // execution time should be short (<100ms)
}

void SelectNextLocation::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)

}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr SelectNextLocation::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new SelectNextLocation(stateData));
}

