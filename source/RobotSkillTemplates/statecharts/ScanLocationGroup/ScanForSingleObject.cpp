/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2014-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::ScanLocationGroup
 * @author     Mirko Waechter ( mirko dot waechter at kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "ScanForSingleObject.h"

using namespace armarx;
using namespace ScanLocationGroup;

// DO NOT EDIT NEXT LINE
ScanForSingleObject::SubClassRegistry ScanForSingleObject::Registry(ScanForSingleObject::GetName(), &ScanForSingleObject::CreateInstance);



ScanForSingleObject::ScanForSingleObject(XMLStateConstructorParams stateData) :
    XMLStateTemplate<ScanForSingleObject>(stateData),  ScanForSingleObjectGeneratedBase<ScanForSingleObject>(stateData)
{
}

void ScanForSingleObject::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
    std::vector<std::string> classes;
    classes.push_back(in.getObjectToLocalize());
    local.setObjectclassesToLocalize(classes);
}


void ScanForSingleObject::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)

}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr ScanForSingleObject::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new ScanForSingleObject(stateData));
}

