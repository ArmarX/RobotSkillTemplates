/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2014-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::ScanLocationGroup
 * @author     Manfred Kroehnert ( Manfred dot Kroehnert at kit dot edu )
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "ScanLocation.h"

#include <ArmarXCore/core/time/TimeUtil.h>
#include <ArmarXCore/observers/condition/Term.h>
#include <ArmarXCore/observers/variant/TimestampVariant.h>

using namespace armarx;
using namespace ScanLocationGroup;

// DO NOT EDIT NEXT LINE
ScanLocation::SubClassRegistry ScanLocation::Registry(ScanLocation::GetName(), &ScanLocation::CreateInstance);



ScanLocation::ScanLocation(XMLStateConstructorParams stateData) :
    XMLStateTemplate< ScanLocation >(stateData),
    ScanLocationGeneratedBase< ScanLocation >(stateData)
{
}

void ScanLocation::onEnter()
{

    ChannelRefPtr counterId = ChannelRefPtr::dynamicCast(getSystemObserver()->startCounter(0, "ScanLocationCounter"));
    local.setLocationCounterId(counterId);
    local.setStartTime(new armarx::TimestampVariant(TimeUtil::GetTime()));

    auto display = in.getDisplay_Text();
    if (display.size() == 2)
    {
        getMessageDisplay()->setMessage(display.at(0), display.at(1));
    }
    else
    {
        ARMARX_WARNING << "Display text not set (wrong size): " << display.size();
    }

    std::vector<std::string> objectClasses = in.getObjectclassesToLocalize();

    if (objectClasses.empty())
    {
        emitSuccess();
        return;
    }

    memoryx::ObjectMemoryObserverInterfacePrx memoryObserver = getObjectMemoryObserver();

    Term classesLocalized;
    std::vector<ChannelRefPtr> localizationQueries;

    for (std::string className : objectClasses)
    {
        ChannelRefPtr channel = ChannelRefPtr::dynamicCast(memoryObserver->requestObjectClassRepeated(className, in.getCycleTimeMs(), armarx::DEFAULT_VIEWTARGET_PRIORITY));

        if (!channel)
        {
            continue;
        }
        localizationQueries.push_back(channel);
        //        classBaseChannels.push_back(channel);
        //        ChannelRefPtr classChannel = ChannelRefPtr::dynamicCast(channel);

        //        if (classChannel)
        //        {
        //            Literal localizationFinished(classChannel->getDataFieldIdentifier("localizationFinished"), "equals", Literal::createParameterList(true));
        //            classesLocalized = classesLocalized && localizationFinished;
        //        }
    }
    local.setLocalizationQueries(localizationQueries);

}


void ScanLocation::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
    memoryx::ObjectMemoryObserverInterfacePrx memoryObserver = getObjectMemoryObserver();
    std::vector<armarx::ChannelRefPtr> resultInstances;
    std::vector<std::string> objectClassNames = in.getObjectclassesToLocalize();

    for (std::string const& className : objectClassNames)
    {
        if (className.find("/") != std::string::npos)
        {
            ARMARX_INFO << "Not adding ObjectInstanceChannel for ObjectPoseStorage instance: "
                        << className;
        }
        else
        {
            // TODO: only consider classes at scan locations
            ARMARX_INFO << "Checking for instances of type " << className;
            auto instances = memoryObserver->getObjectInstancesByClass(className);

            for (auto& channel : instances)
            {
                auto objectInstanceChannelRef = ChannelRefPtr::dynamicCast(channel);

                if (objectInstanceChannelRef)
                {
                    resultInstances.push_back(objectInstanceChannelRef);
                }
                else
                {
                    ARMARX_WARNING << "Channelref of instance " << className << " is null";
                }
            }
        }
    }

    ARMARX_IMPORTANT << "Found the following objectInstances object instances: " << resultInstances;
    out.setObjectInstanceChannels(resultInstances);

    for (ChannelRefPtr channel : local.getLocalizationQueries())
    {
        ARMARX_INFO << "Releasing " << channel->channelName;
        memoryObserver->releaseObjectClass(channel);
    }
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr ScanLocation::CreateInstance(XMLStateConstructorParams stateData)
{
    ARMARX_IMPORTANT_S << "ScanLocation::CreateInstance";
    return XMLStateFactoryBasePtr(new ScanLocation(stateData));
}

