/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::ScanLocationGroup::ScanLocationGroupRemoteStateOfferer
 * @author     armar-user ( armar-user at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "ScanLocationGroupRemoteStateOfferer.h"

namespace armarx::ScanLocationGroup
{
    // DO NOT EDIT NEXT LINE
    ScanLocationGroupRemoteStateOfferer::SubClassRegistry ScanLocationGroupRemoteStateOfferer::Registry(ScanLocationGroupRemoteStateOfferer::GetName(), &ScanLocationGroupRemoteStateOfferer::CreateInstance);

    ScanLocationGroupRemoteStateOfferer::ScanLocationGroupRemoteStateOfferer(StatechartGroupXmlReaderPtr reader) :
        XMLRemoteStateOfferer < ScanLocationGroupStatechartContext > (reader)
    {}

    void ScanLocationGroupRemoteStateOfferer::onInitXMLRemoteStateOfferer() {}

    void ScanLocationGroupRemoteStateOfferer::onConnectXMLRemoteStateOfferer() {}

    void ScanLocationGroupRemoteStateOfferer::onExitXMLRemoteStateOfferer() {}

    // DO NOT EDIT NEXT FUNCTION
    std::string ScanLocationGroupRemoteStateOfferer::GetName()
    {
        return "ScanLocationGroupRemoteStateOfferer";
    }

    // DO NOT EDIT NEXT FUNCTION
    XMLStateOffererFactoryBasePtr ScanLocationGroupRemoteStateOfferer::CreateInstance(StatechartGroupXmlReaderPtr reader)
    {
        return XMLStateOffererFactoryBasePtr(new ScanLocationGroupRemoteStateOfferer(reader));
    }
}
