/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    RobotSkillTemplates::RTMotionControlGoup
* @author     Christoph Pohl ( christoph dot pohl at kit dot edu )
* @date       2019
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "RTCartesianPositionControl.h"

//#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>


#include <RobotAPI/libraries/core/Pose.h>
#include <RobotAPI/libraries/core/CartesianPositionController.h>
#include <RobotAPI/interface/units/RobotUnit/NJointCartesianVelocityControllerWithRamp.h>
#include <ArmarXCore/core/time/CycleUtil.h>

using namespace armarx;
using namespace RTMotionControlGoup;

// DO NOT EDIT NEXT LINE
RTCartesianPositionControl::SubClassRegistry RTCartesianPositionControl::Registry(RTCartesianPositionControl::GetName(), &RTCartesianPositionControl::CreateInstance);



void RTCartesianPositionControl::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
}

void RTCartesianPositionControl::run()
{
    // put your user code for the execution-phase here
    // runs in seperate thread, thus can do complex operations
    // should check constantly whether isRunningTaskStopped() returns true
    CycleUtil cycle(10);
    auto robot = getLocalRobot();
    //auto cameraRobot = getLocalRobot(); // receives new copy
    auto targetPosition = in.getTargetPosition();
    auto targetOrientation = in.getTargetOrientation();
    //ARMARX_CHECK_EXPRESSION(robot.get() != cameraRobot.get());

    auto kinematicChainName = in.getKinematicChainName();
    auto nodeset = robot->getRobotNodeSet(kinematicChainName);
    auto tcp = nodeset->getTCP();

    auto ctrlName = "RTCartesianPositionControlTCPCtrl";
    NJointCartesianVelocityControllerWithRampConfigPtr ctrlCfg = new NJointCartesianVelocityControllerWithRampConfig();
    ctrlCfg->KpJointLimitAvoidance = 0;
    ctrlCfg->jointLimitAvoidanceScale = 1;
    ctrlCfg->mode = CartesianSelectionMode::eAll;
    ctrlCfg->nodeSetName = kinematicChainName;
    ctrlCfg->tcpName = tcp->getName();
    ctrlCfg->maxNullspaceAcceleration = 2;
    ctrlCfg->maxPositionAcceleration = in.getTCPAcceleration();
    ctrlCfg->maxOrientationAcceleration = 1;


    auto baseCtrl = getRobotUnit()->createNJointController("NJointCartesianVelocityControllerWithRamp", ctrlName, ctrlCfg);
    ARMARX_CHECK_EXPRESSION(baseCtrl);
    NJointCartesianVelocityControllerWithRampInterfacePrx ctrl = NJointCartesianVelocityControllerWithRampInterfacePrx::checkedCast(baseCtrl);
    ctrl->activateController();

    CartesianPositionController posController(tcp);
    posController.maxOriVel = in.getMaxOrientationVelocity();
    posController.maxPosVel = in.getMaxTCPVelocity();
    posController.KpOri = in.getKpOrientation();
    posController.KpPos = in.getKp();


    FramedPositionPtr targetPositioninRootFrame = new FramedPosition(*targetPosition);
    targetPositioninRootFrame->changeFrame(robot, robot->getRootNode()->getName());

    // uncomment this if you need a continous run function. Make sure to use sleep or use blocking wait to reduce cpu load.
    while (!isRunningTaskStopped()) // stop run function if returning true
    {
        RemoteRobot::synchronizeLocalClone(robot, getRobotStateComponent());
        auto targetPose = Pose(targetOrientation->toRootEigen(robot), targetPosition->toRootEigen(robot)).toEigen();
        float distanceToTarget = (tcp->getPositionInRootFrame() - targetPositioninRootFrame->toEigen()).norm();
        ARMARX_INFO << deactivateSpam(1) << "Distance To Target: " << distanceToTarget;
        ARMARX_INFO << deactivateSpam(1) << "Position Error: " << posController.getPositionError(targetPose);
        ARMARX_INFO << deactivateSpam(1) << "Orientation Error" << posController.getOrientationError(targetPose);

        if (posController.getPositionError(targetPose) < in.getAbsPositionDifference() && posController.getOrientationError(targetPose) < in.getAbsOrientationDifference())
        {
            //if (distanceToTarget < 100){
            ARMARX_INFO << "TARGET REACHED";
            emitTargetReached();
            break;
        }
        Eigen::VectorXf cv = posController.calculate(targetPose, VirtualRobot::IKSolver::All);
        ctrl->setTargetVelocity(cv(0), cv(1), cv(2), cv(3), cv(4), cv(5));

        cycle.waitForCycleDuration();
    }
    ctrl->deactivateAndDeleteController();
}

//void RTCartesianPositionControl::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void RTCartesianPositionControl::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr RTCartesianPositionControl::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new RTCartesianPositionControl(stateData));
}

