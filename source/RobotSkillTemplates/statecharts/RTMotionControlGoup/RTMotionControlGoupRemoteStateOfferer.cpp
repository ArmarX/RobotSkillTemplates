/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::RTMotionControlGoup::RTMotionControlGoupRemoteStateOfferer
 * @author     Christoph Pohl ( christoph dot pohl at kit dot edu )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "RTMotionControlGoupRemoteStateOfferer.h"

using namespace armarx;
using namespace RTMotionControlGoup;

// DO NOT EDIT NEXT LINE
RTMotionControlGoupRemoteStateOfferer::SubClassRegistry RTMotionControlGoupRemoteStateOfferer::Registry(RTMotionControlGoupRemoteStateOfferer::GetName(), &RTMotionControlGoupRemoteStateOfferer::CreateInstance);



RTMotionControlGoupRemoteStateOfferer::RTMotionControlGoupRemoteStateOfferer(StatechartGroupXmlReaderPtr reader) :
    XMLRemoteStateOfferer < RTMotionControlGoupStatechartContext > (reader)
{
}

void RTMotionControlGoupRemoteStateOfferer::onInitXMLRemoteStateOfferer()
{

}

void RTMotionControlGoupRemoteStateOfferer::onConnectXMLRemoteStateOfferer()
{

}

void RTMotionControlGoupRemoteStateOfferer::onExitXMLRemoteStateOfferer()
{

}

// DO NOT EDIT NEXT FUNCTION
std::string RTMotionControlGoupRemoteStateOfferer::GetName()
{
    return "RTMotionControlGoupRemoteStateOfferer";
}

// DO NOT EDIT NEXT FUNCTION
XMLStateOffererFactoryBasePtr RTMotionControlGoupRemoteStateOfferer::CreateInstance(StatechartGroupXmlReaderPtr reader)
{
    return XMLStateOffererFactoryBasePtr(new RTMotionControlGoupRemoteStateOfferer(reader));
}



