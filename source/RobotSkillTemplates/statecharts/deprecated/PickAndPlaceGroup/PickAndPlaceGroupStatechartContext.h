/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2014-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::PickAndPlaceGroup
 * @author     [Author Name] ( [Author Email] )
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once

#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/system/ImportExportComponent.h>
#include <ArmarXCore/statechart/StatechartContext.h>


// Custom Includes
#include <MemoryX/interface/components/WorkingMemoryInterface.h>
#include <MemoryX/interface/observers/ObjectMemoryObserverInterface.h>
#include <MemoryX/interface/components/PriorKnowledgeInterface.h>
#include <RobotAPI/interface/units/KinematicUnitInterface.h>
#include <RobotAPI/interface/units/TCPControlUnit.h>
#include <RobotAPI/interface/observers/KinematicUnitObserverInterface.h>
#include <RobotAPI/interface/units/ForceTorqueUnit.h>
#include <RobotAPI/interface/units/HeadIKUnit.h>
#include <RobotAPI/interface/core/RobotState.h>
//#include <ArmarXSimulation/interface/simulator/SimulatorInterface.h>
#include <VirtualRobot/VirtualRobot.h>



namespace armarx::PickAndPlaceGroup
{

    struct PickAndPlaceGroupStatechartContextProperties : StatechartContextPropertyDefinitions
    {
        PickAndPlaceGroupStatechartContextProperties(std::string prefix):
            StatechartContextPropertyDefinitions(prefix)
        {
            defineOptionalProperty<std::string>("KinematicUnitName", "Name of the kinematic unit that should be used");
            defineOptionalProperty<std::string>("KinematicUnitObserverName", "Name of the kinematic unit observer that should be used");
            defineOptionalProperty<std::string>("RobotStateComponentName", "RobotStateComponent", "Name of the robot state component that should be used");
            defineOptionalProperty<std::string>("TCPControlUnitName", "TCPControlUnit", "Name of the tcp control unit component that should be used");
            //defineOptionalProperty<std::string>("TCPControlUnitObserverName", "TCPControlUnitObserver", "Name of the tcp control unit observer that should be used");
            //defineOptionalProperty<std::string>("ForceTorqueUnitName", "ForceTorqueUnit", "Name of the force torque unit component that should be used");
            defineOptionalProperty<std::string>("ForceTorqueObserverName", "ForceTorqueObserver", "Name of the force torque observer that should be used");
            //defineOptionalProperty<std::string>("SimulatorName", "Simulator", "Name of the simulator component that should be used");
            defineOptionalProperty<std::string>("HeadIKUnitName", "HeadIKUnit", "Name of the head unit that should be used.");
            defineOptionalProperty<std::string>("HeadIKKinematicChainName", "", "Name of the inematic chain that should be used for head IK.");
            defineOptionalProperty<std::string>("WorkingMemoryName", "WorkingMemory", "Name of the WorkingMemory component that should be used");
            defineOptionalProperty<std::string>("ObjectMemoryObserverName", "ObjectMemoryObserver", "Name of the ObjectMemoryObserver component that should be used");
            defineOptionalProperty<std::string>("PriorKnowledgeName", "PriorKnowledge", "Name of the PriorKnowledge component that should be used");
        }
    };

    /**
     * @class PickAndPlaceGroupStatechartContext is a custom implementation of the StatechartContext
     * for a statechart
     */
    class ARMARXCOMPONENT_IMPORT_EXPORT PickAndPlaceGroupStatechartContext :
        virtual public XMLStatechartContext
    {
    public:
        // inherited from Component
        virtual std::string getDefaultName()
        {
            return "PickAndPlaceGroupStatechartContext";
        }
        virtual void onInitStatechartContext();
        virtual void onConnectStatechartContext();


        const VirtualRobot::RobotPtr getRobot()
        {
            return remoteRobot;
        }
        RobotStateComponentInterfacePrx getRobotStateComponent()
        {
            return robotStateComponent;
        }
        KinematicUnitInterfacePrx getKinematicUnit()
        {
            return kinematicUnitPrx;
        }
        std::string getKinematicUnitObserverName()
        {
            return getProperty<std::string>("KinematicUnitObserverName").getValue();
        }
        TCPControlUnitInterfacePrx getTCPControlUnit()
        {
            return tcpControlPrx;
        }
        //std::string getTCPControlUnitObserverName() { return getProperty<std::string>("TCPControlUnitObserverName").getValue(); }
        //ForceTorqueUnitInterfacePrx getForceTorqueUnit() { return ftUnitPrx;}
        ForceTorqueUnitObserverInterfacePrx getForceTorqueUnitObserver()
        {
            return ftUnitObserverPrx;
        }
        std::string getForceTorqueObserverName()
        {
            return getProperty<std::string>("ForceTorqueObserverName").getValue();
        }
        //SimulatorInterfacePrx getSimulatorUnit(){ return simulatorProxy;}
        HeadIKUnitInterfacePrx getHeadIKUnit()
        {
            return headIKUnitPrx;
        }
        std::string getHeadIKKinematicChainName()
        {
            return headIKKinematicChainName;
        }
        memoryx::WorkingMemoryInterfacePrx getWorkingMemoryProxy()
        {
            return workingMemoryProxy;
        }
        memoryx::ObjectMemoryObserverInterfacePrx getObjectMemoryObserverProxy()
        {
            return objectMemoryObserverProxy;
        }
        memoryx::PriorKnowledgeInterfacePrx getPriorKnowledgeProxy()
        {
            return priorKnowledgeProxy;
        }

    private:


        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        virtual PropertyDefinitionsPtr createPropertyDefinitions();


        RobotStateComponentInterfacePrx robotStateComponent;
        KinematicUnitInterfacePrx kinematicUnitPrx;
        KinematicUnitObserverInterfacePrx kinematicUnitObserverPrx;
        TCPControlUnitInterfacePrx tcpControlPrx;
        //TCPControlUnitObserverInterfacePrx tcpControlUnitObserverPrx;
        //ForceTorqueUnitInterfacePrx ftUnitPrx;
        ForceTorqueUnitObserverInterfacePrx ftUnitObserverPrx;
        VirtualRobot::RobotPtr remoteRobot;
        memoryx::WorkingMemoryInterfacePrx workingMemoryProxy;
        memoryx::ObjectMemoryObserverInterfacePrx objectMemoryObserverProxy;
        memoryx::PriorKnowledgeInterfacePrx priorKnowledgeProxy;
        //SimulatorInterfacePrx simulatorProxy;
        HeadIKUnitInterfacePrx headIKUnitPrx;
        std::string headIKKinematicChainName;
        std::string headIKUnitName;
    };

}


