/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2014-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::PickAndPlaceGroup
 * @author     [Author Name] ( [Author Email] )
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "PickAndPlaceGroupStatechartContext.h"

#include <RobotAPI/libraries/core/remoterobot/RemoteRobot.h>
//#include <RobotAPI/libraries/core/Pose.h>

#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/system/ImportExportComponent.h>
#include <ArmarXCore/statechart/Statechart.h>



namespace armarx
{
    namespace PickAndPlaceGroup
    {
        void PickAndPlaceGroupStatechartContext::onInitStatechartContext()
        {
            // Register dependencies
            usingProxy(getProperty<std::string>("KinematicUnitName").getValue());
            usingProxy(getProperty<std::string>("KinematicUnitObserverName").getValue());
            usingProxy(getProperty<std::string>("RobotStateComponentName").getValue());
            usingProxy(getProperty<std::string>("TCPControlUnitName").getValue());
            // usingProxy(getProperty<std::string>("TCPControlUnitObserverName").getValue());
            //usingProxy(getProperty<std::string>("ForceTorqueUnitName").getValue());
            usingProxy(getProperty<std::string>("ForceTorqueObserverName").getValue());
            usingProxy(getProperty<std::string>("WorkingMemoryName").getValue());
            usingProxy(getProperty<std::string>("ObjectMemoryObserverName").getValue());
            usingProxy(getProperty<std::string>("PriorKnowledgeName").getValue());
            /*std::string simPrxName = getProperty<std::string>("SimulatorName").getValue();
            if (!simPrxName.empty())
            {
                usingProxy(simPrxName);
            }*/
            headIKUnitName = getProperty<std::string>("HeadIKUnitName").getValue();
            ARMARX_LOG << eINFO << "headIKUnitName:" << headIKUnitName << flush;
            headIKKinematicChainName = getProperty<std::string>("HeadIKKinematicChainName").getValue();

            if (!headIKUnitName.empty())
            {
                usingProxy(headIKUnitName);
            }
        }


        void PickAndPlaceGroupStatechartContext::onConnectStatechartContext()
        {

            // retrieve proxies
            robotStateComponent = getProxy<RobotStateComponentInterfacePrx>(getProperty<std::string>("RobotStateComponentName").getValue());
            kinematicUnitPrx = getProxy<KinematicUnitInterfacePrx>(getProperty<std::string>("KinematicUnitName").getValue());
            kinematicUnitObserverPrx = getProxy<KinematicUnitObserverInterfacePrx>(getProperty<std::string>("KinematicUnitObserverName").getValue());
            tcpControlPrx = getProxy<TCPControlUnitInterfacePrx>(getProperty<std::string>("TCPControlUnitName").getValue());
            //tcpControlUnitObserverPrx = getProxy<TCPControlUnitObserverInterfacePrx>(getProperty<std::string>("TCPControlUnitObserverName").getValue());
            //ftUnitPrx = getProxy<ForceTorqueUnitInterfacePrx>(getProperty<std::string>("ForceTorqueUnitName").getValue());
            ftUnitObserverPrx = getProxy<ForceTorqueUnitObserverInterfacePrx>(getProperty<std::string>("ForceTorqueObserverName").getValue());
            //simulatorProxy = getProxy<SimulatorInterfacePrx>(getProperty<std::string>("SimulatorName").getValue());
            workingMemoryProxy = getProxy<memoryx::WorkingMemoryInterfacePrx>(getProperty<std::string>("WorkingMemoryName").getValue());
            objectMemoryObserverProxy = getProxy<memoryx::ObjectMemoryObserverInterfacePrx>(getProperty<std::string>("ObjectMemoryObserverName").getValue());
            priorKnowledgeProxy = getProxy<memoryx::PriorKnowledgeInterfacePrx>(getProperty<std::string>("PriorKnowledgeName").getValue());

            if (!headIKUnitName.empty())
            {
                headIKUnitPrx = getProxy<HeadIKUnitInterfacePrx>(headIKUnitName);
                ARMARX_LOG << eINFO << "Fetched headIK proxy " << headIKUnitName << ":" << headIKUnitPrx << ", head IK kin chain:" << headIKKinematicChainName << flush;
            }

            // initialize remote robot
            remoteRobot.reset(new RemoteRobot(robotStateComponent->getSynchronizedRobot()));
            //simulatorProxy = getProxy<SimulatorInterfacePrx>("Simulator");
        }

        PropertyDefinitionsPtr PickAndPlaceGroupStatechartContext::createPropertyDefinitions()
        {
            return PropertyDefinitionsPtr(new PickAndPlaceGroupStatechartContextProperties(
                                              getConfigIdentifier()));
        }
    }


}

