/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2014-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::FindAndGraspObjectGroup
 * @author     Valerij Wittenbeck ( valerij dot wittenbeck at student dot kit dot edu )
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "FindAndGraspObject.h"

using namespace armarx;
using namespace FindAndGraspObjectGroup;

// DO NOT EDIT NEXT LINE
FindAndGraspObject::SubClassRegistry FindAndGraspObject::Registry(FindAndGraspObject::GetName(), &FindAndGraspObject::CreateInstance);



FindAndGraspObject::FindAndGraspObject(XMLStateConstructorParams stateData) :
    XMLStateTemplate < FindAndGraspObject > (stateData)
{
}

void FindAndGraspObject::onEnter()
{
    ARMARX_VERBOSE << "Done FindAndGraspObject::onEnter()" << flush;
}

void FindAndGraspObject::onExit()
{
    ARMARX_VERBOSE << "Done FindAndGraspObject::onExit()" << flush;
}

// DO NOT EDIT NEXT FUNCTION
std::string FindAndGraspObject::GetName()
{
    return "FindAndGraspObject";
}

// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr FindAndGraspObject::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new FindAndGraspObject(stateData));
}

