armarx_component_set_name("FindAndGraspObjectGroup")
set(COMPONENT_LIBS
    ArmarXCoreStatechart
    ArmarXCoreObservers
    RobotAPICore
				   
    MotionControl

    VisionXInterfaces
    MemoryXVirtualRobotHelpers
    MemoryXCore
    MemoryXMemoryTypes
)

# Sources

set(SOURCES
FindAndGraspObjectGroupRemoteStateOfferer.cpp
./FindAndGraspObject.cpp
VisualServoObjectSubGroup/VisualServoObject.cpp
VisualServoObjectSubGroup/LocalizeObject.cpp
VisualServoObjectSubGroup/SelectObject.cpp
VisualServoObjectSubGroup/VisualServoSubGroup/VisualServo.cpp
VisualServoObjectSubGroup/VisualServoSubGroup/WaitForUpdates.cpp
VisualServoObjectSubGroup/VisualServoSubGroup/CalcVelocities.cpp
VisualServoObjectSubGroup/VisualServoWrapper.cpp
GraspingWithTorquesSubGroup/GraspingWithTorques.cpp
GraspingWithTorquesSubGroup/Preshape.cpp
GraspingWithTorquesSubGroup/CloseHandWithTorques.cpp
GraspingWithTorquesSubGroup/CloseHandWithJointAngles.cpp
GraspingWithTorquesSubGroup/InstallTerminateConditions.cpp
LiftObjectSubGroup/LiftObject.cpp
LiftObjectSubGroup/ControlModeChooser.cpp
LiftObjectSubGroup/MoveJointsVelControl.cpp
LiftObjectSubGroup/MoveJointsPosControl.cpp
LiftObjectSubGroup/MoveJointsPosVelControl.cpp
LiftObjectSubGroup/ValidateTcpPose.cpp
#@TEMPLATE_LINE@@COMPONENT_PATH@/@COMPONENT_NAME@.cpp
)

set(HEADERS
FindAndGraspObjectGroupRemoteStateOfferer.h
./FindAndGraspObject.h
./FindAndGraspObjectContext.h
VisualServoObjectSubGroup/VisualServoObject.h
VisualServoObjectSubGroup/LocalizeObject.h
VisualServoObjectSubGroup/SelectObject.h
VisualServoObjectSubGroup/VisualServoSubGroup/VisualServo.h
VisualServoObjectSubGroup/VisualServoSubGroup/WaitForUpdates.h
VisualServoObjectSubGroup/VisualServoSubGroup/CalcVelocities.h
VisualServoObjectSubGroup/VisualServoWrapper.h
GraspingWithTorquesSubGroup/GraspingWithTorques.h
GraspingWithTorquesSubGroup/Preshape.h
GraspingWithTorquesSubGroup/CloseHandWithTorques.h
GraspingWithTorquesSubGroup/CloseHandWithJointAngles.h
GraspingWithTorquesSubGroup/InstallTerminateConditions.h
LiftObjectSubGroup/LiftObject.h
LiftObjectSubGroup/ControlModeChooser.h
LiftObjectSubGroup/MoveJointsVelControl.h
LiftObjectSubGroup/MoveJointsPosControl.h
LiftObjectSubGroup/MoveJointsPosVelControl.h
LiftObjectSubGroup/ValidateTcpPose.h
#@TEMPLATE_LINE@@COMPONENT_PATH@/@COMPONENT_NAME@.h
)

armarx_add_component("${SOURCES}" "${HEADERS}")
