/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2014-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::FindAndGraspObjectGroup
 * @author     Valerij Wittenbeck ( valerij dot wittenbeck at student dot kit dot edu )
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "InstallTerminateConditions.h"

#include <ArmarXCore/observers/variant/DatafieldRef.h>

using namespace armarx;
using namespace FindAndGraspObjectGroup;

// DO NOT EDIT NEXT LINE
InstallTerminateConditions::SubClassRegistry InstallTerminateConditions::Registry(InstallTerminateConditions::GetName(), &InstallTerminateConditions::CreateInstance);



InstallTerminateConditions::InstallTerminateConditions(XMLStateConstructorParams stateData) :
    XMLStateTemplate < InstallTerminateConditions > (stateData)
{
}

void InstallTerminateConditions::onEnter()
{
    ARMARX_VERBOSE << "Entering InstallTerminateConditions::onEnter()";

    ARMARX_DEBUG << "Installing timeoutGrasp condition";
    float timeoutGrasp = getInput<float>("timeoutGrasp");
    condGraspTimeout = setTimeoutEvent(timeoutGrasp, createEvent<EvMinExecutionTimeout>());

    float thresholdVelocity = getInput<float>("thresholdVelocity");
    ARMARX_DEBUG << "Installing allJointVelocitiesLow condition, threshold: " << thresholdVelocity << flush;

    //=======
    //first try ... (with ChannelRef)
    //=======

    /*
    Term allJointVelocitiesLow;
    SingleTypeVariantListPtr jointNamesList = getInput<SingleTypeVariantList>("jointNames");
    for (int i=0; i<jointNamesList->getSize(); i++)
    {
       allJointVelocitiesLow = allJointVelocitiesLow && Literal(getInput<ChannelRef>("jointVelocityChannel")
                     ->getDataFieldIdentifier(jointNamesList->getVariant(i)->getString()), "smaller", Literal::createParameterList(thresholdVelocity));
    }
    condAllJointVelocitiesLow = installCondition(allJointVelocitiesLow, createEvent<EvAllJointVelocitiesLow>());
    */

    //=======
    //second try ... (with DatafieldRef)
    //=======
    Term allJointVelocitiesLow_NEW;
    //SingleTypeVariantListPtr jointNamesList = getInput<SingleTypeVariantList>("jointNames");
    SingleTypeVariantListPtr dataFieldsList = getInput<SingleTypeVariantList>("jointVelocitiesDatafields");

    //for (int i=0; i<jointNamesList->getSize(); i++)
    for (int i = 0; i < dataFieldsList->getSize(); i++)
    {
        allJointVelocitiesLow_NEW = allJointVelocitiesLow_NEW &&
                                    Literal(dataFieldsList->getVariant(i)->get<DatafieldRef>()->getDataFieldIdentifier(),
                                            "inrange", Literal::createParameterList(-thresholdVelocity, thresholdVelocity));
    }

    condAllJointVelocitiesLow = installCondition(allJointVelocitiesLow_NEW, createEvent<EvAllJointVelocitiesLow>());

    ARMARX_VERBOSE << "Done InstallTerminateConditions::onEnter()";
}

void InstallTerminateConditions::onExit()
{
    ARMARX_VERBOSE << "Entering InstallTerminateConditions::onExit()";
    removeTimeoutEvent(condGraspTimeout);
    removeCondition(condAllJointVelocitiesLow);

    //---------
    //set joint velocities manually to zero (DEBUG)
    //---------

    std::string handUnitName = getInput<std::string>("handUnitName");
    ARMARX_DEBUG << "xx..................SENDING OBJECTGRASPED TO hand unit name " << handUnitName << "...................";

    RobotStatechartContext* rsContext = getContext<RobotStatechartContext>();

    if (rsContext->getHandUnit(handUnitName))
    {
        ARMARX_DEBUG << "xx..................SENDING OBJECTGRASPED TO hand unit name " << handUnitName << ".......OK: sending Vitalis............";
        rsContext->getHandUnit(handUnitName)->setObjectGrasped("Vitalis");
    }

    SingleTypeVariantListPtr jointNames = getInput<SingleTypeVariantList>("jointNames");
    //SingleTypeVariantListPtr jointTorquesGraspList = getInput<SingleTypeVariantList>("jointTorquesGrasp");
    NameValueMap jointNamesAndValues;
    NameControlModeMap controlModes;

    for (int j = 0; j < jointNames->getSize(); j++)
    {
        // no, now we set it to zero.... we want to ensure that the object stayes grasped -> apply a small velocity (todo: find a better solution!)
        jointNamesAndValues[jointNames->getVariant(j)->getString()] = 0.0;//1f;  //set everything to zero
        controlModes[jointNames->getVariant(j)->getString()] = eVelocityControl;
    }

    rsContext->kinematicUnitPrx->switchControlMode(controlModes);
    //        rsContext->kinematicUnitPrx->setJointTorques(jointNamesAndValues);
    rsContext->kinematicUnitPrx->setJointVelocities(jointNamesAndValues);
    ARMARX_VERBOSE << "Done InstallTerminateConditions::onExit()";
}

// DO NOT EDIT NEXT FUNCTION
std::string InstallTerminateConditions::GetName()
{
    return "InstallTerminateConditions";
}

// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr InstallTerminateConditions::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new InstallTerminateConditions(stateData));
}

