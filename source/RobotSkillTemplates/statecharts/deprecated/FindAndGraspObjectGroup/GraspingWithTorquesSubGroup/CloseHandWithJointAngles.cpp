/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2014-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::FindAndGraspObjectGroup
 * @author     Valerij Wittenbeck ( valerij dot wittenbeck at student dot kit dot edu )
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "CloseHandWithJointAngles.h"

using namespace armarx;
using namespace FindAndGraspObjectGroup;

// DO NOT EDIT NEXT LINE
CloseHandWithJointAngles::SubClassRegistry CloseHandWithJointAngles::Registry(CloseHandWithJointAngles::GetName(), &CloseHandWithJointAngles::CreateInstance);



CloseHandWithJointAngles::CloseHandWithJointAngles(XMLStateConstructorParams stateData) :
    XMLStateTemplate < CloseHandWithJointAngles > (stateData)
{
}

void CloseHandWithJointAngles::onEnter()
{
    ARMARX_VERBOSE << "Entering CloseHandWithJointAngles::onEnter()";

    RobotStatechartContext* rsContext = getContext<RobotStatechartContext>();
    SingleTypeVariantListPtr jointNames = getInput<SingleTypeVariantList>("jointNames");
    SingleTypeVariantListPtr jointAnglesGraspList = getInput<SingleTypeVariantList>("jointAnglesGrasp");
    NameValueMap jointNamesAndValues;
    NameControlModeMap controlModes;

    if (jointNames->getSize() == jointAnglesGraspList->getSize())
    {
        for (int j = 0; j < jointNames->getSize(); j++)
        {
            jointNamesAndValues[jointNames->getVariant(j)->getString()] = jointAnglesGraspList->getVariant(j)->getFloat();
            controlModes[jointNames->getVariant(j)->getString()] = ePositionControl;
        }
    }
    else
    {
        throw LocalException("stateCloseHandWithJointAngles: List lengths do not match!");
    }

    rsContext->kinematicUnitPrx->switchControlMode(controlModes);
    rsContext->kinematicUnitPrx->setJointAngles(jointNamesAndValues);

    ARMARX_LOG << eINFO << "Installing timeoutGrasp condition";
    float timeoutGrasp = getInput<float>("timeoutGrasp");
    condGraspWithJointAnglesTimeout = setTimeoutEvent(timeoutGrasp, createEvent<EvGraspWithJointAnglesTimeout>());

    ARMARX_VERBOSE << "Done CloseHandWithJointAngles::onEnter()";
}

void CloseHandWithJointAngles::onExit()
{
    removeTimeoutEvent(condGraspWithJointAnglesTimeout);
}

// DO NOT EDIT NEXT FUNCTION
std::string CloseHandWithJointAngles::GetName()
{
    return "CloseHandWithJointAngles";
}

// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr CloseHandWithJointAngles::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new CloseHandWithJointAngles(stateData));
}

