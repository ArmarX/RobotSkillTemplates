/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2014-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::FindAndGraspObjectGroup
 * @author     Valerij Wittenbeck ( valerij dot wittenbeck at student dot kit dot edu )
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "CloseHandWithTorques.h"

using namespace armarx;
using namespace FindAndGraspObjectGroup;

// DO NOT EDIT NEXT LINE
CloseHandWithTorques::SubClassRegistry CloseHandWithTorques::Registry(CloseHandWithTorques::GetName(), &CloseHandWithTorques::CreateInstance);



CloseHandWithTorques::CloseHandWithTorques(XMLStateConstructorParams stateData) :
    XMLStateTemplate < CloseHandWithTorques > (stateData)
{
}

void CloseHandWithTorques::onEnter()
{
    ARMARX_VERBOSE << "Entering CloseHandWithTorques::onEnter()";

    RobotStatechartContext* rsContext = getContext<RobotStatechartContext>();
    SingleTypeVariantListPtr jointNames = getInput<SingleTypeVariantList>("jointNames");
    SingleTypeVariantListPtr jointTorquesGraspList = getInput<SingleTypeVariantList>("jointTorquesGrasp");
    NameValueMap jointNamesAndValues;

    if (jointNames->getSize() == jointTorquesGraspList->getSize())
    {
        for (int j = 0; j < jointNames->getSize(); j++)
        {
            jointNamesAndValues[jointNames->getVariant(j)->getString()] = jointTorquesGraspList->getVariant(j)->getFloat();
        }
    }
    else
    {
        throw LocalException("StateCloseHandWithTorques: List lengths do not match!");
    }

    rsContext->kinematicUnitPrx->setJointTorques(jointNamesAndValues);

    ARMARX_DEBUG << "Installing timeoutMinExecutionTime condition";
    float timeoutMinExecutionTime = getInput<float>("timeoutMinExecutionTime");
    condMinimumExecutionTimeout = setTimeoutEvent(timeoutMinExecutionTime, createEvent<EvMinExecutionTimeout>());

    ARMARX_VERBOSE << "Done CloseHandWithTorques::onEnter()";
}

void CloseHandWithTorques::onExit()
{
    removeTimeoutEvent(condMinimumExecutionTimeout);
}

// DO NOT EDIT NEXT FUNCTION
std::string CloseHandWithTorques::GetName()
{
    return "CloseHandWithTorques";
}

// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr CloseHandWithTorques::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new CloseHandWithTorques(stateData));
}

