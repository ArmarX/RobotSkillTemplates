/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2014-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::FindAndGraspObjectGroup
 * @author     Valerij Wittenbeck ( valerij dot wittenbeck at student dot kit dot edu )
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "GraspingWithTorques.h"
#include <VirtualRobot/RobotNodeSet.h>
#include <VirtualRobot/Robot.h>
#include <ArmarXCore/observers/variant/DatafieldRef.h>

using namespace armarx;
using namespace FindAndGraspObjectGroup;

// DO NOT EDIT NEXT LINE
GraspingWithTorques::SubClassRegistry GraspingWithTorques::Registry(GraspingWithTorques::GetName(), &GraspingWithTorques::CreateInstance);



GraspingWithTorques::GraspingWithTorques(XMLStateConstructorParams stateData) :
    XMLStateTemplate < GraspingWithTorques > (stateData)
{
}

void GraspingWithTorques::onEnter()
{
    ARMARX_VERBOSE << "Entering GraspingWithTorques::onEnter()";

    RobotStatechartContext* context = getContext<RobotStatechartContext>();
    //setLocal("jointVelocityChannel", context->getChannelRef(context->getKinematicUnitObserverName(),"jointvelocities"));
    ChannelRefPtr tempChannelRef = context->getChannelRef(context->getKinematicUnitObserverName(), "jointvelocities");

    VirtualRobot::RobotNodeSetPtr nodeSet = context->remoteRobot->getRobotNodeSet(getInput<std::string>("robotNodeSetName"));    //woher robotNodeSetName holen? (Argument für getRobotNodeSet())

    SingleTypeVariantList jointNames(VariantType::String);
    SingleTypeVariantList dataFields(VariantType::DatafieldRef);

    for (size_t i = 0; i < nodeSet->getSize(); i++)
    {
        jointNames.addVariant(nodeSet->getNode(i)->getName());      //nodes = joints
        dataFields.addVariant(context->getDatafieldRef(tempChannelRef, nodeSet->getNode(i)->getName()));
    }

    setLocal("jointNames", jointNames);
    setLocal("jointVelocitiesDatafields", dataFields);
}

void GraspingWithTorques::onExit()
{
    ARMARX_VERBOSE << "Done GraspingWithTorques::onExit()";
}

// DO NOT EDIT NEXT FUNCTION
std::string GraspingWithTorques::GetName()
{
    return "GraspingWithTorques";
}

// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr GraspingWithTorques::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new GraspingWithTorques(stateData));
}

