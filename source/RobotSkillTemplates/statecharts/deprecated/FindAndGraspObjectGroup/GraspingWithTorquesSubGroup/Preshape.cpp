/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2014-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::FindAndGraspObjectGroup
 * @author     Valerij Wittenbeck ( valerij dot wittenbeck at student dot kit dot edu )
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "Preshape.h"

using namespace armarx;
using namespace FindAndGraspObjectGroup;

// DO NOT EDIT NEXT LINE
Preshape::SubClassRegistry Preshape::Registry(Preshape::GetName(), &Preshape::CreateInstance);



Preshape::Preshape(XMLStateConstructorParams stateData) :
    XMLStateTemplate < Preshape > (stateData)
{
}

void Preshape::onEnter()
{
    ARMARX_VERBOSE << "Entering Preshape::onEnter()";

    RobotStatechartContext* rsContext = getContext<RobotStatechartContext>();
    SingleTypeVariantListPtr jointNames = getInput<SingleTypeVariantList>("jointNames");
    SingleTypeVariantListPtr jointAnglesPreshapeList = getInput<SingleTypeVariantList>("jointAnglesPreshape");
    NameValueMap jointNamesAndValues;

    if (jointNames->getSize() == jointAnglesPreshapeList->getSize())
    {
        for (int j = 0; j < jointNames->getSize(); j++)
        {
            jointNamesAndValues[jointNames->getVariant(j)->getString()] = jointAnglesPreshapeList->getVariant(j)->getFloat();
        }
    }
    else
    {
        throw LocalException("Preshape: List lengths do not match!");
    }

    rsContext->kinematicUnitPrx->setJointAngles(jointNamesAndValues);

    ARMARX_DEBUG << "Installing preshape timeout condition";
    float timeoutPreshape = getInput<float>("timeoutPreshape");

    bool useTorquesForGrasping = getInput<bool>("useTorquesForGrasping");

    if (useTorquesForGrasping)
    {
        condPreshapeTimeout = setTimeoutEvent(timeoutPreshape, createEvent<EvPreshapeTimeout_ToCloseWithTorques>());
    }
    else
    {
        condPreshapeTimeout = setTimeoutEvent(timeoutPreshape, createEvent<EvPreshapeTimeout_ToCloseWithJointAngles>());
    }

    ARMARX_VERBOSE << "Done Preshape::onEnter()";
}

void Preshape::onExit()
{
    removeTimeoutEvent(condPreshapeTimeout);
    ARMARX_VERBOSE << "Done Preshape::onExit()";
}

// DO NOT EDIT NEXT FUNCTION
std::string Preshape::GetName()
{
    return "Preshape";
}

// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr Preshape::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new Preshape(stateData));
}

