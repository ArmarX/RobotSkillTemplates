/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2014-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::FindAndGraspObjectGroup
 * @author     Valerij Wittenbeck ( valerij dot wittenbeck at student dot kit dot edu )
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "VisualServo.h"

using namespace armarx;
using namespace FindAndGraspObjectGroup;

// DO NOT EDIT NEXT LINE
VisualServo::SubClassRegistry VisualServo::Registry(VisualServo::GetName(), &VisualServo::CreateInstance);



VisualServo::VisualServo(XMLStateConstructorParams stateData) :
    XMLStateTemplate < VisualServo > (stateData)
{
}

void VisualServo::onEnter()
{
    ARMARX_VERBOSE << "Entering VisualServo::onEnter()";

    // timeout condition (Failure)
    float visualServoTimeoutMS = getInput<float>("timeoutTotalMsec");
    condServoTimeout = setTimeoutEvent(visualServoTimeoutMS, createEvent<Failure>());

    float minRecognitionLikelihood = getInput<float>("minRecognitionLikelihood");

    //OLD:
    /*
    // object lost condition (Failure)                                    //TODO (maybe): move this to the VSExample (where the condObjectUpdate also is)
    ChannelRefPtr objectChannel = getInput<ChannelRef>("objectChannel");
    Literal objectLost(*objectChannel->getDataFieldIdentifier("existenceCertainty"), "smaller", Literal::createParameterList(minRecognitionLikelihood));
    condObjectLost = installCondition(objectLost, createEvent<Failure>());
    */

    // marker lost condition (Failure)
    ChannelRefPtr markerChannel = getInput<ChannelRef>("markerChannel");
    Literal markerLost(*markerChannel->getDataFieldIdentifier("existenceCertainty"), "smaller", Literal::createParameterList(minRecognitionLikelihood));
    condMarkerLost = installCondition(markerLost, createEvent<Failure>());

    ARMARX_IMPORTANT << "Done VisualServo::onEnter()";
}

void VisualServo::onExit()
{
    //    VisualServoContext* visualServoContext = getContext<VisualServoContext>();

    //    std::string robotNodeSetName = getInput<std::string>("kinematicChain");
    //        VirtualRobot::RobotPtr localRobot = RemoteRobot::createLocalClone(visualServoContext->robotStateComponent);
    //    VirtualRobot::RobotNodeSetPtr robotNodeSet = visualServoContext->remoteRobot->getRobotNodeSet(robotNodeSetName);
    //        VirtualRobot::RobotNodeSetPtr robotNodeSet = localRobot->getRobotNodeSet(robotNodeSetName);

    // Stop the robot
    // build name value map
    //        NameValueMap targetVelocities;
    ////        NameValueMap targetPositions;
    //        NameControlModeMap controlModes;
    //        const std::vector< VirtualRobot::RobotNodePtr > nodes =  robotNodeSet->getAllRobotNodes();
    //        std::vector< VirtualRobot::RobotNodePtr >::const_iterator iter = nodes.begin();
    //        while (iter != nodes.end())
    //        {
    //            targetVelocities.insert(std::make_pair((*iter)->getName(), 0.0f));
    //            controlModes.insert(std::make_pair((*iter)->getName(), eVelocityControl));
    //            iter++;
    //        };
    //        // execute velocities
    //        visualServoContext->kinematicUnitPrx->switchControlMode(controlModes);
    //        visualServoContext->kinematicUnitPrx->setJointVelocities(targetVelocities);


    removeTimeoutEvent(condServoTimeout);
    //removeCondition(condObjectLost);            //TODO (maybe): move this to the VSExample (where the condObjectUpdate also is)
    removeCondition(condMarkerLost);

    ARMARX_VERBOSE << "Done VisualServo::onExit()";
}

// DO NOT EDIT NEXT FUNCTION
std::string VisualServo::GetName()
{
    return "VisualServo";
}

// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr VisualServo::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new VisualServo(stateData));
}

