/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2014-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::FindAndGraspObjectGroup
 * @author     Valerij Wittenbeck ( valerij dot wittenbeck at student dot kit dot edu )
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "WaitForUpdates.h"

using namespace armarx;
using namespace FindAndGraspObjectGroup;

// DO NOT EDIT NEXT LINE
WaitForUpdates::SubClassRegistry WaitForUpdates::Registry(WaitForUpdates::GetName(), &WaitForUpdates::CreateInstance);



WaitForUpdates::WaitForUpdates(XMLStateConstructorParams stateData) :
    XMLStateTemplate < WaitForUpdates > (stateData)
{
}

void WaitForUpdates::onEnter()
{
    // retrieve channels
    //ChannelRefPtr objectChannel = getInput<ChannelRef>("objectChannel");         //2014-03-07: not needed any more
    ChannelRefPtr markerChannel = getInput<ChannelRef>("markerChannel");

    float minRecognitionLikelihood = getInput<float>("minRecognitionLikelihood");

    // condition for object update (EvObjectPoseUpdated)

    //OLD:
    /*
    Literal objectUpdated(*objectChannel->getDataFieldIdentifier("existenceCertainty"), "larger", Literal::createParameterList(minRecognitionLikelihood));
    Literal markerUpdated(*markerChannel->getDataFieldIdentifier("existenceCertainty"), "larger", Literal::createParameterList(minRecognitionLikelihood));
    Term objectOrMarkerUpdated = objectUpdated || markerUpdated;

    condObjectPoseUpdated = installCondition( objectUpdated,
                                createEvent<EvObjectPoseUpdate>());
                                */

    //NEW: now only marker update
    Literal markerUpdated(*markerChannel->getDataFieldIdentifier("existenceCertainty"), "larger", Literal::createParameterList(minRecognitionLikelihood));
    condMarkerPoseUpdated = installCondition(markerUpdated, createEvent<EvMarkerPoseUpdate>());


    //        Vector3Ptr relativePosition = getInput<Vector3>("relativeTargetPosition");
    //        QuaternionPtr relativeOrientation = getInput<Quaternion>("relativeTargetOrientation");


    // TODO: target reached is not nice in this state.  (LEGACY TODO)
    // Once available install this as condition in toplevel state
    // also remove the reference frame input
    //    VisualServoContext* visualServoContext = getContext<VisualServoContext>();
    //std::string refFrame = getInput<std::string>("referenceFrame");

    //OLD:
    //Eigen::Matrix4f objectPose_ref = getObjectPoseInRefFrame(objectChannel, refFrame, visualServoContext);  //TODO: get objectPose_ref from elsewhere! (replace by framedPose)
    //NEW:
    FramedPosePtr targetPose = getInput<FramedPose>("targetPose");
    //Eigen::Matrix4f objectPose_ref = targetPose->toEigen();

    std::string frame = targetPose->getFrame();
    ARMARX_DEBUG << "Using frame " << frame;

    //Eigen::Matrix4f markerPose_ref = getObjectPoseInRefFrame(markerChannel, frame, visualServoContext);
    /*
        float distance = (objectPose_ref.block(0,3,3,1) - markerPose_ref.block(0,3,3,1)).norm();

        ARMARX_LOG << "Distance: " << distance << flush;
        if(distance < 16.3f && distance >= 15.7f)
        {
            EventPtr reached = new EvTargetReached(EVENTTOALL);
            sendEvent(reached);
        }*/

    ARMARX_VERBOSE << "Done WaitForUpdates::onEnter()";
}

void WaitForUpdates::onExit()
{
    //removeCondition(condObjectPoseUpdated);   //OLD
    removeCondition(condMarkerPoseUpdated);     //NEW
}

// DO NOT EDIT NEXT FUNCTION
std::string WaitForUpdates::GetName()
{
    return "WaitForUpdates";
}

// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr WaitForUpdates::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new WaitForUpdates(stateData));
}

