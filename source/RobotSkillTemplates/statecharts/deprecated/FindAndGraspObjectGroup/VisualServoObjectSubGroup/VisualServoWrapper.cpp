/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2014-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::FindAndGraspObjectGroup
 * @author     Valerij Wittenbeck ( valerij dot wittenbeck at student dot kit dot edu )
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "VisualServoWrapper.h"
#include <VirtualRobot/Grasping/Grasp.h>

using namespace armarx;
using namespace FindAndGraspObjectGroup;

// DO NOT EDIT NEXT LINE
VisualServoWrapper::SubClassRegistry VisualServoWrapper::Registry(VisualServoWrapper::GetName(), &VisualServoWrapper::CreateInstance);



VisualServoWrapper::VisualServoWrapper(XMLStateConstructorParams stateData) :
    XMLStateTemplate < VisualServoWrapper > (stateData)
{
}

void VisualServoWrapper::onEnter()
{
    //We need to set some inputs as outputs, because the successor state might be this state again ... and again .. and again.
    setOutput("objectInstanceChannel", getInput<ChannelRef>("objectInstanceChannel"));
    setOutput("markerInstanceChannel", getInput<ChannelRef>("markerInstanceChannel"));

    //NEW: object pose updates HERE (instead of StateWaitForUpdates in VisualServo)
    //setLocal("referenceFrame", "Platform");

    // retrieve channels
    ChannelRefPtr objectInstanceChannel = getInput<ChannelRef>("objectInstanceChannel");

    float minRecognitionLikelihood = getInput<float>("minObjectCertainty");

    Literal objectUpdated(*objectInstanceChannel->getDataFieldIdentifier("existenceCertainty"), "larger", Literal::createParameterList(minRecognitionLikelihood));
    condObjectPoseUpdated = installCondition(objectUpdated, createEvent<EvObjectPoseUpdate>());

    // object lost condition (Failure)
    //ChannelRefPtr objectChannel = getInput<ChannelRef>("objectChannel");
    Literal objectLost(*objectInstanceChannel->getDataFieldIdentifier("existenceCertainty"), "smaller", Literal::createParameterList(minRecognitionLikelihood));
    condObjectLost = installCondition(objectLost, createEvent<Failure>());


    //=============

    //TODO here:
    //-(OK..)channels -> positions
    //-(OK..)perform relative transform ( given by graspDefinition )
    //-(OK..)set positions to output! ( setLocal() )
    //-(OK..)install condition for object pose update --> has to be done in the parent state (VisualServoingExampleStatechart)

    //TAKEN FROM VisualServo.cpp, StateCalcVelocities::onEnter()

    FindAndGraspObjectContext* context = getContext<FindAndGraspObjectContext>();

    //        RemoteRobot::synchronizeLocalClone(vsContext->localRobot, vsContext->robotStateComponent);
    //        VirtualRobot::RobotPtr localRobot = vsContext->localRobot;
    //        PosePtr globalRobotPose = PosePtr::dynamicCast(vsContext->simulatorProxy->getRobotPose(localRobot->getName()));
    //        localRobot->setGlobalPose(globalRobotPose->toEigen());
    VirtualRobot::RobotPtr localRobot = RemoteRobot::createLocalClone(context->robotStateComponent);
    //PosePtr globalRobotPose = PosePtr::dynamicCast(context->simulatorProxy->getRobotPose(localRobot->getName()));
    //localRobot->setGlobalPose(globalRobotPose->toEigen());

    // retrieve pose of object (assumed to be global)
    Eigen::Matrix4f objectGlobalPose = Eigen::Matrix4f::Identity();
    FramedPositionPtr objectPosition = objectInstanceChannel->get<FramedPosition>("position");
    std::string objectFrame = objectPosition->frame;
    objectGlobalPose.block(0, 3, 3, 1) = objectPosition->toEigen();
    objectGlobalPose.block(0, 0, 3, 3) = objectInstanceChannel->get<FramedOrientation>("orientation")->toEigen();

    if (!objectFrame.empty())
    {
        ARMARX_WARNING << "object frame is currently assumed to be empty" << flush;
    }

    std::string targetFrame = getInput<std::string>("referenceFrame");
    VirtualRobot::RobotNodePtr targetNode = localRobot->getRobotNode(targetFrame);

    if (!targetNode)
    {
        ARMARX_ERROR << "Target frame not part of robot" << std::endl;
    }

    ARMARX_DEBUG << "robot global pose: " << localRobot->getGlobalPose() << std::endl;
    ARMARX_DEBUG << "target frame global pose: " << targetNode->getGlobalPose() << std::endl;


    Eigen::Matrix4f objectFramedPose = targetNode->getGlobalPose().inverse() * objectGlobalPose;

    ARMARX_DEBUG << "object global pose: " << objectGlobalPose << std::endl;
    ARMARX_DEBUG << "object framed pose: " << objectFramedPose << std::endl;

    Eigen::Matrix4f graspFramedPose = objectFramedPose * context->graspDefinition->getTransformation().inverse();

    ARMARX_DEBUG << "grasp framed pose: " << graspFramedPose << std::endl;

    //construct framed pose; then setLocal()
    FramedPosePtr targetFramedPose = new FramedPose(graspFramedPose, targetFrame);
    //        vsContext->simulatorProxy->visualizePose("myDebugPose_1", new FramedPose(targetNode->getGlobalPose() * graspFramedPose, ""));
    setLocal("targetPose", targetFramedPose);       //TODO: make sure there is a "targetPose" input field in THIS state and in the VisualServo (child) state...

    //RobotStatechartContext* rsContext = getContext<RobotStatechartContext>();
    if (context->headIKUnitPrx)
    {
        Eigen::Vector3f viewTarget;
        viewTarget << targetFramedPose->position->x, targetFramedPose->position->y, targetFramedPose->position->z;
        FramedDirectionPtr viewTargetPositionPtr = new FramedDirection(viewTarget, targetFramedPose->frame);
        ARMARX_IMPORTANT << "Set head target to " << viewTarget.transpose();
        context->headIKUnitPrx->setHeadTarget(context->headIKKinematicChainName, viewTargetPositionPtr);
    }
    else
    {
        ARMARX_IMPORTANT << "No head unit ";
    }

    ARMARX_VERBOSE << "Done VisualServoWrapper::onEnter()" << flush;
}

void VisualServoWrapper::onExit()
{
    //NEW: object pose updates:
    removeCondition(condObjectPoseUpdated);
    removeCondition(condObjectLost);            //TODO (maybe): move this to the VSExample (where the condObjectUpdate also is)
}

// DO NOT EDIT NEXT FUNCTION
std::string VisualServoWrapper::GetName()
{
    return "VisualServoWrapper";
}

// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr VisualServoWrapper::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new VisualServoWrapper(stateData));
}

