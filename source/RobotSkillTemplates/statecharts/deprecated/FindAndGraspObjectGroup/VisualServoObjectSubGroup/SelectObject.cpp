/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2014-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::FindAndGraspObjectGroup
 * @author     Valerij Wittenbeck ( valerij dot wittenbeck at student dot kit dot edu )
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "SelectObject.h"

using namespace armarx;
using namespace FindAndGraspObjectGroup;

// DO NOT EDIT NEXT LINE
SelectObject::SubClassRegistry SelectObject::Registry(SelectObject::GetName(), &SelectObject::CreateInstance);



SelectObject::SelectObject(XMLStateConstructorParams stateData) :
    XMLStateTemplate < SelectObject > (stateData)
{
}

void SelectObject::onEnter()
{
    FindAndGraspObjectContext* context = getContext<FindAndGraspObjectContext>();

    memoryx::ChannelRefBaseSequence objectChannelRefs = context->objectMemoryObserver->getObjectInstances(getInput<ChannelRef>("objectChannel"));
    memoryx::ChannelRefBaseSequence markerChannelRefs = context->objectMemoryObserver->getObjectInstances(getInput<ChannelRef>("markerChannel"));
    ARMARX_DEBUG << "objectChannel available: " << objectChannelRefs.size() << "; " << "markerChannel available: " << markerChannelRefs.size();

    if (objectChannelRefs.size() == 0 || markerChannelRefs.size() == 0)
    {
        ARMARX_IMPORTANT << "object or marker channel ref not available (yet?)" << flush;
        waitForObject = setTimeoutEvent(1000, createEvent<EvObjectInstanceNotAvailableYet>());
        return;
    }

    setOutput("objectInstanceChannel", objectChannelRefs.front());
    setOutput("markerInstanceChannel", markerChannelRefs.front());

    ARMARX_DEBUG << "SelectObjectsState::onEnter(): objectChannelRefs.front(): " << objectChannelRefs.front() << " (to: setOutput())" << flush;
    ARMARX_DEBUG << "SelectObjectsState::onEnter(): markerChannelRefs.front(): " << markerChannelRefs.front() << " (to: setOutput())" << flush;
    sendEvent<EvObjectsSelected>();

    ARMARX_VERBOSE << "Done SelectObject::onEnter()" << flush;
}

void SelectObject::onExit()
{
    removeTimeoutEvent(waitForObject);
}

// DO NOT EDIT NEXT FUNCTION
std::string SelectObject::GetName()
{
    return "SelectObject";
}

// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr SelectObject::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new SelectObject(stateData));
}

