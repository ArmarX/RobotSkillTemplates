/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2014-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::FindAndGraspObjectGroup
 * @author     Valerij Wittenbeck ( valerij dot wittenbeck at student dot kit dot edu )
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "LocalizeObject.h"

using namespace armarx;
using namespace FindAndGraspObjectGroup;

// DO NOT EDIT NEXT LINE
LocalizeObject::SubClassRegistry LocalizeObject::Registry(LocalizeObject::GetName(), &LocalizeObject::CreateInstance);



LocalizeObject::LocalizeObject(XMLStateConstructorParams stateData) :
    XMLStateTemplate < LocalizeObject > (stateData)
{
}

void LocalizeObject::onEnter()
{
    Literal objectLocalizedLiteral(getInput<ChannelRef>("objectChannel")->getDataFieldIdentifier("localized"), "equals", Literal::createParameterList(true));
    Literal markerLocalizedLiteral(getInput<ChannelRef>("markerChannel")->getDataFieldIdentifier("localized"), "equals", Literal::createParameterList(true));
    Term objectAndMarkerLocalized = objectLocalizedLiteral && markerLocalizedLiteral;
    objectsLocalized = installCondition(objectAndMarkerLocalized, createEvent<EvObjectsLocalized>());

    //DEBUG: does the above not work because of the working memory error or because some call/variable is missing?
    sendEvent<EvObjectsLocalized>();

    ARMARX_VERBOSE << "Done LocalizeObject::onEnter()" << flush;
}

void LocalizeObject::onExit()
{
    removeCondition(objectsLocalized);
}

// DO NOT EDIT NEXT FUNCTION
std::string LocalizeObject::GetName()
{
    return "LocalizeObject";
}

// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr LocalizeObject::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new LocalizeObject(stateData));
}

