/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2014-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::FindAndGraspObjectGroup
 * @author     Valerij Wittenbeck ( valerij dot wittenbeck at student dot kit dot edu )
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "MoveJointsVelControl.h"
#include <RobotAPI/libraries/core/remoterobot/RemoteRobot.h>

using namespace armarx;
using namespace FindAndGraspObjectGroup;

// DO NOT EDIT NEXT LINE
MoveJointsVelControl::SubClassRegistry MoveJointsVelControl::Registry(MoveJointsVelControl::GetName(), &MoveJointsVelControl::CreateInstance);



MoveJointsVelControl::MoveJointsVelControl(XMLStateConstructorParams stateData) :
    XMLStateTemplate < MoveJointsVelControl > (stateData)
{
}

void MoveJointsVelControl::onEnter()
{
    RobotStatechartContext* context = getContext<RobotStatechartContext>();
    //NameValueMap targetJointVelocities;
    SingleTypeVariantListPtr jointNames = getInput<SingleTypeVariantList>("jointNames");
    SingleTypeVariantListPtr targetJointValues = getInput<SingleTypeVariantList>("targetJointValues");
    float maxVel = 0.5f;

    if (isInputParameterSet("jointMaxSpeed"))
    {
        maxVel = fabs(getInput<float>("jointMaxSpeed"));
    }

    if (jointNames->getSize() != targetJointValues->getSize())
    {
        throw LocalException("Sizes of joint name list and joint value list do not match!");
    }

    //RobotStatechartContext* context = getContext<RobotStatechartContext>();
    VirtualRobot::RobotPtr robot(new RemoteRobot(context->robotStateComponent->getRobotSnapshot("moveJointsVel")));

    Eigen::VectorXf errorJoint(jointNames->getSize());
    Eigen::VectorXf currentJoint(jointNames->getSize());
    ARMARX_VERBOSE << "number of joints that will be set: " << jointNames->getSize() << ", maxVel:" << maxVel << flush;

    for (int i = 0; i < jointNames->getSize(); i++)
    {
        currentJoint(i) = robot->getRobotNode(jointNames->getVariant(i)->getString())->getJointValue();
        errorJoint(i) = targetJointValues->getVariant(i)->getFloat() - currentJoint(i);
    }

    /*
        for (int i=0; i<jointNames->getSize(); i++)
        {
            float newVel = targetJointValues->getVariant(i)->getFloat() - robot->getRobotNode(jointNames->getVariant(i)->getString())->getJointValue();
            if (newVel>maxVel)
                newVel = maxVel;
            if (newVel<-maxVel)
                newVel = -maxVel;
            targetJointVelocities[jointNames->getVariant(i)->getString()] = newVel;
            ARMARX_VERBOSE << "setting joint angle for joint '" << jointNames->getVariant(i)->getString() << "' to " << newVel << std::endl;
        }*/
    /*
        // now install the condition
        Term cond;
        float tolerance = getInput<float>("jointTargetTolerance");
        for (int i=0; i<jointNames->getSize(); i++)
        {
            ARMARX_DEBUG << "creating condition (" << i << " of " << jointNames->getSize() << ")" << flush;
            ParameterList inrangeCheck;
            inrangeCheck.push_back(new Variant(targetJointValues->getVariant(i)->getFloat()-tolerance));
            inrangeCheck.push_back(new Variant(targetJointValues->getVariant(i)->getFloat()+tolerance));

            Literal inrangeLiteral(DataFieldIdentifier(context->getKinematicUnitObserverName(), "jointangles", jointNames->getVariant(i)->getString()), "inrange", inrangeCheck);
            cond = cond && inrangeLiteral;
        }

        ARMARX_VERBOSE << "installing condition: EvJointTargetReached" << std::endl;
        targetReachedCondition = installCondition<MotionControl::EvJointTargetReached>(cond);
        ARMARX_VERBOSE << "condition installed: EvJointTargetReached" << std::endl;
        timeoutEvent = setTimeoutEvent(getInput<int>("timeoutInMs"), createEvent<MotionControl::EvTimeout>());
        ARMARX_VERBOSE << "timeout installed" << std::endl;*/

    float VEL_GAIN = 1.0f;
    float VEL_MAX_COEFF = 0.02f;
    ARMARX_VERBOSE << "JointVelocities " << errorJoint;
    //ARMARX_VERBOSE << "JointVelocities GAIN" << errorJoint * VEL_GAIN;
    float maxV = fabs(errorJoint.maxCoeff());

    if (maxV > VEL_MAX_COEFF)
    {
        errorJoint = errorJoint / maxV * VEL_MAX_COEFF * VEL_GAIN;
        ARMARX_VERBOSE << "JointVelocities VEL_MAX_COEFF" << errorJoint;
    }


    // build name value map
    NameValueMap targetVelocities;
    NameValueMap targetAngles;
    NameControlModeMap controlModes;
    //const std::vector< VirtualRobot::RobotNodePtr > nodes =  robotNodeSet->getAllRobotNodes();

    for (int i = 0; i < jointNames->getSize(); i++)
    {
        std::string jName = jointNames->getVariant(i)->getString();
        targetVelocities[jName] = errorJoint(i) * VEL_GAIN;
        targetAngles[jName] = currentJoint(i) + errorJoint(i);
        controlModes[jName] = ePositionVelocityControl;
        //controlModes[n->getName()] = eVelocityControl;
    }

    // execute velocities
    context->kinematicUnitPrx->switchControlMode(controlModes);
    context->kinematicUnitPrx->setJointAngles(targetAngles);
    context->kinematicUnitPrx->setJointVelocities(targetVelocities);

    //context->kinematicUnitPrx->setJointVelocities(targetJointVelocities);
    sendEvent<EvJointsActuated>();
}



void MoveJointsVelControl::onBreak()
{
    //setJointVelToZero();
    //removeCondition(targetReachedCondition);
    //removeTimeoutEvent(timeoutEvent);
}

void MoveJointsVelControl::onExit()
{
    /*
    SingleTypeVariantListPtr jointNames = getInput<SingleTypeVariantList>("jointNames");
    SingleTypeVariantListPtr targetJointValues = getInput<SingleTypeVariantList>("targetJointValues");
    SingleTypeVariantList resultList(VariantType::Float);

    for(int i=0; i<jointNames->getSize(); i++ )
    {
        DataFieldIdentifierPtr datafield = new DataFieldIdentifier(context->getKinematicUnitObserverName(),"jointangles", jointNames->getVariant(i)->getString());
        float jointAngleActual = context->kinematicUnitObserverPrx->getDataField(datafield)->getFloat();
        float jointTargetValue = targetJointValues->getVariant(i)->getFloat();
        resultList.addVariant(Variant(jointAngleActual - jointTargetValue));
    }
    setOutput("jointDistancesToTargets", resultList);
    */
    //ARMARX_INFO << "output: " << StateUtilFunctions::getDictionaryString(getOutputParameters()) << std::endl;

    //setJointVelToZero();

    //removeCondition(targetReachedCondition);
    //removeTimeoutEvent(timeoutEvent);
}

//TODO: this is a straight copy from LiftObject, find a better way
void MoveJointsVelControl::setJointVelToZero()
{
    // set vel to zero
    ARMARX_DEBUG << "setting joint velocity to zero " << flush;

    std::string kinChainName = getInput<std::string>("kinematicChain");
    RobotStatechartContext* context = getContext<RobotStatechartContext>();

    if (!context)
    {
        ARMARX_WARNING << "Need a RobotStatechartContext" << flush;
        sendEvent<Failure>();
    }

    if (!context->robotStateComponent)
    {
        ARMARX_WARNING << "No RobotStatechartContext->robotStateComponent" << flush;
        sendEvent<Failure>();
    }

    VirtualRobot::RobotPtr robot = RemoteRobot::createLocalClone(context->robotStateComponent);

    if (!robot->hasRobotNodeSet(kinChainName))
    {
        ARMARX_WARNING << "Robot does not know kinematic chain with name " << kinChainName << flush;
        sendEvent<Failure>();
    }

    VirtualRobot::RobotNodeSetPtr rns = robot->getRobotNodeSet(kinChainName);


    NameValueMap targetJointVelocities;

    for (unsigned int i = 0; i < rns->getSize(); i++)
    {
        targetJointVelocities[rns->getNode(i)->getName()] = 0;
    }

    context->kinematicUnitPrx->setJointVelocities(targetJointVelocities);
}

// DO NOT EDIT NEXT FUNCTION
std::string MoveJointsVelControl::GetName()
{
    return "MoveJointsVelControl";
}

// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr MoveJointsVelControl::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new MoveJointsVelControl(stateData));
}

