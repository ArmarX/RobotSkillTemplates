/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2014-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::FindAndGraspObjectGroup
 * @author     Valerij Wittenbeck ( valerij dot wittenbeck at student dot kit dot edu )
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "ValidateTcpPose.h"
#include <RobotAPI/libraries/core/remoterobot/RemoteRobot.h>
#include <VirtualRobot/LinkedCoordinate.h>

using namespace armarx;
using namespace FindAndGraspObjectGroup;

// DO NOT EDIT NEXT LINE
ValidateTcpPose::SubClassRegistry ValidateTcpPose::Registry(ValidateTcpPose::GetName(), &ValidateTcpPose::CreateInstance);



ValidateTcpPose::ValidateTcpPose(XMLStateConstructorParams stateData) :
    XMLStateTemplate < ValidateTcpPose > (stateData)
{
}

void ValidateTcpPose::onEnter()
{
    using namespace Eigen;

    RobotStatechartContext* context = getContext<RobotStatechartContext>();
    VirtualRobot::RobotPtr robot(new RemoteRobot(context->robotStateComponent->getRobotSnapshot("ValidateTCPPoseTime")));
    VirtualRobot::LinkedCoordinate actualPose(robot);
    actualPose.set(robot->getRobotNodeSet(getInput<std::string>("kinematicChainName"))->getTCP()->getName(), Vector3f(0, 0, 0));
    actualPose.changeFrame(robot->getRootNode());
    Vector3f actualPosition = actualPose.getPosition();
    Quaternionf actualOrientation(actualPose.getPose().block<3, 3>(0, 0));


    VirtualRobot::LinkedCoordinate targetPose = FramedPose::createLinkedCoordinate(robot, getInput<FramedPosition>("targetTCPPosition"), getInput<FramedOrientation>("targetTCPOrientation"));
    targetPose.changeFrame(robot->getRootNode());
    Vector3f targetPosition = targetPose.getPosition();
    Quaternionf targetOrientation(targetPose.getPose().block<3, 3>(0, 0));

    float cartesianDistance = sqrtf(((targetPosition - actualPosition).dot(targetPosition - actualPosition)));
    float orientationDistance = actualOrientation.angularDistance(targetOrientation);

    setOutput("TCPDistanceToTarget", cartesianDistance);
    setOutput("TCPOrientationDistanceToTarget", orientationDistance);
    bool withOri = getInput<bool>("ikWithOrientation");

    if (cartesianDistance <= getInput<float>("targetPositionDistanceTolerance") && (!withOri || orientationDistance <= getInput<float>("targetOrientationDistanceTolerance")))
    {
        sendEvent<Success>();
    }
    else
    {
        ARMARX_WARNING << "Target pose has not been reached with the desired accuracy. Cartesian distance: " << cartesianDistance << ", orientation distance: " << orientationDistance << " ";
        ARMARX_INFO << "Actual pose: " << actualPose.getPose();
        ARMARX_INFO << "Target pose: " << targetPose.getPose();
        sendEvent<Failure>();
    }

    ARMARX_VERBOSE << "Done ValidateTcpPose::onEnter()";
}

// DO NOT EDIT NEXT FUNCTION
std::string ValidateTcpPose::GetName()
{
    return "ValidateTcpPose";
}

// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr ValidateTcpPose::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new ValidateTcpPose(stateData));
}

