/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2014-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::FindAndGraspObjectGroup
 * @author     Valerij Wittenbeck ( valerij dot wittenbeck at student dot kit dot edu )
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "LiftObject.h"

using namespace armarx;
using namespace FindAndGraspObjectGroup;

// DO NOT EDIT NEXT LINE
LiftObject::SubClassRegistry LiftObject::Registry(LiftObject::GetName(), &LiftObject::CreateInstance);



LiftObject::LiftObject(XMLStateConstructorParams stateData) :
    XMLStateTemplate < LiftObject > (stateData)
{
}

void LiftObject::onEnter()
{
    ARMARX_VERBOSE << "Entering LiftObject::onEnter()";
    float timeoutLiftObject = getInput<float>("timeoutLiftObject");
    condLiftObjectTimeout = setTimeoutEvent(timeoutLiftObject, createEvent<EvLiftObjectTimeout>());

    //GraspingWithVisualFeedbackContext* context = getContext<GraspingWithVisualFeedbackContext>();
    FindAndGraspObjectContext* context = getContext<FindAndGraspObjectContext>();
    FramedPose currentHandPose(context->remoteRobot->getRobotNode(getInput<std::string>("tcpName"))->getGlobalPose(), context->remoteRobot->getRootNode()->getName());
    Eigen::Matrix4f liftedHandPose = currentHandPose.toEigen();
    liftedHandPose(2, 3) += 150;
    FramedPosition targetPosition(liftedHandPose, currentHandPose.frame);
    FramedOrientation targetOrientation(liftedHandPose, currentHandPose.frame);

    setLocal("targetTCPPosition", targetPosition);
    setLocal("targetTCPOrientation", targetOrientation);
    setLocal("targetPositionDistanceTolerance", 40.0f);
    setLocal("targetOrientationDistanceTolerance", 3.15f);
    setLocal("ikWithOrientation", false);

    setLocal("timeoutInMs", int(timeoutLiftObject));
    setLocal("jointTargetTolerance", 0.01f);
    setLocal("kinematicChainName", getInput<std::string>("kinematicChain"));
    ARMARX_VERBOSE << "Done LiftObject::onEnter()" << flush;
}

void LiftObject::setJointVelToZero()
{
    // set vel to zero
    ARMARX_DEBUG << "setting joint velocity to zero " << flush;

    std::string kinChainName = getInput<std::string>("kinematicChain");
    RobotStatechartContext* context = getContext<RobotStatechartContext>();

    if (!context)
    {
        ARMARX_WARNING << "Need a RobotStatechartContext" << flush;
        sendEvent<Failure>();
    }

    if (!context->robotStateComponent)
    {
        ARMARX_WARNING << "No RobotStatechartContext->robotStateComponent" << flush;
        sendEvent<Failure>();
    }

    VirtualRobot::RobotPtr robot = RemoteRobot::createLocalClone(context->robotStateComponent);

    if (!robot->hasRobotNodeSet(kinChainName))
    {
        ARMARX_WARNING << "Robot does not know kinematic chain with name " << kinChainName << flush;
        sendEvent<Failure>();
    }

    VirtualRobot::RobotNodeSetPtr rns = robot->getRobotNodeSet(kinChainName);


    NameValueMap targetJointVelocities;

    for (unsigned int i = 0; i < rns->getSize(); i++)
    {
        targetJointVelocities[rns->getNode(i)->getName()] = 0;
    }

    context->kinematicUnitPrx->setJointVelocities(targetJointVelocities);
}


void LiftObject::onExit()
{
    setJointVelToZero();
    removeTimeoutEvent(condLiftObjectTimeout);
    ARMARX_VERBOSE << "Done LiftObject::onExit()";

    // test: open hand
    /*
    RobotStatechartContext *context = getContext<RobotStatechartContext>();
    HandUnitInterfacePrx handUnitPrx = context->getHandUnit("RightHandUnit");
    if (handUnitPrx)
    {
        ARMARX_INFO << "TEST: open hand" << flush;
        handUnitPrx->open();
        ARMARX_INFO << "TEST: set object released: Vitalis" << flush;
        handUnitPrx->setObjectReleased("Vitalis");
    }*/

}

// DO NOT EDIT NEXT FUNCTION
std::string LiftObject::GetName()
{
    return "LiftObject";
}

// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr LiftObject::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new LiftObject(stateData));
}

