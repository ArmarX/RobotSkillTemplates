/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2014-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::FindAndGraspObjectGroup
 * @author     Valerij Wittenbeck ( valerij dot wittenbeck at student dot kit dot edu )
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "ControlModeChooser.h"

using namespace armarx;
using namespace FindAndGraspObjectGroup;

// DO NOT EDIT NEXT LINE
ControlModeChooser::SubClassRegistry ControlModeChooser::Registry(ControlModeChooser::GetName(), &ControlModeChooser::CreateInstance);



ControlModeChooser::ControlModeChooser(XMLStateConstructorParams stateData) :
    XMLStateTemplate < ControlModeChooser > (stateData)
{
}

void ControlModeChooser::onEnter()
{
    ARMARX_VERBOSE << "Entering ControlModeChooser::onEnter()";

    const std::string controlMode = getInput<std::string>("jointControlMode");

    if (controlMode == "vel")
    {
        sendEvent<EvVelControl>();
    }
    else
    {
        ARMARX_ERROR << ARMARX_FUNCTION << ": controlMode " << controlMode << "nyi";
    }
}

// DO NOT EDIT NEXT FUNCTION
std::string ControlModeChooser::GetName()
{
    return "ControlModeChooser";
}

// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr ControlModeChooser::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new ControlModeChooser(stateData));
}

