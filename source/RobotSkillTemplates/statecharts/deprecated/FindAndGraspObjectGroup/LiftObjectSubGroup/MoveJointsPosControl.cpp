/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2014-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::FindAndGraspObjectGroup
 * @author     Valerij Wittenbeck ( valerij dot wittenbeck at student dot kit dot edu )
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "MoveJointsPosControl.h"

using namespace armarx;
using namespace FindAndGraspObjectGroup;

// DO NOT EDIT NEXT LINE
MoveJointsPosControl::SubClassRegistry MoveJointsPosControl::Registry(MoveJointsPosControl::GetName(), &MoveJointsPosControl::CreateInstance);



MoveJointsPosControl::MoveJointsPosControl(XMLStateConstructorParams stateData) :
    XMLStateTemplate < MoveJointsPosControl > (stateData)
{
}

void MoveJointsPosControl::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
}

void MoveJointsPosControl::run()
{
    // put your user code for the execution-phase here
    // runs in seperate thread, thus can do complex operations
    // should check constantly whether isRunningTaskStopped() returns true


    while (!isRunningTaskStopped()) // stop run function if returning true
    {
        // do your calculations
    }

}

void MoveJointsPosControl::onBreak()
{
    // put your user code for the breaking point here
    // execution time should be short (<100ms)
}

void MoveJointsPosControl::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)

}

// DO NOT EDIT NEXT FUNCTION
std::string MoveJointsPosControl::GetName()
{
    return "MoveJointsPosControl";
}

// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr MoveJointsPosControl::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new MoveJointsPosControl(stateData));
}

