/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::GazeControlGroup
 * @author     Markus Grotz ( markus dot grotz at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <ArmarXCore/statechart/xmlstates/XMLRemoteStateOfferer.h>
#include "GazeControlGroupStatechartContext.generated.h"

namespace armarx::GazeControlGroup
{
    class GazeControlGroupRemoteStateOfferer :
        virtual public XMLRemoteStateOfferer < GazeControlGroupStatechartContext > // Change this statechart context if you need another context (dont forget to change in the constructor as well)
    {
    public:
        GazeControlGroupRemoteStateOfferer(StatechartGroupXmlReaderPtr reader);

        // inherited from RemoteStateOfferer
        void onInitXMLRemoteStateOfferer() override;
        void onConnectXMLRemoteStateOfferer() override;
        void onExitXMLRemoteStateOfferer() override;

        // static functions for AbstractFactory Method
        static std::string GetName();
        static XMLStateOffererFactoryBasePtr CreateInstance(StatechartGroupXmlReaderPtr reader);
        static SubClassRegistry Registry;


    };
}


