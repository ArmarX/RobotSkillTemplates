/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::GazeControlGroup::GazeControlGroupRemoteStateOfferer
 * @author     Markus Grotz ( markus dot grotz at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "GazeControlGroupRemoteStateOfferer.h"

using namespace armarx;
using namespace GazeControlGroup;

// DO NOT EDIT NEXT LINE
GazeControlGroupRemoteStateOfferer::SubClassRegistry GazeControlGroupRemoteStateOfferer::Registry(GazeControlGroupRemoteStateOfferer::GetName(), &GazeControlGroupRemoteStateOfferer::CreateInstance);



GazeControlGroupRemoteStateOfferer::GazeControlGroupRemoteStateOfferer(StatechartGroupXmlReaderPtr reader) :
    XMLRemoteStateOfferer < GazeControlGroupStatechartContext > (reader)
{
}

void GazeControlGroupRemoteStateOfferer::onInitXMLRemoteStateOfferer()
{

}

void GazeControlGroupRemoteStateOfferer::onConnectXMLRemoteStateOfferer()
{

}

void GazeControlGroupRemoteStateOfferer::onExitXMLRemoteStateOfferer()
{

}

// DO NOT EDIT NEXT FUNCTION
std::string GazeControlGroupRemoteStateOfferer::GetName()
{
    return "GazeControlGroupRemoteStateOfferer";
}

// DO NOT EDIT NEXT FUNCTION
XMLStateOffererFactoryBasePtr GazeControlGroupRemoteStateOfferer::CreateInstance(StatechartGroupXmlReaderPtr reader)
{
    return XMLStateOffererFactoryBasePtr(new GazeControlGroupRemoteStateOfferer(reader));
}



