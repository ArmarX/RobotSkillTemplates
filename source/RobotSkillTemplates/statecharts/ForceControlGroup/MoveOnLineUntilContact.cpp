/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2014-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::ForceControlGroup
 * @author     Mirko Waechter ( mirko dot waechter at kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "MoveOnLineUntilContact.h"

using namespace armarx;
using namespace ForceControlGroup;

#include <RobotAPI/interface/observers/ObserverFilters.h>

// DO NOT EDIT NEXT LINE
MoveOnLineUntilContact::SubClassRegistry MoveOnLineUntilContact::Registry(MoveOnLineUntilContact::GetName(), &MoveOnLineUntilContact::CreateInstance);



MoveOnLineUntilContact::MoveOnLineUntilContact(const XMLStateConstructorParams& stateData) :
    XMLStateTemplate < MoveOnLineUntilContact > (stateData),  MoveOnLineUntilContactGeneratedBase < MoveOnLineUntilContact > (stateData)
{
}

void MoveOnLineUntilContact::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
    DatafieldRefPtr filteredForceDF;
    std::string ftSensorName;
    if (in.isForceSensorDatafieldSet())
    {
        filteredForceDF = in.getForceSensorDatafield();
    }
    else
    {
        if (in.isFTSensorNameSet())
        {
            ftSensorName = in.getFTSensorName();
        }
        else
        {
            ftSensorName = getRobot()->getRobotNodeSet(in.getKinematicChainName())->getTCP()->getName();
        }

        DatafieldRefBasePtr forceDF = getForceTorqueObserver()->getForceDatafield(ftSensorName);
        filteredForceDF = DatafieldRefPtr::dynamicCast(getForceTorqueObserver()->createNulledDatafield(forceDF));
    }
    local.setForceRef(filteredForceDF);
    Literal forceCheck(filteredForceDF->getDataFieldIdentifier(), checks::magnitudelarger, {in.getForceThreshold()});
    installConditionForHighForceDetected(Term(forceCheck).getImpl(), "Condition for reaching a high force");
}

void MoveOnLineUntilContact::run()
{
    // put your user code for the execution-phase here
    // runs in seperate thread, thus can do complex operations
    // should check constantly whether isRunningTaskStopped() returns true

    // uncomment this if you need a continous run function. Make sure to use sleep or use blocking wait to reduce cpu load.
    while (!isRunningTaskStopped()) // stop run function if returning true
    {
        ARMARX_INFO << deactivateSpam(0.2) << "measured force: " << local.getForceRef()->get<FramedDirection>()->toEigen().norm() << " " << in.getForceThreshold();
    }

}

//void MoveOnLineUntilContact::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void MoveOnLineUntilContact::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
    ARMARX_INFO << "Final measure force: " << local.getForceRef()->get<FramedDirection>()->toEigen().norm();
    if (!in.isForceSensorDatafieldSet())
    {
        getForceTorqueObserver()->removeFilteredDatafield(local.getForceRef());
    }
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr MoveOnLineUntilContact::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new MoveOnLineUntilContact(stateData));
}

