/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2014-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::ForceControlGroup
 * @author     Mirko Waechter ( mirko dot waechter at kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "WaitForForce.h"

#include <ArmarXCore/core/time/TimeUtil.h>
#include <RobotSkillTemplates/statecharts/ForceControlGroup/ForceControlGroupStatechartContext.generated.h>

using namespace armarx;
using namespace ForceControlGroup;

// DO NOT EDIT NEXT LINE
WaitForForce::SubClassRegistry WaitForForce::Registry(WaitForForce::GetName(), &WaitForForce::CreateInstance);



WaitForForce::WaitForForce(XMLStateConstructorParams stateData) :
    XMLStateTemplate<WaitForForce>(stateData),  WaitForForceGeneratedBase<WaitForForce>(stateData)
{
}

void WaitForForce::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)

    TimeUtil::MSSleep(in.getDelay());

    ForceControlGroupStatechartContext* c = getContext<ForceControlGroupStatechartContext>();
    DatafieldRefBasePtr rawforce;
    DatafieldRefBasePtr rawtorque;
    DatafieldRefBasePtr force;
    DatafieldRefBasePtr torque;
    if (in.isObserverSensorChannelNameSet())
    {
        rawforce = c->getForceTorqueObserver()->getDataFieldRef(new DataFieldIdentifier(c->getForceTorqueObserver()->getObserverName(),
                   in.getObserverSensorChannelName(),
                   "forces"));
        rawtorque = c->getForceTorqueObserver()->getDataFieldRef(new DataFieldIdentifier(c->getForceTorqueObserver()->getObserverName(),
                    in.getObserverSensorChannelName(),
                    "torques"));
    }
    else if (in.isSensorNameSet())
    {
        std::string sensor = in.getSensorName();

        rawforce = c->getForceTorqueObserver()->getForceDatafield(sensor);
        rawtorque = c->getForceTorqueObserver()->getTorqueDatafield(sensor);
    }

    else
    {
        throw LocalException() << "Either SensorName or ObserverSensorChannelName must be set!";
    }
    force = c->getForceTorqueObserver()->createNulledDatafield(rawforce);
    torque = c->getForceTorqueObserver()->createNulledDatafield(rawtorque);

    local.setforceRef(DatafieldRefPtr::dynamicCast(force));
    local.settorqueRef(DatafieldRefPtr::dynamicCast(torque));
    ARMARX_IMPORTANT << "Installing force torque condition: force threshold: " <<  in.getForceMagnitudeThreshold()
                     << " torque threshold: " << in.getTorqueMagnitudeThreshold();
    Literal literalForce(force, "magnitudelarger", Literal::createParameterList(in.getForceMagnitudeThreshold()));
    Literal literalTorque(torque, "magnitudelarger", Literal::createParameterList(in.getTorqueMagnitudeThreshold()));
    installConditionForForceTorqueThresholdReached(literalForce || literalTorque);

}

void WaitForForce::run()
{
    while (!isRunningTaskStopped())
    {
        ARMARX_INFO << deactivateSpam(0.5) << "Current force value: " << local.getforceRef()->getDataField()->get<FramedDirection>()->toEigen().norm() << " threshold: " << in.getForceMagnitudeThreshold();
        TimeUtil::MSSleep(100);
    }

}



void WaitForForce::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
    ForceControlGroupStatechartContext* c = getContext<ForceControlGroupStatechartContext>();
    ARMARX_INFO << deactivateSpam(0.5) << "Final force value: " << local.getforceRef()->getDataField()->get<FramedDirection>()->toEigen().norm() << " threshold: " << in.getForceMagnitudeThreshold();

    c->getForceTorqueObserver()->removeFilteredDatafield(local.getforceRef());
    c->getForceTorqueObserver()->removeFilteredDatafield(local.gettorqueRef());
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr WaitForForce::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new WaitForForce(stateData));
}

