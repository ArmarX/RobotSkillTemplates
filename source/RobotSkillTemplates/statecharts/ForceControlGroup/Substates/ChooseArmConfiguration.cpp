/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2014-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::ForceControlGroup
 * @author     Peter Kaiser ( peter dot kaiser at kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "ChooseArmConfiguration.h"

using namespace armarx;
using namespace ForceControlGroup;

// DO NOT EDIT NEXT LINE
ChooseArmConfiguration::SubClassRegistry ChooseArmConfiguration::Registry(ChooseArmConfiguration::GetName(), &ChooseArmConfiguration::CreateInstance);



ChooseArmConfiguration::ChooseArmConfiguration(const XMLStateConstructorParams& stateData) :
    XMLStateTemplate<ChooseArmConfiguration>(stateData),  ChooseArmConfigurationGeneratedBase<ChooseArmConfiguration>(stateData)
{
}

void ChooseArmConfiguration::onEnter()
{
    if (in.getHandUpright())
    {
        out.setArmConfigurationValues(in.getUprightArmConfigurationValues());
    }
    else
    {
        out.setArmConfigurationValues(in.getRotatedArmConfigurationValues());
    }

    if (in.getUseRightArm())
    {
        ARMARX_INFO << "Using right arm";
        out.setArmConfigurationJointNames(in.getArmConfigurationJointNamesRight());
        out.setHand(in.getRightHand());
        if (in.isRightObserverChannelNameSet())
        {
            out.setForceTorqueObserverChannelName(in.getRightObserverChannelName());
        }
    }
    else
    {
        ARMARX_INFO << "Using left arm";
        out.setArmConfigurationJointNames(in.getArmConfigurationJointNamesLeft());
        out.setHand(in.getLeftHand());
        if (in.isLeftObserverChannelNameSet())
        {
            out.setForceTorqueObserverChannelName(in.getLeftObserverChannelName());
        }
    }

    if (in.getUseIK())
    {

        if (in.getUseRightArm())
        {
            out.setSelectedTCPPose(in.getRightArmTCPPose());
            out.setSelectedKinematicChain(in.getRightArmKinematicChain());
        }
        else
        {
            Eigen::Matrix4f pose = in.getRightArmTCPPose()->toEigen();
            pose(0, 3) *= -1;

            QuaternionPtr approachOriOld = new armarx::Quaternion(pose);
            QuaternionPtr approachOri = new armarx::Quaternion();
            approachOri->qw = approachOriOld->qz;
            approachOri->qx = approachOriOld->qy * -1;
            approachOri->qy = approachOriOld->qx * -1;
            approachOri->qz = approachOriOld->qw;

            //            Eigen::AngleAxisf aa(pose.block<3, 3>(0, 0));
            //            aa.axis().x() *= -1;
            //            aa.axis().y() *= -1;
            //            pose.block<3, 3>(0, 0) = aa.toRotationMatrix();
            pose.block<3, 3>(0, 0) = approachOri->toEigen();

            out.setSelectedTCPPose(new FramedPose(pose, getLocalRobot()->getRootNode()->getName(), getLocalRobot()->getName()));
            out.setSelectedKinematicChain(in.getLeftArmKinematicChain());
        }
        emitUseIK();
    }
    else
    {

        emitUseJointControl();
    }
}

void ChooseArmConfiguration::onBreak()
{
}

void ChooseArmConfiguration::onExit()
{
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr ChooseArmConfiguration::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new ChooseArmConfiguration(stateData));
}

