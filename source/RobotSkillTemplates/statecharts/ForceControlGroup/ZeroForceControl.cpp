/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2014-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::ForceControlGroup
 * @author     Mirko Waechter ( mirko dot waechter at kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include <RobotSkillTemplates/statecharts/ForceControlGroup/ForceControlGroupStatechartContext.generated.h>

#include "ZeroForceControl.h"

#include <VirtualRobot/Robot.h>
#include <VirtualRobot/RobotNodeSet.h>

using namespace armarx;
using namespace ForceControlGroup;

// DO NOT EDIT NEXT LINE
ZeroForceControl::SubClassRegistry ZeroForceControl::Registry(ZeroForceControl::GetName(), &ZeroForceControl::CreateInstance);



ZeroForceControl::ZeroForceControl(XMLStateConstructorParams stateData) :
    XMLStateTemplate<ZeroForceControl>(stateData),  ZeroForceControlGeneratedBase<ZeroForceControl>(stateData)
{
}

void ZeroForceControl::onEnter()
{
    ForceControlGroupStatechartContext* c = getContext<ForceControlGroupStatechartContext>();
    c->getTCPControlUnit()->request();
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
    std::string tcpName;

    if (in.istcpNameSet())
    {
        tcpName = in.gettcpName();
    }
    else
    {
        tcpName = c->getRobot()->getRobotNodeSet(in.getrobotNodeSetName())->getTCP()->getName();
    }

    DatafieldRefPtr forceRef = DatafieldRefPtr::dynamicCast(c->getForceTorqueObserver()->getForceDatafield(tcpName));
    DatafieldRefPtr torqueRef = DatafieldRefPtr::dynamicCast(c->getForceTorqueObserver()->getTorqueDatafield(tcpName));
    local.setforceRef(DatafieldRefPtr::dynamicCast(c->getForceTorqueObserver()->createNulledDatafield(forceRef)));
    local.settorqueRef(DatafieldRefPtr::dynamicCast(c->getForceTorqueObserver()->createNulledDatafield(torqueRef)));
    local.setstartVelocity(new FramedDirection(Eigen::Vector3f::Zero(), tcpName, c->getRobot()->getName()));

}

void ZeroForceControl::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
    ForceControlGroupStatechartContext* c = getContext<ForceControlGroupStatechartContext>();
    c->getTCPControlUnit()->release();
    c->getForceTorqueObserver()->removeFilteredDatafield(local.getforceRef());

}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr ZeroForceControl::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new ZeroForceControl(stateData));
}

