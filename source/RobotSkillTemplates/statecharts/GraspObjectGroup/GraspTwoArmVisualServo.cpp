/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2014-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    GraspObject::GraspObjectGroup
 * @author     [Author Name] ( [Author Email] )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "GraspTwoArmVisualServo.h"
#include "GraspObjectGroupStatechartContext.generated.h"
using namespace armarx;
using namespace GraspObjectGroup;

// DO NOT EDIT NEXT LINE
GraspTwoArmVisualServo::SubClassRegistry GraspTwoArmVisualServo::Registry(GraspTwoArmVisualServo::GetName(), &GraspTwoArmVisualServo::CreateInstance);



GraspTwoArmVisualServo::GraspTwoArmVisualServo(const XMLStateConstructorParams& stateData) :
    XMLStateTemplate<GraspTwoArmVisualServo>(stateData),  GraspTwoArmVisualServoGeneratedBase<GraspTwoArmVisualServo>(stateData)
{
}

void GraspTwoArmVisualServo::onEnter()
{
    GraspObjectGroupStatechartContext* context = getContext<GraspObjectGroupStatechartContext>();
    local.setStartTimeRef(ChannelRefPtr::dynamicCast(context->systemObserverPrx->startTimer("GraspTwoArmVisualServoStartTime")));



    // put your user code for the enter-point here
    // execution time should be short (<100ms)
}

void GraspTwoArmVisualServo::run()
{
    // put your user code for the execution-phase here
    // runs in seperate thread, thus can do complex operations
    // should check constantly whether isRunningTaskStopped() returns true

    // uncomment this if you need a continous run function. Make sure to use sleep or use blocking wait to reduce cpu load.
    //    while (!isRunningTaskStopped()) // stop run function if returning true
    //    {
    //        // do your calculations
    //    }

}

void GraspTwoArmVisualServo::onBreak()
{
    // put your user code for the breaking point here
    // execution time should be short (<100ms)
}

void GraspTwoArmVisualServo::onExit()
{
    GraspObjectGroupStatechartContext* context = getContext<GraspObjectGroupStatechartContext>();
    context->systemObserverPrx->removeTimer(local.getStartTimeRef());

    // put your user code for the exit point here
    // execution time should be short (<100ms)

}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr GraspTwoArmVisualServo::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new GraspTwoArmVisualServo(stateData));
}

