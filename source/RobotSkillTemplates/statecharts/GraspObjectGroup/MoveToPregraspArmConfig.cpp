/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2014-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::GraspObjectGroup
 * @author     David ( david dot schiebener at kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "MoveToPregraspArmConfig.h"

using namespace armarx;
using namespace GraspObjectGroup;

// DO NOT EDIT NEXT LINE
MoveToPregraspArmConfig::SubClassRegistry MoveToPregraspArmConfig::Registry(MoveToPregraspArmConfig::GetName(), &MoveToPregraspArmConfig::CreateInstance);



MoveToPregraspArmConfig::MoveToPregraspArmConfig(const XMLStateConstructorParams& stateData) :
    XMLStateTemplate<MoveToPregraspArmConfig>(stateData),  MoveToPregraspArmConfigGeneratedBase<MoveToPregraspArmConfig>(stateData)
{
}


void MoveToPregraspArmConfig::onEnter()
{
    std::string handName = in.getHandNameInMemory();
    std::string locationName = in.getLocationName();

    // decide for left or right hand
    bool useRightHand;
    if (handName.find("left") != std::string::npos || handName.find("Left") != std::string::npos)
    {
        useRightHand = false;
    }
    else if (handName.find("right") != std::string::npos || handName.find("Right") != std::string::npos)
    {
        useRightHand = true;
    }
    else if (handName.find("l") != std::string::npos || handName.find("L") != std::string::npos)
    {
        useRightHand = false;
    }
    else
    {
        useRightHand = true;
    }

    // check if it is a special location
    if (locationName.find("fridge") != std::string::npos || locationName.find("Fridge") != std::string::npos)
    {
        if (useRightHand)
        {
            local.setPregraspConfig(in.getPregraspConfigFridgeRight());
        }
        else
        {
            ARMARX_ERROR << "cannot grasp from fridge with left hand";
        }
    }
    else
    {
        if (useRightHand)
        {
            local.setPregraspConfig(in.getPregraspConfigDefaultRight());
        }
        else
        {
            local.setPregraspConfig(in.getPregraspConfigDefaultLeft());
        }
    }
}


void MoveToPregraspArmConfig::run()
{
    // put your user code for the execution-phase here
    // runs in seperate thread, thus can do complex operations
    // should check constantly whether isRunningTaskStopped() returns true

    // uncomment this if you need a continous run function. Make sure to use sleep or use blocking wait to reduce cpu load.
    //    while (!isRunningTaskStopped()) // stop run function if returning true
    //    {
    //        // do your calculations
    //    }

}


void MoveToPregraspArmConfig::onBreak()
{
    // put your user code for the breaking point here
    // execution time should be short (<100ms)
}


void MoveToPregraspArmConfig::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)

}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr MoveToPregraspArmConfig::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new MoveToPregraspArmConfig(stateData));
}

