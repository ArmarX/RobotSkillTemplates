/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2014-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::GraspObjectGroup::GraspObjectGroupRemoteStateOfferer
 * @author     Mirko Waechter ( waechter at kit dot edu )
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "GraspObjectGroupRemoteStateOfferer.h"

using namespace armarx;
using namespace GraspObjectGroup;

// DO NOT EDIT NEXT LINE
GraspObjectGroupRemoteStateOfferer::SubClassRegistry GraspObjectGroupRemoteStateOfferer::Registry(GraspObjectGroupRemoteStateOfferer::GetName(), &GraspObjectGroupRemoteStateOfferer::CreateInstance);



GraspObjectGroupRemoteStateOfferer::GraspObjectGroupRemoteStateOfferer(StatechartGroupXmlReaderPtr reader) :
    XMLRemoteStateOfferer < GraspObjectGroupStatechartContext > (reader)
{
}

void GraspObjectGroupRemoteStateOfferer::onInitXMLRemoteStateOfferer()
{

}

void GraspObjectGroupRemoteStateOfferer::onConnectXMLRemoteStateOfferer()
{

}

void GraspObjectGroupRemoteStateOfferer::onExitXMLRemoteStateOfferer()
{

}

// DO NOT EDIT NEXT FUNCTION
std::string GraspObjectGroupRemoteStateOfferer::GetName()
{
    return "GraspObjectGroupRemoteStateOfferer";
}

// DO NOT EDIT NEXT FUNCTION
XMLStateOffererFactoryBasePtr GraspObjectGroupRemoteStateOfferer::CreateInstance(StatechartGroupXmlReaderPtr reader)
{
    return XMLStateOffererFactoryBasePtr(new GraspObjectGroupRemoteStateOfferer(reader));
}



