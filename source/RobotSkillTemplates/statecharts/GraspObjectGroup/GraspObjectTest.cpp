/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2014-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::GraspObjectGroup
 * @author     David ( david dot schiebener at kit dot edu )
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "GraspObjectTest.h"
#include "GraspObjectGroupStatechartContext.generated.h"

#include <MemoryX/libraries/memorytypes/entity/ObjectClass.h>
#include <MemoryX/libraries/memorytypes/MemoryXTypesObjectFactories.h>
#include <MemoryX/core/MemoryXCoreObjectFactories.h>


using namespace armarx;
using namespace GraspObjectGroup;

// DO NOT EDIT NEXT LINE
GraspObjectTest::SubClassRegistry GraspObjectTest::Registry(GraspObjectTest::GetName(), &GraspObjectTest::CreateInstance);



GraspObjectTest::GraspObjectTest(XMLStateConstructorParams stateData) :
    XMLStateTemplate<GraspObjectTest>(stateData),
    GraspObjectTestGeneratedBase<GraspObjectTest> (stateData)
{
}

void GraspObjectTest::onEnter()
{
    GraspObjectGroupStatechartContext* context = getContext<GraspObjectGroupStatechartContext>();

    Eigen::Vector3f p = in.getInitialViewTarget()->toEigen();
    FramedPositionPtr targetPos(new FramedPosition(p, context->getRobot()->getRootNode()->getName(), context->getRobot()->getName()));
    context->getViewSelection()->addManualViewTarget(targetPos);
}


void GraspObjectTest::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
    //getObjectMemoryObserver()->releaseObjectClass("MultiVitaminJuice");

}

// DO NOT EDIT NEXT FUNCTION
std::string GraspObjectTest::GetName()
{
    return "GraspObjectTest";
}

// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr GraspObjectTest::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new GraspObjectTest(stateData));
}

