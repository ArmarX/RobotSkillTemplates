/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::GraspObjectGroup
 * @author     Markus Grotz ( markus dot grotz at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "ObjectIsReachable.h"

using namespace armarx;
using namespace GraspObjectGroup;

// DO NOT EDIT NEXT LINE
ObjectIsReachable::SubClassRegistry ObjectIsReachable::Registry(ObjectIsReachable::GetName(), &ObjectIsReachable::CreateInstance);


#include <MemoryX/core/GridFileManager.h>
#include <MemoryX/core/MemoryXCoreObjectFactories.h>
#include <MemoryX/libraries/memorytypes/entity/ObjectClass.h>
#include <MemoryX/libraries/helpers/VirtualRobotHelpers/SimoxObjectWrapper.h>
#include <MemoryX/libraries/memorytypes/MemoryXTypesObjectFactories.h>

#include <VirtualRobot/ManipulationObject.h>
#include <VirtualRobot/Grasping/Grasp.h>
#include <VirtualRobot/Grasping/GraspSet.h>
#include <VirtualRobot/XML/ObjectIO.h>



void ObjectIsReachable::onEnter()

{


    if (in.isDesiredTcpOffsetToObjectInputSet())
    {
        local.setDesiredTcpOffsetToObject(in.getDesiredTcpOffsetToObjectInput());

        return;
    }
    else if (in.isObjectNameSet() && in.isGraspNameSet() && in.isGraspSetNameSet())
    {
        std::string objectName = in.getObjectName();
        memoryx::PersistentObjectClassSegmentBasePrx classesSegmentPrx = getPriorKnowledge()->getObjectClassesSegment();
        memoryx::CommonStorageInterfacePrx databasePrx = getPriorKnowledge()->getCommonStorage();
        memoryx::GridFileManagerPtr fileManager(new memoryx::GridFileManager(databasePrx));
        memoryx::EntityBasePtr entity = classesSegmentPrx->getEntityByName(objectName);

        memoryx::ObjectClassPtr objectClass = memoryx::ObjectClassPtr::dynamicCast(entity);
        memoryx::EntityWrappers::SimoxObjectWrapperPtr simoxWrapper = objectClass->addWrapper(new memoryx::EntityWrappers::SimoxObjectWrapper(fileManager));
        VirtualRobot::ManipulationObjectPtr mo = simoxWrapper->getManipulationObject();


        Eigen::Matrix4f transformationFromObjectToTCPPose = Eigen::Matrix4f::Identity();

        std::string graspSetName = in.getGraspSetName();

        if (mo->getGraspSet(graspSetName))
        {
            std::string graspName = in.getGraspName();
            VirtualRobot::GraspPtr grasp = mo->getGraspSet(graspSetName)->getGrasp(graspName);

            if (!grasp)
            {
                ARMARX_ERROR << "No grasp with name " << graspName << " found! ";
                emitFailure();
            }
            else
            {
                transformationFromObjectToTCPPose = grasp->getTransformation().inverse();
            }
        }
        else
        {
            ARMARX_ERROR << "No grasp set with name " << graspSetName << " found! ";
            emitFailure();
        }

        Eigen::Matrix4f desiredTcpOffsetToObjectEigen = transformationFromObjectToTCPPose;
        PosePtr desiredTcpOffsetToObject = new Pose(desiredTcpOffsetToObjectEigen);
        local.setDesiredTcpOffsetToObject(desiredTcpOffsetToObject);
    }
    else
    {
        emitFailure();
    }
}

//void ObjectIsReachable::run()
//{
//    // put your user code for the execution-phase here
//    // runs in seperate thread, thus can do complex operations
//    // should check constantly whether isRunningTaskStopped() returns true
//
//// uncomment this if you need a continous run function. Make sure to use sleep or use blocking wait to reduce cpu load.
//    while (!isRunningTaskStopped()) // stop run function if returning true
//    {
//        // do your calculations
//    }
//}

//void ObjectIsReachable::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void ObjectIsReachable::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr ObjectIsReachable::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new ObjectIsReachable(stateData));
}

