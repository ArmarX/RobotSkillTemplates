/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2014-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::GraspObjectGroup
 * @author     Mirko Waechter ( waechter at kit dot edu )
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "GraspObject.h"

#include "GraspObjectGroupStatechartContext.generated.h"


#include <MemoryX/libraries/observers/ObjectMemoryObserver.h>
#include <RobotAPI/interface/components/ViewSelectionInterface.h>


using namespace armarx;
using namespace GraspObjectGroup;


// DO NOT EDIT NEXT LINE
GraspObject::SubClassRegistry GraspObject::Registry(GraspObject::GetName(), &GraspObject::CreateInstance);



GraspObject::GraspObject(XMLStateConstructorParams stateData) :
    XMLStateTemplate < GraspObject > (stateData),
    GraspObjectGeneratedBase<GraspObject> (stateData)
{
}



void GraspObject::onEnter()
{
    GraspObjectGroupStatechartContext* context = getContext<GraspObjectGroupStatechartContext>();
    context->getViewSelection()->activateAutomaticViewSelection();

    // get memory channel for the hand
    std::string handName = in.getHandNameInMemory();
    ChannelRefBasePtr handMemoryChannel = context->getObjectMemoryObserver()->requestObjectClassRepeated(handName, 150, armarx::DEFAULT_VIEWTARGET_PRIORITY);

    if (handMemoryChannel)
    {
        handMemoryChannel->validate();
        local.setHandMemoryChannel(ChannelRefPtr::dynamicCast(handMemoryChannel));
    }
    else
    {
        emitInitializingGraspingFailed();
        ARMARX_WARNING << "Unknown Object Class: " << handName;
    }

    // get memory channel for the object
    std::string objectName = "";

    if (in.isObjectInstanceChannelSet())
    {
        // make sure that the object is tracked
        objectName = in.getObjectInstanceChannel()->getDataField("className")->getString();
        memoryx::ObjectClassPtr obj = memoryx::ObjectClassPtr::dynamicCast(context->getWorkingMemory()->getObjectClassesSegment()->getEntityByName(objectName));
        ChannelRefBasePtr objectMemoryChannel;
        if (in.isPregivenObjectClassMemoryChannelSet())
        {
            objectMemoryChannel = in.getPregivenObjectClassMemoryChannel();
        }
        else
        {
            objectMemoryChannel = context->getObjectMemoryObserver()->requestObjectClassRepeated(objectName, 1000 / in.getObjectLocalizationFrequency(), armarx::DEFAULT_VIEWTARGET_PRIORITY);
        }

        if (objectMemoryChannel)
        {
            local.setObjectMemoryChannel(ChannelRefPtr::dynamicCast(objectMemoryChannel));
        }
        else
        {
            ARMARX_WARNING << "Something went wrong when requesting " << objectName;
        }
    }
    else
    {
        objectName = in.getObjectName();
        ChannelRefBasePtr objectMemoryChannel;
        if (in.isPregivenObjectClassMemoryChannelSet())
        {
            objectMemoryChannel = in.getPregivenObjectClassMemoryChannel();
        }
        else
        {
            objectMemoryChannel = context->getObjectMemoryObserver()->requestObjectClassRepeated(objectName, 1000 / in.getObjectLocalizationFrequency(), armarx::DEFAULT_VIEWTARGET_PRIORITY);
        }
        if (objectMemoryChannel)
        {
            local.setObjectMemoryChannel(ChannelRefPtr::dynamicCast(objectMemoryChannel));
        }
        else
        {
            emitInitializingGraspingFailed();
            ARMARX_WARNING << "Unknown Object Class: " << objectName;
        }
    }

    std::string speechOutput = "I will now grasp the " + objectName + " with my ";

    if (handName.find("left") != std::string::npos || handName.find("Left") != std::string::npos)
    {
        speechOutput += "left hand";
    }
    else
    {
        speechOutput += "right hand";
    }

    //    ARMARX_INFO << "Speech output: '" << speechOutput << "'";
    //    context->getTextToSpeech()->reportText(speechOutput);

    std::string kinematicChainName = in.getKinematicChainName();
    std::string tcpName = context->getRobot()->getRobotNodeSet(kinematicChainName)->getTCP()->getName();
    local.setTcpNameInRobotModel(tcpName);
}






void GraspObject::onExit()
{
    GraspObjectGroupStatechartContext* context = getContext<GraspObjectGroupStatechartContext>();

    // put your user code for the exit point here
    // execution time should be short (<100ms)
    if (!local.isHandMemoryChannelSet())
    {
        context->getObjectMemoryObserver()->releaseObjectClass(local.getHandMemoryChannel());
    }

    if (!in.isPregivenObjectClassMemoryChannelSet() && local.isObjectMemoryChannelSet())
    {
        context->getObjectMemoryObserver()->releaseObjectClass(local.getObjectMemoryChannel());
    }
}



// DO NOT EDIT NEXT FUNCTION
std::string GraspObject::GetName()
{
    return "GraspObject";
}



// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr GraspObject::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new GraspObject(stateData));
}

