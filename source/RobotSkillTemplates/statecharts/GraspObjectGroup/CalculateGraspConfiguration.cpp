/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::GraspObjectGroup
 * @author     Mirko Waechter ( mirko dot waechter at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "CalculateGraspConfiguration.h"

#include <MemoryX/libraries/memorytypes/MemoryXTypesObjectFactories.h>
#include <MemoryX/libraries/helpers/VirtualRobotHelpers/SimoxObjectWrapper.h>
#include <MemoryX/core/GridFileManager.h>
#include <MemoryX/libraries/memorytypes/entity/ObjectClass.h>
#include <MemoryX/libraries/memorytypes/entity/ObjectInstance.h>
using namespace armarx;
using namespace GraspObjectGroup;

// DO NOT EDIT NEXT LINE
CalculateGraspConfiguration::SubClassRegistry CalculateGraspConfiguration::Registry(CalculateGraspConfiguration::GetName(), &CalculateGraspConfiguration::CreateInstance);



void CalculateGraspConfiguration::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
    ARMARX_INFO << "Instance content: " << getWorkingMemory()->getObjectInstancesSegment()->getJSONEntityById(in.getObjectInstanceChannel()->getDataField("id")->getString());
    memoryx::ObjectInstancePtr objInstance = memoryx::ObjectInstancePtr::dynamicCast(getWorkingMemory()->getObjectInstancesSegment()->getObjectInstanceById(in.getObjectInstanceChannel()->getDataField("id")->getString()));
    ARMARX_INFO << "instance content local: " << *objInstance;
    auto robot = RemoteRobot::createLocalCloneFromFile(getRobotStateComponent(), VirtualRobot::RobotIO::eCollisionModel);
    RemoteRobot::synchronizeLocalClone(robot, getRobotStateComponent());
    memoryx::ObjectClassPtr objclass = memoryx::ObjectClassPtr::dynamicCast(getWorkingMemory()->getObjectClassesSegment()->getEntityByName(in.getObjectInstanceChannel()->getDataField("className")->getString()));
    ARMARX_CHECK_EXPRESSION(objclass);
    memoryx::GridFileManagerPtr fileManager(new memoryx::GridFileManager(getWorkingMemory()->getCommonStorage()));
    memoryx::EntityWrappers::SimoxObjectWrapperPtr simoxObject = objclass->addWrapper(new memoryx::EntityWrappers::SimoxObjectWrapper(fileManager));
    simoxObject->getManipulationObject();
    objInstance->setPose(objInstance->getPose()->toGlobal(robot));
    auto endeffector = robot->getEndEffector(in.getHandName());
    simoxObject->updateFromEntity(objInstance);
    ARMARX_INFO << "GCP Pose: " << endeffector->getGCP()->getGlobalPose();
    ARMARX_INFO << "ObjectPose: " << simoxObject->getManipulationObject()->getGlobalPose();
    ARMARX_INFO << "JointConfig before: " << endeffector->getConfiguration()->getRobotNodeJointValueMap();
    endeffector->closeActors(simoxObject->getManipulationObject());
    //    getEntityDrawerTopic()->setObjectVisu("Grasping", "TargetObjectVisu", objclass, new Pose(simoxObject->getManipulationObject()->getGlobalPose()));

    //    memoryx::ObjectClassPtr handobjclass = memoryx::ObjectClassPtr::dynamicCast(getWorkingMemory()->getObjectClassesSegment()->getEntityByName(in.getHandInstanceChannel()->getDataField("className")->getString()));

    //    getEntityDrawerTopic()->setObjectVisu("Grasping", "HandVisu", handobjclass, new Pose(robot->getRobotNode(in.getHandName())->getGlobalPose()));
    //    getEntityDrawerTopic()->updateObjectColor("Grasping", "HandVisu", DrawColor {1, 0, 0, 0.5});
    out.setJointValueMap(endeffector->getConfiguration()->getRobotNodeJointValueMap());
    emitSuccess();
}

//void CalculateGraspConfiguration::run()
//{
//    // put your user code for the execution-phase here
//    // runs in seperate thread, thus can do complex operations
//    // should check constantly whether isRunningTaskStopped() returns true
//
//// uncomment this if you need a continous run function. Make sure to use sleep or use blocking wait to reduce cpu load.
//    while (!isRunningTaskStopped()) // stop run function if returning true
//    {
//        // do your calculations
//    }
//}

//void CalculateGraspConfiguration::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void CalculateGraspConfiguration::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr CalculateGraspConfiguration::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new CalculateGraspConfiguration(stateData));
}

