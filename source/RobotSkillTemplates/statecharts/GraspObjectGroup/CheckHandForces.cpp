/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2014-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    GraspObject::GraspObjectGroup
 * @author     [Author Name] ( [Author Email] )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "CheckHandForces.h"
#include "GraspObjectGroupStatechartContext.generated.h"

using namespace armarx;
using namespace GraspObjectGroup;

// DO NOT EDIT NEXT LINE
CheckHandForces::SubClassRegistry CheckHandForces::Registry(CheckHandForces::GetName(), &CheckHandForces::CreateInstance);



CheckHandForces::CheckHandForces(const XMLStateConstructorParams& stateData) :
    XMLStateTemplate<CheckHandForces>(stateData),  CheckHandForcesGeneratedBase<CheckHandForces>(stateData)
{
}

void CheckHandForces::onEnter()
{
    GraspObjectGroupStatechartContext* context = getContext<GraspObjectGroupStatechartContext>();
    float maxForceMagnitude = 10.0;
    Eigen::Matrix4f leftTcpPoseBase = context->getRobot()->getRobotNode(in.getLeftHandName())->getPoseInRootFrame();
    Eigen::Matrix4f rightTcpPoseBase = context->getRobot()->getRobotNode(in.getRightHandName())->getPoseInRootFrame();

    if (!in.getLeftHandForceChecked() && !in.getLeftHandClosed())
    {
        //ConditionIdentifier condForcesTooHigh;
        DatafieldRefPtr forceRefLeft = DatafieldRefPtr::dynamicCast(context->getForceTorqueObserver()->getForceDatafield(in.getLeftHandName()));
        FramedDirectionPtr curForceLeft = forceRefLeft->getDataField()->get<FramedDirection>();


        Eigen::Vector3f leftHandForces = curForceLeft->toEigen();

        //Literal forcesTooHigh(DataFieldIdentifier(context->getForceTorqueObserverName(),"Wrist 2 L","forces"), "magnitudelarger", Literal::createParameterList(maxForceMagnitude));
        if (leftHandForces.norm() > maxForceMagnitude && in.getRightHandClosed())
        {
            out.setLeftTcpTargetPoseUpdated(FramedPose(leftTcpPoseBase, "Armar3_Base", "Armar3"));
            out.setRightTcpTargetPoseUpdated(FramedPose(rightTcpPoseBase, "Armar3_Base", "Armar3"));
            emitBothHandForceDetected();
        }
        else if (leftHandForces.norm() > maxForceMagnitude)
        {
            out.setLeftTcpTargetPoseUpdated(FramedPose(leftTcpPoseBase, "Armar3_Base", "Armar3"));
            out.setRightTcpTargetPoseUpdated(in.getRightTcpTargetPose());
            emitLeftHandForceDetected();
        }
        else
        {
            out.setLeftTcpTargetPoseUpdated(in.getLeftTcpTargetPose());
            out.setRightTcpTargetPoseUpdated(in.getRightTcpTargetPose());
            emitNoHandForcesDetected();
        }

        out.setLeftHandForceChecked(true);
        out.setRightHandForceChecked(false);
        //out.setLeftHandClosed(true);
    }
    else if (!in.getRightHandForceChecked()  && !in.getRightHandClosed())
    {
        //ConditionIdentifier condForcesTooHigh;
        DatafieldRefPtr forceRefRight = DatafieldRefPtr::dynamicCast(context->getForceTorqueObserver()->getForceDatafield(in.getRightHandName()));
        FramedDirectionPtr curForceRight = forceRefRight->getDataField()->get<FramedDirection>();
        //Literal forcesTooHigh(DataFieldIdentifier(context->getForceTorqueObserverName(),"Wrist 2 L","forces"), "magnitudelarger", Literal::createParameterList(maxForceMagnitude));
        //condForcesTooHigh = installCondition(forcesTooHigh, createEvent<RightHandForceDetected>());

        Eigen::Vector3f rightHandForces = curForceRight->toEigen();

        //Literal forcesTooHigh(DataFieldIdentifier(context->getForceTorqueObserverName(),"Wrist 2 L","forces"), "magnitudelarger", Literal::createParameterList(maxForceMagnitude));
        if (rightHandForces.norm() > maxForceMagnitude && in.getLeftHandClosed())
        {
            out.setRightTcpTargetPoseUpdated(FramedPose(rightTcpPoseBase, "Armar3_Base", "Armar3"));
            out.setLeftTcpTargetPoseUpdated(FramedPose(leftTcpPoseBase, "Armar3_Base", "Armar3"));
            emitBothHandForceDetected();
        }
        else if (rightHandForces.norm() > maxForceMagnitude)
        {
            out.setRightTcpTargetPoseUpdated(FramedPose(rightTcpPoseBase, "Armar3_Base", "Armar3"));
            out.setLeftTcpTargetPoseUpdated(in.getLeftTcpTargetPose());
            emitRightHandForceDetected();
        }
        else
        {
            out.setRightTcpTargetPoseUpdated(in.getRightTcpTargetPose());
            out.setLeftTcpTargetPoseUpdated(in.getLeftTcpTargetPose());
            emitNoHandForcesDetected();
        }

        out.setLeftHandForceChecked(false);
        out.setRightHandForceChecked(true);
        //out.setRightHandClosed(true);
    }
    else
    {
        out.setLeftHandForceChecked(false);
        out.setRightHandForceChecked(false);
    }

}

void CheckHandForces::run()
{
    // put your user code for the execution-phase here
    // runs in seperate thread, thus can do complex operations
    // should check constantly whether isRunningTaskStopped() returns true

    // uncomment this if you need a continous run function. Make sure to use sleep or use blocking wait to reduce cpu load.
    //    while (!isRunningTaskStopped()) // stop run function if returning true
    //    {
    //        // do your calculations
    //    }

}

void CheckHandForces::onBreak()
{
    // put your user code for the breaking point here
    // execution time should be short (<100ms)
}

void CheckHandForces::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)

}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr CheckHandForces::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new CheckHandForces(stateData));
}

