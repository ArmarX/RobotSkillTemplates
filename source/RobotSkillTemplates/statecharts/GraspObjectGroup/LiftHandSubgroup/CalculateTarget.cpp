/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2014-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::GraspObjectGroup
 * @author     David ( david dot schiebener at kit dot edu )
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "CalculateTarget.h"

#include "GraspObjectGroupStatechartContext.generated.h"

using namespace armarx;
using namespace GraspObjectGroup;

// DO NOT EDIT NEXT LINE
CalculateTarget::SubClassRegistry CalculateTarget::Registry(CalculateTarget::GetName(), &CalculateTarget::CreateInstance);



CalculateTarget::CalculateTarget(XMLStateConstructorParams stateData) :
    XMLStateTemplate<CalculateTarget>(stateData),  CalculateTargetGeneratedBase<CalculateTarget>(stateData)
{
}



void CalculateTarget::onEnter()
{
    // get hand pose from MemoryX
    GraspObjectGroupStatechartContext* context = getContext<GraspObjectGroupStatechartContext>();
    FramedPose handPoseFromMemoryX;
    ChannelRefPtr handMemoryChannel = getInput<ChannelRef>("HandMemoryChannel");
    memoryx::ChannelRefBaseSequence instances = context->getObjectMemoryObserver()->getObjectInstances(handMemoryChannel);

    if (instances.size() == 0)
    {
        ARMARX_ERROR << "No instances of the hand in the memory";
        return;
    }
    else
    {
        ARMARX_VERBOSE << "Getting hand pose from memory";

        FramedPositionPtr position = ChannelRefPtr::dynamicCast(instances.front())->get<FramedPosition>("position");
        FramedOrientationPtr orientation = ChannelRefPtr::dynamicCast(instances.front())->get<FramedOrientation>("orientation");
        handPoseFromMemoryX = FramedPose(orientation->toEigen(), position->toEigen(), position->getFrame(), position->agent);

        if (position->getFrame().empty())
        {
            ARMARX_WARNING << "Empty frame name!";
        }
    }

    ARMARX_IMPORTANT << "Hand pose from MemoryX: " << handPoseFromMemoryX;

    Eigen::Vector3f liftTranslation = in.getLiftTranslation()->toEigen();
    Eigen::Matrix4f targetEigen1 = handPoseFromMemoryX.toRootEigen(getRobot());
    targetEigen1(2, 3) += 0.7f * liftTranslation(2);
    FramedPose target1(targetEigen1, getRobot()->getRootNode()->getName(), handPoseFromMemoryX.agent);
    out.setTcpTargetPose1(target1);
    Eigen::Matrix4f targetEigen2 = handPoseFromMemoryX.toRootEigen(getRobot());
    targetEigen2.block<3, 1>(0, 3) += liftTranslation;
    FramedPose target2(targetEigen1, getRobot()->getRootNode()->getName(), handPoseFromMemoryX.agent);
    out.setTcpTargetPose2(target2);

    emitTargetCalculated();
}






void CalculateTarget::onBreak()
{
    // put your user code for the breaking point here
    // execution time should be short (<100ms)
}



void CalculateTarget::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}



// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr CalculateTarget::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new CalculateTarget(stateData));
}

