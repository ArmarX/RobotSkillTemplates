/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2014-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::GraspObjectGroup
 * @author     David ( david dot schiebener at kit dot edu )
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "LiftHand.h"

#include "GraspObjectGroupStatechartContext.generated.h"

#include <MemoryX/libraries/motionmodels/MotionModelAttachedToOtherObject.h>

#include <MemoryX/libraries/memorytypes/entity/ObjectInstance.h>


using namespace armarx;
using namespace GraspObjectGroup;


// DO NOT EDIT NEXT LINE
LiftHand::SubClassRegistry LiftHand::Registry(LiftHand::GetName(), &LiftHand::CreateInstance);



LiftHand::LiftHand(XMLStateConstructorParams stateData) :
    XMLStateTemplate<LiftHand>(stateData),  LiftHandGeneratedBase<LiftHand>(stateData)
{
}



void LiftHand::onEnter()
{
    GraspObjectGroupStatechartContext* context = getContext<GraspObjectGroupStatechartContext>();

    // request IK units
    ARMARX_VERBOSE << "Requesting TCP control unit";
    context->getTCPControlUnit()->request();
    ARMARX_VERBOSE << "Requested TCP control unit";

    // set motion model AttachedToOtherObject
    if (in.isObjectInstanceChannelSet())
    {
        std::string objName = in.getObjectInstanceChannel()->getDataField("className")->getString();
        auto handInstances = context->getObjectMemoryObserver()->getObjectInstancesByClass(in.getHandMemoryChannel()->getDataField("className")->getString());
        ARMARX_CHECK_EXPRESSION(handInstances.size() != 0);
        ChannelRefBasePtr handChannel = handInstances.front();

        memoryx::MotionModelAttachedToOtherObjectPtr newMotionModel = new memoryx::MotionModelAttachedToOtherObject(context->getRobotStateComponent(), ChannelRefPtr::dynamicCast(handChannel));
        memoryx::ObjectInstancePtr object = memoryx::ObjectInstancePtr::dynamicCast(context->getWorkingMemory()->getObjectInstancesSegment()->getEntityByName(objName));

        context->getWorkingMemory()->getObjectInstancesSegment()->setNewMotionModel(object->getId(), newMotionModel);
        ARMARX_IMPORTANT << "Attached object to " << ChannelRefPtr::dynamicCast(handChannel)->getDataField("className")->getString();
    }

    // TCP visu
    std::string chainName = in.getKinematicChainName();
    memoryx::PersistentObjectClassSegmentBasePrx classesSegmentPrx = context->getPriorKnowledge()->getObjectClassesSegment();
    auto tcpObjectClass = classesSegmentPrx->getObjectClassByName(in.getHandMemoryChannel()->getDataField("className")->getString());
    if (tcpObjectClass)
    {
        context->getEntityDrawerTopic()->setObjectVisu("VisualServoHandVisu", "tcpTarget_" + chainName, tcpObjectClass, new Pose());
        context->getEntityDrawerTopic()->updateObjectTransparency("VisualServoHandVisu", "tcpTarget_" + chainName, 0.3);
    }

    local.setStartTimeRef(ChannelRefPtr::dynamicCast(context->systemObserverPrx->startTimer("LiftHandTimer")));
}






void LiftHand::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)

    GraspObjectGroupStatechartContext* context = getContext<GraspObjectGroupStatechartContext>();
    ARMARX_VERBOSE << "Releasing TCP control unit";
    context->getTCPControlUnit()->release();
    ARMARX_VERBOSE << "Released TCP control unit";

    context->systemObserverPrx->removeTimer(local.getStartTimeRef());
    context->getEntityDrawerTopic()->removeLayer("VisualServoHandVisu");
}



// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr LiftHand::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new LiftHand(stateData));
}

