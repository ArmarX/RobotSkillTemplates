/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::MotionAndPlatformControlGroup::MotionAndPlatformControlGroupRemoteStateOfferer
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "MotionAndPlatformControlGroupRemoteStateOfferer.h"

using namespace armarx;
using namespace MotionAndPlatformControlGroup;

// DO NOT EDIT NEXT LINE
MotionAndPlatformControlGroupRemoteStateOfferer::SubClassRegistry MotionAndPlatformControlGroupRemoteStateOfferer::Registry(MotionAndPlatformControlGroupRemoteStateOfferer::GetName(), &MotionAndPlatformControlGroupRemoteStateOfferer::CreateInstance);



MotionAndPlatformControlGroupRemoteStateOfferer::MotionAndPlatformControlGroupRemoteStateOfferer(StatechartGroupXmlReaderPtr reader) :
    XMLRemoteStateOfferer < MotionAndPlatformControlGroupStatechartContext > (reader)
{
}

void MotionAndPlatformControlGroupRemoteStateOfferer::onInitXMLRemoteStateOfferer()
{

}

void MotionAndPlatformControlGroupRemoteStateOfferer::onConnectXMLRemoteStateOfferer()
{

}

void MotionAndPlatformControlGroupRemoteStateOfferer::onExitXMLRemoteStateOfferer()
{

}

// DO NOT EDIT NEXT FUNCTION
std::string MotionAndPlatformControlGroupRemoteStateOfferer::GetName()
{
    return "MotionAndPlatformControlGroupRemoteStateOfferer";
}

// DO NOT EDIT NEXT FUNCTION
XMLStateOffererFactoryBasePtr MotionAndPlatformControlGroupRemoteStateOfferer::CreateInstance(StatechartGroupXmlReaderPtr reader)
{
    return XMLStateOffererFactoryBasePtr(new MotionAndPlatformControlGroupRemoteStateOfferer(reader));
}



