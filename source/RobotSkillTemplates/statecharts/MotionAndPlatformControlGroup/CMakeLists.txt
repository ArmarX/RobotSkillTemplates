armarx_component_set_name("MotionAndPlatformControlGroup")

set(COMPONENT_LIBS
#   RobotAPIInterfaces RobotAPICore
    ArmarXCoreInterfaces
    ArmarXCore
    ArmarXCoreStatechart
    ArmarXCoreObservers

    RobotAPICore
    RobotAPIInterfaces
)

# Sources

set(SOURCES
MotionAndPlatformControlGroupRemoteStateOfferer.cpp
./MoveJointPlatformTrajectory.cpp
#@TEMPLATE_LINE@@COMPONENT_PATH@/@COMPONENT_NAME@.cpp
)

set(HEADERS
MotionAndPlatformControlGroupRemoteStateOfferer.h
MotionAndPlatformControlGroup.scgxml
./MoveJointPlatformTrajectory.h
#@TEMPLATE_LINE@@COMPONENT_PATH@/@COMPONENT_NAME@.h
./MoveJointPlatformTrajectory.xml
#@TEMPLATE_LINE@@COMPONENT_PATH@/@COMPONENT_NAME@.xml
)

armarx_add_component("${SOURCES}" "${HEADERS}")
