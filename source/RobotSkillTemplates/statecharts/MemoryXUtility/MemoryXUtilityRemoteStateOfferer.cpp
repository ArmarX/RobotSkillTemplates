/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::MemoryXUtility::MemoryXUtilityRemoteStateOfferer
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "MemoryXUtilityRemoteStateOfferer.h"

using namespace armarx;
using namespace MemoryXUtility;

// DO NOT EDIT NEXT LINE
MemoryXUtilityRemoteStateOfferer::SubClassRegistry MemoryXUtilityRemoteStateOfferer::Registry(MemoryXUtilityRemoteStateOfferer::GetName(), &MemoryXUtilityRemoteStateOfferer::CreateInstance);



MemoryXUtilityRemoteStateOfferer::MemoryXUtilityRemoteStateOfferer(StatechartGroupXmlReaderPtr reader) :
    XMLRemoteStateOfferer < MemoryXUtilityStatechartContext > (reader)
{
}

void MemoryXUtilityRemoteStateOfferer::onInitXMLRemoteStateOfferer()
{

}

void MemoryXUtilityRemoteStateOfferer::onConnectXMLRemoteStateOfferer()
{

}

void MemoryXUtilityRemoteStateOfferer::onExitXMLRemoteStateOfferer()
{

}

// DO NOT EDIT NEXT FUNCTION
std::string MemoryXUtilityRemoteStateOfferer::GetName()
{
    return "MemoryXUtilityRemoteStateOfferer";
}

// DO NOT EDIT NEXT FUNCTION
XMLStateOffererFactoryBasePtr MemoryXUtilityRemoteStateOfferer::CreateInstance(StatechartGroupXmlReaderPtr reader)
{
    return XMLStateOffererFactoryBasePtr(new MemoryXUtilityRemoteStateOfferer(reader));
}



