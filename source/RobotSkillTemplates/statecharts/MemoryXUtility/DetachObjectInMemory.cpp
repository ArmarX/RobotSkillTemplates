/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::MemoryXUtility
 * @author     Fabian Paus ( fabian dot paus at kit dot edu )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "DetachObjectInMemory.h"

//#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>

#include <MemoryX/libraries/motionmodels/MotionModelStaticObject.h>

using namespace armarx;
using namespace MemoryXUtility;

// DO NOT EDIT NEXT LINE
DetachObjectInMemory::SubClassRegistry DetachObjectInMemory::Registry(DetachObjectInMemory::GetName(), &DetachObjectInMemory::CreateInstance);



void DetachObjectInMemory::onEnter()
{
    memoryx::ObjectInstanceMemorySegmentBasePrx instanceMemory = getWorkingMemory()->getObjectInstancesSegment();
    std::string attachedObjectId;

    if (in.isAttachedObjectChannelSet() and in.getAttachedObjectChannel())
    {
        try
        {
            TimedVariantPtr idField = ChannelRefPtr::dynamicCast(in.getAttachedObjectChannel())->getDataField("id");
            attachedObjectId = idField->getString();
        }
        catch (std::exception const& ex)
        {
            ARMARX_WARNING << "Could not get ID of attached object from channel. Reason:\n"
                           << ex.what();
            throw;
        }
    }
    else if (in.isAttachedObjectIDSet())
    {
        attachedObjectId = in.getAttachedObjectID();
    }
    else
    {
        ARMARX_INFO << "Attached object is not defined. Either set ID or channel!";
        emitFailure();
        return;
    }

    memoryx::MotionModelStaticObjectPtr newMotionModel(new memoryx::MotionModelStaticObject(getRobotStateComponent()));
    getWorkingMemory()->getObjectInstancesSegment()->setNewMotionModel(attachedObjectId, newMotionModel);
    emitSuccess();
}

//void DetachObjectInMemory::run()
//{
//    // put your user code for the execution-phase here
//    // runs in seperate thread, thus can do complex operations
//    // should check constantly whether isRunningTaskStopped() returns true
//
//// uncomment this if you need a continous run function. Make sure to use sleep or use blocking wait to reduce cpu load.
//    while (!isRunningTaskStopped()) // stop run function if returning true
//    {
//        // do your calculations
//    }
//}

//void DetachObjectInMemory::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void DetachObjectInMemory::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr DetachObjectInMemory::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new DetachObjectInMemory(stateData));
}

