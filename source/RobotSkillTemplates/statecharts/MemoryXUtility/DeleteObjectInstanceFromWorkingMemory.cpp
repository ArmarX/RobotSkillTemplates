/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::MemoryXUtility
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "DeleteObjectInstanceFromWorkingMemory.h"

#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>
#include <RobotAPI/libraries/ArmarXObjects/ice_conversions.h>


using namespace armarx;
using namespace MemoryXUtility;

// DO NOT EDIT NEXT LINE
DeleteObjectInstanceFromWorkingMemory::SubClassRegistry DeleteObjectInstanceFromWorkingMemory::Registry(DeleteObjectInstanceFromWorkingMemory::GetName(), &DeleteObjectInstanceFromWorkingMemory::CreateInstance);

void DeleteObjectInstanceFromWorkingMemory::onEnter()
{
    memoryx::ObjectInstanceMemorySegmentBasePrx instancesSegment = getWorkingMemory()->getObjectInstancesSegment();
    if (in.isObjectClassNameSet() and not in.getObjectClassName().empty())
    {
        std::string objectClassName = in.getObjectClassName();
        if (objectClassName.find("/") != std::string::npos)
        {
            // It is an armarx::ObjectID.
            // In this case, we do nothing.
            ARMARX_INFO << "Got an ObjectID as class name: " << objectClassName;

            objpose::DetachObjectFromRobotNodeInput input;
            toIce(input.objectID, ObjectID(objectClassName));
            getObjectPoseStorage()->detachObjectFromRobotNode(input);
        }
        else
        {
            memoryx::ObjectInstanceList instances = instancesSegment->getObjectInstancesByClass(objectClassName);
            ARMARX_INFO << "Removing " << instances.size() << " of object class '" << objectClassName << "'";
            for (memoryx::ObjectInstanceBasePtr const& instance : instances)
            {
                std::string id = instance->getId();
                ARMARX_INFO << "Removing object of class '" << objectClassName << "' with id '" << id << "'";

                instancesSegment->removeEntity(id);
            }
        }
    }
    else if (in.isObjectInstanceChannelSet() || in.isObjectInstanceIdSet())
    {
        const std::string id = in.isObjectInstanceIdSet() ? in.getObjectInstanceId() : in.getObjectInstanceChannel()->getDataField("id")->getString();
        ARMARX_INFO << "Removing object with id '" << id << "'";

        instancesSegment->removeEntity(id);
    }
    else
    {
        ARMARX_WARNING << "Neither object instance channel nor ID set.";
    }
}

void DeleteObjectInstanceFromWorkingMemory::run()
{
    TimeUtil::MSSleep(in.getWaitTime());
    emitSuccess();
}

// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr DeleteObjectInstanceFromWorkingMemory::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new DeleteObjectInstanceFromWorkingMemory(stateData));
}

