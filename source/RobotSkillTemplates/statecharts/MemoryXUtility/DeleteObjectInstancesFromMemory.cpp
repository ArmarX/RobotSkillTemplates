/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::MemoryXUtility
 * @author     Mirko Waechter ( mirko dot waechter at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "DeleteObjectInstancesFromMemory.h"

#include <MemoryX/core/entity/Entity.h>
#include <MemoryX/core/MemoryXCoreObjectFactories.h>
#include <MemoryX/libraries/memorytypes/MemoryXTypesObjectFactories.h>

#include <ArmarXCore/observers/variant/ChannelRef.h>

//#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>

using namespace armarx;
using namespace MemoryXUtility;

// DO NOT EDIT NEXT LINE
DeleteObjectInstancesFromMemory::SubClassRegistry DeleteObjectInstancesFromMemory::Registry(DeleteObjectInstancesFromMemory::GetName(), &DeleteObjectInstancesFromMemory::CreateInstance);



void DeleteObjectInstancesFromMemory::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)


}

void DeleteObjectInstancesFromMemory::run()
{
    // put your user code for the execution-phase here
    // runs in seperate thread, thus can do complex operations
    // should check constantly whether isRunningTaskStopped() returns true

    auto channels = getObjectMemoryObserver()->getAvailableChannels(false);

    for (auto& channel : channels)
    {
        if (channel.first.find("_query_") != std::string::npos)
        {
            ChannelRefPtr ref = new ChannelRef(getObjectMemoryObserver(), channel.first);
            getObjectMemoryObserver()->releaseObjectClass(ref);
            ARMARX_INFO << "Releasing channel for " << ref->getChannelName();
        }
    }
    auto entities = getPriorKnowledge()->getObjectClassesSegment()->getAllEntities();
    auto instanceSegment = getWorkingMemory()->getObjectInstancesSegment();
    for (memoryx::EntityBasePtr base : entities)
    {
        memoryx::EntityPtr entity = memoryx::EntityPtr::dynamicCast(base);
        auto var = entity->getAttributeValue("recognitionMethod");
        if (var && !var->getString().empty() && var->getString() != "<none>")
        {
            auto objInstances = instanceSegment->getObjectInstancesByClass(entity->getName());
            ARMARX_INFO << "Checking instances of class: " << entity->getName() << " got " << objInstances.size() << " instances";
            for (auto& obj : objInstances)
            {
                instanceSegment->removeEntity(obj->getId());
                ARMARX_INFO << "Removing instance " << obj->getName() << " with id " << obj->getId();
            }
        }
    }
    emitSuccess();

    //    // uncomment this if you need a continous run function. Make sure to use sleep or use blocking wait to reduce cpu load.
    //    while (!isRunningTaskStopped()) // stop run function if returning true
    //    {
    //        // do your calculations
    //    }
}

//void DeleteObjectInstancesFromMemory::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void DeleteObjectInstancesFromMemory::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr DeleteObjectInstancesFromMemory::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new DeleteObjectInstancesFromMemory(stateData));
}

