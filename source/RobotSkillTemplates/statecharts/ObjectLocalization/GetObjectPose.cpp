/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::ObjectLocalization
 * @author     Markus Grotz ( markus dot grotz at kit dot edu )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "GetObjectPose.h"

using namespace armarx;
using namespace ObjectLocalization;

#include <RobotAPI/libraries/core/FramedPose.h>
#include <MemoryX/libraries/memorytypes/entity/ObjectInstance.h>

#include <RobotAPI/libraries/ArmarXObjects/ObjectPose.h>

// DO NOT EDIT NEXT LINE
GetObjectPose::SubClassRegistry GetObjectPose::Registry(GetObjectPose::GetName(), &GetObjectPose::CreateInstance);



void GetObjectPose::onEnter()
{
    // TODO: THIS MUST BE ADAPTED TO HANDLE armarx::ObjectID in the instance channel or object name!
    ChannelRefBasePtr objectInstanceChannel;

    if (in.isObjectInstanceChannelSet() && in.getObjectInstanceChannel())
    {
        ARMARX_INFO << "using object instance channel";
        objectInstanceChannel = in.getObjectInstanceChannel();
    }
    else if (in.isObjectNameSet())
    {
        std::string objectName = in.getObjectName();
        if (objectName.find("/") != std::string::npos)
        {
            // It's an ObjectID.
            ObjectID id(objectName);
            objpose::ObjectPoseSeq objectPoses = objpose::fromIce(getObjectPoseStorage()->getObjectPoses());
            for (const objpose::ObjectPose& p : objectPoses)
            {
                if (p.objectID == id)
                {
                    ARMARX_IMPORTANT << "Found object in ObjectPoseStorage: " << id;
                    // out.setObjectChannel(nullptr); // Optional
                    // Miss-use instance ID for our ObjectID.
                    FramedPosePtr pose(new FramedPose(p.objectPoseRobot, "root", ""));
                    ARMARX_INFO << "Object pose from ObjectPoseStorage is " << pose->output();

                    out.setObjectPose(pose);
                    out.setObjectPosition(new FramedPosition(pose->toEigen(), pose->frame, pose->agent));
                    out.setObjectOrientation(new FramedOrientation(pose->toEigen(), pose->frame, pose->agent));

                    // Do not set optional parameter to nullptr (causes NullHandleException)
                    //out.setObjectInstanceChannel(nullptr);

                    emitSuccess();
                    return;
                }
            }

            ARMARX_WARNING << "Object instance ID '" << objectName << "' looks like an armarx::ObjectID, "
                           << "but no such object was found in ObjectPoseStorage (offering " << objectPoses.size() << " objects).";
            emitNoPoseAvailable();
            return;
        }
        else
        {
            memoryx::ObjectMemoryObserverInterfacePrx objectMemoryObserver = getObjectMemoryObserver();
            std::vector<ChannelRefBasePtr> objectInstanceList = objectMemoryObserver->getObjectInstancesByClass(objectName);

            ARMARX_INFO << "found " << objectInstanceList.size() << " instances for object " << in.getObjectName();
            if (objectInstanceList.size())
            {
                objectInstanceChannel = objectInstanceList.front();
            }
        }
    }

    if (!objectInstanceChannel)
    {
        emitNoPoseAvailable();
        return;
    }

    ChannelRefPtr channelRef = ChannelRefPtr::dynamicCast(objectInstanceChannel);
    if (!channelRef || !channelRef->hasDatafield("pose"))
    {
        ARMARX_WARNING << "unable to cast channel reference or datafield pose does not exists.";
        emitNoPoseAvailable();
        return;
    }

    FramedPosePtr pose = channelRef->get<FramedPose>("pose");
    ARMARX_INFO << "object pose is " << pose->output();

    out.setObjectPose(pose);
    out.setObjectPosition(new FramedPosition(pose->toEigen(), pose->frame, pose->agent));
    out.setObjectOrientation(new FramedOrientation(pose->toEigen(), pose->frame, pose->agent));
    out.setObjectInstanceChannel(channelRef);

    emitSuccess();
}

//void GetObjectPose::run()
//{
//    // put your user code for the execution-phase here
//    // runs in seperate thread, thus can do complex operations
//    // should check constantly whether isRunningTaskStopped() returns true
//
//// uncomment this if you need a continous run function. Make sure to use sleep or use blocking wait to reduce cpu load.
//    while (!isRunningTaskStopped()) // stop run function if returning true
//    {
//        // do your calculations
//    }
//}

//void GetObjectPose::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void GetObjectPose::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr GetObjectPose::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new GetObjectPose(stateData));
}

