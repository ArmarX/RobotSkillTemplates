/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::ObjectLocalization
 * @author     Markus Grotz ( markus dot grotz at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "Wait.h"

#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>

using namespace armarx;
using namespace ObjectLocalization;

// DO NOT EDIT NEXT LINE
Wait::SubClassRegistry Wait::Registry(Wait::GetName(), &Wait::CreateInstance);



void Wait::onEnter()
{
    if ((TimeUtil::GetTime() - in.getStartTime()->toTime()).toSecondsDouble() > in.getTimeout())
    {
        emitTimeout();
        return;
    }

    ARMARX_DEBUG << "sleeping for " << in.getSleepTime() << " seconds";
}

void Wait::run()
{
    TimeUtil::Sleep(in.getSleepTime());

    emitSuccess();
}

//void Wait::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void Wait::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr Wait::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new Wait(stateData));
}

