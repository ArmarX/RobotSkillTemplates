/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::ObjectLocalization
 * @author     Markus Grotz ( markus dot grotz at kit dot edu )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "GetLocalizationStatus.h"


#include <MemoryX/core/MemoryXCoreObjectFactories.h>
#include <MemoryX/core/entity/Entity.h>
#include <MemoryX/libraries/memorytypes/MemoryXTypesObjectFactories.h>

using namespace armarx;
using namespace ObjectLocalization;

// DO NOT EDIT NEXT LINE
GetLocalizationStatus::SubClassRegistry GetLocalizationStatus::Registry(GetLocalizationStatus::GetName(), &GetLocalizationStatus::CreateInstance);



void GetLocalizationStatus::onEnter()
{
    const memoryx::ObjectMemoryObserverInterfacePrx objectMemoryObserver = getObjectMemoryObserver();
    std::map<std::string, armarx::ChannelRegistryEntry> channels =  objectMemoryObserver->getAvailableChannels(false);

    for (auto& channel : channels)
    {
        if (channel.first.find("_query_"))
        {
            if (channel.first.find(in.getObjectName()) != std::string::npos)
            {
                emitObjectRequested();
                return;
            }
        }
    }
    emitObjectNotRequested();
}

//void GetLocalizationStatus::run()
//{
//    // put your user code for the execution-phase here
//    // runs in seperate thread, thus can do complex operations
//    // should check constantly whether isRunningTaskStopped() returns true
//
//// uncomment this if you need a continous run function. Make sure to use sleep or use blocking wait to reduce cpu load.
//    while (!isRunningTaskStopped()) // stop run function if returning true
//    {
//        // do your calculations
//    }
//}

//void GetLocalizationStatus::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void GetLocalizationStatus::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr GetLocalizationStatus::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new GetLocalizationStatus(stateData));
}

