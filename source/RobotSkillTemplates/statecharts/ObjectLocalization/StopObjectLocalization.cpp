/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::ObjectLocalization
 * @author     Markus Grotz ( markus dot grotz at kit dot edu )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "StopObjectLocalization.h"


#include <MemoryX/libraries/memorytypes/entity/ObjectInstance.h>
#include <MemoryX/libraries/memorytypes/MemoryXTypesObjectFactories.h>
#include <MemoryX/core/MemoryXCoreObjectFactories.h>


using namespace armarx;
using namespace ObjectLocalization;

// DO NOT EDIT NEXT LINE
StopObjectLocalization::SubClassRegistry StopObjectLocalization::Registry(StopObjectLocalization::GetName(), &StopObjectLocalization::CreateInstance);



void StopObjectLocalization::onEnter()
{
    if (in.isObjectInstanceChannelSet())
    {
        memoryx::ObjectMemoryObserverInterfacePrx objectMemoryObserver = getObjectMemoryObserver();
        objectMemoryObserver->releaseObjectClass(in.getObjectInstanceChannel());
    }
    if (in.isObjectIDSet())
    {
        // TODO: Stop localizing the object in the ObjectPoseStorage?
        ARMARX_INFO << "Stopping localization for object '" << in.getObjectID() << "'";
    }

    emitSuccess();
}

//void StopObjectLocalization::run()
//{
//    // put your user code for the execution-phase here
//    // runs in seperate thread, thus can do complex operations
//    // should check constantly whether isRunningTaskStopped() returns true
//
//// uncomment this if you need a continous run function. Make sure to use sleep or use blocking wait to reduce cpu load.
//    while (!isRunningTaskStopped()) // stop run function if returning true
//    {
//        // do your calculations
//    }
//}

//void StopObjectLocalization::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void StopObjectLocalization::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr StopObjectLocalization::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new StopObjectLocalization(stateData));
}

