/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::ObjectLocalization
 * @author     Markus Grotz ( markus dot grotz at kit dot edu )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "RequestObjectLocalization.h"


#include <MemoryX/libraries/observers/ObjectMemoryObserver.h>
#include <RobotAPI/interface/components/ViewSelectionInterface.h>


using namespace armarx;
using namespace ObjectLocalization;

// DO NOT EDIT NEXT LINE
RequestObjectLocalization::SubClassRegistry RequestObjectLocalization::Registry(RequestObjectLocalization::GetName(), &RequestObjectLocalization::CreateInstance);



void RequestObjectLocalization::onEnter()
{
    ARMARX_INFO << "Requesting object class " << in.getObjectName() << "  with cycle time  " <<  in.getCycleTime() << ".";

    if (in.getObjectName().find("/") != std::string::npos)
    {
        // TODO: Check in ObjectPoseStorage whether the class is requested
        ARMARX_INFO << "Using ObjectPoseStorage object, not setting ObjectChannel";
        // out.setObjectChannel(nullptr);
        emitSuccess();
        return;
    }

    memoryx::ObjectMemoryObserverInterfacePrx objectMemoryObserver = getObjectMemoryObserver();

    int priority = in.isPrioritySet() ? in.getPriority() : armarx::DEFAULT_VIEWTARGET_PRIORITY;

    int cycleTime = static_cast<int>(in.getCycleTime() / 1000.0f);
    // todo test if object exists
    ChannelRefBasePtr channelRef = objectMemoryObserver->requestObjectClassRepeated(in.getObjectName(), cycleTime, priority);
    if (channelRef)
    {
        out.setObjectChannel(channelRef);
        emitSuccess();
    }
    else
    {
        emitFailure();
    }
}

//void RequestObjectLocalization::run()
//{
//    // put your user code for the execution-phase here
//    // runs in seperate thread, thus can do complex operations
//    // should check constantly whether isRunningTaskStopped() returns true
//s
//// uncomment this if you need a continous run function. Make sure to use sleep or use blocking wait to reduce cpu load.
//    while (!isRunningTaskStopped()) // stop run function if returning true
//    {
//        // do your calculations
//    }
//}

//void RequestObjectLocalization::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void RequestObjectLocalization::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr RequestObjectLocalization::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new RequestObjectLocalization(stateData));
}

