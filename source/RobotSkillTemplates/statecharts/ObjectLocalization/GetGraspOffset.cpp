/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::ObjectLocalization
 * @author     Markus Grotz ( markus dot grotz at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "GetGraspOffset.h"

#include <MemoryX/core/GridFileManager.h>
#include <MemoryX/core/MemoryXCoreObjectFactories.h>

#include <MemoryX/libraries/memorytypes/entity/ObjectClass.h>
#include <MemoryX/libraries/memorytypes/MemoryXTypesObjectFactories.h>

#include <MemoryX/libraries/helpers/VirtualRobotHelpers/SimoxObjectWrapper.h>

#include <VirtualRobot/ManipulationObject.h>
#include <VirtualRobot/Grasping/Grasp.h>
#include <VirtualRobot/Grasping/GraspSet.h>

using namespace armarx;
using namespace ObjectLocalization;

// DO NOT EDIT NEXT LINE
GetGraspOffset::SubClassRegistry GetGraspOffset::Registry(GetGraspOffset::GetName(), &GetGraspOffset::CreateInstance);


void GetGraspOffset::onEnter()
{
    memoryx::PersistentObjectClassSegmentBasePrx objectClassesSegment = getPriorKnowledge()->getObjectClassesSegment();
    memoryx::CommonStorageInterfacePrx commonStorage = getPriorKnowledge()->getCommonStorage();
    memoryx::GridFileManagerPtr fileManager(new memoryx::GridFileManager(commonStorage));

    std::string objectName;
    if (in.isObjectInstanceChannelSet())
    {
        ARMARX_DEBUG << "using object instance channel";
        objectName = in.getObjectInstanceChannel()->getDataField("className")->getString();
    }
    else if (in.isObjectNameSet())
    {
        ARMARX_DEBUG << "using object name";
        objectName = in.isObjectNameSet();
    }
    else
    {
        ARMARX_WARNING << "object instance channel not set";
        emitFailure();
    }

    ARMARX_INFO << "retrieving grasp "  << in.getGraspName() << " from grasp set " << in.getGraspSetName() << " for object " << objectName;

    memoryx::EntityBasePtr entity = objectClassesSegment->getEntityByName(objectName);
    memoryx::ObjectClassPtr objectClass = memoryx::ObjectClassPtr::dynamicCast(entity);

    memoryx::EntityWrappers::SimoxObjectWrapperPtr simoxWrapper = objectClass->addWrapper(new memoryx::EntityWrappers::SimoxObjectWrapper(fileManager));
    VirtualRobot::ManipulationObjectPtr manipulationObject = simoxWrapper->getManipulationObject();

    VirtualRobot::GraspSetPtr graspSet = manipulationObject->getGraspSet(in.getGraspSetName());
    if (graspSet)
    {
        VirtualRobot::GraspPtr grasp = graspSet->getGrasp(in.getGraspName());

        if (!grasp)
        {
            ARMARX_ERROR << "No grasp with name " << in.getGraspName() << " found! ";
            emitFailure();
        }
        else
        {
            Eigen::Matrix4f graspPose = grasp->getTransformation().inverse();
            out.setGraspOffset(new Pose(graspPose));
            emitSuccess();
        }
    }
    else
    {
        ARMARX_ERROR << "No grasp set with name " << in.getGraspSetName() << " found! ";
        emitFailure();
    }
}

//void GetGraspOffset::run()
//{
//    // put your user code for the execution-phase here
//    // runs in seperate thread, thus can do complex operations
//    // should check constantly whether isRunningTaskStopped() returns true
//
//// uncomment this if you need a continous run function. Make sure to use sleep or use blocking wait to reduce cpu load.
//    while (!isRunningTaskStopped()) // stop run function if returning true
//    {
//        // do your calculations
//    }
//}

//void GetGraspOffset::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void GetGraspOffset::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr GetGraspOffset::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new GetGraspOffset(stateData));
}

