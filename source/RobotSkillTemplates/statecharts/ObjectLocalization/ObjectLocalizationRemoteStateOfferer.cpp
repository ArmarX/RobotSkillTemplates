/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::ObjectLocalization::ObjectLocalizationRemoteStateOfferer
 * @author     Markus Grotz ( markus dot grotz at kit dot edu )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "ObjectLocalizationRemoteStateOfferer.h"

using namespace armarx;
using namespace ObjectLocalization;

// DO NOT EDIT NEXT LINE
ObjectLocalizationRemoteStateOfferer::SubClassRegistry ObjectLocalizationRemoteStateOfferer::Registry(ObjectLocalizationRemoteStateOfferer::GetName(), &ObjectLocalizationRemoteStateOfferer::CreateInstance);



ObjectLocalizationRemoteStateOfferer::ObjectLocalizationRemoteStateOfferer(StatechartGroupXmlReaderPtr reader) :
    XMLRemoteStateOfferer < ObjectLocalizationStatechartContext > (reader)
{
}

void ObjectLocalizationRemoteStateOfferer::onInitXMLRemoteStateOfferer()
{

}

void ObjectLocalizationRemoteStateOfferer::onConnectXMLRemoteStateOfferer()
{

}

void ObjectLocalizationRemoteStateOfferer::onExitXMLRemoteStateOfferer()
{

}

// DO NOT EDIT NEXT FUNCTION
std::string ObjectLocalizationRemoteStateOfferer::GetName()
{
    return "ObjectLocalizationRemoteStateOfferer";
}

// DO NOT EDIT NEXT FUNCTION
XMLStateOffererFactoryBasePtr ObjectLocalizationRemoteStateOfferer::CreateInstance(StatechartGroupXmlReaderPtr reader)
{
    return XMLStateOffererFactoryBasePtr(new ObjectLocalizationRemoteStateOfferer(reader));
}



