/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::HandOverGroup
 * @author     Mirko Waechter ( mirko dot waechter at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#ifndef _ARMARX_XMLUSERCODE_RobotSkillTemplates_HandOverGroup_ReachOutToHumanPose_H
#define _ARMARX_XMLUSERCODE_RobotSkillTemplates_HandOverGroup_ReachOutToHumanPose_H

#include <RobotSkillTemplates/statecharts/HandOverGroup/ReachOutToHumanPose.generated.h>
#include <Eigen/Core>
namespace armarx::HandOverGroup
{
    class ReachOutToHumanPose :
        public ReachOutToHumanPoseGeneratedBase < ReachOutToHumanPose >
    {
    public:
        ReachOutToHumanPose(const XMLStateConstructorParams& stateData):
            XMLStateTemplate < ReachOutToHumanPose > (stateData), ReachOutToHumanPoseGeneratedBase < ReachOutToHumanPose > (stateData)
        {
        }

        // inherited from StateBase
        void onEnter() override;
        void run() override;
        // void onBreak() override;
        void onExit() override;

        Eigen::VectorXf calculateCentroid(const armarx::Keypoint3DMap& keypointMap);
        armarx::Keypoint3DMap getClosestPerson(const std::vector<armarx::Keypoint3DMap>& trackingData);
        bool isKeypointValid(const armarx::Keypoint3D& point) const;

        // static functions for AbstractFactory Method
        static XMLStateFactoryBasePtr CreateInstance(XMLStateConstructorParams stateData);
        static SubClassRegistry Registry;

        // DO NOT INSERT ANY CLASS MEMBERS,
        // use stateparameters instead,
        // if classmember are neccessary nonetheless, reset them in onEnter
        Eigen::VectorXf calculateMedian(const armarx::Keypoint3DMap& keypointMap);
        Eigen::Vector3f getHeadIKTarget(const armarx::Keypoint3DMap& keypointMap);
    };
}

#endif
