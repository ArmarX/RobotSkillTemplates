/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::HandOverGroup::HandOverGroupRemoteStateOfferer
 * @author     Mirko Waechter ( mirko dot waechter at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "HandOverGroupRemoteStateOfferer.h"

using namespace armarx;
using namespace HandOverGroup;

// DO NOT EDIT NEXT LINE
HandOverGroupRemoteStateOfferer::SubClassRegistry HandOverGroupRemoteStateOfferer::Registry(HandOverGroupRemoteStateOfferer::GetName(), &HandOverGroupRemoteStateOfferer::CreateInstance);



HandOverGroupRemoteStateOfferer::HandOverGroupRemoteStateOfferer(StatechartGroupXmlReaderPtr reader) :
    XMLRemoteStateOfferer < armarx::HandOverGroupStatechartContextExtension > (reader)
{
}

void HandOverGroupRemoteStateOfferer::onInitXMLRemoteStateOfferer()
{

}

void HandOverGroupRemoteStateOfferer::onConnectXMLRemoteStateOfferer()
{

}

void HandOverGroupRemoteStateOfferer::onExitXMLRemoteStateOfferer()
{

}

// DO NOT EDIT NEXT FUNCTION
std::string HandOverGroupRemoteStateOfferer::GetName()
{
    return "HandOverGroupRemoteStateOfferer";
}

// DO NOT EDIT NEXT FUNCTION
XMLStateOffererFactoryBasePtr HandOverGroupRemoteStateOfferer::CreateInstance(StatechartGroupXmlReaderPtr reader)
{
    return XMLStateOffererFactoryBasePtr(new HandOverGroupRemoteStateOfferer(reader));
}



