/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2017, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX
 * @author     Mirko Waechter( mirko.waechter at kit dot edu)
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "HandOverGroupStatechartContext.h"
#include <IceUtil/UUID.h>
#include <ArmarXCore/core/util/algorithm.h>

#include <mutex>

namespace armarx
{

    HandOverGroupStatechartContextExtension::HandOverGroupStatechartContextExtension()
    {
        listener = new OpenPose3DListenerImpl();
    }

    const IceInternal::Handle<OpenPose3DListenerImpl>& HandOverGroupStatechartContextExtension::getPoseData() const
    {
        return listener;
    }
    void HandOverGroupStatechartContextExtension::onInitStatechartContext()
    {
        HandOverGroup::HandOverGroupStatechartContext::onInitStatechartContext();
        auto object = getIceManager()->registerObject(listener, "OpenPose3dListenerImpl" + IceUtil::generateUUID()).first;
        getIceManager()->subscribeTopic(object, "OpenPoseEstimation3D");
    }

    void HandOverGroupStatechartContextExtension::onConnectStatechartContext()
    {
        HandOverGroup::HandOverGroupStatechartContext::onConnectStatechartContext();
    }

    void OpenPose3DListenerImpl::report3DKeypoints(const HumanPose3DMap& data, Ice::Long timestamp, const Ice::Current&)
    {
        ARMARX_INFO << deactivateSpam(5) << "Got new data of size: " << data.size() << " keys: " << ARMARX_STREAM_PRINTER
        {
            if (!data.empty())
            {
                //for (auto& pair : data.at(0))
                //{
                //    out << pair.first << ": " << pair.second.x << ", " << pair.second.y << ", " << pair.second.z << "\n";
                //}
                //                out << armarx::getMapKeys(data.at(0));
            }
        };
        std::unique_lock lock(mutex);
        std::vector<armarx::Keypoint3DMap> vec;
        for (const auto& [name, entity] : data)
        {
            vec.push_back(entity.keypointMap);
        }
        latestData = vec;
        this->timestamp = IceUtil::Time::microSeconds(timestamp);
    }

    std::pair<IceUtil::Time, std::vector<armarx::Keypoint3DMap>> OpenPose3DListenerImpl::getLatestData()
    {
        std::unique_lock lock(mutex);
        return std::make_pair(timestamp, latestData);
    }
} // namespace armarx






