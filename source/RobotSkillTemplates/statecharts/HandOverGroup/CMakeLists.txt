armarx_component_set_name("HandOverGroup")

set(COMPONENT_LIBS
    # ArmarXCore
    ArmarXCoreStatechart
    ArmarXCoreObservers
    # RobotAPI
    RobotAPICore
    RobotStatechartHelpers
    RobotAPIArmarXObjects
    ArViz
    # VisionX
    VisionXInterfaces
)

set(SOURCES HandOverGroupRemoteStateOfferer.cpp HandOverGroupStatechartContext.cpp)
set(HEADERS HandOverGroupRemoteStateOfferer.h   HandOverGroupStatechartContext.h HandOverGroup.scgxml)

armarx_generate_statechart_cmake_lists()
armarx_add_component("${SOURCES}" "${HEADERS}")
