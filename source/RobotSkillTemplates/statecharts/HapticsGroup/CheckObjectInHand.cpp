/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2014-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::HapticsGroup
 * @author     [Author Name] ( [Author Email] )
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "CheckObjectInHand.h"

#include <ArmarXCore/observers/variant/DatafieldRef.h>

using namespace armarx;
using namespace HapticsGroup;

// DO NOT EDIT NEXT LINE
CheckObjectInHand::SubClassRegistry CheckObjectInHand::Registry(CheckObjectInHand::GetName(), &CheckObjectInHand::CreateInstance);



CheckObjectInHand::CheckObjectInHand(XMLStateConstructorParams stateData) :
    XMLStateTemplate < CheckObjectInHand > (stateData)
{
}

void CheckObjectInHand::onEnter()
{
    ARMARX_VERBOSE << "Entering CheckObjectInHand::onEnter()";

    RobotStatechartContext* rsContext = getContext<RobotStatechartContext>();
    SingleTypeVariantListPtr jointNames = getInput<SingleTypeVariantList>("jointNames");
    SingleTypeVariantListPtr jointAngles = getInput<SingleTypeVariantList>("jointAngles");
    SingleTypeVariantListPtr jointTorques = getInput<SingleTypeVariantList>("jointTorques");

    ChannelRefPtr tempChannelRef = rsContext->getChannelRef(rsContext->getKinematicUnitObserverName(), "jointtorques");
    //IF TACTILE IMPLMENTED
    //ChannelRefPtr tactileChannelRef = context->getChannelRef(context->getHapticUnitObserverName(), "tactileSensors");


    SingleTypeVariantList dataFields(VariantType::DatafieldRef);

    for (size_t i = 0; i < jointNames->getSize(); i++)
    {
        dataFields.addVariant(rsContext->getDatafieldRef(tempChannelRef, jointNames->getVariant(i)->getString()));
    }

    NameValueMap jointNamesAndAngles;
    NameValueMap jointNamesAndTorques;
    NameControlModeMap controlModes;
    SingleTypeVariantList currentJointAngles;
    SingleTypeVariantList currentJointNames;

    if (jointNames->getSize() == jointAngles->getSize())
    {
        for (int j = 0; j < jointNames->getSize(); j++)
        {
            jointNamesAndAngles[jointNames->getVariant(j)->getString()] = jointAngles->getVariant(j)->getFloat();
            jointNamesAndTorques[jointNames->getVariant(j)->getString()] = dataFields.getVariant(j)->getFloat() * 1.1;
            controlModes[jointNames->getVariant(j)->getString()] = eTorqueControl;
            currentJointAngles.addVariant(jointAngles->getVariant(j)->getFloat());
            currentJointNames.addVariant(jointNames->getVariant(j)->getString());
        }
    }
    else
    {
        throw LocalException("stateCheckObjectInHand: List lengths do not match!");
    }


    rsContext->kinematicUnitPrx->switchControlMode(controlModes);
    rsContext->kinematicUnitPrx->setJointTorques(jointNamesAndTorques);

    ARMARX_LOG << eINFO << "Installing timeoutGrasp condition";
    float timeOutCheckObjectInHand = getInput<float>("timeoOutCheckObjectInHand");
    condSqueezeWithJointTorquesTimeout = setTimeoutEvent(timeOutCheckObjectInHand, createEvent<EvCheckObjectInHandTimeout>());
    setLocal("TempJointAngles", currentJointAngles);
    setLocal("TempJointNames", currentJointNames);
    ARMARX_VERBOSE << "Done CheckObjectInHand::onEnter()";
}

void CheckObjectInHand::run()
{
    while (!isRunningTaskStopped()) // stop run function if returning true
    {
        SingleTypeVariantListPtr jointAngles = getLocal<SingleTypeVariantList>("jointNamesAndAngles");
        SingleTypeVariantListPtr jointNames =  getLocal<SingleTypeVariantList>("TempJointNames");
        RobotStatechartContext* rsContext = getContext<RobotStatechartContext>();
        ChannelRefPtr tempChannelRef = rsContext->getChannelRef(rsContext->getKinematicUnitObserverName(), "jointangles");
        SingleTypeVariantList dataFields(VariantType::DatafieldRef);
        float threshhold = 0.0;

        for (size_t i = 0; i < jointNames->getSize(); i++)
        {
            dataFields.addVariant(rsContext->getDatafieldRef(tempChannelRef, jointNames->getVariant(i)->getString()));
        }

        float meanSquaredJointAnglesDiff = 0.0;

        for (int j = 0; j < jointNames->getSize(); j++)
        {
            float jointAngleDiff;// = (jointNamesAndAngles[jointNames->getVariant(j)->getString()]-dataFields.getVariant(j)->getFloat());
            meanSquaredJointAnglesDiff += jointAngleDiff * jointAngleDiff;
        }

        meanSquaredJointAnglesDiff = sqrt(meanSquaredJointAnglesDiff);

        if (meanSquaredJointAnglesDiff < threshhold)
        {
            return;
        }
    }

}

void CheckObjectInHand::onBreak()
{
    // put your user code for the breaking point here
    // execution time should be short (<100ms)
}

void CheckObjectInHand::onExit()
{
    removeTimeoutEvent(condSqueezeWithJointTorquesTimeout);
    // MoveBack

}

// DO NOT EDIT NEXT FUNCTION
std::string CheckObjectInHand::GetName()
{
    return "CheckObjectInHand";
}

// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr CheckObjectInHand::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new CheckObjectInHand(stateData));
}

