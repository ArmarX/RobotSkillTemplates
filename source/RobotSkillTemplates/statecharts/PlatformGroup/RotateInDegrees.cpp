/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::PlatformGroup
 * @author     Pascal Weiner ( pascal dot weiner at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "RotateInDegrees.h"

//#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>

using namespace armarx;
using namespace PlatformGroup;

// DO NOT EDIT NEXT LINE
RotateInDegrees::SubClassRegistry RotateInDegrees::Registry(RotateInDegrees::GetName(), &RotateInDegrees::CreateInstance);

void RotateInDegrees::onEnter()
{
    //calc degree to radian
    float radian = ((float)in.getDegrees()) / 180.0 * M_PI;
    if (in.getDirection() == "right")
    {
        radian = -radian;
    }
    else if (!(in.getDirection() == "left"))
    {
        ARMARX_WARNING << in.getDirection() << " is not a valid direction. Valid directions are: left, right";
        return;
    }
    Vector3 rotation(0.0f, 0.0f, radian);
    local.setRotation(rotation);
}

//void RotateInDegrees::run()
//{
//    // put your user code for the execution-phase here
//    // runs in seperate thread, thus can do complex operations
//    // should check constantly whether isRunningTaskStopped() returns true
//
//// uncomment this if you need a continous run function. Make sure to use sleep or use blocking wait to reduce cpu load.
//    while (!isRunningTaskStopped()) // stop run function if returning true
//    {
//        // do your calculations
//    }
//}

//void RotateInDegrees::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void RotateInDegrees::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr RotateInDegrees::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new RotateInDegrees(stateData));
}

