/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::PlatformGroup
 * @author     Fabian PK ( fabian dot peller-konrad at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "ResolveLandmark.h"
#include "PlatformContext.h"

//#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>

#include <MemoryX/libraries/memorytypes/MemoryXTypesObjectFactories.h>
#include <MemoryX/core/MemoryXCoreObjectFactories.h>

#include <VirtualRobot/MathTools.h>


namespace armarx::PlatformGroup
{
    // DO NOT EDIT NEXT LINE
    ResolveLandmark::SubClassRegistry ResolveLandmark::Registry(ResolveLandmark::GetName(), &ResolveLandmark::CreateInstance);

    void ResolveLandmark::onEnter()
    {
        // put your user code for the enter-point here
        // execution time should be short (<100ms)
        PlatformContext* c = getContext<PlatformContext>();
        auto priorKnowledge = c->priorKnowledgePrx;
        auto graphSegment = priorKnowledge->getGraphSegment();

        const std::string sceneName = in.getSceneName();

        auto resolveLandmark = [this, graphSegment, sceneName](const std::string & landmarkName) -> Vector3Ptr
        {
            if (!graphSegment->hasNodeWithName(sceneName, landmarkName))
            {
                ARMARX_WARNING << "Target landmark '" << landmarkName << "' doesn't exist in graph " << sceneName;
                auto nodes = graphSegment->getNodesByScene(sceneName);
                for (auto& node : nodes)
                {
                    ARMARX_INFO << "node: " << node->getName();
                }
                return nullptr;
            }

            auto node = graphSegment->getNodeFromSceneByName(sceneName, landmarkName);
            ARMARX_CHECK_NOT_NULL(node);
            Eigen::Vector3f rpy;
            ARMARX_CHECK_NOT_NULL(node->getPose());
            PosePtr pose = PosePtr::dynamicCast(node->getPose());
            ARMARX_CHECK_NOT_NULL(pose);
            VirtualRobot::MathTools::eigen4f2rpy(pose->toEigen(), rpy);
            const float goalAlpha = rpy[2];
            ARMARX_CHECK_NOT_NULL(pose->position);
            ARMARX_CHECK_NOT_NULL(pose);
            Vector3Ptr goalPose(new Vector3(pose->position->x, pose->position->y, goalAlpha));
            ARMARX_INFO << "Landmark: '" << landmarkName << "' => position: [" << goalPose->toEigen().transpose() << "]";

            return goalPose;
        };

        if (in.isLandmarkNamesSet())
        {
            const std::vector<std::string> landmarkNames = in.getLandmarkNames();
            std::map<std::string, Vector3Ptr> poseMap;
            std::vector<Vector3Ptr> poseList;
            bool failure = false;

            for (const std::string& landmarkName : landmarkNames)
            {
                Vector3Ptr pose = resolveLandmark(landmarkName);
                if (pose)
                {
                    poseMap[landmarkName] = pose;
                    poseList.push_back(pose);
                }
                else
                {
                    failure = true;
                }
            }
            // Set output in any case so user can still use a failure.
            out.setLandmarkPoseMap(poseMap);
            out.setLandmarkPoseList(poseList);
            if (failure)
            {
                emitFailure();
            }
            else
            {
                emitSuccess();
            }
        }
        else
        {
            const std::string landmark = in.getLandmarkName();

            Vector3Ptr goalPose = resolveLandmark(landmark);
            if (goalPose)
            {
                out.setlandmarkPosition(goalPose);
                emitSuccess();
            }
            else
            {
                emitFailure();
            }
        }
    }

    //void ResolveLandmark::run()
    //{
    //    // put your user code for the execution-phase here
    //    // runs in seperate thread, thus can do complex operations
    //    // should check constantly whether isRunningTaskStopped() returns true
    //
    //    // get a private kinematic instance for this state of the robot (tick "Robot State Component" proxy checkbox in statechart group)
    //    VirtualRobot::RobotPtr robot = getLocalRobot();
    //
    //// uncomment this if you need a continous run function. Make sure to use sleep or use blocking wait to reduce cpu load.
    //    while (!isRunningTaskStopped()) // stop run function if returning true
    //    {
    //        // do your calculations
    //        // synchronize robot clone to most recent state
    //        RemoteRobot::synchronizeLocalClone(robot, getRobotStateComponent());
    //    }
    //}

    //void ResolveLandmark::onBreak()
    //{
    //    // put your user code for the breaking point here
    //    // execution time should be short (<100ms)
    //}

    void ResolveLandmark::onExit()
    {
        // put your user code for the exit point here
        // execution time should be short (<100ms)
    }


    // DO NOT EDIT NEXT FUNCTION
    XMLStateFactoryBasePtr ResolveLandmark::CreateInstance(XMLStateConstructorParams stateData)
    {
        return XMLStateFactoryBasePtr(new ResolveLandmark(stateData));
    }
}
