/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::PlatformGroup
 * @author     Pascal Weiner ( pascal dot weiner at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "MoveInSteps.h"

//#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>

using namespace armarx;
using namespace PlatformGroup;

// DO NOT EDIT NEXT LINE
MoveInSteps::SubClassRegistry MoveInSteps::Registry(MoveInSteps::GetName(), &MoveInSteps::CreateInstance);

static std::map<std::string, Vector3> StringToDirection =
{
    {"forward", Vector3(0.0, 1.0, 0.0)},
    {"left", Vector3(-1.0, 0.0, 0)},
    {"right", Vector3(1.0, 0.0, 0)},
    {"back", Vector3(0.0, -1.0, 0)}
};

/*Vector3 MoveInSteps::elementwiseMultiply(Vector3 v, Vector3 w) {
    return Vector3(v.x*w.x, v.y*w.y, v.z*w.z);
}*/

void MoveInSteps::onEnter()
{
    auto dirIter = StringToDirection.find(in.getDirection());
    if (dirIter != StringToDirection.end())
    {
        Vector3 dir = dirIter->second;
        local.setRelativePose(Eigen::Vector3f(dir.toEigen() * in.getMillimeterPerStep()*in.getStepCount()));
    }
    else
    {
        ARMARX_WARNING << in.getDirection() << " is not a valid direction. Valid directions are: foreward, left, right, back";
    }
}

//void MoveInSteps::run()
//{
//    // put your user code for the execution-phase here
//    // runs in seperate thread, thus can do complex operations
//    // should check constantly whether isRunningTaskStopped() returns true
//
//// uncomment this if you need a continous run function. Make sure to use sleep or use blocking wait to reduce cpu load.
//    while (!isRunningTaskStopped()) // stop run function if returning true
//    {
//        // do your calculations
//    }
//}

//void MoveInSteps::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void MoveInSteps::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr MoveInSteps::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new MoveInSteps(stateData));
}

