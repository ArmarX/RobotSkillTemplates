/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2014-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::MovePlatformToLandmarkGroup
 * @author     Valerij Wittenbeck ( valerij dot wittenbeck at student dot kit dot edu )
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <ArmarXCore/statechart/xmlstates/XMLState.h>
#include <RobotSkillTemplates/statecharts/PlatformGroup/PlatformContext.h>
#include "CalcPath.generated.h"

namespace armarx::PlatformGroup
{
    DEFINEEVENT(EvPathFound)
    DEFINEEVENT(EvNoPathFound)

    class CalcPath :
        public CalcPathGeneratedBase<CalcPath>
    {
    public:
        CalcPath(XMLStateConstructorParams stateData);

        // inherited from StateBase
        void onEnter() override;
        void onExit() override;

        float getPathLength(::memoryx::GraphNodeBaseList& path);

        // static functions for AbstractFactory Method
        static std::string GetName();
        static XMLStateFactoryBasePtr CreateInstance(XMLStateConstructorParams stateData);
        static SubClassRegistry Registry;

        // DO NOT INSERT ANY CLASS MEMBERS,
        // use stateparameters instead,
        // if classmember are neccessary nonetheless, reset them in onEnter
    };
}


