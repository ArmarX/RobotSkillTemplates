/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2014-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::PlatformGroup
 * @author     Mirko Waechter ( mirko dot waechter at kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "MovePlatformRelative.h"
#include "PlatformContext.h"

#include <ArmarXCore/observers/variant/DatafieldRef.h>
#include <RobotAPI/libraries/core/math/MathUtils.h>

using namespace armarx;
using namespace PlatformGroup;

// DO NOT EDIT NEXT LINE
MovePlatformRelative::SubClassRegistry MovePlatformRelative::Registry(MovePlatformRelative::GetName(), &MovePlatformRelative::CreateInstance);



MovePlatformRelative::MovePlatformRelative(XMLStateConstructorParams stateData) :
    XMLStateTemplate<MovePlatformRelative>(stateData),  MovePlatformRelativeGeneratedBase<MovePlatformRelative>(stateData)
{
}

void MovePlatformRelative::onEnter()
{
    if (!in.getUsePlatformNativeController())
    {
        // put your user code for the enter-point here
        // execution time should be short (<100ms)
        PlatformContext* c = getContext<PlatformContext>();
        auto pose = in.getRelativePose();

        DatafieldRefPtr posXRef = new DatafieldRef(c->platformUnitObserverPrx, "platformPose", "positionX");
        DatafieldRefPtr posYRef = new DatafieldRef(c->platformUnitObserverPrx, "platformPose", "positionY");
        DatafieldRefPtr oriRef = new DatafieldRef(c->platformUnitObserverPrx, "platformPose", "rotation");
        float posX = posXRef->getDataField()->getFloat();
        float posY = posYRef->getDataField()->getFloat();
        float ori = oriRef->getDataField()->getFloat();

        Eigen::Vector3f localPos {pose->x, pose->y, 0};
        Eigen::Matrix3f m;
        m = Eigen::AngleAxisf(ori, Eigen::Vector3f::UnitZ());
        Eigen::Vector3f globalPos = m * localPos;

        if (pose->z > M_PI)
        {
            pose->z -= M_PI * 2.0;
        }

        if (pose->z < -M_PI)
        {
            pose->z += M_PI * 2.0;
        }

        float targetX = posX + globalPos[0];
        float targetY = posY + globalPos[1];
        float targetOri = ori + pose->z;

        while (targetOri < 0)
        {
            targetOri += M_PI * 2.0;
        }

        while (targetOri > M_PI * 2.0)
        {
            targetOri -= M_PI * 2.0;
        }

        ARMARX_INFO << "Relative target pose: " << pose->toEigen().transpose();
        ARMARX_INFO << "Global target pose: " << targetX << " " << targetY << " " << targetOri;
        local.setAbsoluteTargetPosition(new Vector3(targetX, targetY, targetOri));
    }
    else
    {
        cancelSubstates();
        PlatformContext* context = getContext<PlatformContext>();
        std::string platformDatafieldName = in.getPlatformPoseDatafieldName();
        DataFieldIdentifierPtr posXId = new DataFieldIdentifier(context->platformUnitObserverName + "." + platformDatafieldName + ".positionX");
        DataFieldIdentifierPtr posYId = new DataFieldIdentifier(context->platformUnitObserverName + "." + platformDatafieldName + ".positionY");
        DataFieldIdentifierPtr rotId = new DataFieldIdentifier(context->platformUnitObserverName + "." + platformDatafieldName + ".rotation");
        auto datafields = context->platformUnitObserverPrx->getDataFields({posXId, posYId, rotId});
        auto relativeTargetPose = in.getRelativePose();

        Vector3Ptr target = new Vector3(datafields.at(0)->getFloat() + relativeTargetPose->x,
                                        datafields.at(1)->getFloat() + relativeTargetPose->y,
                                        math::MathUtils::angleMod2PI(datafields.at(2)->getFloat() + relativeTargetPose->z));



        Literal checkX(*posXId, checks::approx, {target->x, in.getPositionalAccuracy()});
        Literal checkY(*posYId, checks::approx, {target->y, in.getPositionalAccuracy()});

        Literal checkAngle(*rotId, checks::approx, {target->z, in.getOrientationalAccuracy()});
        // for special case of orientations around 2*PI
        float checkAngleValueOffset;

        if (target->z > M_PI)
        {
            checkAngleValueOffset = - 2 * M_PI;
        }
        else
        {
            checkAngleValueOffset =  2 * M_PI;
        }

        Literal checkAngle2(rotId, checks::approx, { (float)(target->z + checkAngleValueOffset), in.getOrientationalAccuracy()});

        installConditionForTargetReached(checkX && checkY && (checkAngle || checkAngle2));
        setTimeoutEvent(in.getTimeout(), this->createEventTimeout());
        //            usleep(20000);

    }
}

void MovePlatformRelative::run()
{
    std::string platformDatafieldName = in.getPlatformPoseDatafieldName();
    if (in.getUsePlatformNativeController())
    {
        PlatformContext* context = getContext<PlatformContext>();
        DataFieldIdentifierPtr posXId = new DataFieldIdentifier(context->platformUnitObserverName + "." + platformDatafieldName + ".positionX");
        DataFieldIdentifierPtr posYId = new DataFieldIdentifier(context->platformUnitObserverName + "." + platformDatafieldName + ".positionY");
        DataFieldIdentifierPtr rotId = new DataFieldIdentifier(context->platformUnitObserverName + "." + platformDatafieldName + ".rotation");
        auto datafields = context->platformUnitObserverPrx->getDataFields({posXId, posYId, rotId});
        auto relativeTargetPose = in.getRelativePose();
        Vector3Ptr target = new Vector3(datafields.at(0)->getFloat() + relativeTargetPose->x,
                                        datafields.at(1)->getFloat() + relativeTargetPose->y,
                                        math::MathUtils::angleMod2PI(datafields.at(2)->getFloat() + relativeTargetPose->z));
        context->platformUnitPrx->moveRelative(relativeTargetPose->x, relativeTargetPose->y, relativeTargetPose->z, in.getPositionalAccuracy(), in.getOrientationalAccuracy());
        auto getError = [&]()
        {
            auto datafields = context->platformUnitObserverPrx->getDataFields({posXId, posYId, rotId});

            Eigen::Vector2f currentPos(datafields.at(0)->getFloat(),
                                       datafields.at(1)->getFloat());
            float yaw = datafields.at(2)->getFloat();
            Eigen::Vector2f posError = currentPos - target->toEigen().head(2);
            float yawError = yaw - target->z;
            return Vector3Ptr(new Vector3(posError(0), posError(1), yawError));
        };
        while (!isRunningTaskStopped())
        {
            auto error = getError();
            ARMARX_INFO << deactivateSpam(1) << VAROUT(*error);

            usleep(20000);
        }
        auto error = getError();
        ARMARX_INFO << "remaining error: " << *error;
        out.setRemainingError(error);
    }
}



void MovePlatformRelative::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
    waitForRunningTaskToFinish();

}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr MovePlatformRelative::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new MovePlatformRelative(stateData));
}

