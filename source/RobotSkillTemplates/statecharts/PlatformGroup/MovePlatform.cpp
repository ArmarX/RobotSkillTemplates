/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2014-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::MovePlatformToLandmarkGroup
 * @author     Valerij Wittenbeck ( valerij dot wittenbeck at student dot kit dot edu )
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "MovePlatform.h"
#include "PlatformContext.h"
#include <ArmarXCore/statechart/StatechartContext.h>

#include <RobotAPI/components/ArViz/Client/Client.h>
#include <RobotAPI/components/ArViz/Client/elements/Path.h>

#include "PlatformGroupStatechartContext.generated.h"

using namespace armarx;
using namespace PlatformGroup;

// DO NOT EDIT NEXT LINE
MovePlatform::SubClassRegistry MovePlatform::Registry(MovePlatform::GetName(), &MovePlatform::CreateInstance);



MovePlatform::MovePlatform(XMLStateConstructorParams stateData) :
    XMLStateTemplate<MovePlatform>(stateData),
    MovePlatformGeneratedBase<MovePlatform> (stateData)
{



}

void MovePlatform::onEnter()
{
    PlatformContext* c = getContext<PlatformContext>();
    ChannelRefPtr counter = ChannelRefPtr::dynamicCast(c->systemObserverPrx->startCounter(0, "PlatformPositionCounter"));
    local.setpositionCounter(counter);

    ARMARX_INFO << "getinput";
    SingleTypeVariantListPtr points = getInput<SingleTypeVariantList>("targetPositions");

    std::vector<Eigen::Vector3f> path;

    ARMARX_INFO << "access points";

    path.reserve(points->getSize());

    ARMARX_INFO << "Conversion";

    for (int i = 0; i < points->getSize(); i++)
    {
        Vector3Ptr point = points->getVariant(i)->get<Vector3>();
        if (point)
        {
            path.emplace_back(Eigen::Vector3f{point->x, point->y, point->z});
        }
    }

    ARMARX_INFO << "Arviz";

    ARMARX_INFO << "arviz connecting...";



    ARMARX_INFO << __LINE__;

    ARMARX_INFO << __LINE__;

    viz::Client arviz(*c);



    ARMARX_INFO << __LINE__;

    //    const auto& arviz = getArvizTopic();
    ARMARX_INFO << "arviz connecting2...";

    //    auto client = viz::Client::createFromTopic("MovePlatform", getArvizTopic());

    arvizClient.reset(new viz::ScopedClient(arviz));

    ARMARX_INFO << "arviz connected.";

    auto layer = arvizClient->layer("path");
    layer.add(viz::Path("path").points(path));
    arvizClient->commit({layer});
}

void MovePlatform::onExit()
{
    PlatformContext* c = getContext<PlatformContext>();


    ChannelRefPtr r = local.getpositionCounter();
    c->systemObserverPrx->removeCounter(r);
    PlatformContext* context = getContext<PlatformContext>();
    context->platformUnitPrx->move(0, 0, 0);
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr MovePlatform::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new MovePlatform(stateData));
}

