/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2014-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::PlatformGroup
 * @author     Mirko Waechter ( waechter at kit dot edu )
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "newstate.h"

using namespace armarx;
using namespace PlatformGroup;

// DO NOT EDIT NEXT LINE
newstate::SubClassRegistry newstate::Registry(newstate::GetName(), &newstate::CreateInstance);



newstate::newstate(XMLStateConstructorParams stateData) :
    XMLStateTemplate < newstate > (stateData)
{
}

void newstate::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
}

void newstate::run()
{
    // put your user code for the execution-phase here
    // runs in seperate thread, thus can do complex operations
    // should check constantly whether isRunningTaskStopped() returns true


    while (!isRunningTaskStopped()) // stop run function if returning true
    {
        // do your calculations
    }

}

void newstate::onBreak()
{
    // put your user code for the breaking point here
    // execution time should be short (<100ms)
}

void newstate::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)

}

// DO NOT EDIT NEXT FUNCTION
std::string newstate::GetName()
{
    return "newstate";
}

// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr newstate::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new newstate(stateData));
}

