/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::PlatformGroup
 * @author     Stefan Reither ( stef dot reither at web dot de )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "TransformTrajectory.h"

using namespace armarx;
using namespace PlatformGroup;

// DO NOT EDIT NEXT LINE
TransformTrajectory::SubClassRegistry TransformTrajectory::Registry(TransformTrajectory::GetName(), &TransformTrajectory::CreateInstance);



void TransformTrajectory::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
}

void TransformTrajectory::run()
{
    // put your user code for the execution-phase here
    // runs in seperate thread, thus can do complex operations
    // should check constantly whether isRunningTaskStopped() returns true


    std::vector<Vector3Ptr> platformPointList;
    TrajectoryPtr platformTraj = TrajectoryPtr ::dynamicCast(in.getTrajectory());
    for (const Trajectory::TrajData& point : *platformTraj)
    {
        platformPointList.push_back(new Vector3(point.getPosition(0),
                                                point.getPosition(1),
                                                point.getPosition(2)));

        ARMARX_INFO << point.getPosition(0) << " " << point.getPosition(1) << " " << point.getPosition(2);
    }

    out.settargetPoints(platformPointList);
    emitSuccess();


}

//void TransformTrajectory::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void TransformTrajectory::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr TransformTrajectory::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new TransformTrajectory(stateData));
}

