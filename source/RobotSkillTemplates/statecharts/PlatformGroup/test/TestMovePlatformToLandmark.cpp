/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2014-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::MovePlatformToLandmarkGroup
 * @author     Mirko Waechter ( waechter at kit dot edu )
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "TestMovePlatformToLandmark.h"
#include "../PlatformContext.h"

#include <ArmarXCore/observers/filters/GaussianFilter.h>
#include <ArmarXCore/observers/filters/MedianFilter.h>

#include <MemoryX/libraries/memorytypes/variants/GraphNode/GraphNode.h>

using namespace armarx;
using namespace PlatformGroup;

// DO NOT EDIT NEXT LINE
TestMovePlatformToLandmark::SubClassRegistry TestMovePlatformToLandmark::Registry(TestMovePlatformToLandmark::GetName(), &TestMovePlatformToLandmark::CreateInstance);



TestMovePlatformToLandmark::TestMovePlatformToLandmark(XMLStateConstructorParams stateData) :
    XMLStateTemplate < TestMovePlatformToLandmark > (stateData)
{
}

void TestMovePlatformToLandmark::onEnter()
{
    //    PlatformContext* c = getContext<PlatformContext>();
    //    DatafieldRefPtr ref = new DatafieldRef(c->ftObs, "Hand L_pod", "forces_z");
    //    DatafieldRefPtr newField = DatafieldRefPtr::dynamicCast(c->ftObs->createNulledDatafield(ref));
    //    ARMARX_INFO << "new Datafield: " << newField->getDataField();
    // put your user code for the enter-point here
    // execution time should be short (<100ms)

    PlatformContext* context = getContext<PlatformContext>();
    auto graphSegment = context->priorKnowledgePrx->getGraphSegment();


    std::string sceneName {"TestMovePlatformToLandmarkTestGraphSceneName"};
    setLocal("SceneName", sceneName);

    //reset scene
    graphSegment->clearScene(sceneName);

    //generate nodes
    ::memoryx::GraphNodePtr nodeStart                   {new ::memoryx::GraphNode{   0.f,    0.f,  0.f, "Start", sceneName}};
    ::memoryx::GraphNodePtr node1                       {new ::memoryx::GraphNode{4200.f,    0.f,  0.f, "1", sceneName}};
    ::memoryx::GraphNodePtr node2                       {new ::memoryx::GraphNode{4200.f, 4000.f,  0.f, "2", sceneName}};
    ::memoryx::GraphNodePtr nodeHandover                {new ::memoryx::GraphNode{4200.f, 4000.f,  3.f, "Handover", sceneName}};
    ::memoryx::GraphNodePtr node3                       {new ::memoryx::GraphNode{3100.f,  600.f,  0.f, "3", sceneName}};
    ::memoryx::GraphNodePtr node4                       {new ::memoryx::GraphNode{3300.f, 8300.f,  0.f, "4", sceneName}};
    ::memoryx::GraphNodePtr nodeOven                    {new ::memoryx::GraphNode{2600.f, 7600.f,  0.f, "Oven", sceneName}};
    ::memoryx::GraphNodePtr nodeTableWithRolls          {new ::memoryx::GraphNode{3300.f, 7150.f, -1.f, "TableWithRolls", sceneName}};
    ::memoryx::GraphNodePtr nodeSink                    {new ::memoryx::GraphNode{2600.f, 9600.f,  0.f, "Sink", sceneName}};
    ::memoryx::GraphNodePtr nodeCupboard7Door           {new ::memoryx::GraphNode{4500.f, 9400.f,  0.f, "Cupboard7Door", sceneName}};
    ::memoryx::GraphNodePtr nodeSinkTable               {new ::memoryx::GraphNode{3200.f, 9700.f,  0.f, "SinkTable", sceneName}};
    ::memoryx::GraphNodePtr nodeCloseToTableWithRolls   {new ::memoryx::GraphNode{3500.f, 7150.f, -1.f, "CloseToTableWithRolls", sceneName}};
    ::memoryx::GraphNodePtr nodeHandoverTable           {new ::memoryx::GraphNode{3000.f, 5550.f,  2.f, "HandoverTable", sceneName}};
    //add nodes
    auto  nodeStartId                 = graphSegment->addNode(nodeStart);
    auto  node1Id                     = graphSegment->addNode(node1);
    auto  node2Id                     = graphSegment->addNode(node2);
    auto  nodeHandoverId              = graphSegment->addNode(nodeHandover);
    auto  node3Id                     = graphSegment->addNode(node3);
    auto  node4Id                     = graphSegment->addNode(node4);
    auto  nodeOvenId                  = graphSegment->addNode(nodeOven);
    auto  nodeTableWithRollsId        = graphSegment->addNode(nodeTableWithRolls);
    auto  nodeSinkId                  = graphSegment->addNode(nodeSink);
    auto  nodeCupboard7DoorId         = graphSegment->addNode(nodeCupboard7Door);
    auto  nodeSinkTableId             = graphSegment->addNode(nodeSinkTable);
    auto  nodeCloseToTableWithRollsId = graphSegment->addNode(nodeCloseToTableWithRolls);
    auto  nodeHandoverTableId         = graphSegment->addNode(nodeHandoverTable);
    //add edges (should be undirected graph => add bidirectional edges)
    graphSegment->addEdge(nodeStartId, node1Id);
    graphSegment->addEdge(node1Id, nodeStartId);
    graphSegment->addEdge(node1Id, node2Id);
    graphSegment->addEdge(node2Id, node1Id);
    graphSegment->addEdge(node2Id, nodeHandoverId);
    graphSegment->addEdge(nodeHandoverId, node2Id);
    graphSegment->addEdge(node2Id, node3Id);
    graphSegment->addEdge(node3Id, node2Id);
    graphSegment->addEdge(node3Id, node4Id);
    graphSegment->addEdge(node4Id, node3Id);
    graphSegment->addEdge(node3Id, nodeOvenId);
    graphSegment->addEdge(nodeOvenId, node3Id);
    graphSegment->addEdge(node3Id, nodeTableWithRollsId);
    graphSegment->addEdge(nodeTableWithRollsId, node3Id);
    graphSegment->addEdge(node4Id, nodeOvenId);
    graphSegment->addEdge(nodeOvenId, node4Id);
    graphSegment->addEdge(node4Id, nodeTableWithRollsId);
    graphSegment->addEdge(nodeTableWithRollsId, node4Id);
    graphSegment->addEdge(node4Id, nodeSinkId);
    graphSegment->addEdge(nodeSinkId, node4Id);
    graphSegment->addEdge(node4Id, nodeCupboard7DoorId);
    graphSegment->addEdge(nodeCupboard7DoorId, node4Id);
    graphSegment->addEdge(nodeTableWithRollsId, nodeSinkTableId);
    graphSegment->addEdge(nodeSinkTableId, nodeTableWithRollsId);
    graphSegment->addEdge(nodeCloseToTableWithRollsId, nodeTableWithRollsId);
    graphSegment->addEdge(nodeTableWithRollsId, nodeCloseToTableWithRollsId);
    graphSegment->addEdge(nodeTableWithRollsId, nodeHandoverTableId);
    graphSegment->addEdge(nodeHandoverTableId, nodeTableWithRollsId);
    graphSegment->addEdge(nodeHandoverId, nodeHandoverTableId);
    graphSegment->addEdge(nodeHandoverTableId, nodeHandoverId);

}

void TestMovePlatformToLandmark::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)

}

// DO NOT EDIT NEXT FUNCTION
std::string TestMovePlatformToLandmark::GetName()
{
    return "TestMovePlatformToLandmark";
}

// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr TestMovePlatformToLandmark::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new TestMovePlatformToLandmark(stateData));
}

