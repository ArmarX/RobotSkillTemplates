/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    RobotSkillTemplates::MovePlatform
* @author     Valerij Wittenbeck
* @date       2014
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/


#pragma once

#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/system/ImportExportComponent.h>
#include <ArmarXCore/statechart/StatechartContext.h>
#include <RobotAPI/libraries/core/remoterobot/RemoteRobot.h>
#include <RobotAPI/interface/units/PlatformUnitInterface.h>
#include <RobotAPI/interface/observers/PlatformUnitObserverInterface.h>
#include <RobotAPI/interface/visualization/DebugDrawerInterface.h>
#include <RobotAPI/libraries/core/RobotAPIObjectFactories.h>
#include <MemoryX/components/PriorKnowledge/PriorKnowledge.h>
#include <MemoryX/components/GraphNodePoseResolver/GraphNodePoseResolver.h>

#include <RobotAPI/components/units/PlatformUnitObserver.h>
//#include <VirtualRobot/VirtualRobot.h>
#include <IceUtil/Time.h>
#include <RobotAPI/interface/ArViz/Component.h>

namespace armarx
{

    // ****************************************************************
    // Component and context
    // ****************************************************************

    struct PlatformContextProperties : StatechartContextPropertyDefinitions
    {
        PlatformContextProperties(std::string prefix):
            StatechartContextPropertyDefinitions(prefix)
        {
            defineOptionalProperty<std::string>("PlatformUnitName", "PlatformUnit", "Name of the PlatformUnit to use");
            defineOptionalProperty<std::string>("PlatformUnitObserverName", "PlatformUnitObserver", "Name of the PlatformUnitObserver to use");
            defineOptionalProperty<std::string>("DebugDrawerTopic", "DebugDrawerUpdates", "Name of the DebugDrawerTopic to use");
            defineOptionalProperty<std::string>("PriorKnowledgeName", "PriorKnowledge", "Name of the PriorKnowledge memory to use");
            defineOptionalProperty<std::string>("GraphNodePoseResolverName", "GraphNodePoseResolver", "Name of the GraphNodePoseResolver to use");
            defineOptionalProperty<std::string>("ArVizTopicName", "ArVizTopic", "Name of the ar viz topic that should be used");

        }
    };
    class ARMARXCOMPONENT_IMPORT_EXPORT PlatformContext :
        virtual public XMLStatechartContext
    {
    public:
        // inherited from Component
        std::string getDefaultName() const override
        {
            return "PlatformContext";
        }
        void onInitStatechartContext() override
        {
            ARMARX_INFO << "Init PlatformContext" << flush;

            platformUnitObserverName = getProperty<std::string>("PlatformUnitObserverName").getValue();
            platformUnitDynamicSimulationName = getProperty<std::string>("PlatformUnitName").getValue();
            priorKnowledgeName = getProperty<std::string>("PriorKnowledgeName").getValue();

            usingProxy(platformUnitDynamicSimulationName);
            usingProxy(platformUnitObserverName);
            offeringTopic(getProperty<std::string>("DebugDrawerTopic").getValue());
            offeringTopic("DebugObserver");
            usingProxy(priorKnowledgeName);
            offeringTopic(getProperty<std::string>("ArVizTopicName").getValue());
            usingProxy(getProperty<std::string>("GraphNodePoseResolverName").getValue());
        }

        void onConnectStatechartContext() override
        {
            ARMARX_INFO << "Starting PlatformContext" << flush;

            // retrieve proxies
            debugObserverPrx = getTopic<DebugObserverInterfacePrx>("DebugObserver");
            platformUnitPrx = getProxy<PlatformUnitInterfacePrx>(platformUnitDynamicSimulationName);
            platformUnitObserverPrx = getProxy<PlatformUnitObserverInterfacePrx>(platformUnitObserverName);
            debugDrawer = getTopic<DebugDrawerInterfacePrx>(getProperty<std::string>("DebugDrawerTopic").getValue());
            priorKnowledgePrx = getProxy<memoryx::PriorKnowledgeInterfacePrx>(priorKnowledgeName);
            graphNodePoseResolverPrx = getProxy<memoryx::GraphNodePoseResolverInterfacePrx>(getProperty<std::string>("GraphNodePoseResolverName").getValue());
            arvizTopic = getTopic<armarx::viz::TopicPrx>(getProperty<std::string>("ArVizTopicName").getValue());
        }


        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        PropertyDefinitionsPtr createPropertyDefinitions() override
        {
            return PropertyDefinitionsPtr(new PlatformContextProperties(
                                              getConfigIdentifier()));
        }
        std::string getPlatformUnitObserverName()
        {
            return platformUnitObserverName;
        }

        PlatformUnitInterfacePrx platformUnitPrx;
        PlatformUnitObserverInterfacePrx platformUnitObserverPrx;
        DebugDrawerInterfacePrx debugDrawer;
        memoryx::GraphNodePoseResolverInterfacePrx graphNodePoseResolverPrx;
        std::string platformUnitObserverName;
        std::string platformUnitDynamicSimulationName;
        DebugObserverInterfacePrx debugObserverPrx;
        armarx::viz::TopicPrx arvizTopic;

        std::string priorKnowledgeName;
        memoryx::PriorKnowledgeInterfacePrx priorKnowledgePrx;

    private:

    };
}

