/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::PlatformGroup
 * @author     Markus Grotz ( markus dot grotz at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "MovePlatformVelocity.h"


#include "PlatformContext.h"


using namespace armarx;
using namespace PlatformGroup;

// DO NOT EDIT NEXT LINE
MovePlatformVelocity::SubClassRegistry MovePlatformVelocity::Registry(MovePlatformVelocity::GetName(), &MovePlatformVelocity::CreateInstance);



void MovePlatformVelocity::onEnter()
{
    PlatformUnitInterfacePrx platform = getContext<PlatformContext>()->platformUnitPrx;
    platform->move(in.getTargetPlatformVelocityX(), in.getTargetPlatformVelocityY(), in.getTargetPlatformVelocityRotation());
    emitSuccess();
}

//void MovePlatformVelocity::run()
//{
//    // put your user code for the execution-phase here
//    // runs in seperate thread, thus can do complex operations
//    // should check constantly whether isRunningTaskStopped() returns true
//
//// uncomment this if you need a continous run function. Make sure to use sleep or use blocking wait to reduce cpu load.
//    while (!isRunningTaskStopped()) // stop run function if returning true
//    {
//        // do your calculations
//    }
//}

//void MovePlatformVelocity::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void MovePlatformVelocity::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr MovePlatformVelocity::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new MovePlatformVelocity(stateData));
}

