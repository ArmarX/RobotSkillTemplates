/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotSkillTemplates::PlayMMMFile
 * @author     Philipp Schmidt ( ufedv at student dot kit dot edu )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "StartPlayback.h"

using namespace armarx;
using namespace PlayMMMFile;

// DO NOT EDIT NEXT LINE
StartPlayback::SubClassRegistry StartPlayback::Registry(StartPlayback::GetName(), &StartPlayback::CreateInstance);



void StartPlayback::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
}

void StartPlayback::run()
{
    // put your user code for the execution-phase here
    // runs in seperate thread, thus can do complex operations
    // should check constantly whether isRunningTaskStopped() returns true

    //Go to initial pose
    this->getTrajectoryPlayer()->resetTrajectoryPlayer(false);
    this->getTrajectoryPlayer()->setIsVelocityControl(true);

    //Start
    if (!this->getTrajectoryPlayer()->startTrajectoryPlayer())
    {
        this->emitFailure();
    }

    // uncomment this if you need a continous run function. Make sure to use sleep or use blocking wait to reduce cpu load.
    while (!isRunningTaskStopped()) // stop run function if returning true
    {
        if (this->getTrajectoryPlayer()->getCurrentTime() >= this->getTrajectoryPlayer()->getEndTime())
        {
            ARMARX_LOG << "emitSuccess()";
            this->emitSuccess();
            return;
        }
        //Wait 50ms
        usleep(50000);
    }
}

//void StartPlayback::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void StartPlayback::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr StartPlayback::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new StartPlayback(stateData));
}

