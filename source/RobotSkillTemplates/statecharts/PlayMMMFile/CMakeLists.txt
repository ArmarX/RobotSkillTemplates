armarx_component_set_name("PlayMMMFile")

set(COMPONENT_LIBS
    ArmarXCoreInterfaces ArmarXCore ArmarXCoreStatechart ArmarXCoreObservers RobotAPICore RobotComponentsInterfaces RobotStatechartHelpers)

# Sources

set(SOURCES
PlayMMMFileRemoteStateOfferer.cpp
./PlayMMMFileState.cpp
./StartPlayback.cpp
#@TEMPLATE_LINE@@COMPONENT_PATH@/@COMPONENT_NAME@.cpp
)

set(HEADERS
PlayMMMFileRemoteStateOfferer.h
PlayMMMFile.scgxml
./PlayMMMFileState.h
./StartPlayback.h
#@TEMPLATE_LINE@@COMPONENT_PATH@/@COMPONENT_NAME@.h
./PlayMMMFileState.xml
./StartPlayback.xml
#@TEMPLATE_LINE@@COMPONENT_PATH@/@COMPONENT_NAME@.xml
)

armarx_add_component("${SOURCES}" "${HEADERS}")
