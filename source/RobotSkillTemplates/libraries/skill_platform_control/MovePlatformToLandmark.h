/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include "MovePlatformToPose.h"

// Base Class
#include "PlatformControlSkill.h"

#include <RobotAPI/libraries/armem/client.h>

#include <RobotSkillTemplates/libraries/skill_platform_control/aron/MovePlatformToLandmarkAcceptedType.aron.generated.h>

namespace armarx::skills
{
    class MovePlatformToLandmark :
            public PlatformControlSkill,
            public SpecializedSkill<platform_control::arondto::MovePlatformToLandmarkAcceptedType>
    {
    public:
        using ArgType = platform_control::arondto::MovePlatformToLandmarkAcceptedType;

        MovePlatformToLandmark(armem::client::MemoryNameSystem& mns, armarx::viz::Client& arviz, PlatformControlSkillContext& context);

    private:
        MainResult main(const SpecializedMainInput& in) final;

        Eigen::Matrix4f convertLandmarkToPose(const std::string& graphName, const std::string& landmarkName, const armem::client::Reader& memoryReader)
        {
            auto latest = memoryReader.getLatestSnapshotIn(armem::MemoryID("Navigation", "Location", graphName, landmarkName));

            ARMARX_CHECK_EQUAL(latest->size(), 1);
            auto data = latest->getInstance(0).data();
            auto eigen = aron::converter::AronEigenConverter::ConvertToMatrix4f(aron::data::NDArray::DynamicCastAndCheck(data->getElement("globalRobotPose")));

            ARMARX_INFO << "Converted pose for landmark '" << graphName << "/" << landmarkName << "is: " << eigen;

            return eigen;
        }

    public:
        static SkillDescription Description;
    };
}
