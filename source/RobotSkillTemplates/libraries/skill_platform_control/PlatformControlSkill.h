/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

// STD/STL
#include <string>

// ArmarX
#include <ArmarXCore/core/Component.h>

// RobotAPI
#include <RobotAPI/libraries/skills/provider/PeriodicSpecializedSkill.h>
#include <RobotAPI/libraries/skills/provider/SpecializedSkill.h>
#include <RobotAPI/libraries/skills/provider/SkillContext.h>
#include <RobotAPI/libraries/aron/converter/eigen/EigenConverter.h>

// Proxies
#include <RobotAPI/interface/observers/PlatformUnitObserverInterface.h>

// Skill Mixins
#include <RobotAPI/libraries/skills/provider/mixins/All.h>


namespace armarx::skills
{
    struct PlatformControlSkillContext : public SkillContext
    {
        // Properties
        PlatformUnitInterfacePrx platformUnitPrx;

        void defineProperties(const armarx::PropertyDefinitionsPtr& defs, const std::string& prefix) final
        {
            defs->component(platformUnitPrx, "platformUnitName", prefix + "PlatformUnitInterfaceName");
        }
    };

    class PlatformControlSkill :
            public skills::mixin::ArvizSkillMixin,
            public skills::mixin::MNSSkillMixin
    {
    public:
        PlatformControlSkill(armem::client::MemoryNameSystem& mns, armarx::viz::Client& arviz, const std::string& layerName, PlatformControlSkillContext& c) :
            mixin::ArvizSkillMixin({arviz, layerName}),
            mixin::MNSSkillMixin({mns}),
            context(c)
        {
        };

    protected:
        TerminatedSkillStatus stopPlatformMovement()
        {
            context.platformUnitPrx->move(0, 0, 0);
            return TerminatedSkillStatus::Succeeded;
        }

    protected:
        PlatformControlSkillContext& context;
    };
}
