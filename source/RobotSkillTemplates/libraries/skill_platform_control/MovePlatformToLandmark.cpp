#include "MovePlatformToLandmark.h"

#include "util/PlatformUtil.h"

#include <RobotAPI/libraries/core/FramedPose.h>

#include <RobotAPI/libraries/skills/provider/SkillProxy.h>

namespace armarx::skills
{

    namespace
    {
        platform_control::arondto::MovePlatformToLandmarkAcceptedType GetDefaultParameterization()
        {
            platform_control::arondto::MovePlatformToLandmarkAcceptedType ret;
            ret.orientationalAccuracy = 0.1;
            ret.positionalAccuracy = 100;
            return ret;
        }
    }

    SkillDescription MovePlatformToLandmark::Description = skills::SkillDescription{
        "MovePlatformToLandmark", "Move a platform unit to target landmark. Uses a navigation memory to resolve the landmark.",
        {}, armarx::Duration::MilliSeconds(100) + MovePlatformToPose::Description.timeout,
        platform_control::arondto::MovePlatformToLandmarkAcceptedType::ToAronType(),
        GetDefaultParameterization().toAron()
    };

    MovePlatformToLandmark::MovePlatformToLandmark(armem::client::MemoryNameSystem& mns, armarx::viz::Client& arviz, PlatformControlSkillContext& context) :
        PlatformControlSkill(mns, arviz, Description.skillName, context),
        SpecializedSkill<ArgType>(Description)
    {
    }

    Skill::MainResult MovePlatformToLandmark::main(const SpecializedMainInput& in)
    {
        armem::client::Reader navigationReader = mns.getReader(armem::MemoryID("Navigation"));

        // convert landmark
        auto conversion = convertLandmarkToPose(in.params.graph, in.params.landmark, navigationReader);

        // create params to reuse skill
        platform_control::arondto::MovePlatformToPoseAcceptedType args;
        args.robotName = in.params.robotName;
        args.orientationalAccuracy = in.params.orientationalAccuracy;
        args.positionalAccuracy = in.params.positionalAccuracy;
        args.pose = conversion;

        SkillProxy prx({manager, providerName, MovePlatformToPose::Description});
        return {prx.executeFullSkill(getSkillId().toString(in.executorName), args.toAron()).status, nullptr};
    }
}
