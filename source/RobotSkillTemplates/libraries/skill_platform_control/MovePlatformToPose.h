/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

// ArmarX
#include <ArmarXCore/core/Component.h>

// Base Class
#include "PlatformControlSkill.h"

// Type
#include <RobotSkillTemplates/libraries/skill_platform_control/aron/MovePlatformToPoseAcceptedType.aron.generated.h>

// Proxies
#include <RobotAPI/interface/units/PlatformUnitInterface.h>

// Others
#include <RobotAPI/libraries/core/FramedPose.h>
#include <RobotAPI/libraries/armem_robot_state/client/common/VirtualRobotReader.h>
#include <RobotAPI/libraries/armem_objects/client/instance/ObjectReader.h>
#include <RobotAPI/libraries/armem_grasping/client/KnownGraspCandidateReader.h>
#include <RobotAPI/libraries/RobotAPIComponentPlugins/ArVizComponentPlugin.h>


namespace armarx::skills
{

    class MovePlatformToPose :
            public PlatformControlSkill,
            public mixin::RobotReadingSkillMixin,
            public PeriodicSpecializedSkill<platform_control::arondto::MovePlatformToPoseAcceptedType>
    {
    public:
        using ArgType = platform_control::arondto::MovePlatformToPoseAcceptedType;

        MovePlatformToPose(armem::client::MemoryNameSystem& mns, armarx::viz::Client& arviz, PlatformControlSkillContext& context);

    private:
        // executor
        Skill::InitResult init(const SpecializedInitInput& in) final;
        Skill::ExitResult exit(const SpecializedExitInput& in) final;
        StepResult step(const SpecializedMainInput& in) final;

    public:
        static SkillDescription Description;

    private:
        float signedMin(float newValue, float minAbsValue) const
        {
            return std::copysign(std::min<float>(fabs(newValue), minAbsValue), newValue);
        }
    };
}
