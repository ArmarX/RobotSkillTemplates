#include "BringObjectToLocation.h"

#include <RobotAPI/libraries/skills/provider/SkillProxy.h>

namespace armarx::skills
{
    SkillDescription BringObjectToLocation::Description = SkillDescription{
        "BringObjectToLocation", "Bring an object to some location",
        {}, armarx::Duration::MilliSeconds(60000),
        nullptr
    };

    BringObjectToLocation::BringObjectToLocation(const BringObjectSkillContext& context) :
        BringObjectSkill(Description, context)
    {
    }

    Skill::MainResult BringObjectToLocation::main(const MainInput& in)
    {
        std::string objName = "Kitchen/green-cup/0";
        std::string graphName = "KitchenKK";
        std::string objLandmarkName = "sideboard";
        std::string targetLandmarkName = "sink";
        std::string graspSetName = "TCP L";
        std::string kinematicChainName = "LeftArm";
        std::string otherKinematicChainName = "RightArm";
        std::string robotName = "Armar3";
        float platformOrientationalAccuracy = 0.1;
        float platformPositionalAccuracy = 20;
        float tcpOrientationalAccuracy = 0.3;
        float tcpPositionalAccuracy = 40;

        // move to landmark1
        {
            platform_control::arondto::MovePlatformToLandmarkAcceptedType params;
            params.landmark = objLandmarkName;
            params.robotName = robotName;
            params.graph = graphName;
            params.orientationalAccuracy = platformOrientationalAccuracy;
            params.positionalAccuracy = platformPositionalAccuracy;

            SkillProxy prx(manager, context.platformControlSkillProvider, skills::MovePlatformToLandmark::Description);

            if (prx.executeFullSkill(getSkillId().toString(in.executorName), params.toAron()).status != TerminatedSkillStatus::Succeeded)
            {
                return {TerminatedSkillStatus::Failed, nullptr};
            }
        }

        // grasp Object
        {
            grasp_object::arondto::GraspObjectAcceptedType params;
            params.objectEntityId = objName;
            params.robotName = robotName;
            params.tcpOrientationalAccuracy = tcpOrientationalAccuracy;
            params.tcpPositionalAccuracy = tcpPositionalAccuracy;
            params.platformOrientationalAccuracy = platformOrientationalAccuracy;
            params.platformPositionalAccuracy = platformPositionalAccuracy;

            SkillProxy prx({manager, context.graspControlSkillProvider, skills::GraspObjectSkill::Description});

            if (prx.executeFullSkill(getSkillId().toString(in.executorName), params.toAron()).status != TerminatedSkillStatus::Succeeded)
            {
                return {TerminatedSkillStatus::Failed, nullptr};
            }
        }

        // Move to target landmark
        {
            platform_control::arondto::MovePlatformToLandmarkAcceptedType params;
            params.landmark = targetLandmarkName;
            params.robotName = robotName;
            params.graph = graphName;
            params.orientationalAccuracy = platformOrientationalAccuracy;
            params.positionalAccuracy = platformPositionalAccuracy;

            SkillProxy prx({manager, context.platformControlSkillProvider, skills::MovePlatformToLandmark::Description.skillName});

            if (prx.executeFullSkill(getSkillId().toString(in.executorName), params.toAron()).status != TerminatedSkillStatus::Succeeded)
            {
                return {TerminatedSkillStatus::Failed, nullptr};
            }
        }

        // putdown object
        {
            grasp_object::arondto::PutdownObjectAcceptedType params;
            params.kinematicChainName = kinematicChainName;
            params.objectEntityId = objName;
            params.otherArmKinematicChainName = otherKinematicChainName;
            params.robotName = robotName;
            params.tcpOrientationalAccuracy = tcpOrientationalAccuracy;
            params.tcpPositionalAccuracy = tcpPositionalAccuracy;
            params.platformOrientationalAccuracy = platformOrientationalAccuracy;
            params.platformPositionalAccuracy = platformPositionalAccuracy;

            SkillProxy prx({manager, context.graspControlSkillProvider, skills::PutdownObjectSkill::Description.skillName});

            if (prx.executeFullSkill(getSkillId().toString(in.executorName), params.toAron()).status != TerminatedSkillStatus::Succeeded)
            {
                return {TerminatedSkillStatus::Failed, nullptr};
            }
        }

        return {TerminatedSkillStatus::Succeeded, nullptr};
    }
}
