/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

// STD/STL
#include <string>

// ArmarX
#include <ArmarXCore/core/Component.h>

// RobotAPI
#include <RobotAPI/libraries/skills/provider/SpecializedSkill.h>
#include <RobotAPI/libraries/skills/provider/SkillContext.h>

// Proxies
#include <RobotAPI/interface/units/HandUnitInterface.h>

// ArmarX
#include <ArmarXCore/core/Component.h>
#include <RobotAPI/libraries/RobotAPIComponentPlugins/ArVizComponentPlugin.h>
#include <RobotAPI/libraries/armem/client/MemoryNameSystem.h>

namespace armarx::skills
{
    struct BringObjectSkillContext : public SkillContext
    {
        std::string graspControlSkillProvider = "GraspControlSkillProvider";
        std::string platformControlSkillProvider = "PlatformControlSkillProvider";

        void defineProperties(const armarx::PropertyDefinitionsPtr& defs, const std::string& prefix) final
        {
            defs->optional(graspControlSkillProvider, prefix + "graspControlSkillProviderName");
            defs->optional(platformControlSkillProvider, prefix + "platformControlSkillProviderName");
        }

        virtual void onInit(armarx::Component& parent) final
        {
            parent.usingProxy(graspControlSkillProvider);
            parent.usingProxy(platformControlSkillProvider);
        }
    };

    class BringObjectSkill : public Skill
    {
    public:
        using Base = Skill;

        BringObjectSkill(const SkillDescription& desc, const BringObjectSkillContext& c) :
            Base(desc),
            context(c)
        {
        };

    protected:
        const BringObjectSkillContext& context;
    };
}
