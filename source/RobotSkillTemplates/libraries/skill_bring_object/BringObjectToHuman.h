/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

// ArmarX
#include <ArmarXCore/core/Component.h>

// RobotAPI
#include <RobotAPI/libraries/skills/provider/SkillProviderComponentPlugin.h>
#include <RobotAPI/libraries/armem/client/plugins/PluginUser.h>
#include <RobotAPI/libraries/RobotAPIComponentPlugins/ArVizComponentPlugin.h>

// Best put your skills in seperate libs
#include <RobotSkillTemplates/libraries/skill_grasp_object/grasp/GraspObject.h>
#include <RobotSkillTemplates/libraries/skill_grasp_object/handover/SimpleHandoverObject.h>
#include <RobotSkillTemplates/libraries/skill_platform_control/MovePlatformToLandmark.h>

namespace armarx::skills
{
    /*class BringObjectToHuman : public Skill
    {
    public:

        BringObjectToHuman(armem::client::MemoryNameSystem& mns, armarx::viz::Client& arviz, GraspControlSkillContext& context);

        void defineProperties(const armarx::PropertyDefinitionsPtr defs, const std::string& prefix);

        Status _execute(const aron::data::DictPtr&, const CallbackT&) final;

        void _notifyStopped() final;
        void _notifyTimeoutReached() final;
        void _reset() final;

    private:

        // Input
        armem::client::MemoryNameSystem& mns;
        armarx::viz::Client& arviz;

        // Members
        GraspControlSkillContext& context;

        // other skill
        GraspObject graspObject;
        SimpleHandoverObject handoverObject;
        MovePlatformToLandmark movePlatformToLandmark;
    };*/
}
