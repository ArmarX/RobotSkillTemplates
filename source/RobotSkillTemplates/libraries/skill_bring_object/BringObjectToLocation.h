/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

// BaseClass
#include "BringObjectSkill.h"

// ArmarX
#include <ArmarXCore/core/Component.h>

// RobotAPI
#include <RobotAPI/libraries/skills/provider/SkillProviderComponentPlugin.h>
#include <RobotAPI/libraries/armem/client/plugins/PluginUser.h>
#include <RobotAPI/libraries/RobotAPIComponentPlugins/ArVizComponentPlugin.h>

// Best put your skills in seperate libs
#include <RobotSkillTemplates/libraries/skill_grasp_object/grasp/GraspObject.h>
#include <RobotSkillTemplates/libraries/skill_grasp_object/putdown/PutdownObject.h>
#include <RobotSkillTemplates/libraries/skill_platform_control/MovePlatformToLandmark.h>


namespace armarx::skills
{
    class BringObjectToLocation : public BringObjectSkill
    {
    public:

        BringObjectToLocation(const BringObjectSkillContext& context);

        MainResult main(const MainInput& in) final;

        static SkillDescription Description;
    };
}
