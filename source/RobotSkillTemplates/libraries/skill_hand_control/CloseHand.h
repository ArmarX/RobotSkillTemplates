/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

// Base Class
#include "HandControlSkill.h"

#include <RobotAPI/libraries/armem/client.h>

#include <RobotSkillTemplates/libraries/skill_hand_control/aron/CloseHandAcceptedType.aron.generated.h>

namespace armarx::skills
{
    class CloseHand :
            public HandControlSkill,
            public SpecializedSkill<hand_control::arondto::CloseHandAcceptedType>
    {
    public:
        using ArgType = hand_control::arondto::CloseHandAcceptedType;

        CloseHand(HandControlSkillContext& context);

    private:
        MainResult main(const SpecializedMainInput& in) final;

    public:
        static SkillDescription Description;
    };
}
