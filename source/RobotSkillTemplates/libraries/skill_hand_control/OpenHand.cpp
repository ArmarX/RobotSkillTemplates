#include "OpenHand.h"

#include "util/HandUtil.h"

namespace armarx::skills
{

    SkillDescription OpenHand::Description = skills::SkillDescription{
        "OpenHand", "Open a hand",
        {}, armarx::Duration::MilliSeconds(2000),
        hand_control::arondto::OpenHandAcceptedType::ToAronType()
    };

    OpenHand::OpenHand(HandControlSkillContext& context) :
        HandControlSkill(context),
        SpecializedSkill<ArgType>(Description)
    {
    }

    Skill::MainResult OpenHand::main(const SpecializedMainInput& in)
    {
        if (simox::alg::starts_with(in.params.kinematicChainName, "Left"))
        {
            openLeftHand();
            return {TerminatedSkillStatus::Succeeded, nullptr};
        }

        if (simox::alg::starts_with(in.params.kinematicChainName, "Right"))
        {
            openRightHand();
            return {TerminatedSkillStatus::Succeeded, nullptr};
        }

        return {TerminatedSkillStatus::Failed, nullptr};
    }
}
