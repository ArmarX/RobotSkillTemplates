#include "CloseHand.h"

#include "util/HandUtil.h"

namespace armarx::skills
{

    SkillDescription CloseHand::Description = skills::SkillDescription{
        "CloseHand", "Close a hand",
        {}, armarx::Duration::MilliSeconds(2000),
        hand_control::arondto::CloseHandAcceptedType::ToAronType()
    };

    CloseHand::CloseHand(HandControlSkillContext& context) :
        HandControlSkill(context),
        SpecializedSkill<ArgType>(Description)
    {
    }

    Skill::MainResult CloseHand::main(const SpecializedMainInput& in)
    {
        if (simox::alg::starts_with(in.params.kinematicChainName, "Left"))
        {
            closeLeftHand();
            return {TerminatedSkillStatus::Succeeded, nullptr};
        }

        if (simox::alg::starts_with(in.params.kinematicChainName, "Right"))
        {
            closeRightHand();
            return {TerminatedSkillStatus::Succeeded, nullptr};
        }

        return {TerminatedSkillStatus::Failed, nullptr};
    }
}
