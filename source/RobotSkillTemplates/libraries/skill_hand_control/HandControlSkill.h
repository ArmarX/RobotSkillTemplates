/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

// STD/STL
#include <string>

// ArmarX
#include <ArmarXCore/core/Component.h>

// RobotAPI
#include <RobotAPI/libraries/skills/provider/SpecializedSkill.h>
#include <RobotAPI/libraries/skills/provider/SkillContext.h>

// Proxies
#include <RobotAPI/interface/units/HandUnitInterface.h>

// ArmarX
#include <ArmarXCore/core/Component.h>
#include <RobotAPI/libraries/RobotAPIComponentPlugins/ArVizComponentPlugin.h>
#include <RobotAPI/libraries/armem/client/MemoryNameSystem.h>

namespace armarx::skills
{
    struct HandControlSkillContext : public SkillContext
    {
        // Properties
        HandUnitInterfacePrx leftHandUnitPrx;
        HandUnitInterfacePrx rightHandUnitPrx;

        void defineProperties(const armarx::PropertyDefinitionsPtr& defs, const std::string& prefix) final
        {
            defs->component(leftHandUnitPrx, "LeftHandUnitName", prefix + "LeftHandUnitInterfaceName");
            defs->component(rightHandUnitPrx, "RightHandUnitName", prefix + "RightHandUnitInterfaceName");
        }
    };

    class HandControlSkill
    {
    public:
        HandControlSkill(HandControlSkillContext& c) :
            context(c)
        {
        };

    protected:
        void closeLeftHand()
        {
            context.leftHandUnitPrx->setShape("Close Preshape");
            std::this_thread::sleep_for(std::chrono::milliseconds(1000));
        }

        void openLeftHand()
        {
            context.leftHandUnitPrx->setShape("Open Preshape");
            std::this_thread::sleep_for(std::chrono::milliseconds(1000));
        }

        void closeRightHand()
        {
            context.rightHandUnitPrx->setShape("Close Preshape");
            std::this_thread::sleep_for(std::chrono::milliseconds(1000));
        }

        void openRightHand()
        {
            context.rightHandUnitPrx->setShape("Open Preshape");
            std::this_thread::sleep_for(std::chrono::milliseconds(1000));
        }

    protected:
        HandControlSkillContext& context;
    };
}
