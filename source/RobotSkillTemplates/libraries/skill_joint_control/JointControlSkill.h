/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

// STD/STL
#include <string>

// ArmarX
#include <ArmarXCore/core/Component.h>

// RobotAPI
#include <RobotAPI/libraries/skills/provider/PeriodicSpecializedSkill.h>
#include <RobotAPI/libraries/skills/provider/SpecializedSkill.h>
#include <RobotAPI/libraries/skills/provider/SkillContext.h>

// Mixins
#include <RobotAPI/libraries/skills/provider/mixins/All.h>

// Proxies
#include <RobotAPI/interface/units/KinematicUnitInterface.h>

// ArmarX
#include <ArmarXCore/core/Component.h>
#include <RobotAPI/libraries/armem/client/MemoryNameSystem.h>

namespace armarx::skills
{
    struct JointControlSkillContext : public SkillContext
    {
        // Properties
        KinematicUnitInterfacePrx kinematicUnitPrx;

        void defineProperties(const armarx::PropertyDefinitionsPtr& defs, const std::string& prefix) final
        {
            defs->component(kinematicUnitPrx, "KinematicUnitName", prefix + "KinematicUnitInterfaceName");
        }
    };

    class JointControlSkill
    {
    public:
        JointControlSkill(armem::client::MemoryNameSystem& mns, JointControlSkillContext& c) :
            mns(mns),
            context(c)
        {
        };

    protected:
        TerminatedSkillStatus stopJointMovement(const std::vector<std::string>& joints)
        {
            std::map<std::string, float> zeroVel;
            for (const auto& p : joints)
            {
                zeroVel[p] = 0.0f;
            }
            context.kinematicUnitPrx->setJointVelocities(zeroVel);
            std::this_thread::sleep_for(std::chrono::milliseconds(100));
            return TerminatedSkillStatus::Succeeded;
        }

    protected:
        armem::client::MemoryNameSystem& mns;

        JointControlSkillContext& context;
    };
}
