/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

// Base Class
#include "JointControlSkill.h"

#include <RobotSkillTemplates/libraries/skill_joint_control/aron/MoveJointsToPositionAcceptedType.aron.generated.h>

namespace armarx::skills
{
    class MoveJointsToPosition :
            public JointControlSkill,
            public mixin::RobotReadingSkillMixin,
            public PeriodicSpecializedSkill<joint_control::arondto::MoveJointsToPositionAcceptedType>
    {
    public:
        using ArgType = joint_control::arondto::MoveJointsToPositionAcceptedType;

        MoveJointsToPosition(armem::client::MemoryNameSystem& mns, JointControlSkillContext& context);

    private:
        void reset() final;
        Skill::InitResult init(const SpecializedInitInput& in) final;
        PeriodicSkill::StepResult step(const SpecializedMainInput& in) final;
        Skill::ExitResult exit(const SpecializedExitInput& in) final;

    public:
        static SkillDescription Description;

    private:
        bool already_running = false;
    };
}
