#include "MoveJointsWithVelocity.h"

#include "util/JointControlUtil.h"

namespace armarx::skills
{

    SkillDescription MoveJointsWithVelocity::Description = skills::SkillDescription{
        "MoveJointsWithVelocity", "Move the joints with velocity",
        {}, armarx::Duration::MilliSeconds(2000),
        joint_control::arondto::MoveJointsWithVelocityAcceptedType::ToAronType()
    };

    MoveJointsWithVelocity::MoveJointsWithVelocity(armem::client::MemoryNameSystem& mns, JointControlSkillContext& context) :
        JointControlSkill(mns, context),
        SpecializedSkill<ArgType>(Description)
    {
    }

    Skill::MainResult MoveJointsWithVelocity::main(const SpecializedMainInput& in)
    {
        return {TerminatedSkillStatus::Failed, nullptr};
    }
}
