#pragma once

// BaseClass
# include "../GraspControlSkill.h"

// Type
#include <RobotSkillTemplates/libraries/skill_grasp_object/aron/PutdownObjectAcceptedType.aron.generated.h>

// ArmarX
#include <RobotAPI/libraries/armem_objects/client/instance/ObjectReader.h>
#include <RobotAPI/libraries/armem_objects/client/instance/ObjectWriter.h>
#include <RobotAPI/libraries/armem_grasping/client/KnownGraspCandidateReader.h>

namespace armarx::skills
{
    /*class SimpleHandoverObject : public GraspControlSkill<grasp_object::arondto::PutdownObjectAcceptedType>
    {
    public:
        using Base = GraspControlSkill<grasp_object::arondto::PutdownObjectAcceptedType>;

        SimpleHandoverObject(armem::client::MemoryNameSystem& mns, armarx::viz::Client& arviz, GraspControlSkillContext&);

        Status execute(const grasp_object::arondto::PutdownObjectAcceptedType&, const CallbackT&) final;

        void notifyStopped() final;
        void notifyTimeoutReached() final;
        void reset() final;

    private:
        // other skill
        MovePlatformToPose movePlatformToPose;
    };*/
}
