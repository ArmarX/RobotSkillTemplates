#pragma once

// ArmarX
#include <RobotAPI/libraries/skills/provider/mixins/All.h>

namespace armarx::skills
{
    namespace grasp_control::util
    {
        struct SelectBestKinematicChainForObjectGraspInput
        {
            armem::arondto::ObjectInstance& objectInstance;

            VirtualRobot::Robot& synchronizedRobot;
        };
        struct SelectBestKinematicChainForObjectGraspOutput
        {
            std::string tcpName;
            std::string kinematicChainName;
            float distanceToObject;
        };
        SelectBestKinematicChainForObjectGraspOutput SelectBestKinematicChainForObjectGrasp(const SelectBestKinematicChainForObjectGraspInput& in);

        struct SelectBestGraspSetForObjectInput
        {
            armem::grasping::arondto::KnownGraspInfo& graspInfo;
            std::string tcpName;
        };
        struct SelectBestGraspSetForObjectOutput
        {
            std::string graspSetName;
            armarx::armem::grasping::arondto::KnownGraspSet graspSet;
        };
        SelectBestGraspSetForObjectOutput SelectBestGraspSetForObject(const SelectBestGraspSetForObjectInput& in);

        struct GetPlatformOffsetForObjectGraspInput
        {
            armem::arondto::ObjectInstance& objectInstance;

            VirtualRobot::Robot& synchronizedRobot;
            std::string tcpName;
        };
        struct GetPlatformOffsetForObjectGraspOutput
        {
            Eigen::Matrix4f platformGlobalPose;
            float distanceToDrive;
        };
        GetPlatformOffsetForObjectGraspOutput GetGlobalPlatformPoseForObjectGrasp(const GetPlatformOffsetForObjectGraspInput& in);


        struct GetPlatformOffsetAfterObjectGraspInput
        {
            VirtualRobot::Robot& synchronizedRobot;
        };
        struct GetPlatformOffsetAfterObjectGraspOutput
        {
            Eigen::Matrix4f platformGlobalPose;
            float distanceToDrive;
        };
        GetPlatformOffsetAfterObjectGraspOutput GetGlobalPlatformPoseAfterObjectGrasp(const GetPlatformOffsetAfterObjectGraspInput& in);
    }
}
