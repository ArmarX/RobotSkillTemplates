#pragma once

// BaseClass
# include "../GraspControlSkill.h"

// Type
#include <RobotSkillTemplates/libraries/skill_grasp_object/aron/MovePlatformAfterGraspAcceptedType.aron.generated.h>

// ArmarX
#include <RobotAPI/libraries/skills/provider/mixins/All.h>

namespace armarx::skills
{
    class MovePlatformAfterGraspSkill :
            public TwoArmGraspControlSkill,
            public SpecializedSkill<grasp_object::arondto::MovePlatformAfterGraspAcceptedType>
    {
    public:
        using ArgType = grasp_object::arondto::MovePlatformAfterGraspAcceptedType;

        MovePlatformAfterGraspSkill(armem::client::MemoryNameSystem& mns, armarx::viz::Client& arviz, TwoArmGraspControlSkillContext&);

    private:
        MainResult main(const SpecializedMainInput& in) final;

    public:
        static SkillDescription Description;
    };
}
