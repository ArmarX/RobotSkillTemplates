#pragma once

// BaseClass
# include "../GraspControlSkill.h"

// Type
#include <RobotSkillTemplates/libraries/skill_grasp_object/aron/ExecuteGraspAcceptedType.aron.generated.h>

// ArmarX
#include <RobotAPI/libraries/skills/provider/mixins/All.h>

namespace armarx::skills
{
    class ExecuteGraspSkill :
            public TwoArmGraspControlSkill,
            public SpecializedSkill<grasp_object::arondto::ExecuteGraspAcceptedType>
    {
    public:
        using ArgType = grasp_object::arondto::ExecuteGraspAcceptedType;

        ExecuteGraspSkill(armem::client::MemoryNameSystem& mns, armarx::viz::Client& arviz, TwoArmGraspControlSkillContext&);

    private:
        MainResult main(const SpecializedMainInput& in) final;

    public:
        static SkillDescription Description;
    };
}
