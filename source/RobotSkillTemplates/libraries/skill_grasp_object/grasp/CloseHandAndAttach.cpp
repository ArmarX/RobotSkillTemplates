#include "CloseHandAndAttach.h"

#include <RobotAPI/libraries/skills/provider/SkillProxy.h>

#include <VirtualRobot/RobotNodeSet.h>

namespace armarx::skills
{
    SkillDescription CloseHandAndAttachSkill::Description  = skills::SkillDescription{
        "CloseHandAndAttach", "Close the hand and attach an object to it",
        {}, armarx::Duration::MilliSeconds(5000),
        grasp_object::arondto::CloseHandAndAttachAcceptedType::ToAronType()
    };

    CloseHandAndAttachSkill::CloseHandAndAttachSkill(armem::client::MemoryNameSystem& mns, armarx::viz::Client& arviz, TwoArmGraspControlSkillContext& context) :
        TwoArmGraspControlSkill(mns, arviz, Description.skillName, context),
        SpecializedSkill<ArgType>(Description)
    {
    }

    Skill::MainResult CloseHandAndAttachSkill::main(const SpecializedMainInput& in)
    {
        armem::robot_state::VirtualRobotReader robotReader(mns);
        armem::obj::instance::Writer objectWriter(mns);
        armem::obj::instance::Reader objectReader(mns, context.objectPoseProviderPrx);

        robotReader.connect();
        objectWriter.connect();
        objectReader.connect();

        // //////////////////////////////
        // get robot
        // //////////////////////////////
        auto robot = robotReader.getSynchronizedRobot(in.params.robotName, armem::Time::Now(), VirtualRobot::RobotIO::RobotDescription::eStructure);
        if (!robot)
        {
            ARMARX_ERROR << "Lost robot.";
            return {TerminatedSkillStatus::Failed, nullptr};
        }

        // //////////////////////////////
        // get object pose
        // //////////////////////////////
        objectReader.requestLocalization(in.params.objectEntityId, core::time::Duration::MilliSeconds(500));
        std::this_thread::sleep_for(std::chrono::milliseconds(500));

        auto objInstance = objectReader.queryObjectByEntityID(in.params.objectEntityId, armem::Time::Now());
        if (!objInstance)
        {
            ARMARX_ERROR << "Lost object pose.";
            return {TerminatedSkillStatus::Failed, nullptr};
        }

        // Close hand
        skills::hand_control::arondto::CloseHandAcceptedType params;
        params.kinematicChainName = in.params.kinematicChainName;

        SkillProxy prx({manager, "HandControlSkillProvider", CloseHand::Description.skillName});
        auto su = prx.executeFullSkill(getSkillId().toString(in.executorName), params.toAron());

        ARMARX_CHECK(robotReader.synchronizeRobot(*robot, armem::Time::Now()));
        objInstance = objectReader.queryObjectByEntityID(in.params.objectEntityId, armem::Time::Now());

        auto objPoseInGlobalFrame = objInstance->pose.objectPoseGlobal;
        auto objPoseInRootFrame = robot->getRootNode()->toLocalCoordinateSystem(objPoseInGlobalFrame);
        auto objPoseInTCPFrame = robot->getRobotNodeSet(in.params.kinematicChainName)->getTCP()->toLocalCoordinateSystem(objPoseInGlobalFrame);

        objInstance->pose.attachmentValid = true;
        objInstance->pose.attachment.agentName = robot->getName();
        objInstance->pose.attachment.frameName = robot->getRobotNodeSet(in.params.kinematicChainName)->getTCP()->getName();
        objInstance->pose.attachment.poseInFrame = objPoseInTCPFrame;
        objInstance->pose.objectPoseRobot = objPoseInRootFrame;
        objInstance->pose.robotPose = objPoseInGlobalFrame;
        objInstance->pose.robotConfig = robot->getJointValues();

        objectWriter.commitObject(*objInstance, objInstance->pose.providerName, armem::Time::Now());

        return {su.status, nullptr};
    }
}
