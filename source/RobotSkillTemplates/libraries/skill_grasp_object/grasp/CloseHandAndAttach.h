#pragma once

// BaseClass
# include "../GraspControlSkill.h"

// Type
#include <RobotSkillTemplates/libraries/skill_grasp_object/aron/CloseHandAndAttachAcceptedType.aron.generated.h>

// ArmarX
#include <RobotAPI/libraries/skills/provider/mixins/All.h>
#include <RobotSkillTemplates/libraries/skill_hand_control/CloseHand.h>

namespace armarx::skills
{
    class CloseHandAndAttachSkill :
            public TwoArmGraspControlSkill,
            public SpecializedSkill<grasp_object::arondto::CloseHandAndAttachAcceptedType>
    {
    public:
        using ArgType = grasp_object::arondto::CloseHandAndAttachAcceptedType;

        CloseHandAndAttachSkill(armem::client::MemoryNameSystem& mns, armarx::viz::Client& arviz, TwoArmGraspControlSkillContext&);

    private:
        MainResult main(const SpecializedMainInput& in) final;

    public:
        static SkillDescription Description;
    };
}
