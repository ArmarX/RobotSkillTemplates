#include "ExecuteGrasp.h"

#include "util/GraspObjectUtil.h"
#include <RobotSkillTemplates/libraries/skill_tcp_control/MoveTCPToTargetPose.h>

#include <RobotAPI/libraries/skills/provider/SkillProxy.h>

#include <RobotAPI/libraries/core/FramedPose.h>

namespace armarx::skills
{
    namespace
    {
        grasp_object::arondto::ExecuteGraspAcceptedType GetDefaultParameterization()
        {
            grasp_object::arondto::ExecuteGraspAcceptedType ret;
            ret.orientationalAccuracy = 0.01;
            ret.positionalAccuracy = 25;
            return ret;
        }
    }

    SkillDescription ExecuteGraspSkill::Description  = skills::SkillDescription{
        "ExecuteGrasp", "Execute a graspset on some object",
        {}, armarx::Duration::MilliSeconds(110000),
        grasp_object::arondto::ExecuteGraspAcceptedType::ToAronType(),
        GetDefaultParameterization().toAron()
    };

    ExecuteGraspSkill::ExecuteGraspSkill(armem::client::MemoryNameSystem& mns, armarx::viz::Client& arviz, TwoArmGraspControlSkillContext& context) :
        TwoArmGraspControlSkill(mns, arviz, Description.skillName, context),
        SpecializedSkill<ArgType>(Description)
    {
    }

    Skill::MainResult ExecuteGraspSkill::main(const SpecializedMainInput& in)
    {
        armem::robot_state::VirtualRobotReader robotReader(mns);
        armem::obj::instance::Reader objectReader(mns, context.objectPoseProviderPrx);
        armem::grasping::known_grasps::Reader graspReader(mns);

        robotReader.connect();
        objectReader.connect();
        graspReader.connect();

        // //////////////////////////////
        // get robot
        // //////////////////////////////
        auto robot = robotReader.getSynchronizedRobot(in.params.robotName, armem::Time::Now(), VirtualRobot::RobotIO::RobotDescription::eStructure);
        if (!robot)
        {
            ARMARX_ERROR << "Lost robot.";
            return {TerminatedSkillStatus::Failed, nullptr};
        }

        // //////////////////////////////
        // get object pose
        // //////////////////////////////
        auto objInstance = objectReader.queryObjectByEntityID(in.params.objectEntityId, armem::Time::Now());
        if (!objInstance)
        {
            ARMARX_ERROR << "Lost object pose.";
            return {TerminatedSkillStatus::Failed, nullptr};
        }

        // //////////////////////////////
        // get all possible grasps
        // //////////////////////////////
        auto graspInfo = graspReader.queryKnownGraspInfoByEntityName(in.params.objectEntityId, armem::Time::Now());
        if (!graspInfo)
        {
            ARMARX_ERROR << "Lost grasp info.";
            return {TerminatedSkillStatus::Failed, nullptr};
        }

        if (const auto& it = graspInfo->graspSets.find(in.params.graspSetName); it == graspInfo->graspSets.end())
        {
            ARMARX_ERROR << "Grasp set name " << in.params.graspSetName << " not found.";
            return {TerminatedSkillStatus::Failed, nullptr};
        }

        // //////////////////////////////
        // Others, may be removed later
        // //////////////////////////////
        auto bestHandRootNodeName = "Hand L Base";
        auto robotAPIHandFileName = "RobotAPI/robots/Armar3/ArmarIII-LeftHand.xml";
        if (simox::alg::starts_with(in.params.kinematicChainName, "Right"))
        {
            bestHandRootNodeName = "Hand R Base";
            robotAPIHandFileName = "RobotAPI/robots/Armar3/ArmarIII-RightHand.xml";
        }

        for (const auto& grasp : graspInfo->graspSets.at(in.params.graspSetName).grasps)
        {
            {
                // Generate Grasp
                auto objPose = objectReader.queryObjectByEntityID(in.params.objectEntityId, armem::Time::Now());

                const Eigen::Matrix4f graspPoseGlobal =  objPose->pose.objectPoseGlobal * grasp.pose.inverse();
                const Eigen::Matrix4f tcpTargetPoseGlobal = graspPoseGlobal;

            //}

            // Move TCP to poses
            //{
                skills::tcp_control::arondto::MoveTCPToTargetPoseAcceptedType params;
                params.robotName = in.params.robotName;
                params.handFileName = robotAPIHandFileName;
                params.handRootNodeName = bestHandRootNodeName;
                params.kinematicChainName = in.params.kinematicChainName;
                params.targetPoseGlobal = tcpTargetPoseGlobal;
                params.orientationalAccuracy = in.params.orientationalAccuracy;
                params.positionalAccuracy = in.params.positionalAccuracy;

                SkillProxy prx({manager, context.tcpControlSkillProvider, skills::MoveTCPToTargetPose::Description.skillName});

                if (prx.executeFullSkill(getSkillId().toString(in.executorName), params.toAron()).status != TerminatedSkillStatus::Succeeded)
                {
                    return {TerminatedSkillStatus::Failed, nullptr};
                }
            }
        }
        return {TerminatedSkillStatus::Succeeded, nullptr};
    }
}
