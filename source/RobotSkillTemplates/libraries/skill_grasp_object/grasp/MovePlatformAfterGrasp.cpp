#include "MovePlatformAfterGrasp.h"

#include "util/GraspObjectUtil.h"
#include <RobotSkillTemplates/libraries/skill_platform_control/MovePlatformToPose.h>

#include <RobotAPI/libraries/skills/provider/SkillProxy.h>

#include <RobotAPI/libraries/core/FramedPose.h>

namespace armarx::skills
{
    namespace
    {
        grasp_object::arondto::MovePlatformAfterGraspAcceptedType GetDefaultParameterization()
        {
            grasp_object::arondto::MovePlatformAfterGraspAcceptedType ret;
            ret.orientationalAccuracy = 0.01;
            ret.positionalAccuracy = 25;
            return ret;
        }
    }

    SkillDescription MovePlatformAfterGraspSkill::Description  = skills::SkillDescription{
        "MovePlatformAfterGrasp", "Move Platform after agrasp to ease the navigation",
        {}, armarx::Duration::MilliSeconds(12000),
        grasp_object::arondto::MovePlatformAfterGraspAcceptedType::ToAronType(),
        GetDefaultParameterization().toAron()
    };

    MovePlatformAfterGraspSkill::MovePlatformAfterGraspSkill(armem::client::MemoryNameSystem& mns, armarx::viz::Client& arviz, TwoArmGraspControlSkillContext& context) :
        TwoArmGraspControlSkill(mns, arviz, Description.skillName, context),
        SpecializedSkill<ArgType>(Description)
    {
    }

    Skill::MainResult MovePlatformAfterGraspSkill::main(const SpecializedMainInput& in)
    {
        armem::robot_state::VirtualRobotReader robotReader(mns);
        armem::obj::instance::Reader objectReader(mns, context.objectPoseProviderPrx);

        robotReader.connect();
        objectReader.connect();

        // //////////////////////////////
        // get robot
        // //////////////////////////////
        auto robot = robotReader.getSynchronizedRobot(in.params.robotName, armem::Time::Now(), VirtualRobot::RobotIO::RobotDescription::eStructure);
        if (!robot)
        {
            ARMARX_ERROR << "Lost robot.";
            return {TerminatedSkillStatus::Failed, nullptr};
        }

        auto targetPose = skills::grasp_control::util::GetGlobalPlatformPoseAfterObjectGrasp({*robot});

        skills::platform_control::arondto::MovePlatformToPoseAcceptedType params;
        params.robotName = in.params.robotName;
        params.orientationalAccuracy = in.params.orientationalAccuracy;
        params.positionalAccuracy = in.params.positionalAccuracy;
        params.pose = targetPose.platformGlobalPose;

        SkillProxy prx({manager, context.platformControlSkillProvider, MovePlatformToPose::Description.skillName});
        return {prx.executeFullSkill(getSkillId().toString(in.executorName), params.toAron()).status, nullptr};
    }
}
