#include "OpenHandAndDetach.h"

#include <RobotSkillTemplates/libraries/skill_hand_control/OpenHand.h>

#include <RobotAPI/libraries/skills/provider/SkillProxy.h>

#include <VirtualRobot/RobotNodeSet.h>

namespace armarx::skills
{
    SkillDescription OpenHandAndDetachSkill::Description  = skills::SkillDescription{
        "OpenHandAndDetach", "Open the hand and detach an object from it",
        {}, armarx::Duration::MilliSeconds(120000),
        grasp_object::arondto::OpenHandAndDetachAcceptedType::ToAronType()
    };

    OpenHandAndDetachSkill::OpenHandAndDetachSkill(armem::client::MemoryNameSystem& mns, armarx::viz::Client& arviz, TwoArmGraspControlSkillContext& context) :
        TwoArmGraspControlSkill(mns, arviz, Description.skillName, context),
        SpecializedSkill<ArgType>(Description)
    {
    }

    Skill::MainResult OpenHandAndDetachSkill::main(const SpecializedMainInput& in)
    {
        armem::robot_state::VirtualRobotReader robotReader(mns);
        armem::obj::instance::Writer objectWriter(mns);
        armem::obj::instance::Reader objectReader(mns, context.objectPoseProviderPrx);

        robotReader.connect();
        objectWriter.connect();
        objectReader.connect();

        // //////////////////////////////
        // get robot
        // //////////////////////////////
        auto robot = robotReader.getSynchronizedRobot(in.params.robotName, armem::Time::Now(), VirtualRobot::RobotIO::RobotDescription::eStructure);
        if (!robot)
        {
            ARMARX_ERROR << "Lost robot.";
            return {TerminatedSkillStatus::Failed, nullptr};
        }

        // //////////////////////////////
        // get object pose
        // //////////////////////////////
        auto objInstance = objectReader.queryObjectByEntityID(in.params.objectEntityId, armem::Time::Now());
        if (!objInstance)
        {
            ARMARX_ERROR << "Lost object pose.";
            return {TerminatedSkillStatus::Failed, nullptr};
        }

        // Open hand
        skills::hand_control::arondto::OpenHandAcceptedType params;
        params.kinematicChainName = in.params.kinematicChainName;

        SkillProxy prx({manager, context.handControlSkillProvider, OpenHand::Description.skillName});
        auto su = prx.executeFullSkill(getSkillId().toString(in.executorName), params.toAron());

        ARMARX_CHECK(robotReader.synchronizeRobot(*robot, armem::Time::Now()));
        objInstance = objectReader.queryObjectByEntityID(in.params.objectEntityId, armem::Time::Now());

        auto objPoseInRootFrame = objInstance->pose.objectPoseRobot;
        auto robotPoseGlobal = robot->getRootNode()->getGlobalPose();

        auto newObjPoseGlobal = robotPoseGlobal * objPoseInRootFrame;

        //auto objPoseInTCPFrame = robot->getRobotNodeSet(in.kinematicChainName)->getTCP()->toLocalCoordinateSystem(objPoseInGlobalFrame);

        objInstance->pose.attachmentValid = false;
        objInstance->pose.attachment.agentName = "";
        objInstance->pose.attachment.frameName = "";
        objInstance->pose.objectPoseGlobal = newObjPoseGlobal;
        objInstance->pose.objectPoseRobot = objPoseInRootFrame;
        objInstance->pose.robotPose = robot->getRootNode()->getGlobalPose();
        objInstance->pose.robotConfig = robot->getJointValues();

        objectWriter.commitObject(*objInstance, objInstance->pose.providerName, armem::Time::Now());

        return {su.status, nullptr};
    }
}
