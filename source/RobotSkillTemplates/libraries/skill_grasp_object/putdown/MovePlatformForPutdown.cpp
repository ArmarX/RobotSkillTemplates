#include "MovePlatformForPutdown.h"

#include "util/PutdownObjectUtil.h"
#include <RobotSkillTemplates/libraries/skill_platform_control/MovePlatformToPose.h>

#include <RobotAPI/libraries/skills/provider/SkillProxy.h>

#include <RobotAPI/libraries/core/FramedPose.h>

namespace armarx::skills
{
    namespace
    {
        grasp_object::arondto::MovePlatformForPutdownAcceptedType GetDefaultParameterization()
        {
            grasp_object::arondto::MovePlatformForPutdownAcceptedType ret;
            ret.orientationalAccuracy = 0.01;
            ret.positionalAccuracy = 25;
            return ret;
        }
    }

    SkillDescription MovePlatformForPutdownSkill::Description  = skills::SkillDescription{
        "MovePlatformForPutdown", "Move Platform in order to ease the putdown of an object",
        {}, armarx::Duration::MilliSeconds(120000),
        grasp_object::arondto::MovePlatformForPutdownAcceptedType::ToAronType(),
        GetDefaultParameterization().toAron()
    };

    MovePlatformForPutdownSkill::MovePlatformForPutdownSkill(armem::client::MemoryNameSystem& mns, armarx::viz::Client& arviz, TwoArmGraspControlSkillContext& context) :
        TwoArmGraspControlSkill(mns, arviz, Description.skillName, context),
        SpecializedSkill<ArgType>(Description)
    {
    }

    Skill::MainResult MovePlatformForPutdownSkill::main(const SpecializedMainInput& in)
    {
        armem::robot_state::VirtualRobotReader robotReader(mns);
        armem::obj::instance::Reader objectReader(mns, context.objectPoseProviderPrx);

        robotReader.connect();
        objectReader.connect();

        // //////////////////////////////
        // get robot
        // //////////////////////////////
        auto robot = robotReader.getSynchronizedRobot(in.params.robotName, armem::Time::Now(), VirtualRobot::RobotIO::RobotDescription::eStructure);
        if (!robot)
        {
            ARMARX_ERROR << "Lost robot.";
            return {TerminatedSkillStatus::Failed, nullptr};
        }

        // //////////////////////////////
        // get object pose
        // //////////////////////////////
        auto objInstance = objectReader.queryObjectByEntityID(in.params.objectEntityId, armem::Time::Now());
        if (!objInstance)
        {
            ARMARX_ERROR << "Lost object pose.";
            return {TerminatedSkillStatus::Failed, nullptr};
        }

        // //////////////////////////////
        // Exec move skill
        // //////////////////////////////
        auto targetPose = skills::grasp_control::util::GetGlobalPlatformPoseForObjectPutdown({*robot, in.params.tcpName});

        platform_control::arondto::MovePlatformToPoseAcceptedType params;
        params.pose = targetPose.platformGlobalPose;
        params.robotName = in.params.robotName;
        params.orientationalAccuracy = in.params.orientationalAccuracy;
        params.positionalAccuracy = in.params.positionalAccuracy;

        SkillProxy prx({manager, context.platformControlSkillProvider, MovePlatformToPose::Description.skillName});
        return {prx.executeFullSkill(getSkillId().toString(in.executorName), params.toAron()).status, nullptr};
    }
}
