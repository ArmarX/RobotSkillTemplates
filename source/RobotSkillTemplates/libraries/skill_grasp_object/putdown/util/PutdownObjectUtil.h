#pragma once

// ArmarX
#include <RobotAPI/libraries/skills/provider/mixins/All.h>

namespace armarx::skills
{
    namespace grasp_control::util
    {
        struct GetPlatformOffsetForObjectPutdownInput
        {
            VirtualRobot::Robot& synchronizedRobot;
            std::string tcpName;
        };
        struct GetPlatformOffsetForObjectPutdownOutput
        {
            Eigen::Matrix4f platformGlobalPose;
            float distanceToDrive;
        };
        GetPlatformOffsetForObjectPutdownOutput GetGlobalPlatformPoseForObjectPutdown(const GetPlatformOffsetForObjectPutdownInput& in);


        struct GetPlatformOffsetAfterObjectPutdownInput
        {
            VirtualRobot::Robot& synchronizedRobot;
        };
        struct GetPlatformOffsetAfterObjectPutdownOutput
        {
            Eigen::Matrix4f platformGlobalPose;
            float distanceToDrive;
        };
        GetPlatformOffsetAfterObjectPutdownOutput GetGlobalPlatformPoseAfterObjectPutdown(const GetPlatformOffsetAfterObjectPutdownInput& in);
    }
}
