#include "ExecutePutdown.h"

#include "util/PutdownObjectUtil.h"
#include <RobotSkillTemplates/libraries/skill_joint_control/MoveJointsToPosition.h>
#include <RobotSkillTemplates/libraries/skill_tcp_control/MoveTCPToTargetPose.h>
#include <VirtualRobot/RobotNodeSet.h>

#include <RobotAPI/libraries/skills/provider/SkillProxy.h>

#include <RobotAPI/libraries/core/FramedPose.h>

#include <RobotAPI/libraries/skills/provider/SkillProxy.h>

namespace armarx::skills
{
    namespace
    {
        grasp_object::arondto::ExecutePutdownAcceptedType GetDefaultParameterization()
        {
            grasp_object::arondto::ExecutePutdownAcceptedType ret;
            ret.orientationalAccuracy = 0.01;
            ret.positionalAccuracy = 25;
            return ret;
        }
    }

    SkillDescription ExecutePutdownSkill::Description  = skills::SkillDescription{
        "ExecutePutdown", "Execute a putdown for some grasped object",
        {}, armarx::Duration::MilliSeconds(120000),
        grasp_object::arondto::ExecutePutdownAcceptedType::ToAronType(),
        GetDefaultParameterization().toAron()
    };

    ExecutePutdownSkill::ExecutePutdownSkill(armem::client::MemoryNameSystem& mns, armarx::viz::Client& arviz, TwoArmGraspControlSkillContext& context) :
        TwoArmGraspControlSkill(mns, arviz, Description.skillName, context),
        SpecializedSkill<ArgType>(Description)
    {
    }

    Skill::MainResult ExecutePutdownSkill::main(const SpecializedMainInput& in)
    {
        armem::robot_state::VirtualRobotReader robotReader(mns);
        armem::obj::instance::Reader objectReader(mns, context.objectPoseProviderPrx);
        armem::grasping::known_grasps::Reader graspReader(mns);

        robotReader.connect();
        objectReader.connect();
        graspReader.connect();

        // //////////////////////////////
        // get robot
        // //////////////////////////////
        auto robot = robotReader.getSynchronizedRobot(in.params.robotName, armem::Time::Now(), VirtualRobot::RobotIO::RobotDescription::eStructure);
        if (!robot)
        {
            ARMARX_ERROR << "Lost robot.";
            return {TerminatedSkillStatus::Failed, nullptr};
        }

        // //////////////////////////////
        // get object pose
        // //////////////////////////////
        auto objInstance = objectReader.queryObjectByEntityID(in.params.objectEntityId, armem::Time::Now());
        if (!objInstance)
        {
            ARMARX_ERROR << "Lost object pose.";
            return {TerminatedSkillStatus::Failed, nullptr};
        }

        // //////////////////////////////
        // Others, may be removed later
        // //////////////////////////////
        auto bestHandRootNodeName = "Hand L Base";
        auto robotAPIHandFileName = "RobotAPI/robots/Armar3/ArmarIII-LeftHand.xml";
        if (simox::alg::starts_with(in.params.kinematicChainName, "Right"))
        {
            bestHandRootNodeName = "Hand R Base";
            robotAPIHandFileName = "RobotAPI/robots/Armar3/ArmarIII-RightHand.xml";
        }

        // //////////////////////////////
        // Move TCPs
        // //////////////////////////////
        {
            skills::joint_control::arondto::MoveJointsToPositionAcceptedType params;
            params.robotName = in.params.robotName;
            params.jointTargetTolerance = 0.03;
            params.jointMaxSpeed = 0.75;
            params.accelerationTime = 500;
            params.setVelocitiesToZeroAtEnd = true;

            params.targetJointMap[robot->getRobotNodeSet(in.params.otherArmKinematicChainName)->getNodeNames()[0]] = 0.47;
            params.targetJointMap[robot->getRobotNodeSet(in.params.otherArmKinematicChainName)->getNodeNames()[1]] = 0.0;
            params.targetJointMap[robot->getRobotNodeSet(in.params.otherArmKinematicChainName)->getNodeNames()[2]] = 0.0;
            params.targetJointMap[robot->getRobotNodeSet(in.params.otherArmKinematicChainName)->getNodeNames()[3]] = 0.32;
            params.targetJointMap[robot->getRobotNodeSet(in.params.otherArmKinematicChainName)->getNodeNames()[4]] = 0.47;

            SkillProxy prx({manager, context.jointControlSkillProvider, MoveJointsToPosition::Description.skillName});

            if (prx.executeFullSkill(getSkillId().toString(in.executorName), params.toAron()).status != TerminatedSkillStatus::Succeeded)
            {
                return {TerminatedSkillStatus::Failed, nullptr};
            }
        }

        // Look at hand
        {
            skills::joint_control::arondto::MoveJointsToPositionAcceptedType params;
            params.robotName = in.params.robotName;
            params.jointTargetTolerance = 0.03; // Todo
            params.jointMaxSpeed = 0.75;
            params.accelerationTime = 500;
            params.setVelocitiesToZeroAtEnd = true;
            params.targetJointMap["Neck_1_Pitch"] = 0.6;

            SkillProxy prx({manager, context.jointControlSkillProvider, MoveJointsToPosition::Description.skillName});

            if (prx.executeFullSkill(getSkillId().toString(in.executorName), params.toAron()).status != TerminatedSkillStatus::Succeeded)
            {
                return {TerminatedSkillStatus::Failed, nullptr};
            }
        }

        ARMARX_CHECK(robotReader.synchronizeRobot(*robot, armem::Time::Now()));

        const auto objPoseLocal = objInstance->pose.objectPoseRobot;
        const auto objPoseGlobal = robot->getRootNode()->toGlobalCoordinateSystem(objPoseLocal);
        const auto objectInTCPFrame = objInstance->pose.attachment.poseInFrame;
        Eigen::Matrix4f tcpPlacePoseGlobal = objPoseGlobal * objectInTCPFrame.inverse();
        tcpPlacePoseGlobal(2,3) += 50;

        auto tcpPlacePrePoseGlobal = tcpPlacePoseGlobal;
        tcpPlacePrePoseGlobal(2,3) += 80;

        std::vector<Eigen::Matrix4f> targetPosesGlobal = {tcpPlacePrePoseGlobal, tcpPlacePoseGlobal};

        for (const auto& tcpTargetPoseGlobal : targetPosesGlobal)
        {
            skills::tcp_control::arondto::MoveTCPToTargetPoseAcceptedType params;
            params.robotName = in.params.robotName;
            params.handFileName = robotAPIHandFileName;
            params.handRootNodeName = bestHandRootNodeName;
            params.kinematicChainName = in.params.kinematicChainName;
            params.targetPoseGlobal = tcpTargetPoseGlobal;
            params.orientationalAccuracy = in.params.orientationalAccuracy;
            params.positionalAccuracy = in.params.positionalAccuracy;

            SkillProxy prx({manager, context.tcpControlSkillProvider, skills::MoveTCPToTargetPose::Description.skillName});

            if (prx.executeFullSkill(getSkillId().toString(in.executorName), params.toAron()).status != TerminatedSkillStatus::Succeeded)
            {
                return {TerminatedSkillStatus::Failed, nullptr};
            }
        }

        // Look up
        {
            skills::joint_control::arondto::MoveJointsToPositionAcceptedType params;
            params.robotName = in.params.robotName;
            params.jointTargetTolerance = 0.03; // Todo
            params.jointMaxSpeed = 0.75;
            params.accelerationTime = 500;
            params.setVelocitiesToZeroAtEnd = true;
            params.targetJointMap["Neck_1_Pitch"] = 0.0;

            SkillProxy prx({manager, context.jointControlSkillProvider, MoveJointsToPosition::Description.skillName});

            if (prx.executeFullSkill(getSkillId().toString(in.executorName), params.toAron()).status != TerminatedSkillStatus::Succeeded)
            {
                return {TerminatedSkillStatus::Failed, nullptr};
            }
        }

        return {TerminatedSkillStatus::Succeeded, nullptr};
    }
}
