#pragma once

// BaseClass
# include "../GraspControlSkill.h"

// Type
#include <RobotSkillTemplates/libraries/skill_grasp_object/aron/PutdownObjectAcceptedType.aron.generated.h>

// ArmarX
#include <RobotAPI/libraries/skills/provider/mixins/All.h>

// Subskills
#include "MovePlatformForPutdown.h"
#include "MovePlatformAfterPutdown.h"
#include "OpenHandAndDetach.h"
#include "ExecutePutdown.h"
#include <RobotSkillTemplates/libraries/skill_visual_search/WhatCanYouSeeNow.h>
#include <RobotSkillTemplates/libraries/skill_hand_control/OpenHand.h>
#include <RobotSkillTemplates/libraries/skill_tcp_control/MoveTCPToTargetPose.h>
#include <RobotSkillTemplates/libraries/skill_visual_servo_tcp_control/VisualServoTCPToTargetPose.h>
#include <RobotSkillTemplates/libraries/skill_joint_control/MoveJointsToPosition.h>

namespace armarx::skills
{
    class PutdownObjectSkill :
            public TwoArmGraspControlSkill,
            public SpecializedSkill<grasp_object::arondto::PutdownObjectAcceptedType>
    {
    public:
        using ArgType = grasp_object::arondto::PutdownObjectAcceptedType;

        PutdownObjectSkill(armem::client::MemoryNameSystem& mns, armarx::viz::Client& arviz, TwoArmGraspControlSkillContext&);

    private:
        MainResult main(const SpecializedMainInput& in) final;

    public:
        static SkillDescription Description;
    };
}
