#pragma once

// BaseClass
# include "../GraspControlSkill.h"

// Type
#include <RobotSkillTemplates/libraries/skill_grasp_object/aron/ExecutePutdownAcceptedType.aron.generated.h>

// ArmarX
#include <RobotAPI/libraries/skills/provider/mixins/All.h>

namespace armarx::skills
{
    class ExecutePutdownSkill :
            public TwoArmGraspControlSkill,
            public SpecializedSkill<grasp_object::arondto::ExecutePutdownAcceptedType>
    {
    public:
        using ArgType = grasp_object::arondto::ExecutePutdownAcceptedType;

        ExecutePutdownSkill(armem::client::MemoryNameSystem& mns, armarx::viz::Client& arviz, TwoArmGraspControlSkillContext&);

    private:
        MainResult main(const SpecializedMainInput& in) final;

    public:
        static SkillDescription Description;
    };
}
