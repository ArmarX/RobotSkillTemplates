/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

// Base Class
#include "TCPControlSkill.h"

#include <RobotSkillTemplates/libraries/skill_tcp_control/aron/MoveTCPToTargetPoseAcceptedType.aron.generated.h>

#include <RobotAPI/libraries/core/FramedPose.h>
#include <VirtualRobot/RobotNodeSet.h>

namespace armarx::skills
{
    class MoveTCPToTargetPose :
            public TCPControlSkill,
            public mixin::RobotReadingSkillMixin,
            public PeriodicSpecializedSkill<tcp_control::arondto::MoveTCPToTargetPoseAcceptedType>
    {
    public:
        using ArgType = armarx::skills::tcp_control::arondto::MoveTCPToTargetPoseAcceptedType;

        MoveTCPToTargetPose(armem::client::MemoryNameSystem& mns, armarx::viz::Client& arviz, TCPControlSkillContext& context);

    private:
        Skill::InitResult init(const SpecializedInitInput& in) final;
        StepResult step(const SpecializedMainInput& in) final;
        Skill::ExitResult exit(const SpecializedExitInput& in) final;

    public:
        static SkillDescription Description;

    private:
        bool tcpControlUnitRequestedOnStart = false;
    };
}
