/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

// STD/STL
#include <string>

// ArmarX
#include <ArmarXCore/core/Component.h>

// RobotAPI
#include <RobotAPI/libraries/skills/provider/PeriodicSpecializedSkill.h>
#include <RobotAPI/libraries/skills/provider/SpecializedSkill.h>
#include <RobotAPI/libraries/skills/provider/SkillContext.h>

// Proxies
#include <RobotAPI/interface/units/TCPControlUnit.h>

// Skill Mixins
#include <RobotAPI/libraries/skills/provider/mixins/All.h>

#include <RobotAPI/libraries/core/FramedOrientedPoint.h>
#include <RobotAPI/libraries/core/FramedPose.h>

namespace armarx::skills
{
    struct TCPControlSkillContext : public SkillContext
    {
        // Properties
        TCPControlUnitInterfacePrx tcpControlUnitPrx;

        void defineProperties(const armarx::PropertyDefinitionsPtr& defs, const std::string& prefix) override
        {
            defs->component(tcpControlUnitPrx, "TCPControlUnitName", prefix + "TCPControlUnitInterfaceName");
        }
    };

    class TCPControlSkill :
            public mixin::ArvizSkillMixin,
            public mixin::MNSSkillMixin
    {
    public:

        TCPControlSkill(armem::client::MemoryNameSystem& mns, armarx::viz::Client& arviz, const std::string& layerName, TCPControlSkillContext& c) :
            mixin::ArvizSkillMixin({arviz, layerName}),
            mixin::MNSSkillMixin({mns}),
            context(c)
        {
        };

    protected:
        void stopTCPMovement(const std::string& kinematicChainName, const std::string& tcpName)
        {
            FramedDirectionPtr cartesianVelocity = new FramedDirection(Eigen::Vector3f::Zero(), GlobalFrame, "");
            FramedDirectionPtr angleVelocity = new FramedDirection(Eigen::Vector3f::Zero(), GlobalFrame, "");

            context.tcpControlUnitPrx->setTCPVelocity(kinematicChainName, tcpName, cartesianVelocity, angleVelocity);
            std::this_thread::sleep_for(std::chrono::milliseconds(500));
        }

    protected:

        TCPControlSkillContext& context;
    };
}
