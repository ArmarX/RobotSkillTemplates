#include "WhatCanYouSeeNow.h"

#include <RobotAPI/libraries/skills/provider/SkillProxy.h>

#include <RobotSkillTemplates/libraries/skill_joint_control/MoveJointsToPosition.h>
#include "util/VisualSearchUtil.h"

namespace armarx::skills
{

    SkillDescription WhatCanYouSeeNow::Description = skills::SkillDescription{
        "WhatCanYouSeeNow", "Move the head to three known configurations and search for objects",
        {}, armarx::Duration::MilliSeconds(2000),
        visual_search::arondto::WhatCanYouSeeNowAcceptedType::ToAronType()
    };

    WhatCanYouSeeNow::WhatCanYouSeeNow(armem::client::MemoryNameSystem& mns, armarx::viz::Client& arviz, VisualSearchSkillContext& context) :
        VisualSearchSkill(mns, arviz, Description.skillName, context),
        SpecializedSkill<ArgType>(Description)
    {
    }

    Skill::MainResult WhatCanYouSeeNow::main(const SpecializedMainInput& in)
    {
        armem::robot_state::VirtualRobotReader robotReader(mns);
        armem::obj::instance::Writer objectWriter(mns);
        armem::obj::instance::Reader objectReader(mns, context.objectPoseProviderPrx);

        robotReader.connect();
        objectWriter.connect();
        objectReader.connect();

        // //////////////////////////////
        // get robot
        // //////////////////////////////
        auto robot = robotReader.getSynchronizedRobot(in.params.robotName, armem::Time::Now(), VirtualRobot::RobotIO::RobotDescription::eStructure);
        if (!robot)
        {
            ARMARX_ERROR << "Lost robot.";
            return {TerminatedSkillStatus::Failed, nullptr};
        }

        // Look down
        {
            skills::joint_control::arondto::MoveJointsToPositionAcceptedType params;
            params.robotName = in.params.robotName;
            params.jointTargetTolerance = 0.03; // Todo
            params.jointMaxSpeed = 0.75;
            params.accelerationTime = 500;
            params.setVelocitiesToZeroAtEnd = true;
            params.targetJointMap["Neck_1_Pitch"] = 0.6;

            SkillProxy prx({manager, context.jointControlSkillProvider, MoveJointsToPosition::Description.skillName});

            if (prx.executeFullSkill(getSkillId().toString(in.executorName), params.toAron()).status != TerminatedSkillStatus::Succeeded)
            {
                return {TerminatedSkillStatus::Failed, nullptr};
            }
        }

        // //////////////////////////////
        // get object pose
        // //////////////////////////////
        objectReader.requestLocalization(in.params.objectEntityId, core::time::Duration::MilliSeconds(500));
        std::this_thread::sleep_for(std::chrono::milliseconds(500));

        // Look up
        {
            skills::joint_control::arondto::MoveJointsToPositionAcceptedType params;
            params.robotName = in.params.robotName;
            params.jointTargetTolerance = 0.03; // Todo
            params.jointMaxSpeed = 0.75;
            params.accelerationTime = 500;
            params.setVelocitiesToZeroAtEnd = true;
            params.targetJointMap["Neck_1_Pitch"] = 0.0;

            SkillProxy prx({manager, context.jointControlSkillProvider, MoveJointsToPosition::Description.skillName});

            if (prx.executeFullSkill(getSkillId().toString(in.executorName), params.toAron()).status != TerminatedSkillStatus::Succeeded)
            {
                return {TerminatedSkillStatus::Failed, nullptr};
            }
        }

        return {TerminatedSkillStatus::Succeeded, nullptr};
    }
}
