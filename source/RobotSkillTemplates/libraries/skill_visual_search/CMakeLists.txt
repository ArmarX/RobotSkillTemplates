set(LIB_NAME       VisualSearchSkill)

armarx_set_target("Library: ${LIB_NAME}")

armarx_add_library(
    LIBS
        ArmarXCore
        RobotAPIInterfaces
        RobotAPIComponentPlugins
        aroneigenconverter
        armem
        armem_robot_state
        armem_objects
        RobotAPISkills
        JointControlSkill
    SOURCES
        util/VisualSearchUtil.cpp
        WhatCanYouSeeNow.cpp
        VisualSearchSkill.cpp
    HEADERS
        util/VisualSearchUtil.h
        WhatCanYouSeeNow.h
        VisualSearchSkill.h
    ARON_FILES
        ./aron/WhatCanYouSeeNowAcceptedType.xml
)


add_library(RobotSkillTemplates::skills::VisualSearchSkill ALIAS VisualSearchSkill)
