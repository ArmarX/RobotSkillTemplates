/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

// STD/STL
#include <string>

// ArmarX
#include <ArmarXCore/core/Component.h>

// RobotAPI
#include <RobotAPI/libraries/skills/provider/SpecializedSkill.h>
#include <RobotAPI/libraries/skills/provider/SkillContext.h>

// Proxies
#include <RobotAPI/interface/units/KinematicUnitInterface.h>
#include <RobotAPI/interface/objectpose/ObjectPoseProvider.h>

// Skills
#include <RobotAPI/libraries/skills/provider/mixins/All.h>

// ArmarX
#include <ArmarXCore/core/Component.h>
#include <RobotAPI/libraries/RobotAPIComponentPlugins/ArVizComponentPlugin.h>
#include <RobotAPI/libraries/armem/client/MemoryNameSystem.h>

namespace armarx::skills
{
    struct VisualSearchSkillContext : public SkillContext
    {
        // Properties
        std::string jointControlSkillProvider = "JointControlSkillProvider";
        KinematicUnitInterfacePrx kinematicUnitPrx;
        armarx::objpose::ObjectPoseProviderPrx objectPoseProviderPrx;

        void defineProperties(const armarx::PropertyDefinitionsPtr& defs, const std::string& prefix) final
        {
            defs->optional(jointControlSkillProvider, prefix + "jointControlSkillProviderName");
            defs->component(objectPoseProviderPrx, "ObjectPoseProvider", prefix + "objectPoseProviderInterfaceName");
            defs->component(kinematicUnitPrx, "KinematicUnitName", prefix + "KinematicUnitInterfaceName");
        }
    };

    class VisualSearchSkill :
            public mixin::ArvizSkillMixin,
            public mixin::MNSSkillMixin
    {
    public:
        VisualSearchSkill(armem::client::MemoryNameSystem& mns, armarx::viz::Client& arviz, const std::string& layerName, VisualSearchSkillContext& c) :
            mixin::ArvizSkillMixin(arviz, layerName),
            mixin::MNSSkillMixin(mns),
            context(c)
        {
        };

    protected:
        VisualSearchSkillContext& context;
    };
}
